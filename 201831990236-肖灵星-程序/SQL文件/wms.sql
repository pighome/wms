/*
Navicat MySQL Data Transfer

Source Server         : mysql2
Source Server Version : 80015
Source Host           : localhost:3306
Source Database       : wms

Target Server Type    : MYSQL
Target Server Version : 80015
File Encoding         : 65001

Date: 2020-06-05 08:02:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for administrators
-- ----------------------------
DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators` (
  `admin_NO` int(50) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(50) NOT NULL,
  `gender` enum('M','F') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'M',
  `call_phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '0',
  `adress` varchar(255) NOT NULL,
  `powerID` enum('A','B') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`admin_NO`),
  KEY `powerID` (`powerID`),
  KEY `admin_name` (`admin_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of administrators
-- ----------------------------
INSERT INTO `administrators` VALUES ('1', 'admin', 'M', '18199998799', '浙江金华', 'A', '所有权限');
INSERT INTO `administrators` VALUES ('2', 'user1', 'F', '19875643258', '甘肃平凉', 'B', '部分权限');

-- ----------------------------
-- Table structure for authority
-- ----------------------------
DROP TABLE IF EXISTS `authority`;
CREATE TABLE `authority` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `powerID` enum('A','B','C') NOT NULL,
  `powerContent` varchar(255) NOT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `powerID` (`powerID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of authority
-- ----------------------------
INSERT INTO `authority` VALUES ('1', 'A', '拥有对数据库所有内容的操作权限', '1');
INSERT INTO `authority` VALUES ('2', 'B', '查询，修改仓库信息和添加对数据库增删改查的权限', '<=10');

-- ----------------------------
-- Table structure for bstock
-- ----------------------------
DROP TABLE IF EXISTS `bstock`;
CREATE TABLE `bstock` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_No` varchar(50) NOT NULL,
  `Warehouse_No` varchar(50) NOT NULL,
  `Now_amout` int(20) NOT NULL,
  `avg_price` decimal(10,4) DEFAULT NULL,
  `Amount_Sum` decimal(50,4) NOT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Item_No` (`Item_No`),
  KEY `Warehouse_No` (`Warehouse_No`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of bstock
-- ----------------------------
INSERT INTO `bstock` VALUES ('1', 'B03010602', 'W00001', '80', '5.0000', '400.0000', '');
INSERT INTO `bstock` VALUES ('2', 'B03010602', 'W00002', '68', '5.0000', '340.0000', null);
INSERT INTO `bstock` VALUES ('20', 'B01010101', 'W00001', '10', '7.5200', '75.2000', '测试');

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `Deptno` int(11) NOT NULL AUTO_INCREMENT,
  `Dname` varchar(20) DEFAULT NULL,
  `adress` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `avgsalary` int(11) DEFAULT NULL,
  `persons` int(11) DEFAULT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Deptno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES ('1', 'Manage', 'Beijing', '3730', '2', null);
INSERT INTO `dept` VALUES ('2', 'Office', 'Shanghai', '5000', '1', null);
INSERT INTO `dept` VALUES ('3', 'Finance', 'Hangzhou', '4000', '1', null);
INSERT INTO `dept` VALUES ('4', 'Research', 'Jinhua', '6400', '1', null);
INSERT INTO `dept` VALUES ('5', 'Business', 'Yiwu', '4300', '1', null);

-- ----------------------------
-- Table structure for employees
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `emp_no` varchar(11) NOT NULL,
  `emp_name` varchar(50) NOT NULL,
  `job` varchar(50) NOT NULL,
  `salary` decimal(10,2) NOT NULL,
  `gender` enum('M','F') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `call_phone` char(11) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `deptno` int(11) NOT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `emp_no` (`emp_no`),
  KEY `deptno` (`deptno`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of employees
-- ----------------------------
INSERT INTO `employees` VALUES ('1', 'E0001', '王小强', '采购员', '4300.00', 'M', '18365478962', '浙江金华', '5', '负责采购商品');
INSERT INTO `employees` VALUES ('2', 'B0001', '小李', '业务员', '5000.00', 'F', '15687944542', '浙江杭州', '2', null);
INSERT INTO `employees` VALUES ('3', 'A0001', '小陈', '仓管', '3700.00', 'M', '13356478996', '上海', '1', null);
INSERT INTO `employees` VALUES ('4', 'A0002', '小郭', '仓管', '3760.00', 'F', '15532655478', '北京', '1', null);
INSERT INTO `employees` VALUES ('5', 'C0001', '小何', '会计', '4000.00', 'F', '15466988789', '广东', '3', '');
INSERT INTO `employees` VALUES ('6', 'D0001', '小猪', '商务员', '6400.00', 'M', '13369877564', '甘肃', '4', '负责外交谈生意');

-- ----------------------------
-- Table structure for inventory
-- ----------------------------
DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Inventory_Odd` varchar(50) NOT NULL,
  `Item_No` varchar(50) NOT NULL,
  `Warehouse_No` varchar(50) NOT NULL,
  `dateTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Inventory_num` int(11) NOT NULL,
  `stock_num` int(11) NOT NULL,
  `profit_loss` int(11) NOT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `Operator` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Inventory_Odd` (`Inventory_Odd`),
  KEY `Item_No` (`Item_No`),
  KEY `Warehouse_No` (`Warehouse_No`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of inventory
-- ----------------------------
INSERT INTO `inventory` VALUES ('1', 'IVT000001', 'B03010602', 'W00001', '2020-05-03 00:00:43', '75', '80', '-5', '0', '');
INSERT INTO `inventory` VALUES ('2', 'IVT000002', 'B03010602', 'W00001', '2020-05-03 00:00:43', '80', '75', '5', '0', '');
INSERT INTO `inventory` VALUES ('3', 'IVT000003', 'B03010602', 'W00001', '2020-05-04 09:45:12', '80', '75', '5', '', '');
INSERT INTO `inventory` VALUES ('6', 'IVT000004', 'B01010101', 'W00001', '2020-05-08 00:00:10', '12', '5', '7', '盘点测试', '李潇懵');
INSERT INTO `inventory` VALUES ('7', 'IVT202005000016', 'B01010101', 'W00001', '2020-05-26 23:34:44', '10', '12', '-2', '库存盘点', '打发');

-- ----------------------------
-- Table structure for in_materials
-- ----------------------------
DROP TABLE IF EXISTS `in_materials`;
CREATE TABLE `in_materials` (
  `Item_No` varchar(50) NOT NULL,
  `Item_Name` varchar(255) NOT NULL,
  `IN_Odd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `norms` varchar(50) NOT NULL,
  `IN_number` int(20) NOT NULL,
  `company` varchar(50) NOT NULL,
  `unit_price` decimal(10,4) NOT NULL,
  `Total` decimal(20,4) NOT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Item_No`,`IN_Odd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of in_materials
-- ----------------------------
INSERT INTO `in_materials` VALUES ('B01010101', '高分断断路器', 'ICJ202005000229', 'DZ47-6A/1P', '5', '只', '7.5200', '37.6000', '测完hi无二');
INSERT INTO `in_materials` VALUES ('B01010101', '高分断断路器', 'ICT202005000008', 'DZ47-6A/1P', '-5', '只', '7.5200', '-37.6000', '退货出库');
INSERT INTO `in_materials` VALUES ('B01010101', '高分断断路器', 'TZTZ00001', 'DZ47-6A/1P', '10', '只', '7.5200', '75.2000', '测试');

-- ----------------------------
-- Table structure for in_tables
-- ----------------------------
DROP TABLE IF EXISTS `in_tables`;
CREATE TABLE `in_tables` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Buyer` varchar(50) NOT NULL,
  `IN_Odd` varchar(100) CHARACTER SET cp1250 COLLATE cp1250_general_ci NOT NULL,
  `Sender` varchar(50) NOT NULL,
  `Warehouse_No` varchar(50) NOT NULL,
  `dateTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Checker` varchar(50) NOT NULL,
  `Check_Status` enum('审核通过','审核不通过') NOT NULL,
  `SupplierId` varchar(50) NOT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `Creator` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IN_Odd` (`IN_Odd`),
  KEY `Warehouse_No` (`Warehouse_No`),
  KEY `SupplierId` (`SupplierId`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of in_tables
-- ----------------------------
INSERT INTO `in_tables` VALUES ('26', '小黄', 'TZTZ00001', '晓明', 'W00001', '2020-05-07 16:16:05', '皮特', '审核通过', 'S00001', '测试', '小强');
INSERT INTO `in_tables` VALUES ('31', '贷多少', 'ICJ202005000229', '大', 'W00001', '2020-05-24 20:22:34', '大上的', '审核通过', 'S00001', '测完hi无二', '多大事');
INSERT INTO `in_tables` VALUES ('33', '', 'ICT202005000008', 'sad', 'W00001', '2020-05-24 20:49:48', '打发', '审核通过', 'S00001', '退货出库', '大三');

-- ----------------------------
-- Table structure for ledger
-- ----------------------------
DROP TABLE IF EXISTS `ledger`;
CREATE TABLE `ledger` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Odds` varchar(50) NOT NULL,
  `Item_No` varchar(50) NOT NULL,
  `Warehouse_No` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `IN_number` int(50) DEFAULT NULL,
  `IN_price` decimal(20,4) DEFAULT NULL,
  `IN_sum` decimal(50,4) DEFAULT NULL,
  `OUT_number` int(50) DEFAULT NULL,
  `OUT_price` decimal(20,4) DEFAULT NULL,
  `OUT_sum` decimal(50,4) DEFAULT NULL,
  `balance_number` int(50) NOT NULL,
  `balance_price` decimal(50,4) NOT NULL,
  `balance_sum` decimal(50,4) NOT NULL,
  `dateTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Warehouse_No` (`Warehouse_No`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of ledger
-- ----------------------------
INSERT INTO `ledger` VALUES ('12', 'eTZTZ00001', 'B01010101', 'W00001', '10', '7.5200', '75.2000', null, null, null, '10', '7.5200', '75.2000', '2020-05-07 19:49:24', '入库台账');
INSERT INTO `ledger` VALUES ('13', 'oTZTZ00001', 'B01010101', 'W00001', null, null, null, null, '7.5200', '37.6000', '5', '7.5200', '37.6000', '2020-05-07 17:17:58', '出库台账');
INSERT INTO `ledger` VALUES ('15', 'IVT000004', 'B01010101', 'W00001', null, null, null, '7', '7.5200', '52.6400', '12', '7.5200', '90.2400', '2020-05-08 00:01:12', '盘点数据');
INSERT INTO `ledger` VALUES ('16', 'ICJ202005000229', 'B01010101', 'W00001', '5', '7.5200', '37.6000', null, null, null, '17', '7.5200', '127.8400', '2020-05-24 20:26:31', '入库台账');
INSERT INTO `ledger` VALUES ('17', 'ICT202005000008', 'B01010101', 'W00001', '-5', '7.5200', '-37.6000', null, null, null, '12', '7.5200', '90.2400', '2020-05-24 20:51:45', '入库台账');
INSERT INTO `ledger` VALUES ('18', '单号：OCL202005000027', 'B01010101', 'W00001', null, null, null, '5', '7.5200', '37.6000', '7', '7.5200', '52.6400', '2020-05-24 21:28:47', '出库台账');
INSERT INTO `ledger` VALUES ('19', 'OCT202005000004', 'B01010101', 'W00001', null, null, null, '-5', '7.5200', '-37.6000', '12', '7.5200', '90.2400', '2020-05-24 21:53:00', '出库台账');
INSERT INTO `ledger` VALUES ('23', 'AOT202005000038', 'B03010602', 'W00001', null, null, null, '2', '5.0000', '10.0000', '78', '5.0000', '390.0000', '2020-05-26 19:25:56', '出库台账');
INSERT INTO `ledger` VALUES ('24', 'AOT202005000039', 'B03010602', 'W00001', null, null, null, '-2', '5.0000', '10.0000', '80', '4.7500', '380.0000', '2020-05-26 19:30:29', '出库台账');
INSERT INTO `ledger` VALUES ('26', 'AOT202005000041', 'B03010602', 'W00001', null, null, null, '-2', '5.0000', '-10.0000', '80', '5.0000', '400.0000', '2020-05-26 19:43:26', '出库台账');
INSERT INTO `ledger` VALUES ('32', 'O_AOT202005000050', 'B03010602', 'W00001', null, null, null, '0', '5.0000', '0.0000', '80', '5.0000', '400.0000', '2020-05-26 20:35:40', '出库台账');
INSERT INTO `ledger` VALUES ('33', 'I_AOT202005000050', 'B03010602', 'W00002', null, null, null, '0', '5.0000', '0.0000', '68', '5.0000', '340.0000', '2020-05-26 20:36:36', '出库台账');
INSERT INTO `ledger` VALUES ('34', 'IVT202005000016', 'B01010101', 'W00001', null, null, null, '-2', '7.5200', '-15.0400', '10', '7.5200', '75.2000', '2020-05-26 22:35:38', '盘点数据');

-- ----------------------------
-- Table structure for logininfo
-- ----------------------------
DROP TABLE IF EXISTS `logininfo`;
CREATE TABLE `logininfo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `pwd` varchar(10) NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `user` (`user`),
  CONSTRAINT `user` FOREIGN KEY (`user`) REFERENCES `administrators` (`admin_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of logininfo
-- ----------------------------
INSERT INTO `logininfo` VALUES ('1', 'admin', '000000', null);
INSERT INTO `logininfo` VALUES ('2', 'user1', '000000', null);

-- ----------------------------
-- Table structure for mainstock
-- ----------------------------
DROP TABLE IF EXISTS `mainstock`;
CREATE TABLE `mainstock` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Item_No` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Item_Name` varchar(50) NOT NULL,
  `norms` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `company` varchar(20) NOT NULL,
  `avg_price` decimal(20,4) NOT NULL,
  `Now_amout` int(11) NOT NULL DEFAULT '0',
  `Amount_sum` decimal(50,4) NOT NULL DEFAULT '0.0000',
  `Remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Item_No` (`Item_No`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=842 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of mainstock
-- ----------------------------
INSERT INTO `mainstock` VALUES ('1', 'B01010101', '高分断断路器', 'DZ47-6A/1P', '只', '7.5200', '10', '75.2000', null);
INSERT INTO `mainstock` VALUES ('2', 'B01010102', '高分断断路器', 'DZ47-10A/1P', '只', '5.4600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('3', 'B01010103', '高分断断路器', 'DZ47-16A/1P', '只', '7.5200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('4', 'B01010104', '高分断断路器', 'DZ47-20A/1P', '只', '6.4900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('5', 'B01010105', '高分断断路器', 'DZ47-25A/1P', '只', '6.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('6', 'B01010106', '高分断断路器', 'DZ47-32A/1P', '只', '6.3700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('7', 'B01010107', '高分断断路器', 'DZ47-50A/1P', '只', '6.4900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('8', 'B01010108', '高分断断路器', 'DZ47-60A/1P', '只', '6.3700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('9', 'B01010109', '高分断断路器', 'DZ47-80A/1P', '只', '30.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('10', 'B01010110', '高分断断路器', 'DZ47-100A/1P', '只', '28.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('11', 'B01010201', '塑壳断路器', 'CM1-100/330(100A)', '只', '439.5700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('12', 'B01010202', '塑壳断路器', 'DZ5-20/330', '只', '118.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('13', 'B01010203', '塑壳断路器', 'TM30-100/3300', '只', '315.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('14', 'B01010204', '塑壳断路器', 'TM30-200/3300(200A)', '只', '630.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('15', 'B01010205', '塑壳断路器', 'TD-100/3300', '只', '315.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('16', 'B01010206', '塑壳断路器', 'TD-225/3300(200A)', '只', '315.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('17', 'B01010207', '塑壳断路器', 'DZ10-100/330(30A)', '只', '87.1800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('18', 'B01010208', '塑壳断路器', 'DZ10-100/330(50A)', '只', '87.1800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('19', 'B01010209', '塑壳断路器', 'DZ10-100/330(80A)', '只', '87.1800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('20', 'B01010210', '塑壳断路器', 'DZ10-100/330(100A)', '只', '87.1800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('21', 'B01010301', '抽屉式断路器', 'HSW1-2000/3（800A）', '只', '11194.8700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('22', 'B01010302', '抽屉式断路器', 'HSW1-3200/3（2500A）', '只', '20374.3600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('23', 'B01010303', '智能断路器', 'ZW1-2000/3（1600A）', '只', '10000.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('24', 'B01010304', '框架式断路器', 'DW15-400/3', '只', '1413.7000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('25', 'B01010305', '框架式断路器', 'DW15-630/3', '只', '1188.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('26', 'B01010306', '框架式断路器', 'DW15-1000/3', '只', '2465.8100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('27', 'B01010307', '框架式断路器', 'DW15-1600/3', '只', '2397.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('28', 'B01010308', '框架式断路器', 'DW15-2500/3', '只', '10500.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('29', 'B01010309', '万能断路器', 'YSA2-630A/3', '只', '8247.8600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('30', 'B01010310', '智能断路器', 'YSA3-2000/2000', '台', '8625.6400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('31', 'B01010311', '万能式断路器', 'YSA2-2000A/3', '只', '8888.8900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('32', 'B01010401', '漏电开关', 'DZ47L-16A/2P', '只', '23.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('33', 'B01010402', '漏电断路器', 'DZ10L-150/430', '只', '470.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('34', 'B01010403', '漏电断路器', 'DZ10L-250/430(200A)', '只', '239.3200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('35', 'B01010404', '漏电断路器', 'DZ10L-250/430(250A)', '只', '244.4500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('36', 'B01010405', '漏电断路器', 'DZ10L-400/430(400A)', '只', '0.0200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('37', 'B01010406', '漏电断路器', 'DZ25L-160/430', '只', '364.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('38', 'B01010407', '漏电断路器', 'DZ25L-200/4330(200A)', '只', '382.9100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('39', 'B01010408', '漏电断路器', 'DZ15L-40/2901(20A)', '只', '130.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('40', 'B01010409', '漏电断路器', 'DZ15L-100/2901(63A)', '只', '85.4700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('41', 'B01010410', '漏电断路器', 'DZ15L-100/2901(100A)', '只', '66.6700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('42', 'B01010500', '断路器', 'SM30S-225/330(200A)', '只', '623.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('43', 'B01010501', '断路器', 'SM30S-400/330(400A)', '只', '2923.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('44', 'B01010502', '空气开关', 'SM30S-630/3340(630A)', '只', '1510.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('45', 'B01010503', '空气开关', 'F7-25/2', '只', '194.8700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('46', 'B01010504', '空气开关', 'L7-63/4', '只', '186.3200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('47', 'B01010505', '空气开关', 'DZ20G-100/3300(40A)', '只', '239.3200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('48', 'B01010506', '空气开关', 'DZ20G-100/3300(32A)', '只', '239.3200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('49', 'B01010507', '空气开关', 'DZ20G-225/3300(180A)', '只', '478.6300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('50', 'B01010508', '空气开关', 'SM30S-225/3300(125A)', '只', '487.1800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('51', 'B01010509', '空气开关', 'SM30H-225/3300(100A)', '只', '617.1600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('52', 'B01010510', '断路器', 'SM30H-225/3300(200A)', '只', '617.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('53', 'B01010512', '断路器', 'SM30H-225/3300(140A)', '只', '617.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('54', 'B01010601', '断路器', 'HSM8Ⅱ-63/3 15A', '只', '59.8300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('55', 'B01010602', '断路器', 'HSM1-250M/3300 200A', '只', '683.7600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('56', 'B01010603', '断路器', 'HSM1-250M/3300 250A', '只', '683.7600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('57', 'B01010604', '断路器', 'HSM1-250M/3300 160A', '只', '683.7600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('58', 'B01010605', '断路器', 'HSM1-400M/3300 315A', '只', '854.7000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('59', 'B01010606', '断路器', 'HSM8Ⅱ-63/1 10A', '只', '16.2400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('60', 'B01010607', '断路器', 'HSM8Ⅱ-63/1 16A', '只', '16.2400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('61', 'B01010608', '断路器', 'HSM8Ⅱ-63/1 63A', '只', '20.5100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('62', 'B01010609', '断路器', 'HSM8LⅡ-63/2 10A', '只', '71.7900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('63', 'B01010610', '断路器', 'HSM8LⅡ-63/2 63A', '只', '71.7900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('64', 'B01010701', '漏电开关', 'C65N-32A/4P+VIGIC65-32A/4PELM', '只', '367.5200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('65', 'B01010702', '漏电开关', 'ZB30L-63/4(63A)', '只', '126.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('66', 'B01010703', '漏电开关', 'C65N/3P+VIGIC65N3P63AELM', '只', '495.7300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('67', 'B01010704', '漏电开关', 'SM30L-100/4300', '只', '752.1400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('68', 'B01010705', '漏电开关', 'SM30L-225/4300  200A', '只', '1179.4900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('69', 'B01010706', '漏电开关', 'SM30L-225/4300  140A', '只', '1179.4900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('70', 'B01010707', '漏电开关', 'NM1LE-100/3+N  100A', '只', '256.4100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('71', 'B01010708', '漏电开关', 'NM1LE-225/3+N  160A', '只', '358.9700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('72', 'B01010709', '漏电开关', 'NC100LS-4P 63/D+VIGINC100ELM', '只', '1195.7300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('73', 'B01010710', '漏电开关', 'SM30L-225S/4300 160A', '只', '1179.4900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('74', 'B01010711', '漏电断路器', 'DZ158L-100A/4P', '只', '128.2100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('75', 'B01010712', '漏电开关', 'SM30L-100S/4300  63A', '只', '752.1400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('76', 'B01010801', '断路器', 'Hum18-63/4D 40A', '只', '126.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('77', 'B01010802', '断路器', 'Hum18-63/3D 32A', '只', '96.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('78', 'B01010803', '断路器', 'Hum18-63/1C 16A', '只', '23.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('79', 'B01010804', '断路器', 'Hum18-250S/4B 100A', '只', '820.5100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('80', 'B01010805', '断路器', 'ZM30E-100/3300 100A', '台', '89.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('81', 'B01010806', '断路器', 'Hum18-63/4C 20A', '只', '103.4200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('82', 'B01010807', '断路器', 'Hum18-63/1P 16A', '只', '24.7800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('83', 'B01010808', '断路器', 'Hum18-63/4P 25A', '只', '103.4200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('84', 'B01010809', '断路器', 'Hum18-63/4P 40A', '只', '103.4200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('85', 'B01010810', '断路器', 'CKM33-225S/3300 125A', '台', '512.8200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('86', 'B01010811', '塑壳断路器', 'YSM1-400S/3300 250A', '台', '615.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('87', 'B01010812', '断路器', 'Hum18-250/4B 125A', '只', '820.5100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('88', 'B01010901', '塑壳断路器', 'TIM1-H-250/3300(250A)', '只', '705.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('89', 'B01010902', '塑壳断路器', 'TIM1-H-250/3300(200A)', '只', '705.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('90', 'B01010903', '塑壳断路器', 'TIM1-S-160/3300(160A)', '只', '405.7700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('91', 'B01010904', '塑壳断路器', 'TIM1-S-125/3300(100A)', '只', '334.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('92', 'B01010905', '智能断路器', 'TIW1-2000/1000/3', '只', '9735.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('93', 'B01010906', '智能断路器', 'TIW1-2000/1000/3 (5S)', '只', '10247.8600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('94', 'B01010907', '智能断路器', 'TIW1-2000/3 1250A', '只', '13226.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('95', 'B01010908', '塑壳断路器', 'TIM1-S-160/3300 100A', '只', '118.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('96', 'B01010909', '塑壳断路器', 'TIM1-S-400/3300 400A', '只', '666.6700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('97', 'B01010910', '塑壳断路器', 'TIM1-S-250/3300 200A', '只', '594.7000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('98', 'B01010911', '塑壳断路器', 'TIM1-S-400/3300 315A', '只', '333.3400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('99', 'B01010912', '小型断路器', 'TIB1-63C40', '只', '19.3600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('100', 'B01010913', '智能断路器', 'TIW1-2000/3（630A）', '台', '8636.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('101', 'B01010914', '智能断路器', 'TIW1-3200/3（3200A）', '台', '21713.6800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('102', 'B01010915', '塑壳断路器', 'TIM1-H-400/3300 400A', '只', '80.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('103', 'B01010916', '塑壳断路器', 'TIM1-H-400/3300 315A', '只', '769.2300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('104', 'B01010917', '塑壳断路器', 'TIM1-S-125/3300 125A', '只', '176.9200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('105', 'B01020101', '刀开关', 'HD11B-200/38', '把', '118.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('106', 'B01020102', '刀开关', 'HD11B-400/38', '把', '209.8700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('107', 'B01020103', '刀开关', 'HD11B-600/38', '把', '359.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('108', 'B01020104', '刀开关', 'HD12B-400/31', '把', '176.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('109', 'B01020105', '刀开关', 'HD12B-600/31', '把', '540.8600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('110', 'B01020106', '刀开关', 'HD13B-200/31', '把', '125.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('111', 'B01020107', '刀开关', 'HD13B-400/31', '把', '219.0700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('112', 'B01020108', '刀开关', 'HD13B-600/31', '把', '378.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('113', 'B01020109', '刀开关', 'HD13B-1000/31', '把', '368.8900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('114', 'B01020110', '刀开关', 'HD13B-1500/31', '把', '642.4600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('115', 'B01020111', '刀开关', 'HD13BX-100/31', '把', '267.2500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('116', 'B01020112', '刀开关', 'HD13BX-200/31', '把', '184.8700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('117', 'B01020113', '刀开关', 'HD13BX-400/31', '把', '277.4000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('118', 'B01020114', '刀开关', 'HD13BX-600/31', '把', '439.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('119', 'B01020201', '双投刀开关', 'HS11B-200/38', '只', '122.4100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('120', 'B01020202', '双投刀开关', 'HS11B-400/48', '只', '275.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('121', 'B01020203', '双投刀开关', 'HS12B-200/41', '把', '451.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('122', 'B01020204', '双投刀开关', 'HS13B-200/31', '把', '139.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('123', 'B01020205', '双投刀开关', 'HS13B-400/31', '把', '883.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('124', 'B01020206', '双投刀开关', 'HS13B-600/31', '把', '429.3500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('125', 'B01020207', '双投刀开关', 'HS13B-200/41', '把', '295.4300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('126', 'B01020208', '双投刀开关', 'HS13B-400/41', '把', '315.6900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('127', 'B01020209', '双投刀开关', 'HS13B-400/71', '把', '594.7200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('128', 'B01020210', '旋转式双投刀开关', 'HS13BX-400/31', '把', '698.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('129', 'B01020211', '旋转式双投刀开关', 'HS13BX-600/41', '把', '974.6200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('130', 'B01020212', '旋转式双投刀开关', 'HS13BX-1000/41', '把', '1781.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('131', 'B01020213', '旋转式双投刀开关', 'HS13BX-600/31', '把', '625.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('132', 'B01020214', '旋转式双投刀开关', 'HS13BX-1000/31', '把', '933.8400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('133', 'B01020215', '旋转式双投刀开关', 'HS13BX-1500/31', '把', '2427.6900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('134', 'B01020216', '旋转式双投刀开关', 'HS13BX-400/41', '把', '472.6100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('135', 'B01020217', '双投刀开关', 'HS13B-600/41', '只', '564.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('136', 'B01020218', '双投刀开关', 'HS11-200/49', '把', '151.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('137', 'B01020219', '双投刀开关', 'HS13B-1000/41', '把', '1781.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('138', 'B01020220', '双投刀开关', 'HS13B-200/71', '把', '536.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('139', 'B01020221', '双投刀开关', 'HS13BX-400/71', '台', '866.9500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('140', 'B01020222', '双投刀开关', 'HS13BX-1500/41', '把', '2523.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('141', 'B01020223', '双投刀开关', 'HS13B-100/41', '把', '210.4600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('142', 'B01020224', '双投开关', 'HK-100/3', '把', '60.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('143', 'B01020225', '双投刀开关', 'HS11B-400/49', '把', '226.4600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('144', 'B01020226', '双投开关', 'HK2-60/2', '把', '11.9700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('145', 'B01020227', '双投刀开关', 'HS11-200/48', '把', '94.0200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('146', 'B01020228', '双投刀开关', 'HS11-60/48', '把', '61.5400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('147', 'B01020229', '双投刀开关', 'HS13B-1500/41', '把', '2444.3100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('148', 'B01020230', '刀开关', 'HS11-600/48', '只', '435.9000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('149', 'B01020231', '双投刀开关', 'HS11B-630/49', '把', '453.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('150', 'B01020232', '旋转式双投刀开关', 'HS13BX-200/41', '把', '324.3100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('151', 'B01020233', '双投刀开关', 'HS13BX-1500/40', '只', '2326.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('152', 'B01020234', '双投刀开关', 'HS13BX-2500/40', '把', '8318.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('153', 'B01020301', '熔断式刀开关', 'HR3-200/32', '把', '242.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('154', 'B01020302', '熔断式刀开关', 'HR3-400/32', '把', '219.1100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('155', 'B01020303', '熔断式刀开关', 'HR3-600/32', '把', '455.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('156', 'B01020304', '熔断式刀开关', 'HR3-200/31', '把', '244.3400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('157', 'B01020305', '熔断式刀开关', 'HR3-200/34', '把', '240.3700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('158', 'B01020306', '熔断式刀开关', 'HR3-400/34', '把', '298.1900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('159', 'B01020307', '熔断式刀开关', 'HR3-600/34', '把', '423.6700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('160', 'B01020308', '隔离开关', 'HR5-630/30', '把', '558.3500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('161', 'B01020309', '隔离开关', 'HR5-600/30', '把', '547.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('162', 'B01020310', '隔离开关', 'HR5-500/30', '把', '931.6200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('163', 'B01020311', '隔离开关', 'HR5-400/30', '把', '2211.7200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('164', 'B01020312', '隔离开关', 'HR5-200/30', '把', '316.2400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('165', 'B01020313', '隔离开关', 'HR5-100/30', '把', '217.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('166', 'B01020314', '熔断器式隔离器', 'HG30-32/10A', '只', '7.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('167', 'B01020315', '熔断器式隔离器', 'HG30-32/16A', '只', '7.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('168', 'B01020316', '熔断器式隔离器', 'HG30-32/32A', '只', '5.2500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('169', 'B01020317', '熔断器式隔离器', 'HG30-32/20A', '只', '7.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('170', 'B01020318', '熔断器式隔离器', 'HG30-32/1P', '只', '5.5900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('171', 'B01020319', '开关熔断器', 'HH15-250/3', '套', '514.1900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('172', 'B01020320', '开关熔断器', 'HH15P-125A', '只', '255.2100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('173', 'B01020321', '隔离开关', 'HP-1600/3', '台', '1923.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('174', 'B01020322', '熔断式刀开关', 'HR5-100/31', '把', '36.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('175', 'B01020323', '刀开关', 'HH15-400/3', '只', '470.0900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('176', 'B01020324', '熔断式刀开关', 'HR5-200/31', '把', '410.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('177', 'B01020401', '封闭式负荷开关', 'HH3-100/32', '把', '51.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('178', 'B01020402', '封闭式负荷开关', 'HH3-200/32', '把', '242.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('179', 'B01020403', '封闭式负荷开关', 'HH3-400/32', '把', '219.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('180', 'B01020404', '开启式负荷开关', 'HK2-100/3', '把', '45.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('181', 'B01020405', '开启式负荷开关', 'HK2-60/3', '把', '35.6700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('182', 'B01020406', '开启式负荷开关', 'HK2-60/2', '把', '9.3200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('183', 'B01020407', '开启式负荷开关', 'HK2-30/3', '把', '16.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('184', 'B01020408', '开启式负荷开关', 'HK2-30/2', '把', '11.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('185', 'B01020409', '开启式负荷开关', 'HK2-15/3', '把', '11.6400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('186', 'B01020410', '开启式负荷开关', 'ZK1-100A/3', '把', '60.6800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('187', 'B01020411', '开启式负荷开关', 'HK2-15/2', '把', '8.3100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('188', 'B01020412', '隔离开关', 'QSA-250/4 250A', '台', '717.9500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('189', 'B01020413', '隔离开关', 'QSA-400/4  315A', '台', '738.1600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('190', 'B01020414', '隔离开关', 'SX-40/4', '台', '125.6400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('191', 'B01020501', '转换开关', 'LW5-16YH/3', '只', '23.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('192', 'B01020502', '转换开关', 'LW5-16K6894/6#', '只', '81.6100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('193', 'B01020503', '转换开关', 'LW5-16L6560/5', '只', '80.3400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('194', 'B01020504', '转换开关', 'LW5-16L7891/10', '只', '141.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('195', 'B01020505', '转换开关', 'LW5-16D3462/12', '只', '88.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('196', 'B01020506', '转换开关', 'LW5-16TM707/7', '只', '47.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('197', 'B01020507', '万能转换开关', 'LW5-150936/3', '只', '41.4100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('198', 'B01020508', '电压切换开关', 'LW5-15E 1132/4', '只', '0.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('199', 'B01020509', '同期开关', 'LW5-15D 0408/2', '只', '36.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('200', 'B01020510', '转换开关', 'LW5-16E 113/4', '只', '36.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('201', 'B01020511', '转换开关', 'LW5-16C7591/10', '只', '141.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('202', 'B01020512', '转换开关', 'LW2-1/1111F48X', '只', '20.5100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('203', 'B01020513', '智能双电源转换开关', 'CKQ33-100H.4328/80A', '只', '3333.3300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('204', 'B01020514', '双电源自动切换开关', 'TIQ1-160R/160/4F', '台', '3910.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('205', 'B01020601', '组合开关', 'HZ10-25/3', '只', '21.5400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('206', 'B01020602', '组合开关', 'HZ10-60/3', '只', '49.5700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('207', 'B01020603', '电焊机开关', 'KD12-125', '只', '119.6600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('208', 'B01020604', '扭子开关', '？', '只', '2.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('209', 'B01020605', '组合开关', 'HZ10-25P/3', '只', '21.5400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('210', 'B01020606', '组合开关', 'HZ10-60P/3', '只', '49.5700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('211', 'B01020607', '组合开关', 'HZ5-20/4', '只', '65.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('212', 'B01020701', '石板闸刀', 'HRTO-400', '把', '0.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('213', 'B01020702', '石板闸刀', 'HRTO-100A', '把', '51.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('214', 'B01020703', '倒顺开关', '15A', '只', '14.5300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('215', 'B01020704', '倒顺开关', '60A', '把', '18.6300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('216', 'B01020801', '动态无触点开关', '？', '只', '3333.3300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('217', 'B01030101', '接触器', 'CJ10-5A', '只', '21.8800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('218', 'B01030102', '接触器', 'CJ10-10A/220V(380V)', '只', '54.5300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('219', 'B01030103', '接触器', 'CJ10-20A', '只', '65.6800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('220', 'B01030104', '接触器', 'CJ10-40A', '只', '29.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('221', 'B01030105', '接触器', 'CJ10-60A', '只', '136.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('222', 'B01030106', '接触器', 'CJ10-100A/220V', '只', '137.3500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('223', 'B01030107', '接触器', 'CJ10-150A', '只', '230.0700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('224', 'B01030108', '接触器', 'CJ20-40A/380V', '只', '150.4300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('225', 'B01030109', '接触器', 'CJ20-100A/380V', '只', '134.7200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('226', 'B01030110', '接触器', 'CJ20-250A/380V', '只', '529.0600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('227', 'B01030111', '接触器', 'CJ20-250A/220V', '只', '11.9900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('228', 'B01030112', '接触器', 'CJ20-160/380V', '只', '74.2700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('229', 'B01030113', '接触器', 'CJ20-400A/380V', '只', '613.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('230', 'B01030114', '接触器', 'CJ20-630A/380V', '只', '1353.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('231', 'B01030115', '接触器', 'CJ20-100A/220V', '只', '158.7700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('232', 'B01030201', '节能接触器', 'CJC20-150A', '只', '379.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('233', 'B01030202', '节能接触器', 'CJC20-160A', '只', '444.4200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('234', 'B01030203', '节能接触器', 'CJC20-250A', '只', '837.6100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('235', 'B01030204', '节能接触器', 'CJC20-400A', '只', '1132.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('236', 'B01030205', '接触器', 'CJX10-63A', '只', '217.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('237', 'B01030206', '接触器', 'CJX2-163A', '只', '35.9000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('238', 'B01030301', '切换电容器接触器', 'CJ19-32A/11(220V)', '只', '58.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('239', 'B01030302', '切换电容器接触器', 'CJ19-43A/11(220V)', '只', '58.4100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('240', 'B01030303', '切换电容器接触器', 'CJ23-40/220', '只', '106.8400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('241', 'B01030304', '切换电容接触器', 'CJ19-63/11(220V)', '只', '211.9700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('242', 'B01030305', '切换电容接触器', 'CJ19C-63/21', '只', '24.9000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('243', 'B01030306', '切换电容接触器', 'CJ39-40-11/220V', '只', '64.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('244', 'B01030307', '切换电容接触器', 'CJ39-63-21/220V', '只', '125.9900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('245', 'B01030308', '切换电容接触器组合', 'HZG33A-40/11 220V', '套', '65.0200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('246', 'B01030309', '切换电容接触器组合', 'HZG33A-63/21 220V', '套', '217.3500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('247', 'B01030401', '热继电器', 'JR16B-20/3', '只', '19.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('248', 'B01030402', '热继电器', 'JR28-36A', '只', '57.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('249', 'B01030403', '热继电器', 'JR10-10', '只', '32.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('250', 'B01030404', '热继电器', 'JRS1-12316(5A)', '只', '18.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('251', 'B01030405', '热继电器', 'JR16B-63A', '只', '22.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('252', 'B01030501', '时间继电器', 'JST-1A', '只', '44.2300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('253', 'B01030502', '时间继电器', 'JS7-14A-60秒(220V)', '只', '18.5500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('254', 'B01030503', '时间控制器', 'UKD-I', '只', '222.5300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('255', 'B01030504', '时间继电器', 'JS14A-Y/10s/220v', '只', '106.8300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('256', 'B01030505', '时间继电器', 'DS-123AC/220V', '只', '316.2400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('257', 'B01030506', '时间继电器', 'DSJ-13/220V', '只', '356.7700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('258', 'B01030507', '时间继电器', 'DS-122/0.25～3.5〃', '只', '292.3100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('259', 'B01030508', '时间继电器', 'DS-123/0.5～9〃', '只', '200.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('260', 'B01030509', '时间继电器', 'DS-35/220V', '只', '275.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('261', 'B01030510', '定时开关', 'L1E1F/S', '只', '126.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('262', 'B01030601', '电流继电器', 'JL14-5A', '只', '30.7900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('263', 'B01030602', '电流继电器', 'JS14A-60秒 220V', '只', '30.7900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('264', 'B01030603', '电流继电器', 'DL-11/10', '只', '162.3900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('265', 'B01030701', '中间继电器', 'JZ7-44/220V', '只', '32.4800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('266', 'B01030702', '中间继电器', 'JZC1-44/220V', '只', '73.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('267', 'B01030703', '中间继电器', 'JZ1A-33/380V', '只', '47.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('268', 'B01030704', '中间继电器', 'DZJ-207/AC220V', '只', '97.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('269', 'B01030705', '中间继电器', 'CZC4-31/220V', '只', '73.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('270', 'B01030706', '接触器式继电器', 'JZC4-31', '只', '73.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('271', 'B01030707', '继电器', 'HH5-3P/110V', '只', '12.8200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('272', 'B01030708', '继电器', 'JQX-3Z/220V', '只', '12.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('273', 'B01030709', '中间继电器', 'JZ7-44/380V', '只', '32.4800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('274', 'B01030801', '信号继电器', 'DX-11/0.1A', '只', '102.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('275', 'B01030802', '欠频率继电器', 'SQP-6 110V', '只', '3094.0200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('276', 'B01030803', '信号继电器', 'DX-17/30', '只', '275.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('277', 'B01030804', '低周率继电器', 'SZH3/AC100V', '只', '1470.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('278', 'B01030805', '晶体管液位继电器', 'TYR-714/380V', '只', '51.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('279', 'B01030806', '液位继电器', 'JYB-714/220V', '只', '51.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('280', 'B01030901', '电压继电器', 'JD-1B', '只', '389.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('281', 'B01030902', '电压继电器', 'JD-2B', '只', '393.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('282', 'B01030903', '电压继电器', 'DJ-111/400', '只', '188.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('283', 'B01040101', '插入式熔断器', 'RC1-5A', '只', '1.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('284', 'B01040102', '插入式熔断器', 'RC1-10A', '只', '1.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('285', 'B01040103', '插入式熔断器', 'RC1-15A', '只', '1.8800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('286', 'B01040104', '插入式熔断器', 'RC1-30A', '只', '3.4500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('287', 'B01040105', '插入式熔断器', 'RC1-60A', '只', '5.9900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('288', 'B01040106', '插入式熔断器', 'RC1-100A', '只', '12.8200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('289', 'B01040107', '插入式熔断器', 'RC1-200A', '只', '21.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('290', 'B01040202', '熔断器(熔芯)', 'JF2-(5-10)A', '只', '1.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('291', 'B01040204', '管式熔断器(熔芯)', 'R1-5A(6×30)', '盒', '21.3700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('292', 'B01040205', '管式熔芯', 'R1-2A(5×20)', '盒', '17.0900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('293', 'B01040206', '熔断器（熔芯）', 'JFS-2.5RD(20A)', '只', '1.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('294', 'B01040207', '熔断器(熔芯)', 'JFS-2.5RD(6A)', '只', '2.4400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('295', 'B01040302', '低压熔断器（熔座）', 'RT14-32A', '只', '5.6400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('296', 'B01040401', '熔断器端子', 'JFS-2.5RD/10A', '套', '7.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('297', 'B01060101', '单相调压器', 'JDGCZJ-2', '只', '182.0500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('298', 'B01060102', '全自动稳压器', 'TND-1000', '只', '182.0500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('299', 'B01060103', '数字万用表', 'UJ2003', '只', '452.9900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('300', 'B01060104', '秒表', 'SJ9-2', '只', '119.6600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('301', 'B01060105', '漏电保护器', 'SDLB-6B', '只', '188.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('302', 'B01060106', '电度表', 'DT862-(5-10)A', '只', '163.0500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('303', 'B01060107', '兆欧表', 'ZC250-500V(500MΩ)', '只', '158.8700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('304', 'B01060108', '有功功率因数表', '42L6-COSΨ 5A/380V', '只', '51.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('305', 'B01060109', '仿真器', 'AEDK-51S', '套', '1794.8700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('306', 'B01060110', '电度表', 'DT862-3(1.5-6A)', '只', '188.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('307', 'B01060111', '电度表', 'DT862-3(10-40A)380V', '只', '130.4000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('308', 'B01060112', '变压器', 'TDK3-400', '台', '269.2300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('309', 'B01060113', '漏电保护器', 'SDLB-9C', '只', '285.9500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('310', 'B01060114', '频率表', '42L6-HZ45-55HZ', '只', '66.6700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('311', 'B01060115', '无功电能表', 'DS862-380V/5A', '只', '162.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('312', 'B01060116', '自动准同期装置', 'ZZB-I', '只', '0.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('313', 'B01060401', '无功功率补偿自动控制器', 'JKG(B)-4', '只', '633.6300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('314', 'B01060402', '无功功率补偿自动控制器', 'JKG(B)-6', '只', '698.9600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('315', 'B01060403', '无功功率补偿自动控制器', 'JKG(B)-8', '只', '750.9800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('316', 'B01060404', '无功功率补偿自动控制器', 'JKG(B)-10', '只', '785.8900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('317', 'B01060405', '无功功率补偿自动控制器', 'JKGH-10', '只', '427.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('318', 'B01060406', '功率因数调整器（台湾）', 'TS8D-T', '只', '2034.1900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('319', 'B01060407', '无功功率补偿自动控制器', 'JKGD-10/3', '只', '346.3200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('320', 'B01060901', '交流功率表', '42L6W-150/5', '只', '96.7600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('321', 'B01060902', '交流功率表', '42L6W-200/5', '只', '83.7600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('322', 'B01060903', '交流功率表', '42L6W-300/5', '只', '83.5900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('323', 'B01060904', '交流功率表', '42L6W-400/5', '只', '82.7300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('324', 'B01060905', '交流功率表', '42L6W-500/5', '只', '82.4800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('325', 'B01060906', '交流功率表', '42L6W-600/5', '只', '89.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('326', 'B01060907', '交流功率表', '42L6W-800/5', '只', '76.9200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('327', 'B01060908', '交流功率表', '42L6W-1000/5', '只', '76.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('328', 'B01060909', '交流功率表', '42L6W-1500/5', '只', '76.9200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('329', 'B01060910', '交流功率表', '42L6W-100/5', '只', '96.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('330', 'B01060911', '交流功率表', '42L6W-1200/5', '只', '96.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('331', 'B01060912', '交流功率表', '42L6W-250/5', '只', '100.8500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('332', 'B01060913', '交流功率表', '42L6W-2500/5', '只', '96.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('333', 'B01060914', '交流功率表', '42L6W-5000/5', '只', '96.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('334', 'B01060915', '交流功率表', '42L6W-3K/5', '只', '96.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('335', 'B01060916', '交流功率表', '42L6W-2000/5', '只', '0.0500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('336', 'B01070101', '铜芯线', 'BV-1mm2', 'm', '0.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('337', 'B01070102', '铜芯线', 'BV-1.5mm2', 'm', '0.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('338', 'B01070103', '铜芯线', 'BV-2.5mm2', 'm', '1.6400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('339', 'B01070104', '铜芯线', 'BV-4mm2', '米', '1.2700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('340', 'B01070105', '铜芯线', 'BV-6mm2', 'm', '2.1900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('341', 'B01070106', '铜芯线', 'BV-10mm2', 'm', '5.9600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('342', 'B01070107', '铜芯线', 'BV-16mm2', 'm', '10.3700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('343', 'B01070108', '铜芯线', 'BV-25mm2', 'm', '15.3100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('344', 'B01070109', '铜芯线', 'BV-35mm2', 'm', '17.5700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('345', 'B01070110', '铜芯线', 'BV-50mm2', 'm', '15.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('346', 'B01070111', '铜芯线', 'BV-70mm2', 'm', '41.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('347', 'B01070201', '铜芯软线', 'BVR-1mm2', 'm', '0.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('348', 'B01070202', '铜芯软线', 'BVR-1.5mm2', 'm', '0.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('349', 'B01070203', '铜芯软线', 'BVR-2.5mm2', 'm', '1.6100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('350', 'B01070204', '铜芯软线', 'BVR-4mm2', 'm', '1.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('351', 'B01070205', '铜芯软线', 'BVR-6mm2', 'm', '0.8400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('352', 'B01070206', '铜芯软线', 'BVR-16mm2', 'm', '7.4400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('353', 'B01070207', '橡套软线', 'YZ-2×1mm2', 'm', '0.8500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('354', 'B01070208', '橡套软线', 'YZ-3*4mm2+1*2.5mm2', 'm', '7.3700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('355', 'B01070209', '橡套软线', 'YZ-2×1.5', 'm', '1.3400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('356', 'B01070210', '橡套软线', 'YZ-3×1.5', 'm', '5.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('357', 'B01070211', '多股胶线', 'RVS-1mm2', 'm', '0.6000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('358', 'B01070301', '铝排(0.2025kg/m)', 'LYM-3×25', 'm', '1.6000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('359', 'B01070302', '铝排(0.243kg/m)', 'LYM-3×30', 'm', '5.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('360', 'B01070303', '铝排(0.432kg/m)', 'LYM-4×40', 'm', '9.9700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('361', 'B01070304', '铝排(0.675kg/m)', 'LYM-5×50', 'm', '14.4200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('362', 'B01070305', '铝排(0.975kg/m)', 'LYM-6×60', 'm', '22.0200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('363', 'B01070306', '铝排(1.728kg/m)', 'LYM-8×80', 'm', '39.8800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('364', 'B01070307', '铝排(2.700kg/m)', 'LYM-10×100', 'm', '48.3600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('365', 'B01070401', '铜排(0.801kg/m)', 'TYM-3×30', 'm', '49.2900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('366', 'B01070402', '铜排(1.424kg/m)', 'TYM-4×40', 'm', '87.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('367', 'B01070403', '铜排(1.335kg/m)', 'TYM-5×30', 'm', '41.0700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('368', 'B01070404', '铜排(2.136kg/m)', 'TYM-6×40', 'm', '131.4700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('369', 'B01070405', '铜排(2.225kg/m)', 'TYM-5×50', 'm', '136.4300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('370', 'B01070406', '铜排(4.45kg/m)', 'TYM-10×50', 'm', '150.6200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('371', 'B01070407', '铜排(3.204kg/m)', 'TYM-6×60', 'm', '202.6500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('372', 'B01070408', '铜排(8.90kg/m)', 'TYM-10×100', 'm', '509.6600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('373', 'B01070409', '铜排(1.78kg/m)', 'TYM-5×40', 'm', '36.9000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('374', 'B01070410', '铜板', '405×985×5', 'kg', '21.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('375', 'B01070411', '紫铜管', 'φ14', 'kg', '42.3100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('376', 'B01070501', '铜接头', 'DT-16mm2', '只', '1.9700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('377', 'B01070502', '铜接头', 'DT-25mm2', '只', '2.4800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('378', 'B01070503', '铜接头', 'DT-35mm2', '只', '3.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('379', 'B01070504', '铜接头', 'DT-50mm2', '只', '4.8100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('380', 'B01070505', '铜接头', 'DT-70mm2', '只', '7.4400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('381', 'B01070506', '铜接头', 'DT-95mm2', '只', '10.3200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('382', 'B01070507', '铜接头', 'DT-120mm2', '只', '10.8400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('383', 'B01070508', '铜接头', 'DT-185mm2', '只', '27.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('384', 'B01070509', '铜接头', 'DT-240mm2', '只', '15.8200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('385', 'B01070510', '铜接头', 'DT-150mm2', '只', '14.0900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('386', 'B01070511', '接地端棒', '？', '套', '23.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('387', 'B01070512', '插针', 'L45-25', '只', '1.9600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('388', 'B01070513', '铜接头', 'SBG-1-m20', '只', '31.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('389', 'B01070601', '铝铜接头', 'DLT-25mm2', '只', '3.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('390', 'B01070602', '铝铜接头', 'DLT-35mm2', '只', '4.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('391', 'B01070603', '铝铜接头', 'DLT-50mm2', '只', '4.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('392', 'B01070604', '铜铝接头', 'DTL-25 mm2', '只', '1.2500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('393', 'B01070605', '铜铝接头', 'DTL-50 mm2', '只', '3.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('394', 'B01070606', '铜铝接头', 'DTL-35mm2', '只', '1.2500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('395', 'B01070607', '铜铝接头', 'DLT-70mm2', '只', '5.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('396', 'B01070608', '铜铝接头', 'DLT-95mm2', '只', '6.4100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('397', 'B01070609', '铜软连接', '360*60*2', '条', '74.5900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('398', 'B01070701', '圆形裸端头', 'OT-20A', '只', '0.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('399', 'B01070702', '圆形裸端头', 'OT-40A', '只', '0.1900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('400', 'B01070703', '圆形裸端头', 'OT-60A', '只', '0.6500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('401', 'B01070704', '圆形裸端头', 'OT-80A', '只', '0.4000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('402', 'B01070705', '圆形裸端头', 'OT-100A', '只', '0.6600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('403', 'B01070706', '圆形裸端头', 'OT-150A', '只', '1.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('404', 'B01070707', '圆形裸端头', 'OT-200A', '只', '1.1800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('405', 'B01070708', '圆形裸端头', 'OT-250A', '只', '1.4500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('406', 'B01070709', '圆形裸端头', 'OT-400A', '只', '3.0200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('407', 'B01070710', '圆形裸端头', 'OT-50A', '只', '0.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('408', 'B01070711', '圆形裸端头', 'OT-2.5*4', '只', '0.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('409', 'B01070712', '圆形裸端头', 'OT-6mm2', '只', '0.1100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('410', 'B01070713', '圆形裸端头', 'OT-300A', '只', '1.8800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('411', 'B01070714', '圆形裸端头', 'OT-600A', '只', '4.3400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('412', 'B01070715', '圆形裸端头', 'OT-2.5*6mm2', '只', '0.0600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('413', 'B01070716', '圆形裸端头', 'OT-2.5*8', '只', '0.0700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('414', 'B01070717', '圆形裸端头', 'OT-500A', '只', '2.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('415', 'B01070801', '叉形裸端头', 'TU-1.5×3', '只', '0.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('416', 'B01070802', '叉形裸端头', 'TU-1.5×4', '只', '0.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('417', 'B01070803', '叉形裸端头', 'TU-1.5×5', '只', '0.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('418', 'B01070804', '叉形裸端头', 'TU-1.5×6', '只', '0.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('419', 'B01070805', '叉形裸端头', 'TU-2.5×3', '只', '0.0500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('420', 'B01070806', '叉形裸端头', 'TU-2.5×4', '只', '0.0600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('421', 'B01070807', '叉形裸端头', 'TU-2.5×5', '只', '0.0600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('422', 'B01070808', '叉形裸端头', 'TU-2.5×6', '只', '0.0600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('423', 'B01070809', '叉形裸端头', 'TU-6×6', '只', '0.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('424', 'B01070810', '叉形裸端头', 'UT-6×5', '只', '0.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('425', 'B01070812', '叉形裸端头', 'UT-6*4', '只', '0.1100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('426', 'B01070813', '铜接头', 'UT-10*6', '只', '0.3400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('427', 'B01070820', '叉形裸端头', 'OT-2.5×4', '只', '0.0600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('428', 'B01070821', '插针', 'c4516MM2', '只', '1.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('429', 'B01070822', '插针', '35mm2', '只', '2.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('430', 'B01070823', '插针', 'L45-10mm2', '只', '1.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('431', 'B01080101', '接线端子', 'X5-2005', '只', '2.6700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('432', 'B01080102', '接线端子', 'JDO-60/0', '只', '0.6000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('433', 'B01080103', '普通接线端子', 'JH10-2.5/1', '只', '2.1400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('434', 'B01080104', '试验接线端子', 'JH10S-2.5', '只', '3.2900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('435', 'B01080105', '连接接线端子', 'JH10-2.5/2L', '只', '2.1400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('436', 'B01080106', '低压出线桩子', '100KVA（φ12）', '套', '29.9100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('437', 'B01080107', '低压出线桩子', '315KVA（φ20）', '套', '83.7600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('438', 'B01080108', '接线端子', 'TB-2503/L', '只', '1.9000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('439', 'B01080109', '接线端子', 'TB-2504/L', '只', '1.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('440', 'B01080110', '接线板', 'X3-6012', '条', '12.8200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('441', 'B01080111', '接线柱', '633', '只', '4.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('442', 'B01080112', '接线端子', 'JF5-40/5', '只', '3.6500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('443', 'B01080113', '接线端子', 'JF5-60/5', '只', '4.1600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('444', 'B01080114', '连接片', '?', '片', '0.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('445', 'B01080115', '接线端子', 'JF5-10/5', '只', '4.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('446', 'B01080201', '接线柱', '333型', '只', '0.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('447', 'B01080202', '接线板', 'B1-4-10', '块', '11.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('448', 'B01080203', '组合接线盒', 'DFY1-3×380V/220V', '只', '46.5400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('449', 'B01080204', '接线柱', 'P-100A', '只', '15.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('450', 'B01080205', '接线柱', 'P10A', '只', '5.4900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('451', 'B01080206', '补偿控制接线桩头', '？', '付', '0.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('452', 'B01080207', '接线柱', '555型', '只', '6.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('453', 'B01080208', '接线板', 'JF5-2.5/5', '条', '2.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('454', 'B01080209', '接线板', 'TL-100/4', '块', '6.8900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('455', 'B01080210', '接线板', 'TB-2503/L', '条', '1.9000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('456', 'B01080211', '接线板', 'XJ-4/60A', '块', '1.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('457', 'B01080401', '行程开关', 'JLXK1-111', '只', '23.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('458', 'B01080402', '行程开关', 'LX-5-11H', '只', '13.6800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('459', 'B01080403', '行程开关', 'LX-0.28', '只', '9.8300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('460', 'B01080404', '行程开关', 'LX3-11H', '只', '10.6500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('461', 'B01080405', '行程开关', 'LX19-111(5A)', '只', '2.5200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('462', 'B01080406', '行程开关', 'LXK2-411K', '只', '17.8400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('463', 'B01080407', '行程开关', 'LXIP-K', '只', '0.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('464', 'B01080408', '行程开关', 'X2-N', '只', '20.8500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('465', 'B01080409', '限位开关', 'LX44-20', '只', '73.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('466', 'B01080410', '微动开关', 'LXW5-G', '只', '9.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('467', 'B01080501', '控制按钮', 'LA2-220V', '只', '7.0700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('468', 'B01080502', '控制按钮', 'LA18-380V(5A)', '只', '7.6900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('469', 'B01080503', '旋转按钮', 'LAY3-220V', '只', '20.5100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('470', 'B01080504', '按钮', 'KAO-5B', '只', '5.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('471', 'B01080505', '按钮', 'LA10-2H', '只', '10.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('472', 'B01080506', '按钮', 'LA18--22J', '只', '7.6900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('473', 'B01080507', '按钮', 'LASO-A-11', '只', '9.8300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('474', 'B01080508', '按钮', 'LAY3-Z/11', '只', '15.3900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('475', 'B01080509', '按钮', 'LA18-22/380V', '只', '7.6900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('476', 'B01080510', '行车按钮', '4档', '只', '15.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('477', 'B01080511', '按钮', 'AK-6', '只', '76.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('478', 'B01080512', '按钮', 'LAP-25/2', '只', '25.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('479', 'B01080601', '信号灯', 'XD11-6.3', '只', '2.8700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('480', 'B01080602', '信号灯', 'AD11-25/40', '只', '7.6600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('481', 'B01080603', '信号灯', 'XD7-8(380V)', '只', '9.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('482', 'B01080604', '信号灯', 'XD13-220V', '只', '6.6700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('483', 'B01080605', '电铃', 'UC4-75/220V', '只', '15.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('484', 'B01080606', '超声波驱鼠器', '？', '只', '253.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('485', 'B01080607', '主令开关', 'LS2-2', '只', '14.7000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('486', 'B01080608', '电话机', 'HA9000（23）', '台', '98.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('487', 'B01080609', '警铃', '220V', '只', '0.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('488', 'B01080610', '同期灯', 'XD5-380', '只', '7.6900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('489', 'B01080611', '信号灯', 'XD5-220', '只', '7.6900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('490', 'B01080612', '光字牌', 'XD9-220/15W', '只', '6.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('491', 'B01080613', '信号灯', 'AD16-22D/220V', '只', '8.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('492', 'B01080614', '信号灯', 'AD16-22D/380', '只', '6.3200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('493', 'B01080615', '指示灯', 'AD13-6.3V', '只', '6.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('494', 'B01080801', '低压避雷器', 'FYS-0.2', '只', '8.8900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('495', 'B01080802', '高压避雷器', 'HY5Ws-12.7/50', '3只/组', '14.5200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('496', 'B01080803', '避雷器', 'HY1.5W-0.5/2.6', '只', '14.9600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('497', 'B01080804', '避雷器', 'PU65*4', '套', '2350.4300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('498', 'B01080805', '避雷器', 'PU 40×4', '套', '1222.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('499', 'B01080806', '浪涌装置', 'ZU30-20/40KA2P', '只', '358.9700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('500', 'B01080807', '浪涌保护器', 'TIU1-65/440 4P', '台', '1997.8600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('501', 'B01080808', '浪涌保护器', 'ZU1-C/4P 20-40KA/420V', '台', '871.7900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('502', 'B01080809', '浪涌保护器', 'TIU1-40/440/4P', '只', '1198.7200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('503', 'B01080810', '浪涌保护器', 'ZU1C-30/60/4P', '只', '1307.6900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('504', 'B01120101', '行线槽', '3×30', 'm', '2.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('505', 'B01120102', '行线槽', '5×45', '条', '11.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('506', 'B01120103', '行线槽', '4×40', '条', '3.2900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('507', 'B01120201', '无边绕线管', 'φ8-6', 'Kg', '15.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('508', 'B01120202', '有边绕线管', 'φ8-6', 'Kg', '29.4000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('509', 'B01120301', '记号管', 'Φ2.5-Φ4', '卷', '18.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('510', 'B01120302', '记号管', 'Φ6', '卷', '18.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('511', 'B01120303', '记号管', 'LP-1.50', '圈', '32.7600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('512', 'B01120304', '记号管', 'LP-4', '卷', '168.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('513', 'B01120305', '记号管', 'LP-6', '卷', '34.1900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('514', 'B01120306', '记号管', 'LP-1.0mm2', '卷', '34.1900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('515', 'B01120307', '记号管', '10mm2', '圈', '18.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('516', 'B01120308', '记号管', 'LP-15', '卷', '0.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('517', 'B01120401', '线扣', 'CZ-100', '袋', '4.3100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('518', 'B01120402', '线扣', 'CZ-200', '袋', '24.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('519', 'B01120403', '线卡', 'Φ6', '袋', '2.5400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('520', 'B01120404', '钢精轧头', '?', '包', '1.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('521', 'B01120405', '管卡', '4分', '只', '0.0900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('522', 'B01120406', '线卡', '10#', '盒', '2.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('523', 'B01120407', '线卡', '16#', '包', '6.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('524', 'B01120408', '线卡', '8#', '盒', '1.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('525', 'B01120501', '元线夹', '大', '只', '0.1600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('526', 'B01120502', '元线夹', '中', '只', '0.3500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('527', 'B01120503', '元线夹', '小', '只', '0.1400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('528', 'B01120601', '橡皮圈', 'Φ25', '只', '0.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('529', 'B01120602', '橡皮圈', 'Φ28', '只', '0.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('530', 'B01120603', '橡皮圈', 'Φ30', '只', '0.3300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('531', 'B01120604', '橡皮圈', 'Φ60', '只', '0.2500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('532', 'B01120605', '橡皮圈', 'φ70', '只', '0.9200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('533', 'B01120606', '橡皮圈', 'ф80', '只', '0.4300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('534', 'B01120607', '橡皮圈', 'φ75', '只', '0.6200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('535', 'B01120608', '橡胶O型圈', '12×30', '只', '0.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('536', 'B01120609', '橡皮圈', 'φ90', '只', '0.3300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('537', 'B01120701', '橡皮嵌条', '？', '米', '7.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('538', 'B01120702', '矩形橡皮嵌条', '155×275', '只', '5.1900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('539', 'B01120703', '密封条', '?', 'm', '3.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('540', 'B01120704', '矩形橡皮嵌条', '130×200', '只', '5.4500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('541', 'B01120801', '标签框', '15×50', '只', '0.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('542', 'B01120802', '标签框', '30×80', '只', '0.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('543', 'B01120803', '标鉴框', '18×60', '只', '0.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('544', 'B01120901', '塑料框', '90×90', '只', '0.6100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('545', 'B01120902', '塑料框', '100×200', '只', '2.4000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('546', 'B01120903', '塑料框', '230×130', '只', '2.4000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('547', 'B01120904', '塑料框', '80×90', '只', '1.3600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('548', 'B01120905', '透明塑料框', '80×90', '只', '1.3700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('549', 'B01120906', '动力表箱框', '500×802', '只', '0.8500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('550', 'B01120907', '表框', '360×90', '片', '5.0600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('551', 'B01120908', '表框', '200×90', '片', '3.3100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('552', 'B01130101', '日光灯架', '40W', '只', '47.8600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('553', 'B01130102', '碘钨灯座', '1000W', '只', '6.8400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('554', 'B01130103', '日光灯座', '30W', '付', '25.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('555', 'B01130104', '仿比利时灯罩', '？', '只', '158.9800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('556', 'B01130105', '桶灯灯罩', '？', '只', '12.8200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('557', 'B01130106', '圆球罩', 'φ350', '只', '41.0600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('558', 'B01130107', '琵琶式路灯罩', '110W', '只', '80.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('559', 'B01130108', '中波纹罩', '？', '只', '53.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('560', 'B01130109', '白塔松灯罩', '？', '只', '53.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('561', 'B01130110', '元球玻璃罩', 'φ350　', '只', '35.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('562', 'B01130111', '蘑菇罩', '?', '只', '39.3200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('563', 'B01130112', '高钠路灯罩', 'JTY37-110', '只', '117.6500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('564', 'B01130113', '高钠路灯罩', 'JTY37-250', '只', '90.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('565', 'B01130114', '高钠路灯罩', 'JTY40-250', '只', '280.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('566', 'B01130115', '高钠路灯罩', 'AUS-790', '只', '106.8400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('567', 'B01130116', '瓷灯座', 'E27', '只', '2.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('568', 'B01130117', '草坪灯头子', '？', '只', '58.1100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('569', 'B01130201', '日光灯管', '40W', '支', '5.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('570', 'B01130202', '碘钨灯管', '500W', '支', '5.4000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('571', 'B01130203', '碘钨灯管', '1000W', '支', '0.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('572', 'B01130204', '罗口灯泡', '100W', '只', '1.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('573', 'B01130205', '罗口灯泡', 'JZ36V-100W', '只', '1.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('574', 'B01130206', '插口灯泡', '60-100W', '只', '0.8500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('575', 'B01130207', '自镇流高压汞泡', '125W', '只', '14.8600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('576', 'B01130208', '高压汞灯', '125W', '只', '14.2300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('577', 'B01130209', '高压汞灯', '250W', '只', '58.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('578', 'B01130210', '高压钠灯', '150W', '只', '45.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('579', 'B01130301', '日光灯镇流器', '40W', '只', '6.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('580', 'B01130302', '高压汞灯镇流器', '125W', '只', '58.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('581', 'B01130303', '高压汞灯镇流器', '250W', '只', '58.1100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('582', 'B01130304', '高压钠灯镇流器', '150W', '只', '87.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('583', 'B01130305', '高压钠灯镇流器', '250W', '只', '83.7600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('584', 'B01130306', '触发器', 'CD-2', '只', '13.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('585', 'B01130307', '电子式镇流器', '一拖二', '只', '28.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('586', 'B01130308', '电子式镇流器', '一拖一', '只', '15.3900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('587', 'B01130309', '钠镇流器触发器', '250W', '只', '504.2700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('588', 'B01130310', '高压钠灯镇流器', '400W', '只', '143.5900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('589', 'B01130311', '钠镇流器触发器', '400W', '只', '15.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('590', 'B01130401', '单相插座', '10A', '只', '3.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('591', 'B01130402', '三相插座', 'AC30-25A', '只', '10.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('592', 'B01130403', '三相四线插座', '15A', '只', '3.6800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('593', 'B01130404', '三相四线插座', '30A', '只', '8.5500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('594', 'B01130405', '三相四线插头', '15A', '只', '3.7000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('595', 'B01130406', '三相四线插头', '30A', '只', '5.8100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('596', 'B01130407', '单相插头', '10A', '只', '0.6400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('597', 'B01130408', '三相插头', '16A', '只', '2.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('598', 'B01130409', '普通多用插座', '?', '只', '6.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('599', 'B01130410', '多功能插座', '?', '只', '7.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('600', 'B01130411', '空调插座', '16A', '套', '7.6900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('601', 'B01130501', '启辉器', '?', '只', '1.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('602', 'B01130502', '吊扇', '？', '把', '120.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('603', 'B01130503', '小电珠', '3.8V', '只', '0.4000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('604', 'B01130504', '拉线开关', '250V', '只', '1.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('605', 'B01130505', '灯头', '250V', '只', '-5.5400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('606', 'B01130506', '吊扇吊钩', '？', '只', '1.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('607', 'B01130507', '自镇流汞灯头', '250A', '只', '5.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('608', 'B01130508', '吊扇电容器', '？', '只', '2.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('609', 'B01130509', '吊扇调速器', '？', '只', '10.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('610', 'B01130510', '线令', '？', '只', '0.4300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('611', 'B01160101', '塑壳断路器', 'RMM1-63H/3200 63A', '只', '224.3600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('612', 'B01160102', '塑壳断路器', 'RMM1-100H/4300 63A', '只', '416.6700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('613', 'B01160103', '塑壳断路器', 'RMM1-100H/3200 100A', '只', '288.4600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('614', 'B01160104', '塑壳断路器', 'RMM1-100H/4300 100A', '只', '416.6700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('615', 'B01160105', '塑壳断路器', 'RMM1-250H/4300 100A', '只', '673.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('616', 'B01160106', '塑壳断路器', 'RMM1-250H/4300 125A', '只', '673.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('617', 'B01160107', '塑壳断路器', 'RMM1-250H/4300 160A', '只', '673.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('618', 'B01160108', '塑壳断路器', 'RMM1-250H/4310 160A(AC220V)', '只', '741.4500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('619', 'B01160109', '塑壳断路器', 'RMM1-250H/4300 250A', '只', '673.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('620', 'B01160110', '塑壳断路器', 'RMM1-400H/4310 250A(AC220V)', '只', '1489.3200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('621', 'B01160201', '断路器', 'CKB60-C.1P.25A', '只', '6.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('622', 'B01160202', '断路器', 'CKB60-C.1P.40A', '只', '6.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('623', 'B01160203', '断路器', 'CKB60-C.3P.16A', '只', '22.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('624', 'B01160204', '断路器', 'CKB60-C.3P.20A', '只', '22.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('625', 'B01160205', '断路器', 'CKB60-C.3P.32A', '只', '22.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('626', 'B01160206', '断路器', 'CKB60-C.3P.40A', '只', '22.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('627', 'B01160207', '断路器', 'CKB60-100.C.3P.80A', '只', '68.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('628', 'B01160208', '断路器', 'CKM33-100S/3300 80A', '只', '276.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('629', 'B01160209', '断路器', 'CKM33-160S/4300 160A', '只', '430.7700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('630', 'B01160210', '断路器', 'CKM33-400H/3340 400A', '只', '802.0500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('631', 'B01160211', '断路器', 'CKM33-630H/3340 630A', '只', '1.4400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('632', 'B01160212', '断路器', 'CKM33-100S/3300 40A', '只', '276.9200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('633', 'B01160301', '断路器', 'DZ20J-400/3300 250A', '只', '338.4600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('634', 'B01160302', '断路器', 'DZ20J-630/3300 630A', '只', '385.4700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('635', 'B01160303', '断路器', 'YSM1-225S/3300 225A', '只', '384.6200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('636', 'B01160304', '断路器', 'YSM1-225S/3300 100A', '只', '384.6200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('637', 'B01160305', '断路器', 'CKM33-400H/3310 400A', '只', '880.3400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('638', 'B01160306', '断路器', 'ZM30E-225/3300 100A', '只', '191.4500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('639', 'B01160307', '断路器', 'ZM30E-225/3300 150A', '只', '38.2900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('640', 'B01160308', '断路器', 'ZM30E-630/3300 630A', '只', '442.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('641', 'B01160309', '断路器', 'ZM30E-630/3300 500A', '只', '442.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('642', 'B01160310', '断路器', 'YSM1-400S/3310 400A', '只', '752.1400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('643', 'B01160311', '断路器', 'YSM1-225S/3310 100A', '只', '452.9900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('644', 'B01160312', '断路器', 'YSM3-250S/3300 225A', '只', '384.6200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('645', 'B01160401', '断路器', 'NLM1-630M/3300 630A', '只', '949.5700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('646', 'B01160402', '断路器', 'NLM1-400M/3300 400A', '只', '694.8800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('647', 'B01160403', '断路器', 'NLM1-225L/3300 160A', '只', '321.3700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('648', 'B01160404', '断路器', 'NLM1-630L/3300 500A', '只', '272.3700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('649', 'B01160405', '断路器', 'NLM1-630H/3300 630A', '只', '1081.5400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('650', 'B01160406', '断路器', 'NLM1-630M/3340 630A', '只', '2517.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('651', 'B01160407', '断路器', 'NLM1-225C/3300 200A', '只', '258.2100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('652', 'B01160408', '断路器', 'NLM1-225C/3300 160A', '只', '64.5500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('653', 'B01160409', '断路器', 'NLM1-400C/3300 315A', '只', '0.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('654', 'B01160410', '断路器', 'NLM1-400C/3300 250A', '只', '414.1100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('655', 'B01160411', '断路器', 'NLM1-400C/3300 400A', '只', '523.5100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('656', 'B02050102', '热板', '0.80', 'kg', '3.2300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('657', 'B02050103', '热板', '1.00', 'kg', '4.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('658', 'B02050104', '热板', '1.20', 'kg', '3.7600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('659', 'B02050105', '热板', '1.50m/m', 'kg', '4.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('660', 'B02050106', '热板', '2.00m/m', 'kg', '4.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('661', 'B02050107', '热板', '2.50m/m', 'kg', '2.9400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('662', 'B02050108', '热板（钢板网）', '?', 'm2', '10.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('663', 'B02050201', '冷板', '1.20', 'kg', '3.6400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('664', 'B02050202', '冷板', '1.50', 'kg', '4.7700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('665', 'B02050203', '冷板', '1.00', 'kg', '4.0700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('666', 'B02050204', '冷板', '2.5', 'kg', '3.6400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('667', 'B02050205', '冷板', '0.5m/m', 'kg', '4.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('668', 'B02050206', '冷板', '2.0mm', 'kg', '21.8100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('669', 'B02050301', '中板', '-3', 'kg', '3.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('670', 'B02050302', '中板', '-4', 'kg', '4.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('671', 'B02050303', '中板', '-5', 'kg', '2.7000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('672', 'B02050304', '中板', '-6', 'kg', '3.7600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('673', 'B02050305', '中板', '8', 'kg', '4.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('674', 'B02050306', '中板', '10', 'kg', '3.8500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('675', 'B02050307', '中板', '12', 'kg', '3.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('676', 'B02050308', '中板', '14', 'kg', '2.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('677', 'B02050309', '中板', '16', 'kg', '4.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('678', 'B02050310', '中板', '20', 'kg', '4.6000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('679', 'B02050311', '钢板网', '？', 'm2', '10.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('680', 'B02050312', '中板', '-35m/m', 'kg', '4.2700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('681', 'B02050313', '花纹板', '4.0', 'kg', '3.5300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('682', 'B02050401', '铝钣', '2', 'kg', '20.8200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('683', 'B02050501', '不锈钢板', '1.5mm', 'kg', '28.2500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('684', 'B02050502', '不锈钢板', '1mm', 'kg', '20.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('685', 'B02050503', '不锈钢板', '4.00mm', 'kg', '27.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('686', 'B02050504', '不锈钢板', '2m/m', 'kg', '22.4800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('687', 'B02050505', '不锈钢板', '0.5m/m', 'kg', '19.4500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('688', 'B02050506', '不锈钢板', '0.8m/m', 'kg', '23.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('689', 'B02050507', '不锈钢焊角片', '?', '片', '0.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('690', 'B02050508', '不锈钢板', '1.2m/m', 'kg', '6.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('691', 'B02050509', '不锈钢板', '0.7m/m', 'kg', '15.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('692', 'B02050510', '镜面板', '1.00m/m', 'kg', '18.6300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('693', 'B02050511', '镜面板', '0.7m/m', 'kg', '17.7800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('694', 'B02050512', '不锈钢板', '10.00m/m', 'kg', '13.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('695', 'B02050513', '不锈钢板', '1.00m/m', 'kg', '21.6500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('696', 'B02050514', '不锈钢板', '0.6m/m', '张', '423.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('697', 'B02050601', '镀锌板', '0.5', 'kg', '5.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('698', 'B02050602', '镀锌板', '1mm', 'kg', '4.5300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('699', 'B02050603', '镀锌板', '0.8mm', 'kg', '4.7900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('700', 'B02050701', '不锈钢球', 'φ80', 'kg', '3.3300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('701', 'B02050702', '不锈钢球', 'φ90', '只', '6.0900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('702', 'B02050703', '不锈钢球', 'φ32', '只', '0.6800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('703', 'B02050704', '不锈钢座', 'φ25', '只', '2.5600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('704', 'B02050705', '不锈钢座', 'φ76', '只', '1.9700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('705', 'B02050706', '不锈钢球', 'φ114', '只', '6.1700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('706', 'B02050707', '菱形', '230mm', '只', '3.6100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('707', 'B02050708', '不锈钢弯头', 'φ63', '只', '5.4400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('708', 'B02050709', '不锈钢球座', 'φ63', '只', '15.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('709', 'B02050710', '不锈钢盖', 'φ63', '只', '0.6700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('710', 'B02050711', '不锈钢盖', 'φ38', '只', '1.2300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('711', 'B02050712', '不锈钢弯头', 'φ38', '只', '3.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('712', 'B02050801', '薄板', '多规格', 'kg', '2.7200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('713', 'B03010101', '木螺丝', 'M3×16', '只', '0.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('714', 'B03010102', '木螺丝', 'M4×30', '只', '0.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('715', 'B03010103', '木螺丝', 'M4×40', '只', '0.0200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('716', 'B03010104', '木螺丝', 'M4.5×60', '只', '0.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('717', 'B03010105', '胶木螺丝', '?', '只', '0.1800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('718', 'B03010106', '木螺丝', 'M4×50', '只', '0.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('719', 'B03010107', '木螺丝', 'M5×50', '只', '0.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('720', 'B03010108', '木螺丝', 'M5×40', '只', '0.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('721', 'B03010109', '木螺丝', 'M3.5×25', '', '0.0200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('722', 'B03010201', '铜鼻螺丝', '?', '只', '27.3500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('723', 'B03010202', '自攻螺丝', 'M4×20', '只', '0.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('724', 'B03010203', '热镀锌元头脚钉', 'M16×160', '只', '2.5200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('725', 'B03010204', '自攻螺丝', 'M4×30', '百只', '13.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('726', 'B03010205', '三角螺丝', 'M8', '只', '1.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('727', 'B03010206', '三角螺丝', 'M10×25', '只', '2.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('728', 'B03010207', '圆头自攻螺丝', 'M3×6', '千只', '2.2400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('729', 'B03010208', '圆头自攻螺丝', 'M3×10', '千只', '8.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('730', 'B03010209', '圆头自攻螺丝', 'M4×14', '只', '0.0600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('731', 'B03010210', '沉头螺丝', 'M2×5', '千只', '15.7100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('732', 'B03010301', '平机螺丝', 'M4×20', '只', '0.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('733', 'B03010302', '平机螺丝', 'M6×25', '只', '0.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('734', 'B03010303', '平机螺丝', 'M10×35', '只', '0.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('735', 'B03010304', '平机螺丝', 'M5×15', '只', '0.0100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('736', 'B03010305', '平机螺丝', 'M8×10', '只', '0.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('737', 'B03010306', '平机螺丝', 'M8×25', '只', '0.0700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('738', 'B03010307', '平机螺丝', 'M5×30', '只', '0.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('739', 'B03010308', '平机螺丝', 'M8×20', '百只', '0.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('740', 'B03010309', '平机螺丝', '(M12×60)', '只', '0.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('741', 'B03010310', '平机罗丝', 'M12×50', '只', '0.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('742', 'B03010311', '平机罗丝', 'M5*10', '百只', '1.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('743', 'B03010312', '平机罗丝', '4*10', '千只', '8.4400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('744', 'B03010313', '平机罗丝', 'M4*6', '千只', '10.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('745', 'B03010314', '平机罗丝', 'M4*8', '千只', '7.4500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('746', 'B03010315', '平机罗丝', '12×40', '只', '0.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('747', 'B03010401', '膨胀螺丝', 'M10×80', '只', '0.9000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('748', 'B03010402', '膨胀螺丝', 'M10×100', '只', '0.5100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('749', 'B03010403', '膨胀螺丝', 'M16×150', '只', '2.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('750', 'B03010404', '膨胀螺丝', 'M12×70', '只', '0.9400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('751', 'B03010405', '塑料膨胀螺丝', 'φ6', '只', '0.0900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('752', 'B03010406', '塑料膨胀螺丝', 'φ8', '只', '0.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('753', 'B03010407', '膨胀螺丝', 'φ8', '只', '0.6900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('754', 'B03010408', '膨胀螺丝', 'φ6×60', '只', '0.4000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('755', 'B03010409', '膨胀螺丝', 'M12×150', '只', '1.6000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('756', 'B03010410', '膨胀螺丝', 'M8×60', '只', '0.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('757', 'B03010501', '抽芯铝铆钉', 'M4×13', '只', '0.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('758', 'B03010502', '抽芯铝铆钉', 'Φ2.2×0.1', '只', '0.0300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('759', 'B03010503', '抽芯铝铆钉', '3*5', 'kg', '47.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('760', 'B03010504', '抽芯铝铆钉', '5*13', '千只', '40.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('761', 'B03010505', '抽芯铝铆钉', '4*16', '盒', '18.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('762', 'B03010506', '抽芯铝铆钉', '5×20', '盒', '28.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('763', 'B03010601', '六角螺丝', 'M5×25', '百只', '3.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('764', 'B03010602', '六角螺丝', 'M5×35', '百只', '5.0000', '148', '740.0000', null);
INSERT INTO `mainstock` VALUES ('765', 'B03010603', '六角螺丝', 'M5×40', '百只', '5.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('766', 'B03010604', '六角螺丝', 'M6×20', '百只', '3.0800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('767', 'B03010605', '六角螺丝', 'M6×25', '百只', '9.0600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('768', 'B03010606', '六角螺丝', 'M6×30', '百只', '5.0500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('769', 'B03010607', '六角螺丝', 'M6×35', '百只', '5.0500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('770', 'B03010608', '六角螺丝', 'M8×16', '百只', '7.9700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('771', 'B03010609', '六角螺丝', 'M8×20', '百只', '8.2500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('772', 'B03010610', '六角螺丝', 'M8×25', '百只', '8.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('773', 'B03010611', '六角螺丝', 'M8×30', '百只', '9.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('774', 'B03010701', '热镀锌螺丝', 'M16×300', '只', '3.8100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('775', 'B03010702', '热镀锌螺丝', 'M12×35', '只', '0.0900', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('776', 'B03010703', '热镀锌螺丝', 'M12×60', '只', '0.8300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('777', 'B03010704', '热镀锌螺丝', 'M14×65', '只', '0.8800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('778', 'B03010705', '热镀锌螺丝', 'M16×220', '只', '2.6000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('779', 'B03010706', '热镀锌螺丝', 'M16×35', '只', '1.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('780', 'B03010707', '热镀锌螺丝', 'M16×45', '只', '0.9200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('781', 'B03010708', '热镀锌螺丝', 'M16×55', '只', '1.1100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('782', 'B03010709', '热镀锌螺丝', 'M16×80', '只', '1.5800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('783', 'B03010710', '热镀锌螺丝', 'M16×90', '只', '1.3400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('784', 'B03010711', '热镀锌螺丝', 'M16×400', '只', '2.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('785', 'B03010712', '热镀锌螺丝', 'M18×40', '只', '1.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('786', 'B03010713', '热镀锌螺丝', 'M18×90', '只', '1.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('787', 'B03010901', '内六角螺丝', 'M6×25', '只', '0.2200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('788', 'B03010902', '内六角螺丝', 'M8×25', '只', '0.1100', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('789', 'B03010903', '内六角螺丝', 'M10×80', '只', '0.8700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('790', 'B03010904', '内六角螺丝', 'M12×30', '只', '0.6000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('791', 'B03010905', '内六角螺丝', 'M12×55', '只', '0.8400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('792', 'B03010906', '内六角螺丝', 'M8×16', '只', '0.1200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('793', 'B03010907', '内六角螺丝', 'M10×40', '只', '0.2400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('794', 'B03010908', '内六角螺丝', 'M8×35', '只', '0.1600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('795', 'B03010909', '内六角螺丝', 'M10×25', '只', '0.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('796', 'B03010910', '内六角螺丝', 'M12×80', '只', '1.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('797', 'B03010911', '内六角螺丝', 'M16×40', '只', '1.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('798', 'B03010912', '内六角螺丝', 'M8×50', '只', '0.3000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('799', 'B03010913', '内六角螺丝', 'M10×50', '只', '0.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('800', 'B03010914', '内六角螺丝', 'M12×50', '只', '0.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('801', 'B03010915', '不锈钢内六角平机螺丝', 'M10×16', '只', '1.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('802', 'B03010916', '内六角螺丝', 'M5×20', '只', '0.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('803', 'B03010917', '内六角螺丝', 'M12×40', '只', '0.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('804', 'B03010918', '内六角螺丝', 'M10×30', '只', '0.4200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('805', 'B03010919', '内六角螺丝', 'M12×60', '只', '0.7500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('806', 'B03010920', '内六角螺丝', 'M6×45', '只', '0.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('807', 'B03010921', '内六角罗丝', 'M12×25', '只', '0.4300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('808', 'B03010922', '内六角罗丝', 'M8×20', '只', '0.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('809', 'B03010923', '内六角罗丝', 'M8*60', '只', '0.3800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('810', 'B03010924', '紧定不锈钢内六角罗丝', '8*10', '百只', '35.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('811', 'B03020101', '电焊条', 'φ1.4', 'kg', '5.8000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('812', 'B03020102', '电焊条', 'φ2.5', 'kg', '4.2700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('813', 'B03020103', '电焊条', 'φ3.2', 'kg', '3.9300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('814', 'B03020104', '电焊条', 'φ4', 'kg', '4.2700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('815', 'B03020105', '铜焊条', 'φ3', 'kg', '28.2600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('816', 'B03020106', '不锈钢焊丝', '2.5', 'kg', '23.3300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('817', 'B03020107', '镀锌铁丝', '14#', 'kg', '4.5000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('818', 'B03020108', '镀锌铁丝', '8#', 'kg', '3.4200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('819', 'B03020109', '不锈钢焊条', 'AB2-2.5', 'kg', '46.1500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('820', 'B03020110', '钨极棒', '?', '支', '5.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('821', 'B03020111', '铜焊粉', '?', '瓶', '13.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('822', 'B03020112', '不锈钢焊丝', '1.00m/m', 'kg', '16.9200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('823', 'B03020201', '电焊钳', '500A', '把', '12.8200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('824', 'B03020202', '电焊钳', '300A', '把', '11.5400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('825', 'B03020203', '焊枪', 'H01-6', '把', '34.2000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('826', 'B03020204', '等离子割嘴瓷套', '?', '只', '4.1300', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('827', 'B03020205', '焊嘴', 'H01-6  2号', '只', '4.4700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('828', 'B03020206', '割嘴', 'G01-30', '只', '3.0400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('829', 'B03020207', '割枪', 'G01-30', '把', '42.7400', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('830', 'B03020208', '电焊手套', '?', '双', '4.9700', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('831', 'B03020209', '电焊护目镜片', '?', '片', '0.8500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('832', 'B03020210', '电烙铁', '100W', '只', '13.8800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('833', 'B03020211', '氩弧焊枪', '160', '把', '90.6000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('834', 'B03020212', '电焊护脚', '？', '付', '6.6600', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('835', 'B03020213', '乙炔发生器', 'YTP-1M3', '只', '717.9500', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('836', 'B03020214', '等离子切割机割嘴', '/', '付', '0.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('837', 'B03020215', '塑料焊枪', '？', '把', '64.1000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('838', 'B03020216', '点焊机头子', '16KW', '只', '35.0000', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('839', 'B03020217', '氩弧焊枪瓷嘴', '？', '只', '1.2800', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('840', 'B03020218', '枪嘴、铜夹、枪尾', '?', '套', '3.9200', '0', '0.0000', null);
INSERT INTO `mainstock` VALUES ('841', 'B03020220', '氩弧焊枪开关', 'kw4-02', '只', '3.4200', '0', '0.0000', null);

-- ----------------------------
-- Table structure for materials
-- ----------------------------
DROP TABLE IF EXISTS `materials`;
CREATE TABLE `materials` (
  `Item_Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `norms` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Item_No` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Remark` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of materials
-- ----------------------------
INSERT INTO `materials` VALUES ('材料', '', 'B', '', null);
INSERT INTO `materials` VALUES ('电器材料', '', 'B01', '', null);
INSERT INTO `materials` VALUES ('自动开关', '', 'B0101', '', null);
INSERT INTO `materials` VALUES ('高分断断路器(一)', '', 'B010101', '', null);
INSERT INTO `materials` VALUES ('高分断断路器', 'DZ47-6A/1P', 'B01010101', '只', null);
INSERT INTO `materials` VALUES ('高分断断路器', 'DZ47-10A/1P', 'B01010102', '只', null);
INSERT INTO `materials` VALUES ('高分断断路器', 'DZ47-16A/1P', 'B01010103', '只', null);
INSERT INTO `materials` VALUES ('高分断断路器', 'DZ47-20A/1P', 'B01010104', '只', null);
INSERT INTO `materials` VALUES ('高分断断路器', 'DZ47-25A/1P', 'B01010105', '只', null);
INSERT INTO `materials` VALUES ('高分断断路器', 'DZ47-32A/1P', 'B01010106', '只', null);
INSERT INTO `materials` VALUES ('高分断断路器', 'DZ47-50A/1P', 'B01010107', '只', null);
INSERT INTO `materials` VALUES ('高分断断路器', 'DZ47-60A/1P', 'B01010108', '只', null);
INSERT INTO `materials` VALUES ('高分断断路器', 'DZ47-80A/1P', 'B01010109', '只', null);
INSERT INTO `materials` VALUES ('高分断断路器', 'DZ47-100A/1P', 'B01010110', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器(一)', '', 'B010102', '', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'CM1-100/330(100A)', 'B01010201', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'DZ5-20/330', 'B01010202', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TM30-100/3300', 'B01010203', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TM30-200/3300(200A)', 'B01010204', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TD-100/3300', 'B01010205', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TD-225/3300(200A)', 'B01010206', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'DZ10-100/330(30A)', 'B01010207', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'DZ10-100/330(50A)', 'B01010208', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'DZ10-100/330(80A)', 'B01010209', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'DZ10-100/330(100A)', 'B01010210', '只', null);
INSERT INTO `materials` VALUES ('框架式断路器', '', 'B010103', '', null);
INSERT INTO `materials` VALUES ('抽屉式断路器', 'HSW1-2000/3（800A）', 'B01010301', '只', null);
INSERT INTO `materials` VALUES ('抽屉式断路器', 'HSW1-3200/3（2500A）', 'B01010302', '只', null);
INSERT INTO `materials` VALUES ('智能断路器', 'ZW1-2000/3（1600A）', 'B01010303', '只', null);
INSERT INTO `materials` VALUES ('框架式断路器', 'DW15-400/3', 'B01010304', '只', null);
INSERT INTO `materials` VALUES ('框架式断路器', 'DW15-630/3', 'B01010305', '只', null);
INSERT INTO `materials` VALUES ('框架式断路器', 'DW15-1000/3', 'B01010306', '只', null);
INSERT INTO `materials` VALUES ('框架式断路器', 'DW15-1600/3', 'B01010307', '只', null);
INSERT INTO `materials` VALUES ('框架式断路器', 'DW15-2500/3', 'B01010308', '只', null);
INSERT INTO `materials` VALUES ('万能断路器', 'YSA2-630A/3', 'B01010309', '只', null);
INSERT INTO `materials` VALUES ('智能断路器', 'YSA3-2000/2000', 'B01010310', '台', null);
INSERT INTO `materials` VALUES ('万能式断路器', 'YSA2-2000A/3', 'B01010311', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', '', 'B010104', '', null);
INSERT INTO `materials` VALUES ('漏电开关', 'DZ47L-16A/2P', 'B01010401', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', 'DZ10L-150/430', 'B01010402', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', 'DZ10L-250/430(200A)', 'B01010403', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', 'DZ10L-250/430(250A)', 'B01010404', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', 'DZ10L-400/430(400A)', 'B01010405', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', 'DZ25L-160/430', 'B01010406', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', 'DZ25L-200/4330(200A)', 'B01010407', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', 'DZ15L-40/2901(20A)', 'B01010408', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', 'DZ15L-100/2901(63A)', 'B01010409', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', 'DZ15L-100/2901(100A)', 'B01010410', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器(二)', '', 'B010105', '', null);
INSERT INTO `materials` VALUES ('断路器', 'SM30S-225/330(200A)', 'B01010500', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'SM30S-400/330(400A)', 'B01010501', '只', null);
INSERT INTO `materials` VALUES ('空气开关', 'SM30S-630/3340(630A)', 'B01010502', '只', null);
INSERT INTO `materials` VALUES ('空气开关', 'F7-25/2', 'B01010503', '只', null);
INSERT INTO `materials` VALUES ('空气开关', 'L7-63/4', 'B01010504', '只', null);
INSERT INTO `materials` VALUES ('空气开关', 'DZ20G-100/3300(40A)', 'B01010505', '只', null);
INSERT INTO `materials` VALUES ('空气开关', 'DZ20G-100/3300(32A)', 'B01010506', '只', null);
INSERT INTO `materials` VALUES ('空气开关', 'DZ20G-225/3300(180A)', 'B01010507', '只', null);
INSERT INTO `materials` VALUES ('空气开关', 'SM30S-225/3300(125A)', 'B01010508', '只', null);
INSERT INTO `materials` VALUES ('空气开关', 'SM30H-225/3300(100A)', 'B01010509', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'SM30H-225/3300(200A)', 'B01010510', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'SM30H-225/3300(140A)', 'B01010512', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器(三）', '', 'B010106', '', null);
INSERT INTO `materials` VALUES ('断路器', 'HSM8Ⅱ-63/3 15A', 'B01010601', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'HSM1-250M/3300 200A', 'B01010602', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'HSM1-250M/3300 250A', 'B01010603', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'HSM1-250M/3300 160A', 'B01010604', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'HSM1-400M/3300 315A', 'B01010605', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'HSM8Ⅱ-63/1 10A', 'B01010606', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'HSM8Ⅱ-63/1 16A', 'B01010607', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'HSM8Ⅱ-63/1 63A', 'B01010608', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'HSM8LⅡ-63/2 10A', 'B01010609', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'HSM8LⅡ-63/2 63A', 'B01010610', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器（二）', '', 'B010107', '', null);
INSERT INTO `materials` VALUES ('漏电开关', 'C65N-32A/4P+VIGIC65-32A/4PELM', 'B01010701', '只', null);
INSERT INTO `materials` VALUES ('漏电开关', 'ZB30L-63/4(63A)', 'B01010702', '只', null);
INSERT INTO `materials` VALUES ('漏电开关', 'C65N/3P+VIGIC65N3P63AELM', 'B01010703', '只', null);
INSERT INTO `materials` VALUES ('漏电开关', 'SM30L-100/4300', 'B01010704', '只', null);
INSERT INTO `materials` VALUES ('漏电开关', 'SM30L-225/4300  200A', 'B01010705', '只', null);
INSERT INTO `materials` VALUES ('漏电开关', 'SM30L-225/4300  140A', 'B01010706', '只', null);
INSERT INTO `materials` VALUES ('漏电开关', 'NM1LE-100/3+N  100A', 'B01010707', '只', null);
INSERT INTO `materials` VALUES ('漏电开关', 'NM1LE-225/3+N  160A', 'B01010708', '只', null);
INSERT INTO `materials` VALUES ('漏电开关', 'NC100LS-4P 63/D+VIGINC100ELM', 'B01010709', '只', null);
INSERT INTO `materials` VALUES ('漏电开关', 'SM30L-225S/4300 160A', 'B01010710', '只', null);
INSERT INTO `materials` VALUES ('漏电断路器', 'DZ158L-100A/4P', 'B01010711', '只', null);
INSERT INTO `materials` VALUES ('漏电开关', 'SM30L-100S/4300  63A', 'B01010712', '只', null);
INSERT INTO `materials` VALUES ('高分断断路器（二）', '', 'B010108', '', null);
INSERT INTO `materials` VALUES ('断路器', 'Hum18-63/4D 40A', 'B01010801', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'Hum18-63/3D 32A', 'B01010802', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'Hum18-63/1C 16A', 'B01010803', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'Hum18-250S/4B 100A', 'B01010804', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'ZM30E-100/3300 100A', 'B01010805', '台', null);
INSERT INTO `materials` VALUES ('断路器', 'Hum18-63/4C 20A', 'B01010806', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'Hum18-63/1P 16A', 'B01010807', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'Hum18-63/4P 25A', 'B01010808', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'Hum18-63/4P 40A', 'B01010809', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKM33-225S/3300 125A', 'B01010810', '台', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'YSM1-400S/3300 250A', 'B01010811', '台', null);
INSERT INTO `materials` VALUES ('断路器', 'Hum18-250/4B 125A', 'B01010812', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器TI(四)', '', 'B010109', '', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-H-250/3300(250A)', 'B01010901', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-H-250/3300(200A)', 'B01010902', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-S-160/3300(160A)', 'B01010903', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-S-125/3300(100A)', 'B01010904', '只', null);
INSERT INTO `materials` VALUES ('智能断路器', 'TIW1-2000/1000/3', 'B01010905', '只', null);
INSERT INTO `materials` VALUES ('智能断路器', 'TIW1-2000/1000/3 (5S)', 'B01010906', '只', null);
INSERT INTO `materials` VALUES ('智能断路器', 'TIW1-2000/3 1250A', 'B01010907', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-S-160/3300 100A', 'B01010908', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-S-400/3300 400A', 'B01010909', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-S-250/3300 200A', 'B01010910', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-S-400/3300 315A', 'B01010911', '只', null);
INSERT INTO `materials` VALUES ('小型断路器', 'TIB1-63C40', 'B01010912', '只', null);
INSERT INTO `materials` VALUES ('智能断路器', 'TIW1-2000/3（630A）', 'B01010913', '台', null);
INSERT INTO `materials` VALUES ('智能断路器', 'TIW1-3200/3（3200A）', 'B01010914', '台', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-H-400/3300 400A', 'B01010915', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-H-400/3300 315A', 'B01010916', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'TIM1-S-125/3300 125A', 'B01010917', '只', null);
INSERT INTO `materials` VALUES ('刀开关及转换开关', '', 'B0102', '', null);
INSERT INTO `materials` VALUES ('单投刀开关', '', 'B010201', '', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD11B-200/38', 'B01020101', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD11B-400/38', 'B01020102', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD11B-600/38', 'B01020103', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD12B-400/31', 'B01020104', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD12B-600/31', 'B01020105', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD13B-200/31', 'B01020106', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD13B-400/31', 'B01020107', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD13B-600/31', 'B01020108', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD13B-1000/31', 'B01020109', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD13B-1500/31', 'B01020110', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD13BX-100/31', 'B01020111', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD13BX-200/31', 'B01020112', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD13BX-400/31', 'B01020113', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HD13BX-600/31', 'B01020114', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', '', 'B010202', '', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS11B-200/38', 'B01020201', '只', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS11B-400/48', 'B01020202', '只', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS12B-200/41', 'B01020203', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-200/31', 'B01020204', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-400/31', 'B01020205', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-600/31', 'B01020206', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-200/41', 'B01020207', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-400/41', 'B01020208', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-400/71', 'B01020209', '把', null);
INSERT INTO `materials` VALUES ('旋转式双投刀开关', 'HS13BX-400/31', 'B01020210', '把', null);
INSERT INTO `materials` VALUES ('旋转式双投刀开关', 'HS13BX-600/41', 'B01020211', '把', null);
INSERT INTO `materials` VALUES ('旋转式双投刀开关', 'HS13BX-1000/41', 'B01020212', '把', null);
INSERT INTO `materials` VALUES ('旋转式双投刀开关', 'HS13BX-600/31', 'B01020213', '把', null);
INSERT INTO `materials` VALUES ('旋转式双投刀开关', 'HS13BX-1000/31', 'B01020214', '把', null);
INSERT INTO `materials` VALUES ('旋转式双投刀开关', 'HS13BX-1500/31', 'B01020215', '把', null);
INSERT INTO `materials` VALUES ('旋转式双投刀开关', 'HS13BX-400/41', 'B01020216', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-600/41', 'B01020217', '只', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS11-200/49', 'B01020218', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-1000/41', 'B01020219', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-200/71', 'B01020220', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13BX-400/71', 'B01020221', '台', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13BX-1500/41', 'B01020222', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-100/41', 'B01020223', '把', null);
INSERT INTO `materials` VALUES ('双投开关', 'HK-100/3', 'B01020224', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS11B-400/49', 'B01020225', '把', null);
INSERT INTO `materials` VALUES ('双投开关', 'HK2-60/2', 'B01020226', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS11-200/48', 'B01020227', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS11-60/48', 'B01020228', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13B-1500/41', 'B01020229', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HS11-600/48', 'B01020230', '只', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS11B-630/49', 'B01020231', '把', null);
INSERT INTO `materials` VALUES ('旋转式双投刀开关', 'HS13BX-200/41', 'B01020232', '把', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13BX-1500/40', 'B01020233', '只', null);
INSERT INTO `materials` VALUES ('双投刀开关', 'HS13BX-2500/40', 'B01020234', '把', null);
INSERT INTO `materials` VALUES ('熔断式刀开关', '', 'B010203', '', null);
INSERT INTO `materials` VALUES ('熔断式刀开关', 'HR3-200/32', 'B01020301', '把', null);
INSERT INTO `materials` VALUES ('熔断式刀开关', 'HR3-400/32', 'B01020302', '把', null);
INSERT INTO `materials` VALUES ('熔断式刀开关', 'HR3-600/32', 'B01020303', '把', null);
INSERT INTO `materials` VALUES ('熔断式刀开关', 'HR3-200/31', 'B01020304', '把', null);
INSERT INTO `materials` VALUES ('熔断式刀开关', 'HR3-200/34', 'B01020305', '把', null);
INSERT INTO `materials` VALUES ('熔断式刀开关', 'HR3-400/34', 'B01020306', '把', null);
INSERT INTO `materials` VALUES ('熔断式刀开关', 'HR3-600/34', 'B01020307', '把', null);
INSERT INTO `materials` VALUES ('隔离开关', 'HR5-630/30', 'B01020308', '把', null);
INSERT INTO `materials` VALUES ('隔离开关', 'HR5-600/30', 'B01020309', '把', null);
INSERT INTO `materials` VALUES ('隔离开关', 'HR5-500/30', 'B01020310', '把', null);
INSERT INTO `materials` VALUES ('隔离开关', 'HR5-400/30', 'B01020311', '把', null);
INSERT INTO `materials` VALUES ('隔离开关', 'HR5-200/30', 'B01020312', '把', null);
INSERT INTO `materials` VALUES ('隔离开关', 'HR5-100/30', 'B01020313', '把', null);
INSERT INTO `materials` VALUES ('熔断器式隔离器', 'HG30-32/10A', 'B01020314', '只', null);
INSERT INTO `materials` VALUES ('熔断器式隔离器', 'HG30-32/16A', 'B01020315', '只', null);
INSERT INTO `materials` VALUES ('熔断器式隔离器', 'HG30-32/32A', 'B01020316', '只', null);
INSERT INTO `materials` VALUES ('熔断器式隔离器', 'HG30-32/20A', 'B01020317', '只', null);
INSERT INTO `materials` VALUES ('熔断器式隔离器', 'HG30-32/1P', 'B01020318', '只', null);
INSERT INTO `materials` VALUES ('开关熔断器', 'HH15-250/3', 'B01020319', '套', null);
INSERT INTO `materials` VALUES ('开关熔断器', 'HH15P-125A', 'B01020320', '只', null);
INSERT INTO `materials` VALUES ('隔离开关', 'HP-1600/3', 'B01020321', '台', null);
INSERT INTO `materials` VALUES ('熔断式刀开关', 'HR5-100/31', 'B01020322', '把', null);
INSERT INTO `materials` VALUES ('刀开关', 'HH15-400/3', 'B01020323', '只', null);
INSERT INTO `materials` VALUES ('熔断式刀开关', 'HR5-200/31', 'B01020324', '把', null);
INSERT INTO `materials` VALUES ('负荷开关', '', 'B010204', '', null);
INSERT INTO `materials` VALUES ('封闭式负荷开关', 'HH3-100/32', 'B01020401', '把', null);
INSERT INTO `materials` VALUES ('封闭式负荷开关', 'HH3-200/32', 'B01020402', '把', null);
INSERT INTO `materials` VALUES ('封闭式负荷开关', 'HH3-400/32', 'B01020403', '把', null);
INSERT INTO `materials` VALUES ('开启式负荷开关', 'HK2-100/3', 'B01020404', '把', null);
INSERT INTO `materials` VALUES ('开启式负荷开关', 'HK2-60/3', 'B01020405', '把', null);
INSERT INTO `materials` VALUES ('开启式负荷开关', 'HK2-60/2', 'B01020406', '把', null);
INSERT INTO `materials` VALUES ('开启式负荷开关', 'HK2-30/3', 'B01020407', '把', null);
INSERT INTO `materials` VALUES ('开启式负荷开关', 'HK2-30/2', 'B01020408', '把', null);
INSERT INTO `materials` VALUES ('开启式负荷开关', 'HK2-15/3', 'B01020409', '把', null);
INSERT INTO `materials` VALUES ('开启式负荷开关', 'ZK1-100A/3', 'B01020410', '把', null);
INSERT INTO `materials` VALUES ('开启式负荷开关', 'HK2-15/2', 'B01020411', '把', null);
INSERT INTO `materials` VALUES ('隔离开关', 'QSA-250/4 250A', 'B01020412', '台', null);
INSERT INTO `materials` VALUES ('隔离开关', 'QSA-400/4  315A', 'B01020413', '台', null);
INSERT INTO `materials` VALUES ('隔离开关', 'SX-40/4', 'B01020414', '台', null);
INSERT INTO `materials` VALUES ('转换开关', '', 'B010205', '', null);
INSERT INTO `materials` VALUES ('转换开关', 'LW5-16YH/3', 'B01020501', '只', null);
INSERT INTO `materials` VALUES ('转换开关', 'LW5-16K6894/6#', 'B01020502', '只', null);
INSERT INTO `materials` VALUES ('转换开关', 'LW5-16L6560/5', 'B01020503', '只', null);
INSERT INTO `materials` VALUES ('转换开关', 'LW5-16L7891/10', 'B01020504', '只', null);
INSERT INTO `materials` VALUES ('转换开关', 'LW5-16D3462/12', 'B01020505', '只', null);
INSERT INTO `materials` VALUES ('转换开关', 'LW5-16TM707/7', 'B01020506', '只', null);
INSERT INTO `materials` VALUES ('万能转换开关', 'LW5-150936/3', 'B01020507', '只', null);
INSERT INTO `materials` VALUES ('电压切换开关', 'LW5-15E 1132/4', 'B01020508', '只', null);
INSERT INTO `materials` VALUES ('同期开关', 'LW5-15D 0408/2', 'B01020509', '只', null);
INSERT INTO `materials` VALUES ('转换开关', 'LW5-16E 113/4', 'B01020510', '只', null);
INSERT INTO `materials` VALUES ('转换开关', 'LW5-16C7591/10', 'B01020511', '只', null);
INSERT INTO `materials` VALUES ('转换开关', 'LW2-1/1111F48X', 'B01020512', '只', null);
INSERT INTO `materials` VALUES ('智能双电源转换开关', 'CKQ33-100H.4328/80A', 'B01020513', '只', null);
INSERT INTO `materials` VALUES ('双电源自动切换开关', 'TIQ1-160R/160/4F', 'B01020514', '台', null);
INSERT INTO `materials` VALUES ('组合开关', '', 'B010206', '', null);
INSERT INTO `materials` VALUES ('组合开关', 'HZ10-25/3', 'B01020601', '只', null);
INSERT INTO `materials` VALUES ('组合开关', 'HZ10-60/3', 'B01020602', '只', null);
INSERT INTO `materials` VALUES ('电焊机开关', 'KD12-125', 'B01020603', '只', null);
INSERT INTO `materials` VALUES ('扭子开关', '？', 'B01020604', '只', null);
INSERT INTO `materials` VALUES ('组合开关', 'HZ10-25P/3', 'B01020605', '只', null);
INSERT INTO `materials` VALUES ('组合开关', 'HZ10-60P/3', 'B01020606', '只', null);
INSERT INTO `materials` VALUES ('组合开关', 'HZ5-20/4', 'B01020607', '只', null);
INSERT INTO `materials` VALUES ('其它闸刀', '', 'B010207', '', null);
INSERT INTO `materials` VALUES ('石板闸刀', 'HRTO-400', 'B01020701', '把', null);
INSERT INTO `materials` VALUES ('石板闸刀', 'HRTO-100A', 'B01020702', '把', null);
INSERT INTO `materials` VALUES ('倒顺开关', '15A', 'B01020703', '只', null);
INSERT INTO `materials` VALUES ('倒顺开关', '60A', 'B01020704', '把', null);
INSERT INTO `materials` VALUES ('动态无触点开关', '', 'B010208', '', null);
INSERT INTO `materials` VALUES ('动态无触点开关', '？', 'B01020801', '只', null);
INSERT INTO `materials` VALUES ('接触器和继电器', '', 'B0103', '', null);
INSERT INTO `materials` VALUES ('普通接触器', '', 'B010301', '', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ10-5A', 'B01030101', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ10-10A/220V(380V)', 'B01030102', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ10-20A', 'B01030103', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ10-40A', 'B01030104', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ10-60A', 'B01030105', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ10-100A/220V', 'B01030106', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ10-150A', 'B01030107', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ20-40A/380V', 'B01030108', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ20-100A/380V', 'B01030109', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ20-250A/380V', 'B01030110', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ20-250A/220V', 'B01030111', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ20-160/380V', 'B01030112', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ20-400A/380V', 'B01030113', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ20-630A/380V', 'B01030114', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJ20-100A/220V', 'B01030115', '只', null);
INSERT INTO `materials` VALUES ('节能接触器', '', 'B010302', '', null);
INSERT INTO `materials` VALUES ('节能接触器', 'CJC20-150A', 'B01030201', '只', null);
INSERT INTO `materials` VALUES ('节能接触器', 'CJC20-160A', 'B01030202', '只', null);
INSERT INTO `materials` VALUES ('节能接触器', 'CJC20-250A', 'B01030203', '只', null);
INSERT INTO `materials` VALUES ('节能接触器', 'CJC20-400A', 'B01030204', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJX10-63A', 'B01030205', '只', null);
INSERT INTO `materials` VALUES ('接触器', 'CJX2-163A', 'B01030206', '只', null);
INSERT INTO `materials` VALUES ('切换电容器接触器', '', 'B010303', '', null);
INSERT INTO `materials` VALUES ('切换电容器接触器', 'CJ19-32A/11(220V)', 'B01030301', '只', null);
INSERT INTO `materials` VALUES ('切换电容器接触器', 'CJ19-43A/11(220V)', 'B01030302', '只', null);
INSERT INTO `materials` VALUES ('切换电容器接触器', 'CJ23-40/220', 'B01030303', '只', null);
INSERT INTO `materials` VALUES ('切换电容接触器', 'CJ19-63/11(220V)', 'B01030304', '只', null);
INSERT INTO `materials` VALUES ('切换电容接触器', 'CJ19C-63/21', 'B01030305', '只', null);
INSERT INTO `materials` VALUES ('切换电容接触器', 'CJ39-40-11/220V', 'B01030306', '只', null);
INSERT INTO `materials` VALUES ('切换电容接触器', 'CJ39-63-21/220V', 'B01030307', '只', null);
INSERT INTO `materials` VALUES ('切换电容接触器组合', 'HZG33A-40/11 220V', 'B01030308', '套', null);
INSERT INTO `materials` VALUES ('切换电容接触器组合', 'HZG33A-63/21 220V', 'B01030309', '套', null);
INSERT INTO `materials` VALUES ('热继电器', '', 'B010304', '', null);
INSERT INTO `materials` VALUES ('热继电器', 'JR16B-20/3', 'B01030401', '只', null);
INSERT INTO `materials` VALUES ('热继电器', 'JR28-36A', 'B01030402', '只', null);
INSERT INTO `materials` VALUES ('热继电器', 'JR10-10', 'B01030403', '只', null);
INSERT INTO `materials` VALUES ('热继电器', 'JRS1-12316(5A)', 'B01030404', '只', null);
INSERT INTO `materials` VALUES ('热继电器', 'JR16B-63A', 'B01030405', '只', null);
INSERT INTO `materials` VALUES ('时间继电器', '', 'B010305', '', null);
INSERT INTO `materials` VALUES ('时间继电器', 'JST-1A', 'B01030501', '只', null);
INSERT INTO `materials` VALUES ('时间继电器', 'JS7-14A-60秒(220V)', 'B01030502', '只', null);
INSERT INTO `materials` VALUES ('时间控制器', 'UKD-I', 'B01030503', '只', null);
INSERT INTO `materials` VALUES ('时间继电器', 'JS14A-Y/10s/220v', 'B01030504', '只', null);
INSERT INTO `materials` VALUES ('时间继电器', 'DS-123AC/220V', 'B01030505', '只', null);
INSERT INTO `materials` VALUES ('时间继电器', 'DSJ-13/220V', 'B01030506', '只', null);
INSERT INTO `materials` VALUES ('时间继电器', 'DS-122/0.25～3.5〃', 'B01030507', '只', null);
INSERT INTO `materials` VALUES ('时间继电器', 'DS-123/0.5～9〃', 'B01030508', '只', null);
INSERT INTO `materials` VALUES ('时间继电器', 'DS-35/220V', 'B01030509', '只', null);
INSERT INTO `materials` VALUES ('定时开关', 'L1E1F/S', 'B01030510', '只', null);
INSERT INTO `materials` VALUES ('电流继电器', '', 'B010306', '', null);
INSERT INTO `materials` VALUES ('电流继电器', 'JL14-5A', 'B01030601', '只', null);
INSERT INTO `materials` VALUES ('电流继电器', 'JS14A-60秒 220V', 'B01030602', '只', null);
INSERT INTO `materials` VALUES ('电流继电器', 'DL-11/10', 'B01030603', '只', null);
INSERT INTO `materials` VALUES ('中间继电器', '', 'B010307', '', null);
INSERT INTO `materials` VALUES ('中间继电器', 'JZ7-44/220V', 'B01030701', '只', null);
INSERT INTO `materials` VALUES ('中间继电器', 'JZC1-44/220V', 'B01030702', '只', null);
INSERT INTO `materials` VALUES ('中间继电器', 'JZ1A-33/380V', 'B01030703', '只', null);
INSERT INTO `materials` VALUES ('中间继电器', 'DZJ-207/AC220V', 'B01030704', '只', null);
INSERT INTO `materials` VALUES ('中间继电器', 'CZC4-31/220V', 'B01030705', '只', null);
INSERT INTO `materials` VALUES ('接触器式继电器', 'JZC4-31', 'B01030706', '只', null);
INSERT INTO `materials` VALUES ('继电器', 'HH5-3P/110V', 'B01030707', '只', null);
INSERT INTO `materials` VALUES ('继电器', 'JQX-3Z/220V', 'B01030708', '只', null);
INSERT INTO `materials` VALUES ('中间继电器', 'JZ7-44/380V', 'B01030709', '只', null);
INSERT INTO `materials` VALUES ('信号继电器', '', 'B010308', '', null);
INSERT INTO `materials` VALUES ('信号继电器', 'DX-11/0.1A', 'B01030801', '只', null);
INSERT INTO `materials` VALUES ('欠频率继电器', 'SQP-6 110V', 'B01030802', '只', null);
INSERT INTO `materials` VALUES ('信号继电器', 'DX-17/30', 'B01030803', '只', null);
INSERT INTO `materials` VALUES ('低周率继电器', 'SZH3/AC100V', 'B01030804', '只', null);
INSERT INTO `materials` VALUES ('晶体管液位继电器', 'TYR-714/380V', 'B01030805', '只', null);
INSERT INTO `materials` VALUES ('液位继电器', 'JYB-714/220V', 'B01030806', '只', null);
INSERT INTO `materials` VALUES ('电压继电器', '', 'B010309', '', null);
INSERT INTO `materials` VALUES ('电压继电器', 'JD-1B', 'B01030901', '只', null);
INSERT INTO `materials` VALUES ('电压继电器', 'JD-2B', 'B01030902', '只', null);
INSERT INTO `materials` VALUES ('电压继电器', 'DJ-111/400', 'B01030903', '只', null);
INSERT INTO `materials` VALUES ('熔断器', '', 'B0104', '', null);
INSERT INTO `materials` VALUES ('插入式熔断器', '', 'B010401', '', null);
INSERT INTO `materials` VALUES ('插入式熔断器', 'RC1-5A', 'B01040101', '只', null);
INSERT INTO `materials` VALUES ('插入式熔断器', 'RC1-10A', 'B01040102', '只', null);
INSERT INTO `materials` VALUES ('插入式熔断器', 'RC1-15A', 'B01040103', '只', null);
INSERT INTO `materials` VALUES ('插入式熔断器', 'RC1-30A', 'B01040104', '只', null);
INSERT INTO `materials` VALUES ('插入式熔断器', 'RC1-60A', 'B01040105', '只', null);
INSERT INTO `materials` VALUES ('插入式熔断器', 'RC1-100A', 'B01040106', '只', null);
INSERT INTO `materials` VALUES ('插入式熔断器', 'RC1-200A', 'B01040107', '只', null);
INSERT INTO `materials` VALUES ('其它熔断器(熔芯)', '', 'B010402', '', null);
INSERT INTO `materials` VALUES ('熔断器(熔芯)', 'JF2-(5-10)A', 'B01040202', '只', null);
INSERT INTO `materials` VALUES ('管式熔断器(熔芯)', 'R1-5A(6×30)', 'B01040204', '盒', null);
INSERT INTO `materials` VALUES ('管式熔芯', 'R1-2A(5×20)', 'B01040205', '盒', null);
INSERT INTO `materials` VALUES ('熔断器（熔芯）', 'JFS-2.5RD(20A)', 'B01040206', '只', null);
INSERT INTO `materials` VALUES ('熔断器(熔芯)', 'JFS-2.5RD(6A)', 'B01040207', '只', null);
INSERT INTO `materials` VALUES ('其它熔断器(熔座)', '', 'B010403', '', null);
INSERT INTO `materials` VALUES ('低压熔断器（熔座）', 'RT14-32A', 'B01040302', '只', null);
INSERT INTO `materials` VALUES ('熔断器端子', '', 'B010404', '', null);
INSERT INTO `materials` VALUES ('熔断器端子', 'JFS-2.5RD/10A', 'B01040401', '套', null);
INSERT INTO `materials` VALUES ('互感器', '', 'B0105', '', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LQG2-0.66-150/5', 'B010501', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LM-0.5-300/5', 'B010502', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LMZ1-0.5-150/5', 'B010503', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LMZ1-0.5-200/5', 'B010504', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LMZ1-0.5-400/5', 'B010505', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LMZJ1-0.5-50/5', 'B010506', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LMZJ1-0.5-100/5', 'B010507', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LMZJ1-0.5-150/5', 'B010508', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LMZJ1-0.5-200/5', 'B010509', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LMZJ1-0.5-300/5', 'B010510', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LMZJ1-0.5-400/5', 'B010511', '只', null);
INSERT INTO `materials` VALUES ('电流互感器', 'LMZJ1-0.5-500/5', 'B010512', '只', null);
INSERT INTO `materials` VALUES ('仪器和仪表', '', 'B0106', '', null);
INSERT INTO `materials` VALUES ('其它类', '', 'B010601', '', null);
INSERT INTO `materials` VALUES ('单相调压器', 'JDGCZJ-2', 'B01060101', '只', null);
INSERT INTO `materials` VALUES ('全自动稳压器', 'TND-1000', 'B01060102', '只', null);
INSERT INTO `materials` VALUES ('数字万用表', 'UJ2003', 'B01060103', '只', null);
INSERT INTO `materials` VALUES ('秒表', 'SJ9-2', 'B01060104', '只', null);
INSERT INTO `materials` VALUES ('漏电保护器', 'SDLB-6B', 'B01060105', '只', null);
INSERT INTO `materials` VALUES ('电度表', 'DT862-(5-10)A', 'B01060106', '只', null);
INSERT INTO `materials` VALUES ('兆欧表', 'ZC250-500V(500MΩ)', 'B01060107', '只', null);
INSERT INTO `materials` VALUES ('有功功率因数表', '42L6-COSΨ 5A/380V', 'B01060108', '只', null);
INSERT INTO `materials` VALUES ('仿真器', 'AEDK-51S', 'B01060109', '套', null);
INSERT INTO `materials` VALUES ('电度表', 'DT862-3(1.5-6A)', 'B01060110', '只', null);
INSERT INTO `materials` VALUES ('电度表', 'DT862-3(10-40A)380V', 'B01060111', '只', null);
INSERT INTO `materials` VALUES ('变压器', 'TDK3-400', 'B01060112', '台', null);
INSERT INTO `materials` VALUES ('漏电保护器', 'SDLB-9C', 'B01060113', '只', null);
INSERT INTO `materials` VALUES ('频率表', '42L6-HZ45-55HZ', 'B01060114', '只', null);
INSERT INTO `materials` VALUES ('无功电能表', 'DS862-380V/5A', 'B01060115', '只', null);
INSERT INTO `materials` VALUES ('自动准同期装置', 'ZZB-I', 'B01060116', '只', null);
INSERT INTO `materials` VALUES ('无功率补偿自动控制器', '', 'B010604', '', null);
INSERT INTO `materials` VALUES ('无功功率补偿自动控制器', 'JKG(B)-4', 'B01060401', '只', null);
INSERT INTO `materials` VALUES ('无功功率补偿自动控制器', 'JKG(B)-6', 'B01060402', '只', null);
INSERT INTO `materials` VALUES ('无功功率补偿自动控制器', 'JKG(B)-8', 'B01060403', '只', null);
INSERT INTO `materials` VALUES ('无功功率补偿自动控制器', 'JKG(B)-10', 'B01060404', '只', null);
INSERT INTO `materials` VALUES ('无功功率补偿自动控制器', 'JKGH-10', 'B01060405', '只', null);
INSERT INTO `materials` VALUES ('功率因数调整器（台湾）', 'TS8D-T', 'B01060406', '只', null);
INSERT INTO `materials` VALUES ('无功功率补偿自动控制器', 'JKGD-10/3', 'B01060407', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '', 'B010609', '', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-150/5', 'B01060901', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-200/5', 'B01060902', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-300/5', 'B01060903', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-400/5', 'B01060904', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-500/5', 'B01060905', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-600/5', 'B01060906', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-800/5', 'B01060907', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-1000/5', 'B01060908', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-1500/5', 'B01060909', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-100/5', 'B01060910', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-1200/5', 'B01060911', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-250/5', 'B01060912', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-2500/5', 'B01060913', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-5000/5', 'B01060914', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-3K/5', 'B01060915', '只', null);
INSERT INTO `materials` VALUES ('交流功率表', '42L6W-2000/5', 'B01060916', '只', null);
INSERT INTO `materials` VALUES ('导体', '', 'B0107', '', null);
INSERT INTO `materials` VALUES ('铜芯线', '', 'B010701', '', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-1mm2', 'B01070101', 'm', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-1.5mm2', 'B01070102', 'm', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-2.5mm2', 'B01070103', 'm', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-4mm2', 'B01070104', '米', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-6mm2', 'B01070105', 'm', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-10mm2', 'B01070106', 'm', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-16mm2', 'B01070107', 'm', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-25mm2', 'B01070108', 'm', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-35mm2', 'B01070109', 'm', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-50mm2', 'B01070110', 'm', null);
INSERT INTO `materials` VALUES ('铜芯线', 'BV-70mm2', 'B01070111', 'm', null);
INSERT INTO `materials` VALUES ('铜芯软线', '', 'B010702', '', null);
INSERT INTO `materials` VALUES ('铜芯软线', 'BVR-1mm2', 'B01070201', 'm', null);
INSERT INTO `materials` VALUES ('铜芯软线', 'BVR-1.5mm2', 'B01070202', 'm', null);
INSERT INTO `materials` VALUES ('铜芯软线', 'BVR-2.5mm2', 'B01070203', 'm', null);
INSERT INTO `materials` VALUES ('铜芯软线', 'BVR-4mm2', 'B01070204', 'm', null);
INSERT INTO `materials` VALUES ('铜芯软线', 'BVR-6mm2', 'B01070205', 'm', null);
INSERT INTO `materials` VALUES ('铜芯软线', 'BVR-16mm2', 'B01070206', 'm', null);
INSERT INTO `materials` VALUES ('橡套软线', 'YZ-2×1mm2', 'B01070207', 'm', null);
INSERT INTO `materials` VALUES ('橡套软线', 'YZ-3*4mm2+1*2.5mm2', 'B01070208', 'm', null);
INSERT INTO `materials` VALUES ('橡套软线', 'YZ-2×1.5', 'B01070209', 'm', null);
INSERT INTO `materials` VALUES ('橡套软线', 'YZ-3×1.5', 'B01070210', 'm', null);
INSERT INTO `materials` VALUES ('多股胶线', 'RVS-1mm2', 'B01070211', 'm', null);
INSERT INTO `materials` VALUES ('铝排', '', 'B010703', '', null);
INSERT INTO `materials` VALUES ('铝排(0.2025kg/m)', 'LYM-3×25', 'B01070301', 'm', null);
INSERT INTO `materials` VALUES ('铝排(0.243kg/m)', 'LYM-3×30', 'B01070302', 'm', null);
INSERT INTO `materials` VALUES ('铝排(0.432kg/m)', 'LYM-4×40', 'B01070303', 'm', null);
INSERT INTO `materials` VALUES ('铝排(0.675kg/m)', 'LYM-5×50', 'B01070304', 'm', null);
INSERT INTO `materials` VALUES ('铝排(0.975kg/m)', 'LYM-6×60', 'B01070305', 'm', null);
INSERT INTO `materials` VALUES ('铝排(1.728kg/m)', 'LYM-8×80', 'B01070306', 'm', null);
INSERT INTO `materials` VALUES ('铝排(2.700kg/m)', 'LYM-10×100', 'B01070307', 'm', null);
INSERT INTO `materials` VALUES ('铜排', '', 'B010704', '', null);
INSERT INTO `materials` VALUES ('铜排(0.801kg/m)', 'TYM-3×30', 'B01070401', 'm', null);
INSERT INTO `materials` VALUES ('铜排(1.424kg/m)', 'TYM-4×40', 'B01070402', 'm', null);
INSERT INTO `materials` VALUES ('铜排(1.335kg/m)', 'TYM-5×30', 'B01070403', 'm', null);
INSERT INTO `materials` VALUES ('铜排(2.136kg/m)', 'TYM-6×40', 'B01070404', 'm', null);
INSERT INTO `materials` VALUES ('铜排(2.225kg/m)', 'TYM-5×50', 'B01070405', 'm', null);
INSERT INTO `materials` VALUES ('铜排(4.45kg/m)', 'TYM-10×50', 'B01070406', 'm', null);
INSERT INTO `materials` VALUES ('铜排(3.204kg/m)', 'TYM-6×60', 'B01070407', 'm', null);
INSERT INTO `materials` VALUES ('铜排(8.90kg/m)', 'TYM-10×100', 'B01070408', 'm', null);
INSERT INTO `materials` VALUES ('铜排(1.78kg/m)', 'TYM-5×40', 'B01070409', 'm', null);
INSERT INTO `materials` VALUES ('铜板', '405×985×5', 'B01070410', 'kg', null);
INSERT INTO `materials` VALUES ('紫铜管', 'φ14', 'B01070411', 'kg', null);
INSERT INTO `materials` VALUES ('铜接头', '', 'B010705', '', null);
INSERT INTO `materials` VALUES ('铜接头', 'DT-16mm2', 'B01070501', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'DT-25mm2', 'B01070502', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'DT-35mm2', 'B01070503', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'DT-50mm2', 'B01070504', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'DT-70mm2', 'B01070505', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'DT-95mm2', 'B01070506', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'DT-120mm2', 'B01070507', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'DT-185mm2', 'B01070508', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'DT-240mm2', 'B01070509', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'DT-150mm2', 'B01070510', '只', null);
INSERT INTO `materials` VALUES ('接地端棒', '？', 'B01070511', '套', null);
INSERT INTO `materials` VALUES ('插针', 'L45-25', 'B01070512', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'SBG-1-m20', 'B01070513', '只', null);
INSERT INTO `materials` VALUES ('铝、铜接头', '', 'B010706', '', null);
INSERT INTO `materials` VALUES ('铝铜接头', 'DLT-25mm2', 'B01070601', '只', null);
INSERT INTO `materials` VALUES ('铝铜接头', 'DLT-35mm2', 'B01070602', '只', null);
INSERT INTO `materials` VALUES ('铝铜接头', 'DLT-50mm2', 'B01070603', '只', null);
INSERT INTO `materials` VALUES ('铜铝接头', 'DTL-25 mm2', 'B01070604', '只', null);
INSERT INTO `materials` VALUES ('铜铝接头', 'DTL-50 mm2', 'B01070605', '只', null);
INSERT INTO `materials` VALUES ('铜铝接头', 'DTL-35mm2', 'B01070606', '只', null);
INSERT INTO `materials` VALUES ('铜铝接头', 'DLT-70mm2', 'B01070607', '只', null);
INSERT INTO `materials` VALUES ('铜铝接头', 'DLT-95mm2', 'B01070608', '只', null);
INSERT INTO `materials` VALUES ('铜软连接', '360*60*2', 'B01070609', '条', null);
INSERT INTO `materials` VALUES ('圆形裸端头', '', 'B010707', '', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-20A', 'B01070701', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-40A', 'B01070702', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-60A', 'B01070703', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-80A', 'B01070704', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-100A', 'B01070705', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-150A', 'B01070706', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-200A', 'B01070707', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-250A', 'B01070708', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-400A', 'B01070709', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-50A', 'B01070710', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-2.5*4', 'B01070711', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-6mm2', 'B01070712', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-300A', 'B01070713', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-600A', 'B01070714', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-2.5*6mm2', 'B01070715', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-2.5*8', 'B01070716', '只', null);
INSERT INTO `materials` VALUES ('圆形裸端头', 'OT-500A', 'B01070717', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', '', 'B010708', '', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'TU-1.5×3', 'B01070801', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'TU-1.5×4', 'B01070802', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'TU-1.5×5', 'B01070803', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'TU-1.5×6', 'B01070804', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'TU-2.5×3', 'B01070805', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'TU-2.5×4', 'B01070806', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'TU-2.5×5', 'B01070807', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'TU-2.5×6', 'B01070808', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'TU-6×6', 'B01070809', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'UT-6×5', 'B01070810', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'UT-6*4', 'B01070812', '只', null);
INSERT INTO `materials` VALUES ('铜接头', 'UT-10*6', 'B01070813', '只', null);
INSERT INTO `materials` VALUES ('叉形裸端头', 'OT-2.5×4', 'B01070820', '只', null);
INSERT INTO `materials` VALUES ('插针', 'c4516MM2', 'B01070821', '只', null);
INSERT INTO `materials` VALUES ('插针', '35mm2', 'B01070822', '只', null);
INSERT INTO `materials` VALUES ('插针', 'L45-10mm2', 'B01070823', '只', null);
INSERT INTO `materials` VALUES ('其它电器', '', 'B0108', '', null);
INSERT INTO `materials` VALUES ('接线端子', '', 'B010801', '', null);
INSERT INTO `materials` VALUES ('接线端子', 'X5-2005', 'B01080101', '只', null);
INSERT INTO `materials` VALUES ('接线端子', 'JDO-60/0', 'B01080102', '只', null);
INSERT INTO `materials` VALUES ('普通接线端子', 'JH10-2.5/1', 'B01080103', '只', null);
INSERT INTO `materials` VALUES ('试验接线端子', 'JH10S-2.5', 'B01080104', '只', null);
INSERT INTO `materials` VALUES ('连接接线端子', 'JH10-2.5/2L', 'B01080105', '只', null);
INSERT INTO `materials` VALUES ('低压出线桩子', '100KVA（φ12）', 'B01080106', '套', null);
INSERT INTO `materials` VALUES ('低压出线桩子', '315KVA（φ20）', 'B01080107', '套', null);
INSERT INTO `materials` VALUES ('接线端子', 'TB-2503/L', 'B01080108', '只', null);
INSERT INTO `materials` VALUES ('接线端子', 'TB-2504/L', 'B01080109', '只', null);
INSERT INTO `materials` VALUES ('接线板', 'X3-6012', 'B01080110', '条', null);
INSERT INTO `materials` VALUES ('接线柱', '633', 'B01080111', '只', null);
INSERT INTO `materials` VALUES ('接线端子', 'JF5-40/5', 'B01080112', '只', null);
INSERT INTO `materials` VALUES ('接线端子', 'JF5-60/5', 'B01080113', '只', null);
INSERT INTO `materials` VALUES ('连接片', '?', 'B01080114', '片', null);
INSERT INTO `materials` VALUES ('接线端子', 'JF5-10/5', 'B01080115', '只', null);
INSERT INTO `materials` VALUES ('接线板、盒', '', 'B010802', '', null);
INSERT INTO `materials` VALUES ('接线柱', '333型', 'B01080201', '只', null);
INSERT INTO `materials` VALUES ('接线板', 'B1-4-10', 'B01080202', '块', null);
INSERT INTO `materials` VALUES ('组合接线盒', 'DFY1-3×380V/220V', 'B01080203', '只', null);
INSERT INTO `materials` VALUES ('接线柱', 'P-100A', 'B01080204', '只', null);
INSERT INTO `materials` VALUES ('接线柱', 'P10A', 'B01080205', '只', null);
INSERT INTO `materials` VALUES ('补偿控制接线桩头', '？', 'B01080206', '付', null);
INSERT INTO `materials` VALUES ('接线柱', '555型', 'B01080207', '只', null);
INSERT INTO `materials` VALUES ('接线板', 'JF5-2.5/5', 'B01080208', '条', null);
INSERT INTO `materials` VALUES ('接线板', 'TL-100/4', 'B01080209', '块', null);
INSERT INTO `materials` VALUES ('接线板', 'TB-2503/L', 'B01080210', '条', null);
INSERT INTO `materials` VALUES ('接线板', 'XJ-4/60A', 'B01080211', '块', null);
INSERT INTO `materials` VALUES ('行程开关', '', 'B010804', '', null);
INSERT INTO `materials` VALUES ('行程开关', 'JLXK1-111', 'B01080401', '只', null);
INSERT INTO `materials` VALUES ('行程开关', 'LX-5-11H', 'B01080402', '只', null);
INSERT INTO `materials` VALUES ('行程开关', 'LX-0.28', 'B01080403', '只', null);
INSERT INTO `materials` VALUES ('行程开关', 'LX3-11H', 'B01080404', '只', null);
INSERT INTO `materials` VALUES ('行程开关', 'LX19-111(5A)', 'B01080405', '只', null);
INSERT INTO `materials` VALUES ('行程开关', 'LXK2-411K', 'B01080406', '只', null);
INSERT INTO `materials` VALUES ('行程开关', 'LXIP-K', 'B01080407', '只', null);
INSERT INTO `materials` VALUES ('行程开关', 'X2-N', 'B01080408', '只', null);
INSERT INTO `materials` VALUES ('限位开关', 'LX44-20', 'B01080409', '只', null);
INSERT INTO `materials` VALUES ('微动开关', 'LXW5-G', 'B01080410', '只', null);
INSERT INTO `materials` VALUES ('控制按钮', '', 'B010805', '', null);
INSERT INTO `materials` VALUES ('控制按钮', 'LA2-220V', 'B01080501', '只', null);
INSERT INTO `materials` VALUES ('控制按钮', 'LA18-380V(5A)', 'B01080502', '只', null);
INSERT INTO `materials` VALUES ('旋转按钮', 'LAY3-220V', 'B01080503', '只', null);
INSERT INTO `materials` VALUES ('按钮', 'KAO-5B', 'B01080504', '只', null);
INSERT INTO `materials` VALUES ('按钮', 'LA10-2H', 'B01080505', '只', null);
INSERT INTO `materials` VALUES ('按钮', 'LA18--22J', 'B01080506', '只', null);
INSERT INTO `materials` VALUES ('按钮', 'LASO-A-11', 'B01080507', '只', null);
INSERT INTO `materials` VALUES ('按钮', 'LAY3-Z/11', 'B01080508', '只', null);
INSERT INTO `materials` VALUES ('按钮', 'LA18-22/380V', 'B01080509', '只', null);
INSERT INTO `materials` VALUES ('行车按钮', '4档', 'B01080510', '只', null);
INSERT INTO `materials` VALUES ('按钮', 'AK-6', 'B01080511', '只', null);
INSERT INTO `materials` VALUES ('按钮', 'LAP-25/2', 'B01080512', '只', null);
INSERT INTO `materials` VALUES ('信号灯、电铃', '', 'B010806', '', null);
INSERT INTO `materials` VALUES ('信号灯', 'XD11-6.3', 'B01080601', '只', null);
INSERT INTO `materials` VALUES ('信号灯', 'AD11-25/40', 'B01080602', '只', null);
INSERT INTO `materials` VALUES ('信号灯', 'XD7-8(380V)', 'B01080603', '只', null);
INSERT INTO `materials` VALUES ('信号灯', 'XD13-220V', 'B01080604', '只', null);
INSERT INTO `materials` VALUES ('电铃', 'UC4-75/220V', 'B01080605', '只', null);
INSERT INTO `materials` VALUES ('超声波驱鼠器', '？', 'B01080606', '只', null);
INSERT INTO `materials` VALUES ('主令开关', 'LS2-2', 'B01080607', '只', null);
INSERT INTO `materials` VALUES ('电话机', 'HA9000（23）', 'B01080608', '台', null);
INSERT INTO `materials` VALUES ('警铃', '220V', 'B01080609', '只', null);
INSERT INTO `materials` VALUES ('同期灯', 'XD5-380', 'B01080610', '只', null);
INSERT INTO `materials` VALUES ('信号灯', 'XD5-220', 'B01080611', '只', null);
INSERT INTO `materials` VALUES ('光字牌', 'XD9-220/15W', 'B01080612', '只', null);
INSERT INTO `materials` VALUES ('信号灯', 'AD16-22D/220V', 'B01080613', '只', null);
INSERT INTO `materials` VALUES ('信号灯', 'AD16-22D/380', 'B01080614', '只', null);
INSERT INTO `materials` VALUES ('指示灯', 'AD13-6.3V', 'B01080615', '只', null);
INSERT INTO `materials` VALUES ('避雷器', '', 'B010808', '', null);
INSERT INTO `materials` VALUES ('低压避雷器', 'FYS-0.2', 'B01080801', '只', null);
INSERT INTO `materials` VALUES ('高压避雷器', 'HY5Ws-12.7/50', 'B01080802', '3只/组', null);
INSERT INTO `materials` VALUES ('避雷器', 'HY1.5W-0.5/2.6', 'B01080803', '只', null);
INSERT INTO `materials` VALUES ('避雷器', 'PU65*4', 'B01080804', '套', null);
INSERT INTO `materials` VALUES ('避雷器', 'PU 40×4', 'B01080805', '套', null);
INSERT INTO `materials` VALUES ('浪涌装置', 'ZU30-20/40KA2P', 'B01080806', '只', null);
INSERT INTO `materials` VALUES ('浪涌保护器', 'TIU1-65/440 4P', 'B01080807', '台', null);
INSERT INTO `materials` VALUES ('浪涌保护器', 'ZU1-C/4P 20-40KA/420V', 'B01080808', '台', null);
INSERT INTO `materials` VALUES ('浪涌保护器', 'TIU1-40/440/4P', 'B01080809', '只', null);
INSERT INTO `materials` VALUES ('浪涌保护器', 'ZU1C-30/60/4P', 'B01080810', '只', null);
INSERT INTO `materials` VALUES ('配件', '', 'B0109', '', null);
INSERT INTO `materials` VALUES ('欠压脱扣器线圈', 'DW15(400-630)A', 'B010901', '只', null);
INSERT INTO `materials` VALUES ('欠压脱扣器线圈', 'DW15(1000-1600)A', 'B010902', '只', null);
INSERT INTO `materials` VALUES ('辅助触头', 'DW15-1600/3', 'B010903', '只', null);
INSERT INTO `materials` VALUES ('拉簧', '?', 'B010904', '根', null);
INSERT INTO `materials` VALUES ('分励脱扣器线圈', 'DW15(400-630)A', 'B010905', '只', null);
INSERT INTO `materials` VALUES ('分励脱扣器线圈', 'DW15(1000-1600)A', 'B010906', '只', null);
INSERT INTO `materials` VALUES ('接触器线圈', 'CJ10-150A(220V)', 'B010907', '只', null);
INSERT INTO `materials` VALUES ('接触器线圈', 'CJ10-150A(380V)', 'B010908', '只', null);
INSERT INTO `materials` VALUES ('接触器线圈', 'CJ20-160A(220V)', 'B010909', '只', null);
INSERT INTO `materials` VALUES ('接触器线圈', 'CJ20-160A(380V)', 'B010910', '只', null);
INSERT INTO `materials` VALUES ('接触器线圈', 'CJ20-250A(220V)', 'B010911', '只', null);
INSERT INTO `materials` VALUES ('接触器线圈', 'CJ20-250A(380V)', 'B010912', '只', null);
INSERT INTO `materials` VALUES ('接触器线圈', 'CJ20-400A(220V)', 'B010913', '只', null);
INSERT INTO `materials` VALUES ('绝缘材料', '', 'B0110', '', null);
INSERT INTO `materials` VALUES ('固定母线架', 'GGD-6×60', 'B011001', '付', null);
INSERT INTO `materials` VALUES ('固定母线架', 'GGD-8×80', 'B011002', '付', null);
INSERT INTO `materials` VALUES ('固定母线架', 'GGD-10×100', 'B011003', '付', null);
INSERT INTO `materials` VALUES ('零线母线架', 'LM1-4×40', 'B011004', '付', null);
INSERT INTO `materials` VALUES ('零线母线架', 'LM1-5×50', 'B011005', '付', null);
INSERT INTO `materials` VALUES ('零线母线架', 'LM1-6×60', 'B011006', '付', null);
INSERT INTO `materials` VALUES ('零线母线架', 'LM1-8×80', 'B011007', '付', null);
INSERT INTO `materials` VALUES ('零线母线架', 'LM1-10×100', 'B011008', '付', null);
INSERT INTO `materials` VALUES ('组合母线架', 'ZMJ1-4×40', 'B011009', '付', null);
INSERT INTO `materials` VALUES ('组合母线架', 'ZMJ1-5×50', 'B011010', '付', null);
INSERT INTO `materials` VALUES ('电力电容器', '', 'B0111', '', null);
INSERT INTO `materials` VALUES ('电容器', 'CB-60', 'B011101', '只', null);
INSERT INTO `materials` VALUES ('电容器', '±50/450V', 'B011102', '只', null);
INSERT INTO `materials` VALUES ('电容器', 'BSMJ-0.4-12Kvar', 'B011103', '只', null);
INSERT INTO `materials` VALUES ('电容器', 'BSMJ-0.4-14Kvar', 'B011104', '只', null);
INSERT INTO `materials` VALUES ('电容器', 'BSMJ-0.4-16Kvar', 'B011105', '只', null);
INSERT INTO `materials` VALUES ('电容器', 'BSMJ-0.4-20Kvar', 'B011106', '只', null);
INSERT INTO `materials` VALUES ('电容器', 'BSMJ-0.4-10Kvar', 'B011107', '只', null);
INSERT INTO `materials` VALUES ('电容就地补偿器', 'TBBX0.4-2kvar', 'B011108', '只', null);
INSERT INTO `materials` VALUES ('电容就地补偿器', 'TBBX0.4-3kvar', 'B011109', '只', null);
INSERT INTO `materials` VALUES ('电容就地补偿器', 'TBBX0.4-4kvar', 'B011110', '只', null);
INSERT INTO `materials` VALUES ('电容就地补偿器', 'TBBX0.4-5kvar', 'B011111', '只', null);
INSERT INTO `materials` VALUES ('电容就地补偿器', 'TBBX0.4-6kvar', 'B011112', '只', null);
INSERT INTO `materials` VALUES ('电容就地补偿器', 'TBBX0.4-7kvar', 'B011113', '只', null);
INSERT INTO `materials` VALUES ('电容就地补偿器', 'TBBX0.4-8kvar', 'B011114', '只', null);
INSERT INTO `materials` VALUES ('电容就地补偿器', 'TBBX0.4-9kvar', 'B011115', '只', null);
INSERT INTO `materials` VALUES ('电容就地补偿器', 'TBBX0.4-10kvar', 'B011116', '只', null);
INSERT INTO `materials` VALUES ('电容就地补偿器', 'TBBX0.4-12kvar', 'B011117', '只', null);
INSERT INTO `materials` VALUES ('其它材料', '', 'B0112', '', null);
INSERT INTO `materials` VALUES ('行线槽', '', 'B011201', '', null);
INSERT INTO `materials` VALUES ('行线槽', '3×30', 'B01120101', 'm', null);
INSERT INTO `materials` VALUES ('行线槽', '5×45', 'B01120102', '条', null);
INSERT INTO `materials` VALUES ('行线槽', '4×40', 'B01120103', '条', null);
INSERT INTO `materials` VALUES ('绕线管', '', 'B011202', '', null);
INSERT INTO `materials` VALUES ('无边绕线管', 'φ8-6', 'B01120201', 'Kg', null);
INSERT INTO `materials` VALUES ('有边绕线管', 'φ8-6', 'B01120202', 'Kg', null);
INSERT INTO `materials` VALUES ('记号管', '', 'B011203', '', null);
INSERT INTO `materials` VALUES ('记号管', 'Φ2.5-Φ4', 'B01120301', '卷', null);
INSERT INTO `materials` VALUES ('记号管', 'Φ6', 'B01120302', '卷', null);
INSERT INTO `materials` VALUES ('记号管', 'LP-1.50', 'B01120303', '圈', null);
INSERT INTO `materials` VALUES ('记号管', 'LP-4', 'B01120304', '卷', null);
INSERT INTO `materials` VALUES ('记号管', 'LP-6', 'B01120305', '卷', null);
INSERT INTO `materials` VALUES ('记号管', 'LP-1.0mm2', 'B01120306', '卷', null);
INSERT INTO `materials` VALUES ('记号管', '10mm2', 'B01120307', '圈', null);
INSERT INTO `materials` VALUES ('记号管', 'LP-15', 'B01120308', '卷', null);
INSERT INTO `materials` VALUES ('线扣', '', 'B011204', '', null);
INSERT INTO `materials` VALUES ('线扣', 'CZ-100', 'B01120401', '袋', null);
INSERT INTO `materials` VALUES ('线扣', 'CZ-200', 'B01120402', '袋', null);
INSERT INTO `materials` VALUES ('线卡', 'Φ6', 'B01120403', '袋', null);
INSERT INTO `materials` VALUES ('钢精轧头', '?', 'B01120404', '包', null);
INSERT INTO `materials` VALUES ('管卡', '4分', 'B01120405', '只', null);
INSERT INTO `materials` VALUES ('线卡', '10#', 'B01120406', '盒', null);
INSERT INTO `materials` VALUES ('线卡', '16#', 'B01120407', '包', null);
INSERT INTO `materials` VALUES ('线卡', '8#', 'B01120408', '盒', null);
INSERT INTO `materials` VALUES ('元线夹', '', 'B011205', '', null);
INSERT INTO `materials` VALUES ('元线夹', '大', 'B01120501', '只', null);
INSERT INTO `materials` VALUES ('元线夹', '中', 'B01120502', '只', null);
INSERT INTO `materials` VALUES ('元线夹', '小', 'B01120503', '只', null);
INSERT INTO `materials` VALUES ('橡皮圈', '', 'B011206', '', null);
INSERT INTO `materials` VALUES ('橡皮圈', 'Φ25', 'B01120601', '只', null);
INSERT INTO `materials` VALUES ('橡皮圈', 'Φ28', 'B01120602', '只', null);
INSERT INTO `materials` VALUES ('橡皮圈', 'Φ30', 'B01120603', '只', null);
INSERT INTO `materials` VALUES ('橡皮圈', 'Φ60', 'B01120604', '只', null);
INSERT INTO `materials` VALUES ('橡皮圈', 'φ70', 'B01120605', '只', null);
INSERT INTO `materials` VALUES ('橡皮圈', 'ф80', 'B01120606', '只', null);
INSERT INTO `materials` VALUES ('橡皮圈', 'φ75', 'B01120607', '只', null);
INSERT INTO `materials` VALUES ('橡胶O型圈', '12×30', 'B01120608', '只', null);
INSERT INTO `materials` VALUES ('橡皮圈', 'φ90', 'B01120609', '只', null);
INSERT INTO `materials` VALUES ('橡皮嵌条', '', 'B011207', '', null);
INSERT INTO `materials` VALUES ('橡皮嵌条', '？', 'B01120701', '米', null);
INSERT INTO `materials` VALUES ('矩形橡皮嵌条', '155×275', 'B01120702', '只', null);
INSERT INTO `materials` VALUES ('密封条', '?', 'B01120703', 'm', null);
INSERT INTO `materials` VALUES ('矩形橡皮嵌条', '130×200', 'B01120704', '只', null);
INSERT INTO `materials` VALUES ('标签框', '', 'B011208', '', null);
INSERT INTO `materials` VALUES ('标签框', '15×50', 'B01120801', '只', null);
INSERT INTO `materials` VALUES ('标签框', '30×80', 'B01120802', '只', null);
INSERT INTO `materials` VALUES ('标鉴框', '18×60', 'B01120803', '只', null);
INSERT INTO `materials` VALUES ('塑料框', '', 'B011209', '', null);
INSERT INTO `materials` VALUES ('塑料框', '90×90', 'B01120901', '只', null);
INSERT INTO `materials` VALUES ('塑料框', '100×200', 'B01120902', '只', null);
INSERT INTO `materials` VALUES ('塑料框', '230×130', 'B01120903', '只', null);
INSERT INTO `materials` VALUES ('塑料框', '80×90', 'B01120904', '只', null);
INSERT INTO `materials` VALUES ('透明塑料框', '80×90', 'B01120905', '只', null);
INSERT INTO `materials` VALUES ('动力表箱框', '500×802', 'B01120906', '只', null);
INSERT INTO `materials` VALUES ('表框', '360×90', 'B01120907', '片', null);
INSERT INTO `materials` VALUES ('表框', '200×90', 'B01120908', '片', null);
INSERT INTO `materials` VALUES ('照明灯具', '', 'B0113', '', null);
INSERT INTO `materials` VALUES ('灯架类', '', 'B011301', '', null);
INSERT INTO `materials` VALUES ('日光灯架', '40W', 'B01130101', '只', null);
INSERT INTO `materials` VALUES ('碘钨灯座', '1000W', 'B01130102', '只', null);
INSERT INTO `materials` VALUES ('日光灯座', '30W', 'B01130103', '付', null);
INSERT INTO `materials` VALUES ('仿比利时灯罩', '？', 'B01130104', '只', null);
INSERT INTO `materials` VALUES ('桶灯灯罩', '？', 'B01130105', '只', null);
INSERT INTO `materials` VALUES ('圆球罩', 'φ350', 'B01130106', '只', null);
INSERT INTO `materials` VALUES ('琵琶式路灯罩', '110W', 'B01130107', '只', null);
INSERT INTO `materials` VALUES ('中波纹罩', '？', 'B01130108', '只', null);
INSERT INTO `materials` VALUES ('白塔松灯罩', '？', 'B01130109', '只', null);
INSERT INTO `materials` VALUES ('元球玻璃罩', 'φ350　', 'B01130110', '只', null);
INSERT INTO `materials` VALUES ('蘑菇罩', '?', 'B01130111', '只', null);
INSERT INTO `materials` VALUES ('高钠路灯罩', 'JTY37-110', 'B01130112', '只', null);
INSERT INTO `materials` VALUES ('高钠路灯罩', 'JTY37-250', 'B01130113', '只', null);
INSERT INTO `materials` VALUES ('高钠路灯罩', 'JTY40-250', 'B01130114', '只', null);
INSERT INTO `materials` VALUES ('高钠路灯罩', 'AUS-790', 'B01130115', '只', null);
INSERT INTO `materials` VALUES ('瓷灯座', 'E27', 'B01130116', '只', null);
INSERT INTO `materials` VALUES ('草坪灯头子', '？', 'B01130117', '只', null);
INSERT INTO `materials` VALUES ('灯泡类', '', 'B011302', '', null);
INSERT INTO `materials` VALUES ('日光灯管', '40W', 'B01130201', '支', null);
INSERT INTO `materials` VALUES ('碘钨灯管', '500W', 'B01130202', '支', null);
INSERT INTO `materials` VALUES ('碘钨灯管', '1000W', 'B01130203', '支', null);
INSERT INTO `materials` VALUES ('罗口灯泡', '100W', 'B01130204', '只', null);
INSERT INTO `materials` VALUES ('罗口灯泡', 'JZ36V-100W', 'B01130205', '只', null);
INSERT INTO `materials` VALUES ('插口灯泡', '60-100W', 'B01130206', '只', null);
INSERT INTO `materials` VALUES ('自镇流高压汞泡', '125W', 'B01130207', '只', null);
INSERT INTO `materials` VALUES ('高压汞灯', '125W', 'B01130208', '只', null);
INSERT INTO `materials` VALUES ('高压汞灯', '250W', 'B01130209', '只', null);
INSERT INTO `materials` VALUES ('高压钠灯', '150W', 'B01130210', '只', null);
INSERT INTO `materials` VALUES ('镇流器类', '', 'B011303', '', null);
INSERT INTO `materials` VALUES ('日光灯镇流器', '40W', 'B01130301', '只', null);
INSERT INTO `materials` VALUES ('高压汞灯镇流器', '125W', 'B01130302', '只', null);
INSERT INTO `materials` VALUES ('高压汞灯镇流器', '250W', 'B01130303', '只', null);
INSERT INTO `materials` VALUES ('高压钠灯镇流器', '150W', 'B01130304', '只', null);
INSERT INTO `materials` VALUES ('高压钠灯镇流器', '250W', 'B01130305', '只', null);
INSERT INTO `materials` VALUES ('触发器', 'CD-2', 'B01130306', '只', null);
INSERT INTO `materials` VALUES ('电子式镇流器', '一拖二', 'B01130307', '只', null);
INSERT INTO `materials` VALUES ('电子式镇流器', '一拖一', 'B01130308', '只', null);
INSERT INTO `materials` VALUES ('钠镇流器触发器', '250W', 'B01130309', '只', null);
INSERT INTO `materials` VALUES ('高压钠灯镇流器', '400W', 'B01130310', '只', null);
INSERT INTO `materials` VALUES ('钠镇流器触发器', '400W', 'B01130311', '只', null);
INSERT INTO `materials` VALUES ('插座、插头类', '', 'B011304', '', null);
INSERT INTO `materials` VALUES ('单相插座', '10A', 'B01130401', '只', null);
INSERT INTO `materials` VALUES ('三相插座', 'AC30-25A', 'B01130402', '只', null);
INSERT INTO `materials` VALUES ('三相四线插座', '15A', 'B01130403', '只', null);
INSERT INTO `materials` VALUES ('三相四线插座', '30A', 'B01130404', '只', null);
INSERT INTO `materials` VALUES ('三相四线插头', '15A', 'B01130405', '只', null);
INSERT INTO `materials` VALUES ('三相四线插头', '30A', 'B01130406', '只', null);
INSERT INTO `materials` VALUES ('单相插头', '10A', 'B01130407', '只', null);
INSERT INTO `materials` VALUES ('三相插头', '16A', 'B01130408', '只', null);
INSERT INTO `materials` VALUES ('普通多用插座', '?', 'B01130409', '只', null);
INSERT INTO `materials` VALUES ('多功能插座', '?', 'B01130410', '只', null);
INSERT INTO `materials` VALUES ('空调插座', '16A', 'B01130411', '套', null);
INSERT INTO `materials` VALUES ('其它类', '', 'B011305', '', null);
INSERT INTO `materials` VALUES ('启辉器', '?', 'B01130501', '只', null);
INSERT INTO `materials` VALUES ('吊扇', '？', 'B01130502', '把', null);
INSERT INTO `materials` VALUES ('小电珠', '3.8V', 'B01130503', '只', null);
INSERT INTO `materials` VALUES ('拉线开关', '250V', 'B01130504', '只', null);
INSERT INTO `materials` VALUES ('灯头', '250V', 'B01130505', '只', null);
INSERT INTO `materials` VALUES ('吊扇吊钩', '？', 'B01130506', '只', null);
INSERT INTO `materials` VALUES ('自镇流汞灯头', '250A', 'B01130507', '只', null);
INSERT INTO `materials` VALUES ('吊扇电容器', '？', 'B01130508', '只', null);
INSERT INTO `materials` VALUES ('吊扇调速器', '？', 'B01130509', '只', null);
INSERT INTO `materials` VALUES ('线令', '？', 'B01130510', '只', null);
INSERT INTO `materials` VALUES ('电子元件', '', 'B0114', '', null);
INSERT INTO `materials` VALUES ('发光二极管', 'R05133S', 'B011401', '只', null);
INSERT INTO `materials` VALUES ('发光二极管', 'R03143S', 'B011402', '只', null);
INSERT INTO `materials` VALUES ('发光二极管', 'YG05133S', 'B011403', '只', null);
INSERT INTO `materials` VALUES ('熔丝夹座', '？', 'B011404', '只', null);
INSERT INTO `materials` VALUES ('可控硅', '0409', 'B011405', '块', null);
INSERT INTO `materials` VALUES ('数码管（红）', 'A0361SR-11', 'B011406', '只', null);
INSERT INTO `materials` VALUES ('可控硅', 'MFC（A）25-16', 'B011407', '只', null);
INSERT INTO `materials` VALUES ('散热片', '90×40×300', 'B011408', '片', null);
INSERT INTO `materials` VALUES ('可控硅', 'MFC（A）40-16', 'B011409', '只', null);
INSERT INTO `materials` VALUES ('可控硅', 'MFC（A）55-16', 'B011410', '只', null);
INSERT INTO `materials` VALUES ('10KV高压电器', '', 'B0115', '', null);
INSERT INTO `materials` VALUES ('带电显示器', 'GSN1-10Q', 'B011501', '只', null);
INSERT INTO `materials` VALUES ('传感器', 'CGS-10KV', 'B011502', '只', null);
INSERT INTO `materials` VALUES ('户内电磁锁', 'DSN2-ЩAC220V', 'B011503', '只', null);
INSERT INTO `materials` VALUES ('高压熔断器', 'SFLAJ-10/50', 'B011504', '只', null);
INSERT INTO `materials` VALUES ('高压互感器', 'LZJC-10-50/5', 'B011505', '只', null);
INSERT INTO `materials` VALUES ('高压互感器', 'LZJC-10-75/5', 'B011506', '只', null);
INSERT INTO `materials` VALUES ('高压负荷开关', 'FN5-10/630', 'B011507', '把', null);
INSERT INTO `materials` VALUES ('五防锁', '?', 'B011508', '套', null);
INSERT INTO `materials` VALUES ('高压互感器', 'LZJC-10-100/3', 'B011509', '只', null);
INSERT INTO `materials` VALUES ('高压互感器', 'LZJC-10-300/5', 'B011510', '只', null);
INSERT INTO `materials` VALUES ('高压熔断器(座)', 'XRNT-12/80', 'B011511', '只', null);
INSERT INTO `materials` VALUES ('高压熔管', 'SFLAJ-12/80', 'B011512', '只', null);
INSERT INTO `materials` VALUES ('高压互感器', 'LZJC-10-150/5', 'B011513', '只', null);
INSERT INTO `materials` VALUES ('自动开关（二）', '', 'B0116', '', null);
INSERT INTO `materials` VALUES ('塑壳断路器RM（五）', '', 'B011601', '', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'RMM1-63H/3200 63A', 'B01160101', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'RMM1-100H/4300 63A', 'B01160102', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'RMM1-100H/3200 100A', 'B01160103', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'RMM1-100H/4300 100A', 'B01160104', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'RMM1-250H/4300 100A', 'B01160105', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'RMM1-250H/4300 125A', 'B01160106', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'RMM1-250H/4300 160A', 'B01160107', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'RMM1-250H/4310 160A(AC220V)', 'B01160108', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'RMM1-250H/4300 250A', 'B01160109', '只', null);
INSERT INTO `materials` VALUES ('塑壳断路器', 'RMM1-400H/4310 250A(AC220V)', 'B01160110', '只', null);
INSERT INTO `materials` VALUES ('断路器CKB（六）', '', 'B011602', '', null);
INSERT INTO `materials` VALUES ('断路器', 'CKB60-C.1P.25A', 'B01160201', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKB60-C.1P.40A', 'B01160202', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKB60-C.3P.16A', 'B01160203', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKB60-C.3P.20A', 'B01160204', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKB60-C.3P.32A', 'B01160205', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKB60-C.3P.40A', 'B01160206', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKB60-100.C.3P.80A', 'B01160207', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKM33-100S/3300 80A', 'B01160208', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKM33-160S/4300 160A', 'B01160209', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKM33-400H/3340 400A', 'B01160210', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKM33-630H/3340 630A', 'B01160211', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKM33-100S/3300 40A', 'B01160212', '只', null);
INSERT INTO `materials` VALUES ('断路器（七）', '', 'B011603', '', null);
INSERT INTO `materials` VALUES ('断路器', 'DZ20J-400/3300 250A', 'B01160301', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'DZ20J-630/3300 630A', 'B01160302', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'YSM1-225S/3300 225A', 'B01160303', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'YSM1-225S/3300 100A', 'B01160304', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'CKM33-400H/3310 400A', 'B01160305', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'ZM30E-225/3300 100A', 'B01160306', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'ZM30E-225/3300 150A', 'B01160307', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'ZM30E-630/3300 630A', 'B01160308', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'ZM30E-630/3300 500A', 'B01160309', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'YSM1-400S/3310 400A', 'B01160310', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'YSM1-225S/3310 100A', 'B01160311', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'YSM3-250S/3300 225A', 'B01160312', '只', null);
INSERT INTO `materials` VALUES ('断路器NLM1（八）', '', 'B011604', '', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-630M/3300 630A', 'B01160401', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-400M/3300 400A', 'B01160402', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-225L/3300 160A', 'B01160403', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-630L/3300 500A', 'B01160404', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-630H/3300 630A', 'B01160405', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-630M/3340 630A', 'B01160406', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-225C/3300 200A', 'B01160407', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-225C/3300 160A', 'B01160408', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-400C/3300 315A', 'B01160409', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-400C/3300 250A', 'B01160410', '只', null);
INSERT INTO `materials` VALUES ('断路器', 'NLM1-400C/3300 400A', 'B01160411', '只', null);
INSERT INTO `materials` VALUES ('钢材料', '', 'B02', '', null);
INSERT INTO `materials` VALUES ('角钢', '', 'B0201', '', null);
INSERT INTO `materials` VALUES ('角钢(大钢厂）', '∠30×3', 'B020101', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠40×4', 'B020102', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠50×5', 'B020103', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠60×6', 'B020104', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠70×7', 'B020105', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠80×8', 'B020106', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠40×3', 'B020107', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠56×5', 'B020108', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠63×6', 'B020109', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠75×6', 'B020111', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠80×6', 'B020112', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠36×4', 'B020113', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠45×4', 'B020114', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠45×5', 'B020115', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠63×5', 'B020116', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '边角料', 'B020117', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠35×5', 'B020118', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠60×5', 'B020119', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠65×6', 'B020120', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠40×5', 'B020121', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠50×4', 'B020122', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠75×8', 'B020123', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠25×25', 'B020124', 'kg', null);
INSERT INTO `materials` VALUES ('角钢', '∠100×10', 'B020125', 'kg', null);
INSERT INTO `materials` VALUES ('角钢（小钢厂）', '∠30×3', 'B020126', 'kg', null);
INSERT INTO `materials` VALUES ('角钢(小钢厂)', '∠5×50', 'B020127', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢', '', 'B0202', '', null);
INSERT INTO `materials` VALUES ('扁钢', '-3×30', 'B020201', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢', '-4×40', 'B020202', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢', '-5×50', 'B020203', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢', '-6×60', 'B020204', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢', '-8×80', 'B020205', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢', '-8×100', 'B020206', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢', '-4×20', 'B020207', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢', '-40×20', 'B020208', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢', '-6×40', 'B020209', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢（小厂）', '-3*30', 'B020210', 'kg', null);
INSERT INTO `materials` VALUES ('扁钢(小钢厂)', '-4*40', 'B020211', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', '', 'B0203', '', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ6.5', 'B020301', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ8', 'B020302', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ10', 'B020303', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ12', 'B020304', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ14', 'B020305', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ16', 'B020306', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ18', 'B020307', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ20', 'B020308', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ22', 'B020309', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ25', 'B020310', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ28', 'B020311', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ30', 'B020312', 'kg', null);
INSERT INTO `materials` VALUES ('圆钢', 'Φ36', 'B020313', 'kg', null);
INSERT INTO `materials` VALUES ('钢管', '', 'B0204', '', null);
INSERT INTO `materials` VALUES ('焊管', 'Φ20', 'B020401', 'kg', null);
INSERT INTO `materials` VALUES ('焊管', 'Φ27', 'B020402', 'kg', null);
INSERT INTO `materials` VALUES ('焊管', 'Φ34', 'B020403', 'kg', null);
INSERT INTO `materials` VALUES ('焊管', 'Φ42', 'B020404', 'kg', null);
INSERT INTO `materials` VALUES ('焊管', 'Φ48', 'B020405', 'kg', null);
INSERT INTO `materials` VALUES ('焊管', 'Φ60', 'B020406', 'kg', null);
INSERT INTO `materials` VALUES ('焊管', 'Φ89', 'B020407', 'kg', null);
INSERT INTO `materials` VALUES ('焊管', 'Φ114(65.0kg/支)', 'B020408', 'kg', null);
INSERT INTO `materials` VALUES ('焊管', 'Φ165', 'B020409', 'kg', null);
INSERT INTO `materials` VALUES ('镀锌管', 'Φ21', 'B020410', 'kg', null);
INSERT INTO `materials` VALUES ('钢钣', '', 'B0205', '', null);
INSERT INTO `materials` VALUES ('热板', '', 'B020501', '', null);
INSERT INTO `materials` VALUES ('热板', '0.80', 'B02050102', 'kg', null);
INSERT INTO `materials` VALUES ('热板', '1.00', 'B02050103', 'kg', null);
INSERT INTO `materials` VALUES ('热板', '1.20', 'B02050104', 'kg', null);
INSERT INTO `materials` VALUES ('热板', '1.50m/m', 'B02050105', 'kg', null);
INSERT INTO `materials` VALUES ('热板', '2.00m/m', 'B02050106', 'kg', null);
INSERT INTO `materials` VALUES ('热板', '2.50m/m', 'B02050107', 'kg', null);
INSERT INTO `materials` VALUES ('热板（钢板网）', '?', 'B02050108', 'm2', null);
INSERT INTO `materials` VALUES ('冷板', '', 'B020502', '', null);
INSERT INTO `materials` VALUES ('冷板', '1.20', 'B02050201', 'kg', null);
INSERT INTO `materials` VALUES ('冷板', '1.50', 'B02050202', 'kg', null);
INSERT INTO `materials` VALUES ('冷板', '1.00', 'B02050203', 'kg', null);
INSERT INTO `materials` VALUES ('冷板', '2.5', 'B02050204', 'kg', null);
INSERT INTO `materials` VALUES ('冷板', '0.5m/m', 'B02050205', 'kg', null);
INSERT INTO `materials` VALUES ('冷板', '2.0mm', 'B02050206', 'kg', null);
INSERT INTO `materials` VALUES ('中板', '', 'B020503', '', null);
INSERT INTO `materials` VALUES ('中板', '-3', 'B02050301', 'kg', null);
INSERT INTO `materials` VALUES ('中板', '-4', 'B02050302', 'kg', null);
INSERT INTO `materials` VALUES ('中板', '-5', 'B02050303', 'kg', null);
INSERT INTO `materials` VALUES ('中板', '-6', 'B02050304', 'kg', null);
INSERT INTO `materials` VALUES ('中板', '8', 'B02050305', 'kg', null);
INSERT INTO `materials` VALUES ('中板', '10', 'B02050306', 'kg', null);
INSERT INTO `materials` VALUES ('中板', '12', 'B02050307', 'kg', null);
INSERT INTO `materials` VALUES ('中板', '14', 'B02050308', 'kg', null);
INSERT INTO `materials` VALUES ('中板', '16', 'B02050309', 'kg', null);
INSERT INTO `materials` VALUES ('中板', '20', 'B02050310', 'kg', null);
INSERT INTO `materials` VALUES ('钢板网', '？', 'B02050311', 'm2', null);
INSERT INTO `materials` VALUES ('中板', '-35m/m', 'B02050312', 'kg', null);
INSERT INTO `materials` VALUES ('花纹板', '4.0', 'B02050313', 'kg', null);
INSERT INTO `materials` VALUES ('铝板', '', 'B020504', '', null);
INSERT INTO `materials` VALUES ('铝钣', '2', 'B02050401', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢板', '', 'B020505', '', null);
INSERT INTO `materials` VALUES ('不锈钢板', '1.5mm', 'B02050501', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢板', '1mm', 'B02050502', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢板', '4.00mm', 'B02050503', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢板', '2m/m', 'B02050504', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢板', '0.5m/m', 'B02050505', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢板', '0.8m/m', 'B02050506', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢焊角片', '?', 'B02050507', '片', null);
INSERT INTO `materials` VALUES ('不锈钢板', '1.2m/m', 'B02050508', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢板', '0.7m/m', 'B02050509', 'kg', null);
INSERT INTO `materials` VALUES ('镜面板', '1.00m/m', 'B02050510', 'kg', null);
INSERT INTO `materials` VALUES ('镜面板', '0.7m/m', 'B02050511', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢板', '10.00m/m', 'B02050512', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢板', '1.00m/m', 'B02050513', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢板', '0.6m/m', 'B02050514', '张', null);
INSERT INTO `materials` VALUES ('镀锌板', '', 'B020506', '', null);
INSERT INTO `materials` VALUES ('镀锌板', '0.5', 'B02050601', 'kg', null);
INSERT INTO `materials` VALUES ('镀锌板', '1mm', 'B02050602', 'kg', null);
INSERT INTO `materials` VALUES ('镀锌板', '0.8mm', 'B02050603', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢球', '', 'B020507', '', null);
INSERT INTO `materials` VALUES ('不锈钢球', 'φ80', 'B02050701', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢球', 'φ90', 'B02050702', '只', null);
INSERT INTO `materials` VALUES ('不锈钢球', 'φ32', 'B02050703', '只', null);
INSERT INTO `materials` VALUES ('不锈钢座', 'φ25', 'B02050704', '只', null);
INSERT INTO `materials` VALUES ('不锈钢座', 'φ76', 'B02050705', '只', null);
INSERT INTO `materials` VALUES ('不锈钢球', 'φ114', 'B02050706', '只', null);
INSERT INTO `materials` VALUES ('菱形', '230mm', 'B02050707', '只', null);
INSERT INTO `materials` VALUES ('不锈钢弯头', 'φ63', 'B02050708', '只', null);
INSERT INTO `materials` VALUES ('不锈钢球座', 'φ63', 'B02050709', '只', null);
INSERT INTO `materials` VALUES ('不锈钢盖', 'φ63', 'B02050710', '只', null);
INSERT INTO `materials` VALUES ('不锈钢盖', 'φ38', 'B02050711', '只', null);
INSERT INTO `materials` VALUES ('不锈钢弯头', 'φ38', 'B02050712', '只', null);
INSERT INTO `materials` VALUES ('废料', '', 'B020508', '', null);
INSERT INTO `materials` VALUES ('薄板', '多规格', 'B02050801', 'kg', null);
INSERT INTO `materials` VALUES ('槽钢', '', 'B0206', '', null);
INSERT INTO `materials` VALUES ('槽钢', '[63', 'B020601', 'kg', null);
INSERT INTO `materials` VALUES ('槽钢', '[80', 'B020602', 'kg', null);
INSERT INTO `materials` VALUES ('槽钢', '[100', 'B020603', 'kg', null);
INSERT INTO `materials` VALUES ('槽钢', '[120', 'B020604', 'kg', null);
INSERT INTO `materials` VALUES ('槽钢', '[140', 'B020605', 'kg', null);
INSERT INTO `materials` VALUES ('槽钢', '[50', 'B020606', 'kg', null);
INSERT INTO `materials` VALUES ('槽钢', '［6.3］', 'B020607', 'kg', null);
INSERT INTO `materials` VALUES ('工字钢', '10#', 'B020608', 'kg', null);
INSERT INTO `materials` VALUES ('H型钢', '15#', 'B020609', 'kg', null);
INSERT INTO `materials` VALUES ('C型钢', '100*45*20', 'B020610', 'kg', null);
INSERT INTO `materials` VALUES ('工字钢', '18#', 'B020611', 'kg', null);
INSERT INTO `materials` VALUES ('轻轨', '8kg', 'B020612', 'kg', null);
INSERT INTO `materials` VALUES ('槽钢', '[14]', 'B020613', 'kg', null);
INSERT INTO `materials` VALUES ('槽钢', '[8', 'B020614', 'kg', null);
INSERT INTO `materials` VALUES ('模具钢', '', 'B0207', '', null);
INSERT INTO `materials` VALUES ('模具钢', '？', 'B020701', 'kg', null);
INSERT INTO `materials` VALUES ('45#碳板', '30m/m', 'B020702', 'kg', null);
INSERT INTO `materials` VALUES ('五金材料', '', 'B03', '', null);
INSERT INTO `materials` VALUES ('紧固件', '', 'B0301', '', null);
INSERT INTO `materials` VALUES ('木螺丝', '', 'B030101', '', null);
INSERT INTO `materials` VALUES ('木螺丝', 'M3×16', 'B03010101', '只', null);
INSERT INTO `materials` VALUES ('木螺丝', 'M4×30', 'B03010102', '只', null);
INSERT INTO `materials` VALUES ('木螺丝', 'M4×40', 'B03010103', '只', null);
INSERT INTO `materials` VALUES ('木螺丝', 'M4.5×60', 'B03010104', '只', null);
INSERT INTO `materials` VALUES ('胶木螺丝', '?', 'B03010105', '只', null);
INSERT INTO `materials` VALUES ('木螺丝', 'M4×50', 'B03010106', '只', null);
INSERT INTO `materials` VALUES ('木螺丝', 'M5×50', 'B03010107', '只', null);
INSERT INTO `materials` VALUES ('木螺丝', 'M5×40', 'B03010108', '只', null);
INSERT INTO `materials` VALUES ('木螺丝', 'M3.5×25', 'B03010109', '', null);
INSERT INTO `materials` VALUES ('其它', '', 'B030102', '', null);
INSERT INTO `materials` VALUES ('铜鼻螺丝', '?', 'B03010201', '只', null);
INSERT INTO `materials` VALUES ('自攻螺丝', 'M4×20', 'B03010202', '只', null);
INSERT INTO `materials` VALUES ('热镀锌元头脚钉', 'M16×160', 'B03010203', '只', null);
INSERT INTO `materials` VALUES ('自攻螺丝', 'M4×30', 'B03010204', '百只', null);
INSERT INTO `materials` VALUES ('三角螺丝', 'M8', 'B03010205', '只', null);
INSERT INTO `materials` VALUES ('三角螺丝', 'M10×25', 'B03010206', '只', null);
INSERT INTO `materials` VALUES ('圆头自攻螺丝', 'M3×6', 'B03010207', '千只', null);
INSERT INTO `materials` VALUES ('圆头自攻螺丝', 'M3×10', 'B03010208', '千只', null);
INSERT INTO `materials` VALUES ('圆头自攻螺丝', 'M4×14', 'B03010209', '只', null);
INSERT INTO `materials` VALUES ('沉头螺丝', 'M2×5', 'B03010210', '千只', null);
INSERT INTO `materials` VALUES ('平机螺丝', '', 'B030103', '', null);
INSERT INTO `materials` VALUES ('平机螺丝', 'M4×20', 'B03010301', '只', null);
INSERT INTO `materials` VALUES ('平机螺丝', 'M6×25', 'B03010302', '只', null);
INSERT INTO `materials` VALUES ('平机螺丝', 'M10×35', 'B03010303', '只', null);
INSERT INTO `materials` VALUES ('平机螺丝', 'M5×15', 'B03010304', '只', null);
INSERT INTO `materials` VALUES ('平机螺丝', 'M8×10', 'B03010305', '只', null);
INSERT INTO `materials` VALUES ('平机螺丝', 'M8×25', 'B03010306', '只', null);
INSERT INTO `materials` VALUES ('平机螺丝', 'M5×30', 'B03010307', '只', null);
INSERT INTO `materials` VALUES ('平机螺丝', 'M8×20', 'B03010308', '百只', null);
INSERT INTO `materials` VALUES ('平机螺丝', '(M12×60)', 'B03010309', '只', null);
INSERT INTO `materials` VALUES ('平机罗丝', 'M12×50', 'B03010310', '只', null);
INSERT INTO `materials` VALUES ('平机罗丝', 'M5*10', 'B03010311', '百只', null);
INSERT INTO `materials` VALUES ('平机罗丝', '4*10', 'B03010312', '千只', null);
INSERT INTO `materials` VALUES ('平机罗丝', 'M4*6', 'B03010313', '千只', null);
INSERT INTO `materials` VALUES ('平机罗丝', 'M4*8', 'B03010314', '千只', null);
INSERT INTO `materials` VALUES ('平机罗丝', '12×40', 'B03010315', '只', null);
INSERT INTO `materials` VALUES ('膨胀螺丝', '', 'B030104', '', null);
INSERT INTO `materials` VALUES ('膨胀螺丝', 'M10×80', 'B03010401', '只', null);
INSERT INTO `materials` VALUES ('膨胀螺丝', 'M10×100', 'B03010402', '只', null);
INSERT INTO `materials` VALUES ('膨胀螺丝', 'M16×150', 'B03010403', '只', null);
INSERT INTO `materials` VALUES ('膨胀螺丝', 'M12×70', 'B03010404', '只', null);
INSERT INTO `materials` VALUES ('塑料膨胀螺丝', 'φ6', 'B03010405', '只', null);
INSERT INTO `materials` VALUES ('塑料膨胀螺丝', 'φ8', 'B03010406', '只', null);
INSERT INTO `materials` VALUES ('膨胀螺丝', 'φ8', 'B03010407', '只', null);
INSERT INTO `materials` VALUES ('膨胀螺丝', 'φ6×60', 'B03010408', '只', null);
INSERT INTO `materials` VALUES ('膨胀螺丝', 'M12×150', 'B03010409', '只', null);
INSERT INTO `materials` VALUES ('膨胀螺丝', 'M8×60', 'B03010410', '只', null);
INSERT INTO `materials` VALUES ('抽芯铝铆钉', '', 'B030105', '', null);
INSERT INTO `materials` VALUES ('抽芯铝铆钉', 'M4×13', 'B03010501', '只', null);
INSERT INTO `materials` VALUES ('抽芯铝铆钉', 'Φ2.2×0.1', 'B03010502', '只', null);
INSERT INTO `materials` VALUES ('抽芯铝铆钉', '3*5', 'B03010503', 'kg', null);
INSERT INTO `materials` VALUES ('抽芯铝铆钉', '5*13', 'B03010504', '千只', null);
INSERT INTO `materials` VALUES ('抽芯铝铆钉', '4*16', 'B03010505', '盒', null);
INSERT INTO `materials` VALUES ('抽芯铝铆钉', '5×20', 'B03010506', '盒', null);
INSERT INTO `materials` VALUES ('六角螺丝', '', 'B030106', '', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M5×25', 'B03010601', '百只', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M5×35', 'B03010602', '百只', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M5×40', 'B03010603', '百只', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M6×20', 'B03010604', '百只', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M6×25', 'B03010605', '百只', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M6×30', 'B03010606', '百只', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M6×35', 'B03010607', '百只', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M8×16', 'B03010608', '百只', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M8×20', 'B03010609', '百只', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M8×25', 'B03010610', '百只', null);
INSERT INTO `materials` VALUES ('六角螺丝', 'M8×30', 'B03010611', '百只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', '', 'B030107', '', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M16×300', 'B03010701', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M12×35', 'B03010702', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M12×60', 'B03010703', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M14×65', 'B03010704', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M16×220', 'B03010705', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M16×35', 'B03010706', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M16×45', 'B03010707', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M16×55', 'B03010708', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M16×80', 'B03010709', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M16×90', 'B03010710', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M16×400', 'B03010711', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M18×40', 'B03010712', '只', null);
INSERT INTO `materials` VALUES ('热镀锌螺丝', 'M18×90', 'B03010713', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', '', 'B030109', '', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M6×25', 'B03010901', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M8×25', 'B03010902', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M10×80', 'B03010903', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M12×30', 'B03010904', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M12×55', 'B03010905', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M8×16', 'B03010906', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M10×40', 'B03010907', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M8×35', 'B03010908', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M10×25', 'B03010909', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M12×80', 'B03010910', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M16×40', 'B03010911', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M8×50', 'B03010912', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M10×50', 'B03010913', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M12×50', 'B03010914', '只', null);
INSERT INTO `materials` VALUES ('不锈钢内六角平机螺丝', 'M10×16', 'B03010915', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M5×20', 'B03010916', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M12×40', 'B03010917', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M10×30', 'B03010918', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M12×60', 'B03010919', '只', null);
INSERT INTO `materials` VALUES ('内六角螺丝', 'M6×45', 'B03010920', '只', null);
INSERT INTO `materials` VALUES ('内六角罗丝', 'M12×25', 'B03010921', '只', null);
INSERT INTO `materials` VALUES ('内六角罗丝', 'M8×20', 'B03010922', '只', null);
INSERT INTO `materials` VALUES ('内六角罗丝', 'M8*60', 'B03010923', '只', null);
INSERT INTO `materials` VALUES ('紧定不锈钢内六角罗丝', '8*10', 'B03010924', '百只', null);
INSERT INTO `materials` VALUES ('焊接材料', '', 'B0302', '', null);
INSERT INTO `materials` VALUES ('焊接材料', '', 'B030201', '', null);
INSERT INTO `materials` VALUES ('电焊条', 'φ1.4', 'B03020101', 'kg', null);
INSERT INTO `materials` VALUES ('电焊条', 'φ2.5', 'B03020102', 'kg', null);
INSERT INTO `materials` VALUES ('电焊条', 'φ3.2', 'B03020103', 'kg', null);
INSERT INTO `materials` VALUES ('电焊条', 'φ4', 'B03020104', 'kg', null);
INSERT INTO `materials` VALUES ('铜焊条', 'φ3', 'B03020105', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢焊丝', '2.5', 'B03020106', 'kg', null);
INSERT INTO `materials` VALUES ('镀锌铁丝', '14#', 'B03020107', 'kg', null);
INSERT INTO `materials` VALUES ('镀锌铁丝', '8#', 'B03020108', 'kg', null);
INSERT INTO `materials` VALUES ('不锈钢焊条', 'AB2-2.5', 'B03020109', 'kg', null);
INSERT INTO `materials` VALUES ('钨极棒', '?', 'B03020110', '支', null);
INSERT INTO `materials` VALUES ('铜焊粉', '?', 'B03020111', '瓶', null);
INSERT INTO `materials` VALUES ('不锈钢焊丝', '1.00m/m', 'B03020112', 'kg', null);
INSERT INTO `materials` VALUES ('焊接器具', '', 'B030202', '', null);
INSERT INTO `materials` VALUES ('电焊钳', '500A', 'B03020201', '把', null);
INSERT INTO `materials` VALUES ('电焊钳', '300A', 'B03020202', '把', null);
INSERT INTO `materials` VALUES ('焊枪', 'H01-6', 'B03020203', '把', null);
INSERT INTO `materials` VALUES ('等离子割嘴瓷套', '?', 'B03020204', '只', null);
INSERT INTO `materials` VALUES ('焊嘴', 'H01-6  2号', 'B03020205', '只', null);
INSERT INTO `materials` VALUES ('割嘴', 'G01-30', 'B03020206', '只', null);
INSERT INTO `materials` VALUES ('割枪', 'G01-30', 'B03020207', '把', null);
INSERT INTO `materials` VALUES ('电焊手套', '?', 'B03020208', '双', null);
INSERT INTO `materials` VALUES ('电焊护目镜片', '?', 'B03020209', '片', null);
INSERT INTO `materials` VALUES ('电烙铁', '100W', 'B03020210', '只', null);
INSERT INTO `materials` VALUES ('氩弧焊枪', '160', 'B03020211', '把', null);
INSERT INTO `materials` VALUES ('电焊护脚', '？', 'B03020212', '付', null);
INSERT INTO `materials` VALUES ('乙炔发生器', 'YTP-1M3', 'B03020213', '只', null);
INSERT INTO `materials` VALUES ('等离子切割机割嘴', '/', 'B03020214', '付', null);
INSERT INTO `materials` VALUES ('塑料焊枪', '？', 'B03020215', '把', null);
INSERT INTO `materials` VALUES ('点焊机头子', '16KW', 'B03020216', '只', null);
INSERT INTO `materials` VALUES ('氩弧焊枪瓷嘴', '？', 'B03020217', '只', null);
INSERT INTO `materials` VALUES ('枪嘴、铜夹、枪尾', '?', 'B03020218', '套', null);
INSERT INTO `materials` VALUES ('氩弧焊枪开关', 'kw4-02', 'B03020220', '只', null);
INSERT INTO `materials` VALUES ('机床附件', '', 'B0303', '', null);
INSERT INTO `materials` VALUES ('钻头套', '2-3', 'B030301', '只', null);
INSERT INTO `materials` VALUES ('钻头夹', 'HOTS3-16', 'B030302', '只', null);
INSERT INTO `materials` VALUES ('钻轧头', 'JS13m/m', 'B030303', '只', null);
INSERT INTO `materials` VALUES ('钻轧头钥匙', '?', 'B030304', '只', null);
INSERT INTO `materials` VALUES ('螺纹钻夹头', '10m/m', 'B030305', '只', null);
INSERT INTO `materials` VALUES ('钻轧头', '6m/m', 'B030306', '只', null);
INSERT INTO `materials` VALUES ('钻夹头连接杆', '4＃', 'B030307', '支', null);
INSERT INTO `materials` VALUES ('钻夹头连接杆', '2〃', 'B030308', '只', null);
INSERT INTO `materials` VALUES ('钻夹头', '1.5-13m/m', 'B030309', '只', null);
INSERT INTO `materials` VALUES ('钻夹头连杆', '3#', 'B030310', '支', null);
INSERT INTO `materials` VALUES ('钻夹头', 'JS10', 'B030311', '只', null);
INSERT INTO `materials` VALUES ('起重器材及液压机具', '', 'B0304', '', null);
INSERT INTO `materials` VALUES ('钢丝绳', '?', 'B030401', '米', null);
INSERT INTO `materials` VALUES ('钢丝夹头', 'Φ6', 'B030402', '只', null);
INSERT INTO `materials` VALUES ('液压搬运车', '2T', 'B030403', '台', null);
INSERT INTO `materials` VALUES ('千斤顶', '20T', 'B030404', '台', null);
INSERT INTO `materials` VALUES ('液压开孔器', 'SYK-8型', 'B030405', '把', null);
INSERT INTO `materials` VALUES ('翻斗车', '？', 'B030406', '', null);
INSERT INTO `materials` VALUES ('手动葫芦', '1.5T', 'B030407', '台', null);
INSERT INTO `materials` VALUES ('常用手工具', '', 'B0305', '', null);
INSERT INTO `materials` VALUES ('木榔头', '？', 'B030501', '只', null);
INSERT INTO `materials` VALUES ('扁榔头(小铁锤)', '？', 'B030502', '只', null);
INSERT INTO `materials` VALUES ('电工刀', '？', 'B030503', '把', null);
INSERT INTO `materials` VALUES ('压线钳', '？', 'B030504', '把', null);
INSERT INTO `materials` VALUES ('斜口钳', '？', 'B030505', '把', null);
INSERT INTO `materials` VALUES ('钢丝钳', '？', 'B030506', '把', null);
INSERT INTO `materials` VALUES ('尖嘴钳', '？', 'B030507', '把', null);
INSERT INTO `materials` VALUES ('剥线钳', '？', 'B030508', '把', null);
INSERT INTO `materials` VALUES ('呆板手', '8×10', 'B030509', '把', null);
INSERT INTO `materials` VALUES ('呆板手', '12×14', 'B030510', '把', null);
INSERT INTO `materials` VALUES ('呆板手', '14×17', 'B030511', '把', null);
INSERT INTO `materials` VALUES ('活动板手', '80mm', 'B030512', '把', null);
INSERT INTO `materials` VALUES ('活动板手', '250mm', 'B030513', '把', null);
INSERT INTO `materials` VALUES ('钳工工具', '', 'B0306', '', null);
INSERT INTO `materials` VALUES ('铝板锉', '10号', 'B030601', '把', null);
INSERT INTO `materials` VALUES ('半元锉', '12号', 'B030602', '把', null);
INSERT INTO `materials` VALUES ('中扁锉', '12号', 'B030603', '把', null);
INSERT INTO `materials` VALUES ('元锉', '3号', 'B030604', '把', null);
INSERT INTO `materials` VALUES ('台板锉', '？', 'B030605', '把', null);
INSERT INTO `materials` VALUES ('钢锯架', '？', 'B030606', '把', null);
INSERT INTO `materials` VALUES ('钢锯条', '？', 'B030607', '把', null);
INSERT INTO `materials` VALUES ('三角锉', '6寸', 'B030608', '把', null);
INSERT INTO `materials` VALUES ('平锉', '12#', 'B030609', '把', null);
INSERT INTO `materials` VALUES ('平锉', '10号', 'B030610', '把', null);
INSERT INTO `materials` VALUES ('半圆锉', '10号', 'B030611', '把', null);
INSERT INTO `materials` VALUES ('半圆锉', '6号', 'B030612', '把', null);
INSERT INTO `materials` VALUES ('圆锉', '6#', 'B030613', '把', null);
INSERT INTO `materials` VALUES ('元锉', '12寸', 'B030614', '把', null);
INSERT INTO `materials` VALUES ('圆锉', '10\"', 'B030615', '把', null);
INSERT INTO `materials` VALUES ('测量工具', '', 'B0308', '', null);
INSERT INTO `materials` VALUES ('钢直尺', '500mm', 'B030801', '把', null);
INSERT INTO `materials` VALUES ('角尺', '300mm', 'B030802', '把', null);
INSERT INTO `materials` VALUES ('卷尺', '20m', 'B030803', '把', null);
INSERT INTO `materials` VALUES ('卷尺', '5m', 'B030804', '把', null);
INSERT INTO `materials` VALUES ('电笔', '？', 'B030805', '支', null);
INSERT INTO `materials` VALUES ('钢直尺', '1000mm', 'B030806', '把', null);
INSERT INTO `materials` VALUES ('卷尺', '3000mm', 'B030807', '把', null);
INSERT INTO `materials` VALUES ('游标卡尺', '300m/m', 'B030808', '把', null);
INSERT INTO `materials` VALUES ('游标卡尺', '150m/m', 'B030809', '把', null);
INSERT INTO `materials` VALUES ('氖管', '？', 'B030810', '只', null);
INSERT INTO `materials` VALUES ('塞尺', '0-3', 'B030811', '把', null);
INSERT INTO `materials` VALUES ('温控仪', 'TDA-8001/0-400℃', 'B030812', '只', null);
INSERT INTO `materials` VALUES ('温度计', '0-300℃', 'B030813', '支', null);
INSERT INTO `materials` VALUES ('卷尺', '10M', 'B030814', '把', null);
INSERT INTO `materials` VALUES ('波美度计', '？', 'B030815', '支', null);
INSERT INTO `materials` VALUES ('压力表', 'Y40-1MPa', 'B030816', '只', null);
INSERT INTO `materials` VALUES ('温度计', 'ZC1-4', 'B030817', '只', null);
INSERT INTO `materials` VALUES ('温控仪', 'XMTD-20010-599℃', 'B030818', '只', null);
INSERT INTO `materials` VALUES ('钢卷尺', '3.5m', 'B030819', '把', null);
INSERT INTO `materials` VALUES ('钢卷尺', '10m', 'B030820', '把', null);
INSERT INTO `materials` VALUES ('钢卷尺', '7.5m', 'B030821', '把', null);
INSERT INTO `materials` VALUES ('磅秤', 'TGT-500型', 'B030822', '台', null);
INSERT INTO `materials` VALUES ('电动工具', '', 'B0309', '', null);
INSERT INTO `materials` VALUES ('冲击电锤', 'DH-25', 'B030901', '只', null);
INSERT INTO `materials` VALUES ('磨光机', '100型', 'B030902', '只', null);
INSERT INTO `materials` VALUES ('磨光机', '125型', 'B030903', '只', null);
INSERT INTO `materials` VALUES ('电钻', 'JLZ-10', 'B030904', '只', null);
INSERT INTO `materials` VALUES ('日立调速手电钻', 'FD10BA', 'B030905', '台', null);
INSERT INTO `materials` VALUES ('一字十字批钻头', '？', 'B030906', '支', null);
INSERT INTO `materials` VALUES ('自吸清水泵', '550W', 'B030907', '台', null);
INSERT INTO `materials` VALUES ('电动螺丝刀', 'HX902', 'B030908', '套', null);
INSERT INTO `materials` VALUES ('切割机', 'DW871', 'B030909', '台', null);
INSERT INTO `materials` VALUES ('磨光机', '180型', 'B030910', '只', null);
INSERT INTO `materials` VALUES ('钻攻两用台钻', 'ZS-4112', 'B030911', '台', null);
INSERT INTO `materials` VALUES ('云石切割机', '110', 'B030912', '台', null);
INSERT INTO `materials` VALUES ('吸粉泵', '?', 'B030913', '只', null);
INSERT INTO `materials` VALUES ('得力气动磨光机', '100mm', 'B030914', '只', null);
INSERT INTO `materials` VALUES ('日立手电钻', '10VA', 'B030915', '把', null);
INSERT INTO `materials` VALUES ('自吸清水泵', '750W', 'B030916', '台', null);
INSERT INTO `materials` VALUES ('卧式风扇', '400MM/380', 'B030917', '台', null);
INSERT INTO `materials` VALUES ('电动机', '550W/380v', 'B030918', '台', null);
INSERT INTO `materials` VALUES ('手电钻锁子', '？', 'B030919', '只', null);
INSERT INTO `materials` VALUES ('砂带机', '2250*300', 'B030920', '台', null);
INSERT INTO `materials` VALUES ('门窗附件', '', 'B0310', '', null);
INSERT INTO `materials` VALUES ('开关锁(无柄)', '401', 'B031001', '把', null);
INSERT INTO `materials` VALUES ('开关锁(短柄)', '303', 'B031002', '把', null);
INSERT INTO `materials` VALUES ('开关锁(长柄)', '301', 'B031003', '把', null);
INSERT INTO `materials` VALUES ('开关锁(按钮无钥)', '504', 'B031004', '把', null);
INSERT INTO `materials` VALUES ('开关锁(按钮)', '506', 'B031005', '把', null);
INSERT INTO `materials` VALUES ('插肖', '4\"', 'B031006', '只', null);
INSERT INTO `materials` VALUES ('箱扣', '3号', 'B031007', '只', null);
INSERT INTO `materials` VALUES ('合页', '2\"', 'B031008', '付', null);
INSERT INTO `materials` VALUES ('合页', '3\"', 'B031009', '付', null);
INSERT INTO `materials` VALUES ('铰链', 'Ф8', 'B031010', '付', null);
INSERT INTO `materials` VALUES ('管路附件', '', 'B0311', '', null);
INSERT INTO `materials` VALUES ('镀锌弯头', 'φ48', 'B031101', '只', null);
INSERT INTO `materials` VALUES ('镀锌弯头', 'φ42', 'B031102', '只', null);
INSERT INTO `materials` VALUES ('水笼头', '1.5寸', 'B031103', '只', null);
INSERT INTO `materials` VALUES ('堵头', 'φ15', 'B031104', '只', null);
INSERT INTO `materials` VALUES ('闸阀', 'φ1.5', 'B031105', '只', null);
INSERT INTO `materials` VALUES ('生料带', '?', 'B031106', '只', null);
INSERT INTO `materials` VALUES ('塑料管', 'φ26.5', 'B031107', '米', null);
INSERT INTO `materials` VALUES ('三通', '6分', 'B031108', '只', null);
INSERT INTO `materials` VALUES ('铜球阀', 'φ26.5', 'B031109', '只', null);
INSERT INTO `materials` VALUES ('不锈钢弯头', 'φ32', 'B031110', '只', null);
INSERT INTO `materials` VALUES ('直接', '？', 'B031111', '只', null);
INSERT INTO `materials` VALUES ('活接', 'φ40', 'B031112', '只', null);
INSERT INTO `materials` VALUES ('延时冲洗阀', '6分', 'B031113', '只', null);
INSERT INTO `materials` VALUES ('内接', 'φ40', 'B031114', '只', null);
INSERT INTO `materials` VALUES ('外接', 'φ20', 'B031115', '只', null);
INSERT INTO `materials` VALUES ('内接', 'φ20', 'B031116', '只', null);
INSERT INTO `materials` VALUES ('铜球阀', 'φ15', 'B031117', '只', null);
INSERT INTO `materials` VALUES ('其它器材', '', 'B0312', '', null);
INSERT INTO `materials` VALUES ('控制保护板', '360×40', 'B031201', '块', null);
INSERT INTO `materials` VALUES ('GGD柜体', '800×600×2200', 'B031202', '只', null);
INSERT INTO `materials` VALUES ('GGD柜体', '1000×600×2200', 'B031203', '只', null);
INSERT INTO `materials` VALUES ('下母线夹安装梁', '430', 'B031204', '根', null);
INSERT INTO `materials` VALUES ('安装梁', '940', 'B031205', '根', null);
INSERT INTO `materials` VALUES ('安装梁', '740', 'B031206', '根', null);
INSERT INTO `materials` VALUES ('刀开关旋转机构梁', '977', 'B031207', '根', null);
INSERT INTO `materials` VALUES ('塑料管', 'PVC-50m/m', 'B031208', '米', null);
INSERT INTO `materials` VALUES ('黄腊管', '4%', 'B031209', '支', null);
INSERT INTO `materials` VALUES ('塑料管', 'PVC-32m/m', 'B031210', '米', null);
INSERT INTO `materials` VALUES ('铝合金', '∠20×30', 'B031211', '米', null);
INSERT INTO `materials` VALUES ('印字板', '?', 'B031212', '块', null);
INSERT INTO `materials` VALUES ('生铁铸件', '？', 'B031213', 'Kg', null);
INSERT INTO `materials` VALUES ('铸铜接线端子', '？', 'B031214', '只', null);
INSERT INTO `materials` VALUES ('拉门轨道', '？', 'B031215', '付', null);
INSERT INTO `materials` VALUES ('不锈钢飞轮', '？', 'B031216', '只', null);
INSERT INTO `materials` VALUES ('门窗附件（二）', '', 'B0313', '', null);
INSERT INTO `materials` VALUES ('铅封螺丝', '8×25', 'B031301', '只', null);
INSERT INTO `materials` VALUES ('不锈钢合页', '?', 'B031302', '付', null);
INSERT INTO `materials` VALUES ('铁铅封罗丝', '10×148', 'B031303', '只', null);
INSERT INTO `materials` VALUES ('铁铅封罗丝', '10×145', 'B031304', '只', null);
INSERT INTO `materials` VALUES ('铁罗丝杆', '10×165', 'B031305', '支', null);
INSERT INTO `materials` VALUES ('铁罗丝杆', '10×148', 'B031306', '支', null);

-- ----------------------------
-- Table structure for out_materials
-- ----------------------------
DROP TABLE IF EXISTS `out_materials`;
CREATE TABLE `out_materials` (
  `Item_No` varchar(50) NOT NULL,
  `Item_Name` varchar(100) NOT NULL,
  `OUT_Odd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `OUT_number` int(20) NOT NULL,
  `unit_price` decimal(20,4) NOT NULL,
  `Total` decimal(20,4) NOT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Item_No`,`OUT_Odd`),
  UNIQUE KEY `OUT_Odd` (`OUT_Odd`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of out_materials
-- ----------------------------
INSERT INTO `out_materials` VALUES ('B01010101', '高分断断路器', 'OCT202005000004', '-5', '7.5200', '-37.6000', '仓库退料');
INSERT INTO `out_materials` VALUES ('B01010101', '高分断断路器', 'oTZTZ00001', '5', '7.5200', '37.6000', '出库材料单');
INSERT INTO `out_materials` VALUES ('B01010101', '高分断断路器', '单号：OCL202005000027', '5', '7.5200', '37.6000', '仓库领料');
INSERT INTO `out_materials` VALUES ('B03010602', '六角螺丝', 'AOT202005000038', '2', '5.0000', '10.0000', '仓库调拨');
INSERT INTO `out_materials` VALUES ('B03010602', '六角螺丝', 'AOT202005000039', '-2', '5.0000', '10.0000', '仓库调拨');
INSERT INTO `out_materials` VALUES ('B03010602', '六角螺丝', 'AOT202005000041', '-2', '5.0000', '-10.0000', '仓库调拨测试');
INSERT INTO `out_materials` VALUES ('B03010602', '六角螺丝', 'I_AOT202005000050', '0', '5.0000', '0.0000', 'dasfdaf');
INSERT INTO `out_materials` VALUES ('B03010602', '六角螺丝', 'O_AOT202005000050', '0', '5.0000', '0.0000', 'dasfdaf');

-- ----------------------------
-- Table structure for out_tables
-- ----------------------------
DROP TABLE IF EXISTS `out_tables`;
CREATE TABLE `out_tables` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Receiver` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `OUT_Odd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `deptno` int(11) NOT NULL,
  `Warehouse_No` varchar(50) NOT NULL,
  `dateTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Checker` varchar(50) NOT NULL,
  `Check_Status` enum('审核通过','审核不通过') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  `Creator` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `OUT_Odd` (`OUT_Odd`),
  KEY `Warehouse_No` (`Warehouse_No`),
  KEY `deptno` (`deptno`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of out_tables
-- ----------------------------
INSERT INTO `out_tables` VALUES ('5', 'pig', 'oTZTZ00001', '1', 'W00001', '2020-05-07 17:13:27', '小何', '审核通过', '出库信息', '小赵');
INSERT INTO `out_tables` VALUES ('6', '大胆的', '单号：OCL202005000027', '1', 'W00001', '2020-05-24 21:26:06', '对个人', '审核通过', '仓库领料', '嘎嘎');
INSERT INTO `out_tables` VALUES ('9', '', 'OCT202005000004', '1', 'W00001', '2020-05-24 21:52:16', '爱的色放', '审核通过', '仓库退料', '阿道夫');
INSERT INTO `out_tables` VALUES ('13', '', 'AOT202005000038', '-1', 'W00001', '2020-05-26 19:24:40', '安定坊', '审核通过', '仓库调拨', '矮冬瓜');
INSERT INTO `out_tables` VALUES ('14', '', 'AOT202005000039', '-1', 'W00001', '2020-05-26 19:30:12', '阿凡达', '审核通过', '仓库调拨', '爱的色放');
INSERT INTO `out_tables` VALUES ('18', '', 'AOT202005000041', '-1', 'W00001', '2020-05-26 19:43:26', '阿道夫d', '审核通过', '仓库调拨测试', '大地方');
INSERT INTO `out_tables` VALUES ('32', '', 'O_AOT202005000050', '-1', 'W00001', '2020-05-26 20:35:07', 'adf', '审核通过', 'dasfdaf', 'adf');
INSERT INTO `out_tables` VALUES ('33', '', 'I_AOT202005000050', '-1', 'W00002', '2020-05-26 20:35:44', 'adf', '审核通过', 'dasfdaf', 'adf');

-- ----------------------------
-- Table structure for supply
-- ----------------------------
DROP TABLE IF EXISTS `supply`;
CREATE TABLE `supply` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `supplierID` varchar(80) NOT NULL,
  `suppliername` varchar(50) NOT NULL,
  `concats` varchar(50) NOT NULL,
  `call_phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `adress` varchar(255) NOT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `supplierID` (`supplierID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of supply
-- ----------------------------
INSERT INTO `supply` VALUES ('1', 'S00001', '金华富达机电设备有限公司', '小贺', '18156387954', '浙江杭州', null);
INSERT INTO `supply` VALUES ('2', 'S00002', '台湾富士康电子有限公司', '小猪', '18697535674', '甘肃平凉', '');
INSERT INTO `supply` VALUES ('3', 'S00003', 'agdafad', 'sads', '18569338541', '上海', '阿斯顿发');

-- ----------------------------
-- Table structure for warehouse
-- ----------------------------
DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Warehouse_No` varchar(50) NOT NULL,
  `Warehouse_Name` varchar(50) NOT NULL,
  `emp_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Create_Date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `areas` varchar(255) NOT NULL,
  `located` varchar(255) NOT NULL,
  `Remarks` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Warehouse_No` (`Warehouse_No`) USING BTREE,
  UNIQUE KEY `emp_no` (`emp_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of warehouse
-- ----------------------------
INSERT INTO `warehouse` VALUES ('1', 'W00001', '五金仓库A', 'A0001', '1997-03-05 13:50:00', '5000平方米', '浙江金华', '存储五金杂货');
INSERT INTO `warehouse` VALUES ('2', 'W00002', '五金仓库B', 'A0002', '2000-08-09 02:41:00', '4500平方米', '浙江杭州', '存储五金杂货');
