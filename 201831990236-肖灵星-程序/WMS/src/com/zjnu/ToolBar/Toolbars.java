package com.zjnu.ToolBar;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class Toolbars {
	private static JTable table = null;
	private static Connection conn = null;
	private static String tableName = null;
	private JPanel panelBar = null;
	private JToolBar Jtoolbar = null;
	private static JLabel page = null;
	
	public static JLabel getPage() {
		return page;
	}


	public static void setPage(JLabel page) {
		Toolbars.page = page;
	}

	private List<ToolBarItem> items = null;
	
	{
		Jtoolbar = new JToolBar("工具栏");
		FlowLayout flow = new FlowLayout();
		flow.setHgap(20);
		
		Jtoolbar.setLayout(flow);		
		Jtoolbar.setOpaque(false);
		Jtoolbar.setBounds(10, 20, 600, 50);
		Jtoolbar.setBorderPainted(false);
		Jtoolbar.setFloatable(false);
		items = new ArrayList<ToolBarItem>();
		
		panelBar = new JPanel();
		panelBar.setBounds(10, 10, 800, 80);
		panelBar.setOpaque(false);
		panelBar.setBackground(Color.red);
		panelBar.setVisible(true);
		
		Border border = BorderFactory.createLoweredSoftBevelBorder();
		page = new JLabel("0");
		page.setBorder(border);
		Dimension dim = new Dimension();
		dim.setSize(30, 25);
		page.setPreferredSize(dim);
		page.setHorizontalAlignment(SwingConstants.CENTER);
		
		panelBar.add(Jtoolbar);
		panelBar.add(page);
		
		readXMLFile();
		addButtons();
		setEvents();
	}
	


	public Toolbars() {
		
	}
	

	public Toolbars(JTable table,Connection conn,String tableName) {
		this.table = table;
		this.conn = conn;
		this.tableName = tableName;
	}
	
	
	//以下为属性修改器
	public JPanel getPanelBar() {
		return panelBar;
	}


	public static JTable getTable() {
		return table;
	}


	public static void setTable(JTable table) {
		Toolbars.table = table;
	}


	public void setPanelBar(JPanel panelBar) {
		this.panelBar = panelBar;
	}


	public JToolBar getJtoolbar() {
		return Jtoolbar;
	}


	public void setJtoolbar(JToolBar jtoolbar) {
		Jtoolbar = jtoolbar;
	}


	
	
	//读取xml文件的方法
	private void readXMLFile() {		
		
		//创建读取对象
		SAXReader reader = new SAXReader();
		
		//通过反射获取xml文件，并通过getResourceAsStream获得输入流对象
		InputStream xmlFile = MeanBar.class.getResourceAsStream("toolBar.xml");		
		
		try {
			//Document文HTML或xml文档的根类
			Document doc = reader.read(xmlFile);
			//获取xml文件的根标签的对象
			Element rootElm = doc.getRootElement();
			//通过根标签对象获取文档内“Item”标签的项目内容
			List<Element> elms = rootElm.elements("tool");
			
			//利用循环取出每一个子标签中的内容
			for(Element e:elms) {
				String icon = e.elementTextTrim("icon");
				String function = e.elementTextTrim("function");
				String description = e.elementTextTrim("description");
				
				items.add(new ToolBarItem(icon,function,description));
				
			}
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			System.out.println("readXMLFile()读取失败");
			e.printStackTrace();
		}
	
	}
	
	
	//增加按钮到ToolBar工具条中
	private void addButtons() {
		for(ToolBarItem item:items) {
			ToolLabel labBtn = new ToolLabel(item.getIcon(),item.getFunction(),item.getDescription());
			Jtoolbar.add(labBtn);
		}
	}
	
	//设置按钮的点击事件
	private void setEvents() {
		//遍历产生的按钮对象
		for(Component c:Jtoolbar.getComponents()) {
			if(c instanceof ToolLabel) {
				final ToolLabel labBtn = (ToolLabel)c;
				labBtn.addMouseListener(new MouseAdapter() {
					//设置对应的事件方法
					@Override
					public void mouseClicked(MouseEvent e) {
						// TODO Auto-generated method stub
						String function = labBtn.getFunction();
						String selectedText = labBtn.getDescription();
						
						Toolbars.executeFunction(function, selectedText);
					}
					
				});
				
				
			}
		}
	}
	
	//根据反射+函数名来寻找函数方法。
	private static boolean executeFunction(String function,String TypeText) {
		
		//通过反射寻找对应的函数
		try {
			Class<?> c = Class.forName("com.zjnu.ToolBar.FunctionsSet");
			Method method = c.getDeclaredMethod(function, String.class);
			method.setAccessible(true);
			boolean isSuccessful = (boolean) method.invoke(c.getDeclaredConstructor(JTable.class,Connection.class,String.class).newInstance(Toolbars.table,Toolbars.conn,Toolbars.tableName), TypeText);
			
			if(isSuccessful) {
				Integer num = null;
				switch(TypeText) {
				case "首页":Toolbars.page.setText("0"); break;
				case "下一页":
					num = Integer.valueOf(Toolbars.page.getText());
					num += 1;
					Toolbars.page.setText(String.valueOf(num)); break;
					
				case "上一页":
					num = Integer.valueOf(Toolbars.page.getText());
					num -= 1;
					if(num <0) {
						num = 0;
					}
					Toolbars.page.setText(String.valueOf(num)); break;
					
				}
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("没有找到类加载器");
			e.printStackTrace();
			return false;
			
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			System.out.println("没有找到方法");
			e.printStackTrace();
			return false;
			
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			System.out.println("没有权限访问");
			e.printStackTrace();
			return false;
			
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			
			System.out.println("不合法的输入1");			
			e.printStackTrace();
			return false;
			
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			System.out.println("不合法的输入2");
			e.printStackTrace();
			return false;
			
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			System.out.println("不合法的输入3");
			e.printStackTrace();
			return false;
			
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			System.out.println("不合法的输入4");
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	 
	}
	
	
}
