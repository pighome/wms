package com.zjnu.ToolBar;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.border.Border;

public class ToolLabel extends JLabel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String icon = null;
	private String function = null;
	private String description = null;
	
	public ToolLabel() {
		this(null,null,null);
	}
	
	public ToolLabel(String icon,String function,String description) {
		this.icon = icon;
		this.function = function;
		this.description = description;
		setStyle();
	}
	

	//根据地址将图片地址转换为Icon对象
	private Icon getTransformIcon() {
		return new ImageIcon(ToolBarButtons.class.getResource(icon));
	}
	
	//设置标签样式
	private void setStyle() {
		
		Border border = BorderFactory.createLoweredSoftBevelBorder();
		this.setBackground(Color.decode("#FFE4B5"));
		this.setIcon(this.getTransformIcon());
		this.setBorder(border);
		
	}
	

	public String getIconName() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
