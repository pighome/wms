package com.zjnu.ToolBar;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import com.zjnu.Panels.LedgerPanel;
import com.zjnu.Panels.LitterUtils;
import com.zjnu.Panels.MainStockPanel;
import com.zjnu.Panels.ShiftchangeFunction;
import com.zjnu.Util.JDBCUtil;

public class FunctionsSet {
	
	private JFrame jf = null;
	private JTable table = null;
	private Connection conn = null;
	private String tableName = null;
	
	public FunctionsSet() {
	}
	
	//以下方法为构造器，
	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public FunctionsSet(JFrame jf,Connection conn) {
		this.jf = jf;
		this.conn = conn;
	}
	
	public FunctionsSet(JTable table,Connection conn,String tableName) {
		this.table = table;
		this.conn = conn;
		this.tableName = tableName;
	}
	
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		System.out.println("设置连接属性"+conn);
		this.conn = conn;
	}

	private void manager(String text) {
		System.out.println("暂时么有管理"+"\n"+text);
		ShiftchangeFunction shiftChange = new ShiftchangeFunction(jf);
		shiftChange.initSetting();
	}
	
	private void searches(String text) {
		System.out.println("暂时么有查询"+"\n"+text);
		LedgerPanel ledger = new LedgerPanel(this.conn);
		ledger.initPanel();
	}
	
	private void warehouse(String text) {
		System.out.println("暂时么有库存"+"\n"+text);
		MainStockPanel mainstockPanel = new MainStockPanel(this.conn);
		mainstockPanel.initPanel();
		
	}
	private void helper(String text) {
		System.out.println("暂时么有服务"+"\n"+text);
		JOptionPane.showMessageDialog(jf,"系统维护中","提示信息",JOptionPane.INFORMATION_MESSAGE);
		
		
	}
	private void exitSystem(String text) {
		System.out.println("系统已停止"+"\n"+text);
		
		int option = JOptionPane.showConfirmDialog(jf, "确认退出系统","系统提示", JOptionPane.YES_NO_OPTION);
		
		System.out.println(option);
		
		if(option == JOptionPane.YES_OPTION) {
			
			JDBCUtil.close(null,null, conn);
			jf.dispose();
			System.exit(0);
		}
		
		
	}
	
	private boolean nextPage(String text) {
		System.out.println("下一页");
		//编写分页查询的条件
		
		/*
		 * 1、更新表数据
		 * 2、判断数据是否更新完毕
		 * 3、如果更新完则数字+1；
		 */
		boolean isUpdateNext = false;
		ResultSet rs = null;
		
		try {
			rs = LitterUtils.limitSelected(conn, tableName, 1);

			addDatasTable(rs, table);
			
			isUpdateNext = true;
		}catch(Exception e) {
			
			isUpdateNext = false;
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}		
		
		return isUpdateNext;
		
	}
	
	private boolean prePage(String text) {
		System.out.println("上一页");
		//编写分页查询的条件
		boolean isUpdatePre = false;
		ResultSet rs = null;
		
		//如何传送连接对象conn和表名
		
		
		try {
			rs = LitterUtils.limitSelected(conn, tableName, -1);
			
			addDatasTable(rs, table);
			
			isUpdatePre = true;
		}catch(Exception e) {
			
			isUpdatePre = false;
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}			
		
		return isUpdatePre;
		
	}
	
	private boolean homePage(String text) {
		System.out.println("首页");
		//编写分页查询的条件
		boolean isUpdateHome = false;
		ResultSet rs = null;
		
		
		
		try {
			rs = LitterUtils.limitSelected(conn, tableName, 0);
			
			addDatasTable(rs, table);
			
			isUpdateHome = true;
		}catch(Exception e) {
			
			isUpdateHome = false;
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}	
		
		
		return isUpdateHome;
	}
	
	private boolean lastPage(String text) {
		System.out.println("尾页");
		//编写分页查询的条件
		
		boolean isUpdateLast = false;
		ResultSet rs = null;
		
		try {
			rs = LitterUtils.limitSelected(conn, tableName, 2);		
			
			addDatasTable(rs, table);
			
			isUpdateLast = true;
		}catch(Exception e) {
			
			isUpdateLast = false;
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}			
		
		return isUpdateLast;
	}
	
	private void addDatasTable(ResultSet rs,JTable table) throws SQLException {
		LitterUtils.clearTable(table);
		int row = 0;
		while(rs.next()) {
			for(int col = 0;col<table.getColumnCount();col ++) {
				this.table.setValueAt(rs.getObject(col+1), row, col);
			}
			row ++;
		}
	}
	
}
