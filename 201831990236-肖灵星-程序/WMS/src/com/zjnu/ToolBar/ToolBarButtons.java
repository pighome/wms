package com.zjnu.ToolBar;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.*;

public class ToolBarButtons extends JButton {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String icon = null;
	private String function = null;
	private String description = null;
	
	public ToolBarButtons() {
		this(null,null,null);
	}
	
	public ToolBarButtons(String icon,String function,String description) {
		this.icon = icon;
		this.function = function;
		this.description = description;
		setStyle();
	}
	

	//根据地址将图片地址转换为Icon对象
	private Icon getTransformIcon() {
		return new ImageIcon(ToolBarButtons.class.getResource(icon));
	}
	
	//设置按钮样式
	private void setStyle() {
		
		this.setBackground(Color.decode("#FFE4B5"));
		Insets padding = new Insets(0, 0, 10, 0);	//顶左底右
		this.setMargin(padding);
		this.setText(this.description);
		this.setFont(new Font(null,Font.CENTER_BASELINE,13));
		this.setIcon(this.getTransformIcon());
		this.setHorizontalTextPosition(SwingConstants.CENTER );
		this.setVerticalTextPosition(SwingConstants.BOTTOM);
		
	}
	

	public String getIconName() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
