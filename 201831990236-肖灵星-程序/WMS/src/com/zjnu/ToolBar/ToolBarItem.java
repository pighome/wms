package com.zjnu.ToolBar;

import java.io.Serializable;

/*
 * 临时存放Item类数据是一个javaBean
 */
public class ToolBarItem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String icon = null;
	private String function = null;
	private String description = null;
	
	public ToolBarItem() {
		this(null,null,null);
	}
	
	public ToolBarItem(String icon,String function,String description) {
		this.icon = icon;
		this.function = function;
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
