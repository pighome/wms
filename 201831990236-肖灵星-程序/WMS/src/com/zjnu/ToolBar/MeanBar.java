package com.zjnu.ToolBar;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.*;

import javax.swing.*;

import org.dom4j.*;
import org.dom4j.io.SAXReader;

public class MeanBar {
	private static JFrame jf = null;
	private JPanel panelBar = null;
	private JToolBar Jtoolbar = null;
	private List<ToolBarItem> items = null;
	private static Connection conn = null;
	
	{
		Jtoolbar = new JToolBar("工具栏");
		FlowLayout flow = new FlowLayout();
		flow.setHgap(50);
		
		Jtoolbar.setLayout(flow);		
		Jtoolbar.setOpaque(false);
		Jtoolbar.setBounds(10, 20, 600, 70);
		Jtoolbar.setBorderPainted(false);
		Jtoolbar.setFloatable(false);
		items = new ArrayList<ToolBarItem>();
		
		panelBar = new JPanel(null);
		panelBar.setBounds(10, 10, 800, 80);
		panelBar.setOpaque(false);
		panelBar.setBackground(Color.red);
		panelBar.setVisible(true);
		
		panelBar.add(Jtoolbar);
		
		readXMLFile();
		addButtons();
		setEvents();
	}
	
	
	public static JFrame getJf() {
		return jf;
	}


	public static void setJf(JFrame jf) {
		MeanBar.jf = jf;
	}


	public JPanel getPanelBar() {
		return panelBar;
	}


	public void setPanelBar(JPanel panelBar) {
		this.panelBar = panelBar;
	}


	public JToolBar getJtoolbar() {
		return Jtoolbar;
	}


	public void setJtoolbar(JToolBar jtoolbar) {
		Jtoolbar = jtoolbar;
	}


	public MeanBar() {
		
	}
	

	
	public MeanBar(JFrame jf,Connection conn) {
		MeanBar.jf = jf;
		MeanBar.conn = conn;
	}
	
	
	
	//读取xml文件的方法
	private void readXMLFile() {		
		
		//创建读取对象
		SAXReader reader = new SAXReader();
		
		//通过反射获取xml文件，并通过getResourceAsStream获得输入流对象
		InputStream xmlFile = MeanBar.class.getResourceAsStream("toolBar.xml");		
		
		try {
			//Document文HTML或xml文档的根类
			Document doc = reader.read(xmlFile);
			//获取xml文件的根标签的对象
			Element rootElm = doc.getRootElement();
			//通过根标签对象获取文档内“Item”标签的项目内容
			List<Element> elms = rootElm.elements("Item");
			
			//利用循环取出每一个子标签中的内容
			for(Element e:elms) {
				String icon = e.elementTextTrim("icon");
				String function = e.elementTextTrim("function");
				String description = e.elementTextTrim("description");
				
				items.add(new ToolBarItem(icon,function,description));
				
			}
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			System.out.println("readXMLFile()读取失败");
			e.printStackTrace();
		}
	
	}
	
	
	//增加按钮到ToolBar工具条中
	private void addButtons() {
		for(ToolBarItem item:items) {
			ToolBarButtons btn = new ToolBarButtons(item.getIcon(),item.getFunction(),item.getDescription());
			Jtoolbar.add(btn);
		}
	}
	
	//设置按钮的点击事件
	private void setEvents() {
		//遍历产生的按钮对象
		for(Component c:Jtoolbar.getComponents()) {
			if(c instanceof ToolBarButtons) {
				final ToolBarButtons btn = (ToolBarButtons)c;
				btn.addActionListener(new ActionListener() {
					//设置对应的事件方法
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						String function = btn.getFunction();
						String selectedText = btn.getDescription();
						
						MeanBar.executeFunction(function, selectedText);
					}
					
				});
			}
		}
	}
	
	//根据反射+函数名来寻找函数方法。
	private static boolean executeFunction(String function,String TypeText) {
		
		//通过反射寻找对应的函数
		try {
			Class<?> c = Class.forName("com.zjnu.ToolBar.FunctionsSet");			
			
			Method method = c.getDeclaredMethod(function, String.class);
			method.setAccessible(true);
			method.invoke(c.getDeclaredConstructor(JFrame.class,Connection.class).newInstance(MeanBar.jf,MeanBar.conn), TypeText);
	
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("没有找到类加载器");
			e.printStackTrace();
			return false;
			
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			System.out.println("没有找到方法");
			e.printStackTrace();
			return false;
			
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			System.out.println("没有权限访问");
			e.printStackTrace();
			return false;
			
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			
			System.out.println("不合法的输入1");			
			e.printStackTrace();
			return false;
			
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			System.out.println("不合法的输入2");
			e.printStackTrace();
			return false;
			
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			System.out.println("不合法的输入3");
			e.printStackTrace();
			return false;
			
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			System.out.println("不合法的输入4");
			e.printStackTrace();
			return false;
			
		}
		
		return true;
	 
	}
	
}
