package com.zjnu.JTree;

import java.sql.*;

import com.zjnu.Util.JDBCUtil;
import com.zjnu.Util.WMSfields;
import com.zjnu.Util.WMStables;

public class MainJTree {
	//面板加载对象
	private JTreeUI jTreeUI = null;
	
	//数据库连接对象
	private Connection conn = null;

	public MainJTree() {
		this(null);
	}

	public MainJTree(Connection conn) {
		this.jTreeUI = new JTreeUI(conn);		
		this.conn = conn;
		operationDate();
	}
	
	
	public void stockTree() {
		
		//启用面板
		//InitMainJTree();
		//设置目录树的内容
		operationDate();
		
	}
	
	
	//取数据库的内容
	
	private void operationDate() {
		String MaterialId = null;
		String MaterialName = null;
		String MaterialSpecial = null;
		
		String MatId = WMSfields.ItemNo;
		String MatName = WMSfields.ItemName;
		String MatSpec = WMSfields.norms;
		
		SelectLevel selevtLevel = new SelectLevel(jTreeUI);	
		
		String sql = "select * from "+WMStables.Materials;		
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			
			pst = conn.prepareStatement(sql);	
			
			rs = pst.executeQuery();
			
			while(rs.next()) {
				MaterialId = rs.getString(MatId);
				MaterialName = rs.getString(MatName);
				MaterialSpecial = rs.getString(MatSpec);
				
				if(MaterialSpecial.length() > 0) {
					MaterialSpecial = ",".concat(MaterialSpecial);
					MaterialName = MaterialName.concat(MaterialSpecial);
				}
				
				selevtLevel.rankArt_No(MaterialId, MaterialName);
			}	
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, pst, null);	
			
		}
	}

	public JTreeUI getJtreeUI() {
		return jTreeUI;
	}

	
}
