package com.zjnu.JTree;

public class SelectLevel {
	
	private JTreeUI jTreeUI = null;
	
	public SelectLevel() {
		
	}
	
	public SelectLevel(JTreeUI jTreeUI) {
		this.jTreeUI = jTreeUI;
	}
	
	public void rankArt_No(String Art_No,String Art_Name) {
		String childId = Art_No;
		String childName = Art_Name;
		int num = rankOrder(Art_No);
		
		if(num == 0) {
			jTreeUI.addRoot(Art_No, Art_Name);		
		}else {
			String fatherId = getFatherId(childId);
			jTreeUI.addChildNode(fatherId, childId, childName);
		}		
		
	}
	
	private int rankOrder(String Art_No) {
		int number = -1;
		number = Art_No.length()/2;
		return number;
	}
	
	private String getFatherId(String childId) {
		String fatherId = null;
		
		if(childId != null) {
			fatherId = childId.substring(0,childId.length()-2);
		}
		
		return fatherId;
	}
	
}
