package com.zjnu.JTree;

import java.io.Serializable;

public class Info implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name = null;
	private Object ItemNo = null;
	
	public Info() {
		this(null,null);
	}
	
	public Info(String name,Object ItemNo) {
		this.name = name;
		this.ItemNo = ItemNo;
	}
	
	public Object getItemNo() {
		return ItemNo;
	}

	public void setItemNo(Object itemNo) {
		ItemNo = itemNo;
	}

	//对象实例会自动调用toString方法
	public String toString() {
		return name;
	}
}
