package com.zjnu.JTree;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.zjnu.Panels.LitterUtils;

public class JTreeUI extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//结点定义
	private DefaultMutableTreeNode fatherNode = null;
	private DefaultMutableTreeNode childNode = null;
	private DefaultMutableTreeNode rootNode = null;
	
	private Map<String,Object> collectionNode = new HashMap<String, Object>();
	private JTree[] jtree = new JTree[10];	//创建一个树的数组
	private static int count = 0;
	
	//private JPanel jpContent = null;
	private JScrollPane scrollpane = null;
	private JTable table = null;
	
	private Connection conn = null;
	
	
	private int countTest = 0;
	
	public JTreeUI() {
		this(null);
		
	}
	
	public JTreeUI(Connection conn) {
		this.conn = conn;
		InitUI();
		
	}
	
	/*
	 * public JPanel getJpContent() { return jpContent; }
	 */
	public void setTable(JTable table) {
		this.table = table;
	}

	
	public JTree[] getJtree() {
		return jtree;
	}

	//添加根节点的方法
	public void addRoot(String rootId,String rootName) {
		
		rootNode = new DefaultMutableTreeNode(new Info(rootName,rootId));
		collectionNode.put(rootId, rootNode);
		if(rootNode != null) {
			createJTree(rootNode);
		}
		
	}
	
	
	
	private void InitUI() {
		
		//jpContent = new JPanel(new BorderLayout());
		this.setLayout(new BorderLayout());
		scrollpane = new JScrollPane();
		scrollpane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		UIManager.put("Tree.expandedIcon",new ImageIcon("imges/fileopen.png"));
		UIManager.put("Tree.collapsedIcon", new ImageIcon("imges/fileclose.png"));
		
		///jpContent.add(scrollpane,BorderLayout.CENTER);
		this.add(scrollpane,BorderLayout.CENTER);
		
	}
	
	
	
	public void addChildNode(String fatherId,String childId,String childName) {
		//找到父节点，然后添加元素到父节点中
		fatherNode = findNode(fatherId);
		countTest ++;
		if(fatherNode != null) {
			childNode = new DefaultMutableTreeNode(new Info(childName,childId));
			if(childNode != null) {
				fatherNode.add(childNode);
				collectionNode.put(childId, childNode);
			}
		}else {
			System.out.println("插入的元素父节点不和法  ----》" + countTest);
		}
		
	}

	
	private DefaultMutableTreeNode findNode(String nodeId) {
		DefaultMutableTreeNode value = null;
		
		value = (DefaultMutableTreeNode)collectionNode.get(nodeId);
		
		return value;
	}
	
	private void customStyles(JTree JT) {
		DefaultTreeCellRenderer render = new DefaultTreeCellRenderer();
		//Color c = new Color(0);
		
		render.setOpenIcon(new ImageIcon("imges/file.png"));
		render.setClosedIcon(new ImageIcon("imges/file.png"));
		
		render.setLeafIcon(new ImageIcon("imges/leaf.png"));
		
		render.setFont(new Font("楷体",12,12));
		render.setTextNonSelectionColor(Color.black);
		render.setTextSelectionColor(Color.DARK_GRAY);
		
		render.setBackgroundSelectionColor(Color.LIGHT_GRAY);
		render.setBackgroundNonSelectionColor(Color.white);
		
		JT.setCellRenderer(render);
	}
	
	private void createJTree(DefaultMutableTreeNode rootNode) {
		
		jtree[count] = new JTree(rootNode);
		jtree[count].setShowsRootHandles(true);		//设置显示根结点句柄
		jtree[count].setEditable(false); 	//设置树结点可编辑
		
		jtree[count].addTreeSelectionListener(new TreeSelectionListener() {

			@Override
			public void valueChanged(TreeSelectionEvent e) {
				// TODO Auto-generated method stub
				//应该在这里传入table对象
				DefaultMutableTreeNode select = (DefaultMutableTreeNode) jtree[count-1].getLastSelectedPathComponent();
				
				Class<?> cls = select.getUserObject().getClass();

				Object ItemNo = null;
				try {
					Field field = cls.getDeclaredField("ItemNo");
					
					field.setAccessible(true);
					
					ItemNo = field.get(select.getUserObject());
				} catch (IllegalArgumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NoSuchFieldException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SecurityException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				LitterUtils.selectLikeData(conn, ItemNo, table);
				
				System.out.println("当前被选中的结点：" + ItemNo);

			}
			
		});
		customStyles(jtree[count]);
		
		scrollpane.setViewportView(jtree[count]);	//添加组件
		count ++ ;
	}
	
	
}

