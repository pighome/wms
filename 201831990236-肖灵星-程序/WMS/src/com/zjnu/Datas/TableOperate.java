package com.zjnu.Datas;

import java.sql.*;
import java.util.Map;

import com.zjnu.Interface.DataOperation;
import com.zjnu.Util.WMSUtils;

public class TableOperate implements DataOperation{
	private Connection conn = null;
	private String tableName = null;
	
	public TableOperate() {
		this(null);
	}
	
	public TableOperate(Connection conn) {
		this.conn = conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}
	
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	@Override
	public int insertData(Map<String, Object> data) {
		// TODO Auto-generated method stub
		int count = 0;
		try {
			count = WMSUtils.insertSQL(conn, tableName, data);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(tableName+"表插入出问题了");
			e.printStackTrace();
		}
		
		return count;
	}

	@Override
	public int deleteData(Object... ItemIDs) {
		// TODO Auto-generated method stub
		int count = 0;
		try {
			count = WMSUtils.deleteSQL(conn, tableName, ItemIDs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(tableName+"删除出问题了");
			e.printStackTrace();
		}
		
		return count;
	}

	@Override
	public int updateData(Map<String, Object> data, Object... ItemIDs) {
		// TODO Auto-generated method stub
		int count = 0;
		try {
			count = WMSUtils.updateSQL(conn, tableName, data, ItemIDs);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(tableName+"表更新出问题了");
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public ResultSet selectData(Object condition) {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		try {
			rs = WMSUtils.selectSQL(conn, tableName, condition);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(tableName+"查找出问题了");
			e.printStackTrace();
		}
		return rs;
	}

	@Override
	public boolean truncateData() {
		// TODO Auto-generated method stub
		boolean isClear = false;
		try {
			isClear = WMSUtils.truncateSQL(conn, tableName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(tableName+"清空出问题了");
			e.printStackTrace();
		}
		
		return isClear;
	}

}
