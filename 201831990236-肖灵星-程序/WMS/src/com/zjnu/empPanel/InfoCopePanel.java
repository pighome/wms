package com.zjnu.empPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;


public class InfoCopePanel {
	private JTable table = null;
	private Connection conn = null;
	private JTextField textSearchInfo = null;
	private JFrame jf = null;
	
	private String tableName = null;	
	private String PanelMessage = null;
	
	private String[] fields = null;
	private String[] fields_EN = null;
	
	
	public InfoCopePanel() {
		this(null,null);
	}
	
	public InfoCopePanel(Connection conn,String tableName) {
		this.conn = conn;
		this.tableName = tableName;
	}
	/*
	 * 需要两个部分，javatree  和一个查询信息显示面板
	 */
	
	//设置数据的修改器
	public String getPanelMessage() {
		return PanelMessage;
	}

	public void setPanelMessage(String panelMessage) {
		PanelMessage = panelMessage;
	}

	
	//设置字段的数组值
	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

	public String[] getFields_EN() {
		return fields_EN;
	}

	public void setFields_EN(String[] fields_EN) {
		this.fields_EN = fields_EN;
	}

	//初始化面板
	
	public void initPanel() {
		//若为null了会有异常吗？
		String title = getPanelMessage();
		if(title == null) {
			title = "hello";
		}
		this.jf = new JFrame(title);
		this.jf.setSize(800, 600);
		this.jf.setBackground(Color.decode("#FAF0E6"));
		this.jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		 
		JPanel panel = new JPanel(null);
		panel.setBounds(0, 0,800, 600);
		
		panel.add(createBarMean());
		panel.add(createJTable());
		
		this.jf.add(panel);
		
		this.jf.setResizable(false);
		this.jf.setVisible(true);
	}
	
	
	//创建工具菜单栏
	private JPanel createBarMean() {
		JPanel panel = new JPanel();
		
		panel.setBounds(0, 10, 800,60);
		
		FlowLayout layout = new FlowLayout();
		layout.setHgap(30);
		
		panel.setLayout(layout);
		
		JButton[] btn = OperateUtilPanel.getBtn();
		
		for(int i = 0;i<btn.length-1;i++) {
			panel.add(btn[i]);
		}
	
		JPanel pan = new JPanel();
		pan.add(btn[btn.length-1]);
		
		textSearchInfo = new JTextField(10);
		textSearchInfo.setText("ID号/名称");
		textSearchInfo.addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				textSearchInfo.setText("");
			}

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				String text = textSearchInfo.getText();
				if("".equals(text)) {
					textSearchInfo.setText("ID号/名称");
				}
				
				
			}
			
		});
		
		pan.add(textSearchInfo);
		
		panel.add(pan);
		
		setActionBtn(btn);
		
		return panel;
		
	}
	

	private void setActionBtn(JButton[] btn) {
		//增加按钮
		btn[0].addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				
				//创建小框框的面板
				//调用按钮返回面板，并插入小框框面板中
				JPanel panBtn = OperateUtilPanel.insertBtn(conn, tableName);
				OperateUtilPanel.empInfoPanel(fields,fields_EN,panBtn);
				
			}
			
		});
		
		//删除按钮
		btn[1].addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				boolean isDelete = false;
				isDelete = OperateUtilPanel.DeleteData(conn, tableName, table.getValueAt(table.getSelectedRow(), 0));
				
				String message = "";
				if(isDelete) {
					message = "数据已保存更新至数据库";
					
				}else {
					message="数据更新失败，请重新尝试";
				}
				
				JOptionPane.showMessageDialog(jf,message,"提示信息",JOptionPane.INFORMATION_MESSAGE);
		      
			
			}
			
		});		
		
		//修改按钮
		btn[2].addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				 
				try {
					
					Object Item = table.getValueAt(table.getSelectedRow(), 0);
					JPanel panBtn = OperateUtilPanel.updateBtn(conn, tableName,Item);
					OperateUtilPanel.empInfoPanel(fields,fields_EN,panBtn);
					
				} catch(IndexOutOfBoundsException e1) {
					System.out.println("请选择表格");
					JOptionPane.showMessageDialog(jf,"请选择表格","提示信息",JOptionPane.INFORMATION_MESSAGE);
					e1.printStackTrace();
				}
				
				
			}
			
		});
		
	
		//查询按钮
		btn[3].addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//将rs中数据填充到表格中
				ResultSet rs = OperateUtilPanel.SearchData(conn, tableName, textSearchInfo.getText());
				addDatas(fields_EN,rs,table);
			
			}
			
		});
		
	}
	
	//为表增加数据
		private void addDatas(String[] fields,ResultSet rs,JTable table) {
			clearTable(table);
			try {
				int row = 0;
				while(rs.next()) {
					for(int i=0;i<fields.length;i++) {
						table.setValueAt(rs.getObject(fields[i]), row, i);						
					}
					row ++;
					if((table.getRowCount()-row)<5) {
						Object[] insertRow = new Object[fields.length+1];
						((DefaultTableModel) table.getModel()).addRow(insertRow);
					}
			  
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				if(rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		

	// 清空表格数据
	public void clearTable(JTable table) {
		for (int i = 0; i < table.getRowCount(); i++) {
			for (int j = 0; j < table.getColumnCount(); j++) {
				table.setValueAt(null, i, j);
			}
		}
	}
	
	//创建表格
	private JPanel createJTable() {
		JPanel panel = new JPanel();
		
		panel.setBounds(50, 100,700, 400);
		BorderLayout layout = new BorderLayout();
		
		panel.setLayout(layout);
		
		JScrollPane scrollpanel = new JScrollPane();
		
		
		Object[][] contents = new Object[20][fields.length];
		
		//创建表格
		DefaultTableModel tableModel = new DefaultTableModel(contents,fields) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			@Override
			//设置表格内容不可变
			public boolean isCellEditable(int row,int column){

				return false;
	
			}
			
		};
		
		
		JTable table = new JTable(tableModel);
	     //设置单元格排序
		RowSorter<TableModel> rowSorter = new TableRowSorter<TableModel>(table.getModel());
		table.setRowSorter(rowSorter);
		this.table = table;
		
		table.setRowSelectionAllowed(true);
		
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setAutoscrolls(false);
		
		//设置单元格宽度
		
		int width = 700 / fields.length;
		
		for(int i = 0;i< fields.length;i++) {
			table.getColumnModel().getColumn(i).setPreferredWidth(width);
		}
				
		//增加初始化数据
		
		ResultSet rs = OperateUtilPanel.SearchData(conn, tableName,"");
		addDatas(fields_EN,rs,table);
		
		scrollpanel.setViewportView(table);
		
		panel.add(scrollpanel,BorderLayout.CENTER);
		
		return panel;
	}
	
	//创建查询数据的面板

	
}
