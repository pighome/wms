package com.zjnu.empPanel;

import java.io.Serializable;

import com.zjnu.Util.WMSfields;

public class FieldsArray implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private FieldsArray() {
		
	}
	
	public static String[] getFieldEmp() {
		String[] empField = {
			"员工编号","员工姓名","性        别","工        作","薪        水","联系方式","家庭住址","部门编号","备注"	
		};
		
		return empField;
	}
	
	public static String[] getFieldEmp_EN() {
		String[] empField_EN = {
			WMSfields.empNo,WMSfields.empName,WMSfields.gender,WMSfields.job,WMSfields.salary,
			WMSfields.callPhone,WMSfields.adress,WMSfields.deptno,WMSfields.remarks	
		};
		
		return empField_EN;
	}
	
	public static String[] getFieldDept() {
		String[] deptField = {
			"部门编号","部门名称","地        址","平均薪水","部门人数","备注"	
		};
		
		return deptField;
	}
	
	public static String[] getFieldDept_EN() {
		String[] deptField_EN = {
			WMSfields.deptno,WMSfields.dName,WMSfields.adress,WMSfields.avgSalary,WMSfields.persons,WMSfields.remarks
		};
		
		return deptField_EN;
	}
	
	public static String[] getFieldAdmin() {
		String[] adminField = {
			"管理员编号","管理员姓名","性        别","联系方式","家庭住址","权限等级","备注"	
		};
		
		return adminField;
	}
	
	public static String[] getFieldAdmin_EN() {
		String[] adminField_EN = {
			WMSfields.adminNo,WMSfields.adminName,WMSfields.gender,WMSfields.callPhone,
			WMSfields.adress,WMSfields.powerID,WMSfields.remarks
		};
		
		return adminField_EN;
	}
	
	public static String[] getFieldSupply() {
		String[] supplyField = {
			"供应商编号","供应商姓名","联系    人","联系方式","地        址","备注"	
		};
		
		return supplyField;
	}
	
	public static String[] getFieldSupply_EN() {
		String[] supplyField_EN = {
			WMSfields.supplierId,WMSfields.supplierName,WMSfields.concats,WMSfields.callPhone,
			WMSfields.adress,WMSfields.remarks
		};
		
		return supplyField_EN;
	}
	
	public static String[] getFieldWarehouse() {
		String[] warehouseField = {
			"仓库编号","仓库名称","管理员工","创建日期","面        积","位        置","备注"	
		};
		
		return warehouseField;
	}
	
	public static String[] getFieldWarehouse_EN() {
		String[] warehouseField_EN = {
			WMSfields.wareHouseNo,WMSfields.wareHouseName,WMSfields.empNo,WMSfields.createDate,
			WMSfields.areas,WMSfields.located,WMSfields.remarks
		};
		
		return warehouseField_EN;
	}
}
