package com.zjnu.empPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.zjnu.Util.JDBCUtil;

public class updatePassWord {
	
	private String[] fields= {
		"用户名","旧密码","新密码","确认新密码"	
	};
	
	
	private JTextField[] text = null;
	private Connection conn = null;
	private JFrame interFrame = null;
	private JLabel alert = null;
	
	public updatePassWord() {
		this(null);
	}
	
	public updatePassWord(Connection conn) {
		this.conn = conn;
	}
	
	public void initPanel() {
		interFrame = new JFrame("信息窗口");
		interFrame.setSize(250, 350);		 
		interFrame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
		 
		JPanel pan = updateBtn();
		
		JPanel panel = createInfoPanel(pan);
		
		
		interFrame.add(panel);
			
		interFrame.setVisible(true);

	}
	
	private JPanel createInfoPanel(JPanel pan) {
		JPanel panel = new JPanel(null);
		
		text = new JTextField[4];
		
		for(int i = 0;i<fields.length;i++) {
			JPanel rowPanel = new JPanel();
			
			JLabel labInfo = new JLabel(fields[i]+":");	
			text[i]=new JTextField(10);			
			rowPanel.add(labInfo);
			rowPanel.add(text[i]);
			
			rowPanel.setBounds(10, i*50+20, 230, 50);
			
			panel.add(rowPanel);
		}
		
		JPanel InfoPan = new JPanel();
		InfoPan.setBounds(25, 210,200, 40);
		
		alert = new JLabel("");
		alert.setFont(new Font(null,Font.BOLD,20));
		alert.setVerticalAlignment(SwingConstants.CENTER);
		alert.setForeground(Color.RED);
		
		InfoPan.add(alert);
		
		panel.add(InfoPan);
		panel.add(pan);
		
		copeDate();
		
		return panel;
	}
	
	//设置失去焦点的事件
	private void copeDate() {
		
		text[1].addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				if(text[1].getText().length()>0) {
					boolean isCrrect = isCrrect(text[0].getText(),text[1].getText());
					if(!isCrrect) {
						alert.setText("旧密码错误");
					}else {
						alert.setText("");
					}
				}
				
			
			}
			
		});
		
		text[3].addFocusListener(new FocusListener() {

			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				String newPwd1 = text[2].getText();
				String newPwd2 = text[3].getText();
				
				//处理空指针异常
				if(newPwd1.length()>0) {
					if(!newPwd1.equals(newPwd2)) {
						alert.setText("两次密码输入不一致");
					}else {
						alert.setText("");
					}
				}
				
				
			
			}
			
		});
				
	}
	
	public boolean isCrrect(String user,String pwd) {		
		boolean loginSuccessful = false;
		
		String Sql = "Select * from loginInfo where user=? and pwd = ?";
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement(Sql);
			pst.setString(1, user);
			pst.setString(2, pwd);
			rs = pst.executeQuery();
			
			if(rs.next()) {
				loginSuccessful = true;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loginSuccessful = false;
		}finally {
			JDBCUtil.close(rs, pst, null);
		}		
		
		return loginSuccessful;
	}
	
	//操作按钮
	
	public JPanel updateBtn() {
		JPanel panel = new JPanel();
		panel.setBounds(10,260, 230, 50);
		JButton confirm = new JButton("确认");
		confirm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				boolean isUpdate = false;
			
				isUpdate = UpdateData(conn,"loginInfo",text[0].getText());
					
				String message = "";
				if(isUpdate) {
					message = "数据已保存更新至数据库";
					
					for(int i =0;i<text.length;i++) {
						text[i].setText("");
					}
				}else {
					message="数据更新失败，请重新尝试";
				}
					
				JOptionPane.showMessageDialog(interFrame,message,"提示信息",JOptionPane.INFORMATION_MESSAGE);
	
				
			}
			
		});
		
		JButton quit = new JButton("退出");
		quit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int option = JOptionPane.showConfirmDialog(interFrame, "确认退出？","系统提示",JOptionPane.YES_NO_OPTION);
				if(option == JOptionPane.YES_OPTION) {
					interFrame.dispose();
					interFrame = null;
				}
			}
			
		});				

		panel.add(confirm);
		panel.add(quit);
		
		return panel;
	}
	
	public boolean UpdateData(Connection conn,String tableName,String ItemsID) {
		String sql = "update "+tableName+" set pwd=? where user=?";
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql);
			
			pst.setString(1, text[3].getText());
			pst.setString(2, ItemsID);
			
			int count = pst.executeUpdate();
			
			if(count > 0) {
				return true;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JDBCUtil.close(null, pst, null);
		}		
		
		return false;		
	}
	
}
