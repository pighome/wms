package com.zjnu.empPanel;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import com.zjnu.Datas.TableOperate;
import com.zjnu.ToolBar.ToolBarButtons;
import com.zjnu.Util.WMSUtils;

public class OperateUtilPanel {
	//修改，增加信息面板，查询信息面板，信息面板
	private static ArrayList<JTextField> arrayList = null;
	private static String[] obj = null;
	private static JFrame interFrame = null;
	
	private static String[] icon = {
			"/com/zjnu/sources/insert.png","/com/zjnu/sources/delete.png",
			"/com/zjnu/sources/update.png","/com/zjnu/sources/select.png"
	};
	
	//初始化数据
	public static void empInfoPanel(String[] fields,String[] fields_EN,JPanel pan) {
		 interFrame = new JFrame("信息窗口");
		 interFrame.setSize(400, 300);		 
		 interFrame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
	
		 obj = fields_EN;
		 arrayList = new ArrayList<>();
		 JPanel panel = new JPanel();
		 
		 for(int i = 0;i<fields.length;i++) {
			 JPanel rowPanel = new JPanel();
			 JLabel labInfo = new JLabel(fields[i]+":");
			 JTextField textInfo = null;
			 if(i == fields.length-1) {
				 textInfo = new JTextField(30);
			 }else {
				 textInfo = new JTextField(10);
			 }
			 
			 rowPanel.add(labInfo);
			 rowPanel.add(textInfo);
			 
			 arrayList.add(textInfo);
			 panel.add(rowPanel);
		 }
		 
		 panel.add(pan);//加入按钮面板
			//可能需要调整一下布局
		interFrame.add(panel);
			
		interFrame.setVisible(true);

	}
	
	 /*
	 * 分别设置增，删，改，查等操作的SQL语句
	 */
	public static boolean UpdateData(Connection conn,String tableName,Object ItemsID) {
		TableOperate oper = new TableOperate(conn);
		
		oper.setTableName(tableName);
		
		Map<String,Object> toolMap = new HashMap<String,Object>();
		
		for(int i = 0;i<arrayList.size();i++) {
			String value = arrayList.get(i).getText();
			if( value.length() > 0 && !arrayList.isEmpty()) {
				toolMap.put(obj[i]+"",value);
			}
			
		}
		
		int count = oper.updateData(toolMap, ItemsID);
		
		//获取ArrayList中的数据，并存入到对应的map数据表中
		//只需要放数据到tool和对应的ID号
		
		if(count > 0 )
			return true;
		return false;
		
	}
	
	//设置删除语句
	public static boolean DeleteData(Connection conn,String tableName,Object...ItemsID) {
		TableOperate oper = new TableOperate(conn);
		
		oper.setTableName(tableName);
		
		int count = oper.deleteData(ItemsID);
		
		if(count > 0) 
			return true;
		return false;
	}
	
	//增加数据的语句
	public static boolean InsertData(Connection conn,String tableName) {
		TableOperate oper = new TableOperate(conn);
		
		oper.setTableName(tableName);
		
		Map<String,Object> toolMap = new HashMap<>();
		
		for(int i = 0;!arrayList.isEmpty();i++) {
			
			String value = arrayList.get(i).getText();
			if( value.length() > 0 && !arrayList.isEmpty()) {
				toolMap.put(obj[i]+"",value);
			}
		
		}
		
		int count = oper.insertData(toolMap);
		if(count > 0)
			return true;
		return false;		
		
	}
	
	
	public static ResultSet SearchData(Connection conn,String tableName,Object Items) {
		PreparedStatement pst = null;
		ResultSet rs = null;
		System.out.println("Items:--------"+Items);
		try {
			StringBuffer sql = new StringBuffer("select * from ");
			sql.append(tableName);
			sql.append(" where (");
			sql.append(WMSUtils.searchItemID(tableName));
			sql.append(" like ?) or (");
			sql.append(WMSUtils.searchItemName(tableName));
			sql.append(" like ?)");
			
			pst = conn.prepareStatement(sql.toString());
			
			pst.setObject(1,Items+"%");
			pst.setObject(2, Items+"%");
			
			rs = pst.executeQuery();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		if(rs != null) {
			return rs;
		}
		return null;
	}
	
	/*
	 * 设置按钮的点击事件，分为三类，修改按钮，增加按钮，和查询按钮
	 */
	
	//设置菜单栏按钮
	public static Icon getIconURL(String icon) {
		return new ImageIcon(ToolBarButtons.class.getResource(icon));
	}
	//样式
	private static void setStyle(JButton button,String icon) {
			
		Border border = BorderFactory.createLoweredSoftBevelBorder();
		button.setBackground(Color.decode("#FFE4B5"));
		button.setIcon(getIconURL(icon));
		button.setBorder(border);
			
	}
		
	
	public static JButton[] getBtn() {
		
		JButton[] btn = new JButton[4];
		
		for(int i = 0;i<4;i++) {
			btn[i] = new JButton();
			setStyle(btn[i],icon[i]);
		}
	
		return btn;
	}
	
	//清空面板中的数据
	public static void clearDataPanel(ArrayList<JTextField> arrayList) {
		while(!arrayList.isEmpty()) {
			arrayList.get(0).setText("");
			arrayList.remove(0);
		}
	}
	
	//插入小面板的按钮
	public static JPanel insertBtn(Connection conn,String tableName) {
		JPanel panel = new JPanel();
		JButton confirm = new JButton("确认");
		confirm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				boolean isInsert = false;
				isInsert = InsertData(conn,tableName);
				String message = "";
				if(isInsert) {
					message = "数据已保存更新至数据库";
					clearDataPanel(arrayList);
				}else {
					message="数据更新失败，请重新尝试";
				}
				
				JOptionPane.showMessageDialog(interFrame,message,"提示信息",JOptionPane.INFORMATION_MESSAGE);
		      
			}
			
		});
		
		JButton quit = new JButton("退出");
		quit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int option = JOptionPane.showConfirmDialog(interFrame, "确认退出？","系统提示",JOptionPane.YES_NO_OPTION);
				if(option == JOptionPane.YES_OPTION) {
					interFrame.dispose();
					interFrame = null;
				}
			}
			
		});		
		
		panel.add(confirm);
		panel.add(quit);
		
		return panel;
	}
	
	//修改小面板的按钮
	public static JPanel updateBtn(Connection conn,String tableName,Object ItemsID) {
		JPanel panel = new JPanel();
		JButton confirm = new JButton("确认");
		confirm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				boolean isUpdate = false;
				isUpdate = UpdateData(conn,tableName,ItemsID);
				String message = "";
				if(isUpdate) {
					message = "数据已保存更新至数据库";
					clearDataPanel(arrayList);
				}else {
					message="数据更新失败，请重新尝试";
				}
				
				JOptionPane.showMessageDialog(interFrame,message,"提示信息",JOptionPane.INFORMATION_MESSAGE);
		      
			}
			
		});
		
		JButton quit = new JButton("退出");
		quit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int option = JOptionPane.showConfirmDialog(interFrame, "确认退出？","系统提示",JOptionPane.YES_NO_OPTION);
				if(option == JOptionPane.YES_OPTION) {
					interFrame.dispose();
					interFrame = null;
				}
			}
			
		});				

		panel.add(confirm);
		panel.add(quit);
		
		return panel;
	}

}
