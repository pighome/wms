package com.zjnu.Util;

public class WMStables {
	//所有的表名
	/*
	 * 此类常量表示表的名称常量
	 */
	public final static String administrator = "administrators";
	public final static String authority = "authority";
	public final static String bstock = "bstock";
	public final static String dept = "dept";
	public final static String employee = "employees";
	public final static String InMaterials = "in_materials";
	public final static String InTable = "in_tables";
	public final static String Inventory = "Inventory";
	public final static String ledger = "ledger";
	public final static String mainStock = "mainStock";
	public final static String OutMaterials = "out_materials";
	public final static String OutTable = "out_tables";
	public final static String supply = "supply";
	public final static String wareHouse = "warehouse";
	public final static String Materials = "Materials";
	
}
