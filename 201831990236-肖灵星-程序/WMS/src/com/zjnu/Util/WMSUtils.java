package com.zjnu.Util;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class WMSUtils {
	private WMSUtils() {
		
	}
	
	public static String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
		String formatTime = null;
		formatTime = sdf.format(date);
		return formatTime;
	}
	
	
	public static String searchItemID(String tableName) {
		
		if(WMStables.administrator.equalsIgnoreCase(tableName)) {
			
			return WMSfields.adminNo;
			
		}else if(WMStables.bstock.equalsIgnoreCase(tableName)||WMStables.wareHouse.equalsIgnoreCase(tableName)){
			
			return WMSfields.wareHouseNo;
			
		}else if(WMStables.dept.equalsIgnoreCase(tableName)) {
			
			return WMSfields.deptno;
			
		}else if(WMStables.employee.equalsIgnoreCase(tableName)) {
			
			return WMSfields.empNo;
			
		}else if(WMStables.InMaterials.equalsIgnoreCase(tableName) ||WMStables.InTable.equalsIgnoreCase(tableName)) {
			
			return WMSfields.InOdd;
			
		}else if(WMStables.OutMaterials.equalsIgnoreCase(tableName)||WMStables.OutTable.equalsIgnoreCase(tableName)){
			
			return WMSfields.OutOdd;
			
		}else if(WMStables.Inventory.equalsIgnoreCase(tableName)) {
			
			return WMSfields.InventoryOdd;
			
		}else if(WMStables.ledger.equalsIgnoreCase(tableName)) {
			
			return WMSfields.Odds;
			
		}else if(WMStables.supply.equalsIgnoreCase(tableName)) {
			
			return WMSfields.supplierId;
			
		}else if(WMStables.mainStock.equalsIgnoreCase(tableName)) {
			
			return WMSfields.ItemNo;
			
		}else if(WMStables.authority.equalsIgnoreCase(tableName)) {
			
			return WMSfields.powerID;
			
		}else {
			return null;
		}
	}
	
	
	//通过表名，查询字段的名称
	public static String searchItemName(String tableName) {
		
		if(WMStables.administrator.equalsIgnoreCase(tableName)) {
			
			return WMSfields.adminName;
			
		}else if(WMStables.wareHouse.equalsIgnoreCase(tableName)){
			
			return WMSfields.wareHouseName;
			
		}else if(WMStables.dept.equalsIgnoreCase(tableName)) {
			
			return WMSfields.dName;
			
		}else if(WMStables.employee.equalsIgnoreCase(tableName)) {
			
			return WMSfields.empName;
			
		}else if(WMStables.InMaterials.equalsIgnoreCase(tableName)||WMStables.OutMaterials.equalsIgnoreCase(tableName)) {
			
			return WMSfields.ItemName;
			
		}else if(WMStables.InTable.equalsIgnoreCase(tableName)) {
			
			return WMSfields.supplierId;
			
		}else if(WMStables.OutTable.equalsIgnoreCase(tableName)) {
			
			return WMSfields.deptno;
			
		}else if(WMStables.Inventory.equalsIgnoreCase(tableName)) {
			
			return WMSfields.InventoryOdd;
			
		}else if(WMStables.ledger.equalsIgnoreCase(tableName)) {
			
			return WMSfields.Odds;
			
		}else if(WMStables.supply.equalsIgnoreCase(tableName)) {
			
			return WMSfields.supplierName;
			
		}else if(WMStables.mainStock.equalsIgnoreCase(tableName)) {
			
			return WMSfields.ItemName;
			
		}else {
			return null;
		}
	}
	
	public static int insertSQL(Connection conn,String tableName,Map<String,Object> data) throws SQLException {
	
		int count = 0;
		String insertSql = null;
		PreparedStatement pst = null;		
		StringBuffer addSql = new StringBuffer("insert into ");
		
		addSql.append(tableName);
		addSql.append("(");
		
		Iterator<String> iterator = data.keySet().iterator();
		
		String key = null;
		List<Object> list = new ArrayList<>();
		
		int index = 0;
		
		while(iterator.hasNext()) {
			key = iterator.next();
			list.add(index,data.get(key));
			addSql.append(key);
			if(iterator.hasNext()) {
				addSql.append(",");
			}
			index++;
		}
		
		addSql.append(") values(");
		
		while((--index)>=0) {
			if(index == 0) {
				addSql.append("?)");
			}else {
				addSql.append("?,");
			}				
			
		}
		
		insertSql = addSql.toString();				
			
		pst = conn.prepareStatement(insertSql);
		
		index = 1;
		Object value = null;
		
		while(!list.isEmpty()) {
			value = list.get(0);
			list.remove(0);
			pst.setObject(index++, value);			
		}
		
		list.clear();
		data.clear();
		count = pst.executeUpdate();
		
		JDBCUtil.close(null, pst, null);
		
		return count;		
	}
	
	@SuppressWarnings({ "null", "unchecked" })
	public static int deleteSQL(Connection conn,String tableName,Object...ItemIDs) throws SQLException {
		
		int count = 0;
		
		PreparedStatement pst = null;		
		StringBuffer addSql = new StringBuffer("delete from ");
		
		addSql.append(tableName);
		
		String option = searchItemID(tableName);
		addSql.append(" where ");
		
		if(ItemIDs[0] instanceof Map) {
			addSql.append("(");
			
			Map<String,Object> map = (Map<String,Object>)ItemIDs[0];			
			Iterator<String> iterator = map.keySet().iterator();
			List<Object> list = new ArrayList<Object>();
			
			String key = null;
			Object value = null;
			while(iterator.hasNext()) {
				key = iterator.next();
				value = map.get(key);
				list.add(value);
				addSql.append(key);
				addSql.append(" = ?");
				if(iterator.hasNext()) {
					addSql.append(" and ");
				}else {
					addSql.append(")");
				}
			}
			
			int index = 1;
			while(!list.isEmpty()) {
				value = list.get(0);
				list.remove(0);
				pst.setObject(index++, value);
			}
			
		}else {
		
			addSql.append(option);
			addSql.append(" in (");		
			
			String deleteSql = null;
			
			for(int i =0;i<ItemIDs.length;i++) {
				if(i == (ItemIDs.length-1)) {
					addSql.append("?)");
				}else {
					addSql.append("?,");
				}				
			}			
		
			deleteSql = addSql.toString();			
			
			pst = conn.prepareStatement(deleteSql);
			
			for(int i = 0 ;i<ItemIDs.length;i++) {
				pst.setObject(i+1, ItemIDs[i]);			
			}
		}
		count = pst.executeUpdate();		
		
		JDBCUtil.close(null, pst, null);
		
		return count;
	}

	

public static int updateSQL(Connection conn,String tableName,Map<String,Object> data,Object...ItemIDs) throws SQLException {
		
		int count = 0;
		
		PreparedStatement pst = null;		
		StringBuffer addSql = new StringBuffer("update ");
		addSql.append(tableName);
		addSql.append(" set ");
		
		String updateSql = null;	//sql语句
		
		String key = null;
		Object value = null;
		Iterator<String> iterator = data.keySet().iterator();

		
		List<Object> list = new ArrayList<>();	//用来存储数据集
		
		while(iterator.hasNext()) {
			key = iterator.next();
			value = data.get(key);
			addSql.append(key);
			addSql.append("=?");
			list.add(value);
			if(iterator.hasNext()){
				addSql.append(",");
			}
			
		}
		
		addSql.append(" where ");	
		
		//区分条件判断数据
		if(ItemIDs[0] instanceof Map) {
			
			Map<String,Object> map = (Map<String, Object>)ItemIDs[0];
			
			addSql.append("(");
			
			Iterator<String> iterator2 = map.keySet().iterator();
			
			while(iterator2.hasNext()) {
				key = iterator2.next();
				value =map.get(key);
				addSql.append(key);
				addSql.append(" = ?");
				list.add(value);
				if(iterator2.hasNext()){
					addSql.append(" and ");
				}else {
					addSql.append(")");
				}				
			}
			
			//预处理sql语句
			updateSql = addSql.toString();
			pst = conn.prepareStatement(updateSql);
			
			//放入where语句的结果	
			int index = 1;	
			
			while(!list.isEmpty()) {
				value = list.get(0);
				list.remove(0);
				pst.setObject(index++, value);
			}
			
			
			map.clear();
			
		}else {
			String option = searchItemID(tableName);		
			
			addSql.append(option);
			addSql.append(" in (");		
	
			for(int i =0;i<ItemIDs.length;i++) {
				if(i == (ItemIDs.length-1)) {
					addSql.append("?)");
				}else {
					addSql.append("?,");
				}				
			}			
			
			updateSql = addSql.toString();
		
			pst = conn.prepareStatement(updateSql);
			
			//放入set的键值对数据
			int index = 1;
			while(!list.isEmpty()) {
				pst.setObject(index++, list.get(0));
				list.remove(0);
			}
			
			//放入where子句的条件数据
			for(int i =0;i<ItemIDs.length;i++) {
				pst.setObject(index++, ItemIDs[i]);
			}
			
		}		
		
		
		list.clear();
		data.clear();
		
		count = pst.executeUpdate();		
		JDBCUtil.close(null, pst, null);
		
		return count;
	}
	
	/*
	 * 存在一个问题，解决多个条件，输入查询
	 */
	

	public static ResultSet selectSQL(Connection conn,String tableName,Object condition) throws SQLException {
		
		String selectSql = null;
		ResultSet rs = null;
		PreparedStatement pst = null;
		
		StringBuffer addSql = new StringBuffer("select * from ");
		addSql.append(tableName);
		addSql.append(" where ");
		
		if(condition instanceof Map) {
			System.out.println("yes------------");
			
			addSql.append("(");
			
			Map<String,Object> map = (Map<String, Object>) condition;
			Iterator<String> iterator = map.keySet().iterator();
			String key = null;
			Object value = null;
			List<Object> list = new ArrayList<>();
			while(iterator.hasNext()) {
				key = (String) iterator.next();
				value = map.get(key);
				addSql.append(key);
				addSql.append(" = ?");
				list.add(value);
				if(iterator.hasNext()){
					addSql.append(" and ");
				}else {
					addSql.append(")");
				}				
			}
			
			selectSql = addSql.toString();
			
			pst = conn.prepareStatement(selectSql);			
			
			//给预编译程序赋值
			int i = 1;
			while(!list.isEmpty()) {
				value = list.get(0);
				list.remove(0);
				pst.setObject(i++, value);
			}			
			
			map.clear();
			list.clear();
			
		}else {
		
			String option1 = searchItemName(tableName);
			String option2 = searchItemID(tableName);
			
			addSql.append(option1);
			addSql.append(" = ? or ");
			addSql.append(option2);
			addSql.append(" = ?");
			
			
			selectSql = addSql.toString();
			
			pst = conn.prepareStatement(selectSql);
			
			pst.setObject(1, condition);
			pst.setObject(2, condition);
			
		}
			
		rs = pst.executeQuery();
			
		if(rs != null) {
			return rs;
		}
		
		JDBCUtil.close(null, pst, null);
		
		return null;
	}
	
	/*
	 * 键值对进行查询
	 * 
	 */
	
	
	public static boolean truncateSQL(Connection conn,String tableName) throws SQLException {
		PreparedStatement pst = null;
		String truncateSql = null;
		StringBuffer addSql = new StringBuffer("truncate ?");
		truncateSql = addSql.toString();
		pst  = conn.prepareStatement(truncateSql);
		pst.setString(1, tableName);
		
		boolean isClear = false;
		isClear = pst.execute();	//此语句，执行完成返回false，否则返回true。
		
		
		JDBCUtil.close(null, pst, null);
	
		return !isClear;
	}
	
}
