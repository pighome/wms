package com.zjnu.Util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WMSDatas {
	
	private static Map<String,Object> warehouse = null;
	private static Map<String,Object> administrators = null;
	private static Map<String,Object> bstock = null;
	private static Map<String,Object> dept = null;
	private static Map<String,Object> employees = null;
	private static Map<String,Object> InMaterials = null;
	private static Map<String,Object> InTables = null;
	private static Map<String,Object> Inventory = null;
	private static Map<String,Object> ledger = null;
	private static Map<String,Object> mainStock = null;
	private static Map<String,Object> OutMaterials = null;
	private static Map<String,Object> OutTables = null;
	private static Map<String,Object> supply = null;
	
	private WMSDatas() {
		
	}
	/*
	 * 静态代码块类加载时只执行一次，用于初始化Map对象
	 */

	static {
		warehouse = new HashMap<String,Object>();
		administrators = new HashMap<String,Object>();
		bstock = new HashMap<String,Object>();
		dept = new HashMap<String,Object>();
		employees = new HashMap<String,Object>();
		InMaterials = new HashMap<String,Object>();
		InTables = new HashMap<String,Object>();
		OutMaterials = new HashMap<String,Object>();
		OutTables = new HashMap<String,Object>();
		Inventory = new HashMap<String,Object>();
		ledger = new HashMap<String,Object>();
		mainStock = new HashMap<String,Object>();
		supply = new HashMap<String,Object>();
		
	}
	
	public static Map<String, Object> getWarehouse() {
		return warehouse;
	}

	public static Map<String, Object> getAdministrators() {
		return administrators;
	}

	public static Map<String, Object> getBstock() {
		return bstock;
	}

	public static Map<String, Object> getDept() {
		return dept;
	}

	public static Map<String, Object> getEmployees() {
		return employees;
	}

	public static Map<String, Object> getInMaterials() {
		return InMaterials;
	}

	public static Map<String, Object> getInTables() {
		return InTables;
	}

	public static Map<String, Object> getInventory() {
		return Inventory;
	}

	public static Map<String, Object> getLedger() {
		return ledger;
	}

	public static Map<String, Object> getMainStock() {
		return mainStock;
	}

	public static Map<String, Object> getOutMaterials() {
		return OutMaterials;
	}

	public static Map<String, Object> getOutTables() {
		return OutTables;
	}

	public static Map<String, Object> getSupply() {
		return supply;
	}

	
	/* ------1------
	 * 设置仓管数据的方法
	 */
	
	public static void setWarehouse(Object WarehouseNo,Object WarehouseName,Object empno,Object dateTime,Object areas,Object located,Object Remarks) {
		
		//dateTime = WMSUtils.formatDate(dateTime);
		/*
		Object[] data = {
				WarehouseNo,WarehouseName,empno,areas,nowTime,located,Remarks
		};
		*/
		if(WarehouseNo != null) {
			warehouse.put(WMSfields.wareHouseNo, WarehouseNo);
		}	
		
		if(WarehouseName != null) {
			warehouse.put(WMSfields.wareHouseName,WarehouseName);	
		}
		
		if(empno != null) {			
			warehouse.put(WMSfields.empNo, empno);
		}
		
		if(dateTime != null) {			
			warehouse.put(WMSfields.dateTime, WMSUtils.formatDate((Date)dateTime));
		}
		
		if(areas != null) {
			warehouse.put(WMSfields.areas,areas);	
		}
		
		if(located != null) {			
			warehouse.put(WMSfields.located, located);
		}
		
		if(Remarks != null) {			
			warehouse.put(WMSfields.remarks, Remarks);
		}
		
	}
	
	/*-----2-----
	 * 设置供应商的数据的方法
	 */
	public static void setSupply(Object supplierID,Object supplierName,Object concats,Object phoneNumber,Object adress,Object Remarks) {
		/*
		Object[] data = {
				supplierID,supplierName,concats,phoneNumber,adress,Remarks
		};
		*/
		if(supplierID != null) {
			supply.put(WMSfields.supplierId, supplierID);
		}	
		
		if(supplierName != null) {
			supply.put(WMSfields.supplierName, supplierName);
		}
		
		if(concats != null) {
			supply.put(WMSfields.concats, concats);
		}
		
		if(phoneNumber != null) {
			supply.put(WMSfields.callPhone, phoneNumber);
		}
		
		if(adress != null) {
			supply.put(WMSfields.adress, adress);
		}	
		
		if(Remarks != null) {			
			supply.put(WMSfields.remarks, Remarks);
		}
		
	}
	
	/*-----3-----
	 * 设置本仓数据的方法
	 */
	
	public static void setBstock(Object ItemNo,Object WarehouseNo,Object Nowamout,Object avgprice,Object AmountSum,Object Remarks) {
		
		if(WarehouseNo != null) {
			bstock.put(WMSfields.wareHouseNo, WarehouseNo);
		}	
		
		if(ItemNo != null) {
			bstock.put(WMSfields.ItemNo,ItemNo);	
		}
		
		if(Nowamout != null) {
			bstock.put(WMSfields.nowAmout, Nowamout);
		}
		
		if(avgprice != null) {
			bstock.put(WMSfields.avgPrice, avgprice);
		}	
		
		if(AmountSum != null) {
			bstock.put(WMSfields.amountSum, AmountSum);
		}
		
		if(Remarks != null) {			
			bstock.put(WMSfields.remarks, Remarks);
		}
		
	}
	
	/*-----4------
	 * 设置总仓数据的方法
	 * 
	 * 可能用不上
	 */
	
	public static void setMainStock(Object ItemNo,Object ItemName,Object norms,Object company,Object avgprice,Object Nowamout,Object AmountSum,Object Remarks) {
		/*
		Object[] data = {
				ItemNo,ItemName,norms,company,avgprice,Nowamout,AmountSum,Remarks
		};
		*/
		if(ItemName != null) {
			mainStock.put(WMSfields.ItemName, ItemName);
		}	
		
		if(ItemNo != null) {
			mainStock.put(WMSfields.ItemNo,ItemNo);	
		}
		
		if(norms != null) {
			mainStock.put(WMSfields.norms, norms);
		}
		
		if(company != null) {
			mainStock.put(WMSfields.company, company);
		}	
		
		if(avgprice != null) {
			mainStock.put(WMSfields.avgPrice, avgprice);
		}
		
		if(Nowamout != null) {
			mainStock.put(WMSfields.nowAmout, Nowamout);
		}
		
		if(AmountSum != null) {
			mainStock.put(WMSfields.amountSum, AmountSum);
		}
		
		if(Remarks != null) {			
			mainStock.put(WMSfields.remarks, Remarks);
		}
		
	}
	
	/*-----5-----
	 * 设置台账数据
	 */
	
	public static void setLedger(Object Odds,Object ItemNo,Object WarehouseNo,Object INnumber,Object INprice,Object INSum,Object OUTnumber,Object OUTprice,Object OUTSum,Object BalanceNum,Object BalancePrice,Object BalanceSum,Object dateTime,Object Remarks) {
		/*
		Object[] data = {
				Odds,ItemNo,WarehouseNo,INnumber,INprice,INSumprice,OUTnumber,OUTprice,OUTSumprice, BalanceNum,BalancePrice,BalanceSum,Remarks
		};
		*/	
		
		if(Odds != null) {
			ledger.put(WMSfields.Odds,Odds);	
		}
		
		if(ItemNo != null) {
			ledger.put(WMSfields.ItemNo,ItemNo);	
		}
		
		if(WarehouseNo != null) {
			ledger.put(WMSfields.wareHouseNo, WarehouseNo);
		}
		
		if(INnumber != null) {
			ledger.put(WMSfields.InNumber, INnumber);
		}	
		
		if(INprice != null) {
			ledger.put(WMSfields.InPrice, INprice);
		}
		
		if(INSum != null) {
			ledger.put(WMSfields.InSum, INSum);
		}
		
		if(OUTnumber != null) {
			ledger.put(WMSfields.OutNumber,OUTnumber);
		}
		
		if(OUTprice != null) {
			ledger.put(WMSfields.OutPrice, OUTprice);
		}
		
		if(OUTSum != null) {
			ledger.put(WMSfields.OutSum, OUTSum);
		}
		
		if(BalanceNum!= null) {
			ledger.put(WMSfields.balanceNum, BalanceNum);
		}
		
		if(BalancePrice!= null) {
			ledger.put(WMSfields.balancePrice, BalancePrice);
		}
		
		if(BalanceSum!= null) {
			ledger.put(WMSfields.balanceSum, BalanceSum);
		}
		
		if(dateTime != null) {
			ledger.put(WMSfields.dateTime, WMSUtils.formatDate((Date)dateTime));
		}
		
		if(Remarks != null) {			
			ledger.put(WMSfields.remarks, Remarks);
		}
	
	}
	
	/*-----6-----
	 * 设置仓库盘点数据
	 */
	
	public static void setInventory(Object InventoryOdd,Object ItemNo,Object WarehouseNo,Object InventoryDate,Object InventoryNum,Object StockNum,Object ProfitLoss,Object Remarks,Object Operator) {
		/*
		Object[] data = {
				InventoryOdd,ItemNo,WarehouseNo,InventoryDate,InventoryNum,StockNum,ProfitLoss,Remarks,Operator
		};
		
		*/
		if(InventoryOdd != null) {
			Inventory.put(WMSfields.InventoryOdd, InventoryOdd);
		}	
		
		if(ItemNo != null) {
			Inventory.put(WMSfields.ItemNo,ItemNo);	
		}
		
		if(WarehouseNo != null) {
			Inventory.put(WMSfields.wareHouseNo, WarehouseNo);
		}
		
		if(InventoryDate != null) {
			Inventory.put(WMSfields.dateTime, WMSUtils.formatDate((Date)InventoryDate));
		}	
		
		if(InventoryNum != null) {
			Inventory.put(WMSfields.InventoryNum, InventoryNum);
		}
		
		if(StockNum != null) {
			Inventory.put(WMSfields.stockNum, StockNum);
		}
		
		if(ProfitLoss != null) {
			Inventory.put(WMSfields.profitLoss, ProfitLoss);
		}
		
		if(Remarks != null) {			
			Inventory.put(WMSfields.remarks, Remarks);
		}
		
		if(Operator != null) {
			Inventory.put(WMSfields.operator, Operator);
		}
		
	}
	
	/*-----7-----
	 * 设置员工数据
	 */
	public static void setEmployees(Object empNo,Object empName,Object job,Object salary,Object gender,Object callPhone,Object adress,Object deptno,Object Remarks) {
		/*
		Object[] data = {
				empNo,empName,job,salary,gender,callPhone,adress,deptno,Remarks
		};
		*/
		if(empNo != null) {
			employees.put(WMSfields.empNo, empNo);
		}
		
		if(empName != null) {
			employees.put(WMSfields.empName,empName);
		}
			
		if(job != null) {
			employees.put(WMSfields.job, job);
		}
		
		if(salary != null) {
			employees.put(WMSfields.salary, salary);
		}
		
		if("F".equalsIgnoreCase((String) gender) || "M".equalsIgnoreCase((String) gender)) {
			employees.put(WMSfields.gender,gender);
		}
		
		if(callPhone != null) {			
			employees.put(WMSfields.callPhone,callPhone);
		}
		
		if(adress != null) {			
			employees.put(WMSfields.adress, adress);
		}
		
		if(adress != null) {
			employees.put(WMSfields.deptno, deptno);
		}	
		
		if(Remarks != null) {
			employees.put(WMSfields.remarks, Remarks);
		}
		
	}
	
	
	
	/*----8-----
	 * 设置部门数据
	 */
	public static void setDept(Object Dname,Object location,Object avgsalary,Object numPersons,Object Remarks) {
		/*
		Object[] data = {
				Dname,location,avgsalary,numPersons,Remarks
		};
		*/
		if(Dname != null) {
			dept.put(WMSfields.dName, Dname);
		}
		
		if(location != null) {
			dept.put(WMSfields.adress, location);
		}
		
		if(avgsalary != null) {
			dept.put(WMSfields.avgSalary, avgsalary);
		}
		
		if(numPersons != null) {
			dept.put(WMSfields.persons, numPersons);
		}
		
		if(Remarks != null) {
			dept.put(WMSfields.remarks, Remarks);
		}
		
	}
	
	/*---9----
	 * 设置管理员数据
	 */
	
	public static void setAdministarators(Object adminName,Object gender,Object callPhone,Object adress,Object powerID,Object password,Object Remarks) {
		/*
		Object[] data = {
				adminName,gender,callPhone,adress,powerID,password,Remarks
		};
		*/
		if(adminName != null) {
			administrators.put(WMSfields.adminName, adminName);
		}
		
		if("F".equalsIgnoreCase((String) gender) || "M".equalsIgnoreCase((String) gender)) {
			administrators.put(WMSfields.gender, gender);	
		}
			
		if(callPhone != null) {
			administrators.put(WMSfields.callPhone, callPhone);
		}
		
		if(adress != null) {
			administrators.put(WMSfields.adress, adress);
		}
		
		if("A".equalsIgnoreCase((String) powerID) || "B".equalsIgnoreCase((String) powerID)) {
			administrators.put(WMSfields.powerID, powerID);
		}
		
		if(password != null) {
			administrators.put(WMSfields.password, password);
		}
		
		if(Remarks != null) {
			administrators.put(WMSfields.remarks, Remarks);
		}				
		
	}
	
	/*---10----
	 * 设置入库材料信息表数据
	 */
	public static void setInMaterials(Object ItemNo,Object ItemName,Object INOdd,Object norms,Object INnum,Object company,Object unitprice,Object total,Object Remarks) {
		
		if(ItemName != null) {
			InMaterials.put(WMSfields.ItemName, ItemName);
		}	
		
		if(ItemNo != null) {
			InMaterials.put(WMSfields.ItemNo,ItemNo);	
		}
		
		if(INOdd != null) {
			InMaterials.put(WMSfields.InOdd, INOdd);
		}
		
		if(norms != null) {
			InMaterials.put(WMSfields.norms, norms);
		}
		
		if(INnum != null) {
			InMaterials.put(WMSfields.InNumber, INnum);
		}
		
		if(company != null) {
			InMaterials.put(WMSfields.company, company);
		}	
		
		if(unitprice != null) {
			InMaterials.put(WMSfields.unitPrice, unitprice);
		}
		
		if(total != null) {
			InMaterials.put(WMSfields.total, total);
		}
		
		if(Remarks != null) {			
			InMaterials.put(WMSfields.remarks, Remarks);
		}
	}
	
	/*------11------
	 * 设置入库材料清单表
	 */
	public static void setInTables(Object Buyer,Object INOdd,Object sender,Object warehouseNo,Object InDate,Object checker,Object checkStatus,Object supplierID,Object creator,Object Remarks) {
		
		if(Buyer != null) {
			InTables.put(WMSfields.Buyer,Buyer);	
		}
		
		if(INOdd != null) {
			InTables.put(WMSfields.InOdd,INOdd);	
		}
		
		if(sender != null) {
			InTables.put(WMSfields.sender, sender);
		}
		
		if(warehouseNo != null) {
			InTables.put(WMSfields.wareHouseNo, warehouseNo);
		}
		
		if(InDate != null) {
			InTables.put(WMSfields.dateTime, WMSUtils.formatDate((Date)InDate));
		}	
		
		if(checker != null) {
			InTables.put(WMSfields.checker, checker);
		}
		
		if(checkStatus != null) {
			InTables.put(WMSfields.checkStatus, checkStatus);
		}
		
		if(supplierID != null) {
			InTables.put(WMSfields.supplierId, supplierID);
		}
		
		if(creator != null) {
			InTables.put(WMSfields.creator, creator);
		}
		
		if(Remarks != null) {			
			InTables.put(WMSfields.remarks, Remarks);
		}
		
	}
	
	/*------12------
	 * 设置出库材料信息表
	 */
	public static void setOutMaterials(Object ItemNo,Object ItemName,Object OutOdd,Object OutNum,Object unitprice,Object total,Object remarks) {
		
		if(ItemName != null) {
			OutMaterials.put(WMSfields.ItemName, ItemName);
		}	
		
		if(ItemNo != null) {
			OutMaterials.put(WMSfields.ItemNo,ItemNo);	
		}
		
		if(OutOdd != null) {
			OutMaterials.put(WMSfields.OutOdd,OutOdd);
		}
		
		if(OutNum != null) {
			OutMaterials.put(WMSfields.OutNumber, OutNum);
		}		
		
		if(unitprice != null) {
			OutMaterials.put(WMSfields.unitPrice, unitprice);
		}
		
		if(total != null) {
			OutMaterials.put(WMSfields.total, total);
		}
		
		if(remarks != null) {			
			OutMaterials.put(WMSfields.remarks, remarks);
		}
	}
	
	/*-----13----
	 * 设置出库材料清单表
	 */
	public static void setOutTables(Object receiver,Object OutOdd,Object deptno,Object warehouseNo,Object OutDate,Object checker,Object checkStatus,Object remarks,Object creator) {

		if(receiver != null) {
			OutTables.put(WMSfields.receiver,receiver);	
		}
		
		if(OutOdd != null) {
			OutTables.put(WMSfields.OutOdd,OutOdd);	
		}
		
		if(deptno != null) {
			OutTables.put(WMSfields.deptno, deptno);
		}
		
		if(warehouseNo != null) {
			OutTables.put(WMSfields.wareHouseNo, warehouseNo);
		}
		
		if(OutDate != null) {
			OutTables.put(WMSfields.dateTime, WMSUtils.formatDate((Date)OutDate));
		}	
		
		if(checker != null) {
			OutTables.put(WMSfields.checker, checker);
		}
		
		if(checkStatus != null) {
			OutTables.put(WMSfields.checkStatus, checkStatus);
		}
		
		if(creator != null) {
			OutTables.put(WMSfields.creator, creator);
		}
		
		if(remarks != null) {			
			OutTables.put(WMSfields.remarks, remarks);
		}
		
	}
}
