package com.zjnu.Util;

import java.sql.*;
import java.util.ResourceBundle;

public class JDBCUtil {
	
	private JDBCUtil() {}
	
	//注册数据连接驱动
	static {
		ResourceBundle bundle = ResourceBundle.getBundle("jdbc");
		String driver = bundle.getString("driver");
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * 获取数据连接对象
	 * @return连接对象
	 */
	
	public static Connection getConnection(String url,String user,String password) throws SQLException {
		
		Connection conn = DriverManager.getConnection(url, user, password);
		return conn;
	}
	
	public static void close(ResultSet rs,Statement stmt,Connection conn) {
		if(rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

		if(conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
