package com.zjnu.Util;

import java.awt.Graphics;
import java.awt.Image;
import java.net.URL;

import javax.swing.ImageIcon;

public class ImageUtils {
	
	private ImageUtils() {
		
	}
	
	public static Image getImage(String url) {
		URL path = ImageUtils.class.getResource(url);	//获取图片的绝对路径
		ImageIcon icon = new ImageIcon(path);
		return icon.getImage();
	}
	
	public static void paintComponent(Graphics g) {
		Image img = getImage("/com/zjnu/sources/login.jpg");
		g.drawImage(img, 0, 0, 400, 300, null);
		
	}
}
