package com.zjnu.Util;

public class WMSfields {
	
	/*
	 * 此类常量表示表的各个字段的名称
	 */
	//公共字段
	
	//所有的表
	public final static String remarks = "Remarks";	
	//employee，administrator，supply	
	public final static String callPhone = "call_phone";	
	//warehouse,admininstator,employee,supply	
	public final static String adress = "adress";	
	//warehouse,employee
	public final static String empNo = "emp_no";
	//几乎所有字段
	public final static String ID = "ID";
	//administrator,employee
	public final static String gender = "gender";
	//dept,employee,out_table
	public final static String deptno = "deptno";
	//In_materials,out_materials,mainstock
	public final static String ItemNo = "Item_No";
	public final static String ItemName = "Item_Name";	
	public final static String unitPrice = "unit_price";
	public final static String total = "Total";
	//intables,outtables,ledger
	public final static String dateTime = "dateTime";
	
	//仓库信息字段
	public final static String wareHouseNo = "Warehouse_No";
	public final static String wareHouseName = "Warehouse_Name";
	public final static String areas = "areas";
	public final static String createDate = "Create_Date";
	public final static String located = "located";
	
	//管理员字段
	public final static String adminNo = "admin_NO";
	public final static String adminName = "admin_Name";
	public final static String powerID = "powerID";
	public final static String password = "password";
	
	
	//权限表信息字段
	public final static String powerContent = "powerContent";
	
	//供应商字段
	//public final static String supplierID = "supplierId";
	public final static String supplierName = "supplierName";
	public final static String concats = "concats";
	
	//员工字段
	public final static String empName = "emp_name";
	public final static String job = "job";
	public final static String salary = "salary";
	
	//入库、出库材料信息表	                            
	public final static String InOdd = "IN_Odd";
	public final static String norms = "norms";
	public final static String InNumber = "IN_number";
	public final static String company = "company";
	public final static String OutOdd = "OUT_Odd";
	public final static String OutNumber = "OUT_number";
	
	//入库、出库材料清单表
	public final static String Buyer = "Buyer";                       	
	public final static String sender = "Sender";
	public final static String checker = "Checker";
	public final static String checkStatus = "Check_Status";	                            
	public final static String supplierId = "supplierID";
	public final static String creator = "creator";
	public final static String receiver = "Receiver";
	
	//部门表清单
	public final static String dName = "Dname"; 
	public final static String avgSalary = "avgsalary";
	public final static String persons = "persons";
	
	//c仓库盘点数据字段
	public final static String InventoryOdd = "Inventory_Odd";
	public final static String InventoryNum = "Inventory_num";	
	public final static String stockNum = "stock_num";
	public final static String profitLoss = "profit_loss";
	public final static String operator = "Operator";
	
	//台账数据字段表
	public final static String Odds = "Odds";
	public final static String InPrice = "IN_price";
	public final static String InSum = "IN_sum";
	public final static String OutPrice = "OUT_price";
	public final static String OutSum = "OUT_sum";
	public final static String balanceNum = "balance_number";
	public final static String balancePrice = "balance_price";
	public final static String balanceSum = "balance_sum";

	//本仓数据字段，和总仓数据字段
	public final static String nowAmout = "Now_amout";
	public final static String avgPrice = "avg_price";
	public final static String amountSum = "Amount_Sum";	
	
}
