package com.zjnu.procedure;

import java.sql.*;
import java.util.ResourceBundle;
import com.zjnu.Util.JDBCUtil;


public class Login {
	private static Connection conn = null;
	private String user = null;
	private String pwd = null;
	private String message = null;
	

	static {
		
	}
	
	
	public Login() {
		this(null,null);
	}
	
	public Login(String user,String pwd) {
		this.user = user;
		this.pwd = pwd;
		Register();
	}

	//获取conn连接器
	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		Login.conn = conn;
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	private void Register() {
		
		ResourceBundle bundle = ResourceBundle.getBundle("jdbc");
		String url = bundle.getString("url");
		String user = bundle.getString("user");
		String pwd = bundle.getString("password");
		try {
			conn = JDBCUtil.getConnection(url, user, pwd);
			System.out.println("数据库连接成功");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			message = "数据库连接失败";
			System.out.println(message);
			
			e.printStackTrace();
		}

	}

	
	public boolean isLogin() {		
		boolean loginSuccessful = false;
		
		String Sql = "Select * from loginInfo where user=? and pwd = ?";
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = conn.prepareStatement(Sql);
			pst.setString(1, user);
			pst.setString(2, pwd);
			rs = pst.executeQuery();
			
			if(rs.next()) {
				loginSuccessful = true;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loginSuccessful = false;
		}finally {
			JDBCUtil.close(rs, pst, null);
		}		
		
		return loginSuccessful;
	}
	

	
}
