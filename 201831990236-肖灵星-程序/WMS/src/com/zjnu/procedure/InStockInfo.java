package com.zjnu.procedure;

import java.sql.*;
import java.util.*;
import java.util.Date;

import com.zjnu.Datas.*;
import com.zjnu.Interface.DataOperation;
import com.zjnu.Util.*;

public class InStockInfo {
	
	private Connection conn = null;
	
	private static Map<String,Object> data = null;
	private static Map<String,Object> tempMap = null;	
	private static Map<String,Object> toolMap = null;
	
	private DataOperation operate = null;
	//private int count = 0;
	
	static {
		data = new HashMap<String,Object>(); 
		tempMap = new HashMap<String,Object>(); 
		toolMap = new HashMap<String,Object>();
	}
	
	public InStockInfo() {
		this(null);
	}
	
	public InStockInfo(Connection conn) {
		this.conn = conn;
	}
	
	public void setConnection(Connection conn) {
		this.conn = conn;
	}
	
	public boolean InCopeWith() {
		
		boolean Successfully = false;
		int count = 0;
		
		tempMap.putAll( WMSDatas.getInTables()); 
		
		if("审核不通过".equals(tempMap.get(WMSfields.checkStatus))) {
			Successfully = false;
			return Successfully;
		}else if("审核通过".equals(tempMap.get(WMSfields.checkStatus))) {
			try {
				conn.setAutoCommit(false);		
			
				count += insertInTableInfo(new TableOperate(conn));
				count += insertInMaterialsInfo(new TableOperate(conn));
				count += updateBstock(new TableOperate(conn));
				count += updateMainStock(new TableOperate(conn));
				count += insertLedger(new TableOperate(conn));
				
				if(count == 5 || count == 6) {
					conn.commit();
					Successfully = true;
				}else {
					conn.rollback();
				}
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("入库失败");
				Successfully = false;
				if(conn != null)
				try {
					conn.rollback();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				e.printStackTrace();
			}
		}	
		
		tempMap.clear();
		
		return Successfully;
	}
	
	
	
	//入库材料信息表  in_tables
	private int insertInTableInfo(TableOperate oper) {
		
		this.operate = oper;
		
		int count = 0;
		
		operate.setTableName(WMStables.InTable);
		
		data.putAll(WMSDatas.getInTables());
		
		count += operate.insertData(data);
	
		return count;		
	}
	
	
	//入库材料详细表 in_materials
	private int insertInMaterialsInfo(TableOperate oper) {
		
		this.operate = oper;
		
		int count = 0;
		
		operate.setTableName(WMStables.InMaterials);
		
		data.putAll(WMSDatas.getInMaterials());
		
		tempMap.putAll(data);
		
		count += operate.insertData(data);	//内部会自动清空data
		return count;
	}
	
	
	//将数据插入到本仓数据中
	private int updateBstock(TableOperate oper) {
		
		this.operate = oper;
		
		int count  = 0;
		WMSDatas.setBstock(tempMap.get(WMSfields.ItemNo), tempMap.get(WMSfields.wareHouseNo),tempMap.get(WMSfields.InNumber), tempMap.get(WMSfields.unitPrice),tempMap.get(WMSfields.total),tempMap.get(WMSfields.remarks));
		
		operate.setTableName(WMStables.bstock);
		
		//判断是否在仓库中，如果不在则插入，否则更新数据。
		ResultSet rs = null;		
		// 设置仓库选择的条件
		toolMap.put(WMSfields.ItemNo, tempMap.get(WMSfields.ItemNo));
		toolMap.put(WMSfields.wareHouseNo, tempMap.get(WMSfields.wareHouseNo));
		
		rs = operate.selectData(toolMap);
			
		try {
			if(rs.next()) {
				Integer number = null;
				Double avgPrice = null;
				Double sum = null;
				
				number = rs.getInt(WMSfields.nowAmout);
				
				sum = rs.getDouble(WMSfields.amountSum);
				
				number += Integer.valueOf(tempMap.get(WMSfields.InNumber)+"");
				sum += Double.valueOf(""+tempMap.get(WMSfields.total));
				
				if(number == 0) {
					sum = 0.0;
				}else {
					avgPrice = sum/number;
				}				
				
				//设置等值选择条件选择				
				toolMap.put(WMSfields.wareHouseNo,tempMap.get(WMSfields.wareHouseNo));
				toolMap.put(WMSfields.ItemNo, tempMap.get(WMSfields.ItemNo));
				
				// 更新数量，单价，总计
				data.put(WMSfields.amountSum, sum);
				data.put(WMSfields.nowAmout,number);
				data.put(WMSfields.avgPrice, avgPrice);
				
				//当有两个条件不同时
				count = operate.updateData(data,toolMap);
				
			}else {
				//将数据插入本仓
				data.putAll(WMSDatas.getBstock());
				count = operate.insertData(data);
				
				
			}	
		} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("入库处理-------本仓数据更新");
				e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}
		
		return count;
	}
	
	//将数据更新或插入到总仓
	private int updateMainStock(TableOperate oper) {
		
		this.operate = oper;
		
		int count = 0;
		
		WMSDatas.setMainStock(tempMap.get(WMSfields.ItemNo), tempMap.get(WMSfields.ItemName), tempMap.get(WMSfields.norms), tempMap.get(WMSfields.company), tempMap.get(WMSfields.unitPrice), tempMap.get(WMSfields.InNumber),tempMap.get(WMSfields.total),tempMap.get(WMSfields.remarks));
		
		operate.setTableName(WMStables.mainStock);
		
		ResultSet rs = null;
		
		rs = operate.selectData(tempMap.get(WMSfields.ItemNo));
		try {
			if(rs.next()) {
				
				Double avgPrice = null;
				
				Integer number = rs.getInt(WMSfields.nowAmout);
				
				Double sum = rs.getDouble(WMSfields.amountSum);
				
				number += Integer.valueOf(tempMap.get(WMSfields.InNumber)+"");
				sum += Double.valueOf(tempMap.get(WMSfields.total)+"");
				
				if(number == 0 || number == null) {
					number = 0;
					sum = 0.0;
				}else {
					avgPrice = sum/number;
				}				
				
				// * 更新数量，单价，总计
				toolMap.put(WMSfields.amountSum, sum);
				toolMap.put(WMSfields.nowAmout,number);
				toolMap.put(WMSfields.avgPrice, avgPrice);
				
				count = operate.updateData(toolMap,tempMap.get(WMSfields.ItemNo));
				
			}else {
				//插入数据到总仓
				data.putAll(WMSDatas.getMainStock());
				count = operate.insertData(data);
				
				operate.setTableName(WMStables.Materials);
				data.putAll(WMSDatas.getMainStock());
				count = operate.insertData(data);
				
			}			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("入库处理-------总仓数据更新");
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}
		
		return count;
	}
	
	//将数据更新到台账中
	private int insertLedger(TableOperate oper) {
		this.operate = oper;
		
		int count = 0;
		ResultSet rs = null;
		
		operate.setTableName(WMStables.bstock);
		
		//查询盘点本仓有无此数据
		toolMap.put(WMSfields.ItemNo,tempMap.get(WMSfields.ItemNo));
		toolMap.put(WMSfields.wareHouseNo,tempMap.get(WMSfields.wareHouseNo));
		
		rs = operate.selectData(toolMap);
		
		try {
			if(rs.next()) {
				Integer balanceNumber = rs.getInt(WMSfields.nowAmout);
				Double balancePrice = rs.getDouble(WMSfields.avgPrice);
				Double balanceSum = rs.getDouble(WMSfields.amountSum);
				
				WMSDatas.setLedger(tempMap.get(WMSfields.InOdd), tempMap.get(WMSfields.ItemNo), tempMap.get(WMSfields.wareHouseNo), tempMap.get(WMSfields.InNumber),tempMap.get(WMSfields.unitPrice), tempMap.get(WMSfields.total), null, null, null, balanceNumber, balancePrice, balanceSum, new Date(), "入库台账");
				
				operate.setTableName(WMStables.ledger);	//设置操作的表名
				
				data.putAll(WMSDatas.getLedger());
				
				count = operate.insertData(data);
			}
			
		}catch(SQLException e){
			System.out.println("入库处理-------台账数据更新");
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}
		
		return count;
	}
	
}
