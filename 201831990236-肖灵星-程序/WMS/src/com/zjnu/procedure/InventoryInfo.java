package com.zjnu.procedure;

import java.sql.*;
import java.util.*;
import java.util.Date;

import com.zjnu.Datas.*;
import com.zjnu.Interface.DataOperation;
import com.zjnu.Util.*;

public class InventoryInfo {
	private Connection conn = null;
	
	private static Map<String,Object> data = null;
	private static Map<String,Object> tempMap = null;	
	private static Map<String,Object> toolMap = null;
	
	private DataOperation operate = null;
	
	static {
		data = new HashMap<String,Object>();
		tempMap = new HashMap<String,Object>(); 
		toolMap = new HashMap<String,Object>();
	}
	
	public InventoryInfo() {
		this(null);
	}
	
	public InventoryInfo(Connection conn) {
		this.conn = conn;
	}
	
	public void setConnection(Connection conn) {
		this.conn = conn;
	}
	
	public boolean checkCopeWith() {
		
		boolean Successfully = false;
		int count = 0;
		
		tempMap.putAll(WMSDatas.getInventory());
		
		try {
			conn.setAutoCommit(false);	
			
			count += insertInventoryInfo(new TableOperate(conn));
			count += updateBstock(new TableOperate(conn));				
			count += updateMainStock(new TableOperate(conn));
			count += insertLedger(new TableOperate(conn));
			
			if(count == 4) {
				conn.commit();
				Successfully = true;
			}else {
				conn.rollback();
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("出库失败");
			Successfully = false;
			if(conn != null)
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			e.printStackTrace();
		}
			
	
		tempMap.clear();
		
		return Successfully;
	}
	
	
	
	//盘点材料数据表Inventory
	private int insertInventoryInfo(TableOperate oper) {
		
		this.operate = oper;
		
		int count = 0;
		
		operate.setTableName(WMStables.Inventory);
		
		data.putAll(tempMap);
		
		count += operate.insertData(data);	//内部会自动清空data
		
		return count;
	}
	
	
	//将数据更新到本仓数据中
	private int updateBstock(TableOperate oper) {
		
		this.operate = oper;
		
		int count  = 0;
		
		operate.setTableName(WMStables.bstock);
		
		ResultSet rs = null;
		
		//查询本仓有无此货物
		toolMap.put(WMSfields.ItemNo, tempMap.get(WMSfields.ItemNo));
		toolMap.put(WMSfields.wareHouseNo, tempMap.get(WMSfields.wareHouseNo));
		
		rs = operate.selectData(toolMap);
			
		try {
			if(rs.next()) {
				Integer number = rs.getInt(WMSfields.nowAmout);	//本仓中原始数量				
				Integer profitLoss = Integer.valueOf(""+tempMap.get(WMSfields.profitLoss));//(Integer)tempMap.get(WMSfields.InventoryNum) - number;	//需要盘点的数量
				//将此数据插入到盘点数据中

				//WMSDatas.setInventory(null, null, null, null, null, number, profitLoss, null, null);
				
				//tempMap.putAll(WMSDatas.getInventory());
				
				//设置选择条件选择		
				toolMap.put(WMSfields.wareHouseNo,tempMap.get(WMSfields.wareHouseNo));
				toolMap.put(WMSfields.ItemNo, tempMap.get(WMSfields.ItemNo));
				
				Integer updateNum = number + profitLoss;	//盘点后的仓库理论值				
				Double sum = updateNum * rs.getDouble(WMSfields.avgPrice);
				
				data.put(WMSfields.amountSum, sum);
				data.put(WMSfields.nowAmout,updateNum);
				
				//当有两个条件不同时,更新本仓数据
				count = operate.updateData(data,toolMap);
				
			}else {
				System.out.println("此仓库无此货物-----------------本仓");
			}	
		} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("出库处理-------本仓数据更新");
				e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}
				
		return count;
	}
	
	
	//将数据更新到总仓
	private int updateMainStock(TableOperate oper) {
		
		this.operate = oper;
		
		int count = 0;
		
		operate.setTableName(WMStables.mainStock);
		
		ResultSet rs = null;
		
		rs = operate.selectData(tempMap.get(WMSfields.ItemNo));
		try {
			if(rs.next()) {
				
				Integer number = rs.getInt(WMSfields.nowAmout);
				Double avgPrice = rs.getDouble(WMSfields.avgPrice);
				
				number += Integer.valueOf(""+tempMap.get(WMSfields.profitLoss));
				Double sum = number * avgPrice;
				
				// 更新数量，单价，总计
				toolMap.put(WMSfields.amountSum, sum);
				toolMap.put(WMSfields.nowAmout,number);
				
				count = operate.updateData(toolMap,tempMap.get(WMSfields.ItemNo));
				
			}else {
				System.out.println("仓库中不存在此货物----------总仓");
			}			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("出库处理-------总仓数据更新");
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}
		
		return count;
	}
	
	//将数据更新到台账中
	private int insertLedger(TableOperate oper) {
		
		this.operate = oper;
		
		int count = 0;
		ResultSet rs = null;
		
		operate.setTableName(WMStables.bstock);
		
		toolMap.put(WMSfields.ItemNo,tempMap.get(WMSfields.ItemNo));
		toolMap.put(WMSfields.wareHouseNo,tempMap.get(WMSfields.wareHouseNo));
		//查询判断本仓有无此数据
		rs = operate.selectData(toolMap);		
		
		try {
			if(rs.next()) {
				Integer balanceNumber = rs.getInt(WMSfields.nowAmout);
				Double balancePrice = rs.getDouble(WMSfields.avgPrice);
				Double balanceSum = rs.getDouble(WMSfields.amountSum);
				
				//盘点数量和金额放到出库栏
				Integer outNum = Integer.valueOf(""+tempMap.get(WMSfields.profitLoss));
				Double sum = outNum * balancePrice;
				
				//设置台账中的数据
				WMSDatas.setLedger(tempMap.get(WMSfields.InventoryOdd), tempMap.get(WMSfields.ItemNo), tempMap.get(WMSfields.wareHouseNo),outNum, balancePrice, sum,null, null, null, balanceNumber, balancePrice, balanceSum, new Date(), "盘点数据");
				
				//对台账表进行操作
				operate.setTableName(WMStables.ledger);
				
				data.putAll(WMSDatas.getLedger());
				
				count = operate.insertData(data);
			}
			
		}catch(SQLException e){
			System.out.println("出库处理-------台账数据更新");
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}
		
		return count;
	}
}
