package com.zjnu.UI;

import java.awt.*;
import java.awt.event.*;
import java.sql.Connection;

import javax.swing.*;
import javax.swing.border.Border;

import com.zjnu.Panels.AllotStockPanel;
import com.zjnu.Panels.IN_OUT_Info;
import com.zjnu.Panels.InStockPanel;
import com.zjnu.Panels.InventoryPanel;
import com.zjnu.Panels.LedgerPanel;
import com.zjnu.Panels.MainStockPanel;
import com.zjnu.Panels.OutStockPanel;
import com.zjnu.Panels.PanelFlagNames;
import com.zjnu.Panels.ProductInfoPanel;
import com.zjnu.Panels.SearchIVTPanel;
import com.zjnu.Util.WMStables;
import com.zjnu.empPanel.FieldsArray;
import com.zjnu.empPanel.InfoCopePanel;
import com.zjnu.empPanel.updatePassWord;

/*
 * 包含好几个部分
 */
public class CenterFrame {
	
	private JPanel InStocks = null;
	private JPanel OutStocks = null;
	private JPanel Stocks = null;
	private JPanel Manages = null;
	private JPanel Setting = null;
	
	private JPanel MeanPanel = null;
	
	private JButton[] btnTemp = null;	
	
	private Connection conn = null;
	
	public CenterFrame() {
		this(null);
	}
	
	public CenterFrame(Connection conn) {
		this.conn = conn;
	}
	
	{
		MeanPanel = new JPanel(null);
		MeanPanel.setOpaque(false);
		
		Border border = BorderFactory.createLineBorder(Color.decode("#ccffff"), 2);
		MeanPanel.setBorder(border);	
		
		JLabel title = new JLabel("系统设置");
		title.setFont(new Font(null,Font.BOLD,25));
		title.setBounds(250, 10, 200,30);
		MeanPanel.add(title);
		
		InStocks = createPanels();
		OutStocks = createPanels();
		Stocks = createPanels();
		Manages = createPanels();
		Setting = createPanels();
		
		MeanPanel.add(InStocks);
		MeanPanel.add(OutStocks);
		MeanPanel.add(Stocks);
		MeanPanel.add(Manages);
		MeanPanel.add(Setting);
	
		initPanel();
	}
	
	/*
	 * 以下8个为属性的获取器。
	 */

	//获取中间菜单页面布局的数组
	
	private void initPanel() {
		createInStocks(InStocks);
		createOutStocks(OutStocks);	
		createStock(Stocks);
		createManage(Manages);
		createSetting(Setting);
	}

	private JPanel createPanels(){
		JPanel panel = new JPanel(null);
		panel.setOpaque(false);
		panel.setVisible(false);
		panel.setBounds(50, 50, 500, 500);
		return panel;
	}
	
	//显示面板
	public void showPanel(JPanel panel) {
		panel.setVisible(true);
	}
	
	//隐藏面板
	public void hidePanel(JPanel panel) {
		panel.setVisible(false);
	}
	
	//gets和sets方法

	public JPanel getInStocks() {
		return InStocks;
	}

	public JPanel getOutStocks() {
		return OutStocks;
	}

	public JPanel getStocks() {
		return Stocks;
	}

	public JPanel getManages() {
		return Manages;
	}

	public JPanel getSetting() {
		return Setting;
	}

	public JPanel getMeanPanel() {
		return MeanPanel;
	}

	/*
	 * 设置出、入库面板信息
	 */
	
	private void createInStocks(JPanel panel) {
		btnTemp = createButton(5);		
	
		btnTemp[0].setText("材料进货");
		btnTemp[0].setBounds(50, 50, 150, 30);
		btnTemp[0].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("材料进货");
				
				//添加进货面板
				InStockPanel inStockPanel = new InStockPanel(PanelFlagNames.INStock_ICJ,conn);
				inStockPanel.initPanel();
				
			}
		});
		
		
		btnTemp[1].setText("材料退货");
		btnTemp[1].setBounds(50, 250, 150, 30);
		btnTemp[1].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("材料退货");
				InStockPanel inStockPanel = new InStockPanel(PanelFlagNames.INStock_ICT,conn);
				inStockPanel.initPanel();
			}
		});
		

		btnTemp[2].setText("台账记录");
		btnTemp[2].setBounds(300, 150, 150, 30);
		btnTemp[2].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				System.out.println("台账记录");
				
				LedgerPanel ledgerPanel = new LedgerPanel(conn);
				ledgerPanel.initPanel();
			}
		});
		
		
		btnTemp[3].setText("入库单据");
		btnTemp[3].setBounds(300, 50, 150, 30);
		btnTemp[3].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("入库单据");
				IN_OUT_Info inOutInfo = new IN_OUT_Info(PanelFlagNames.OutStock_IDJ,conn);
				inOutInfo.initPanel();
			}
		});
		
		
		btnTemp[4].setText("当前库存查询");
		btnTemp[4].setBounds(300, 250, 150, 30);
		btnTemp[4].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("库存查询");
				
				MainStockPanel mainstockPanel = new MainStockPanel(conn);
				mainstockPanel.initPanel();
			}
		});
			
		//添加组件
		
		for(int i = 0;i<btnTemp.length;i++) {
			panel.add(btnTemp[i]);
		}
		
	}
	
	
	//设定关于出/入库的按钮的文字和样式以及触发事件
	private void createOutStocks(JPanel panel) {
		
		btnTemp = createButton(5);
			
		btnTemp[0].setText("仓库领料");
		btnTemp[0].setBounds(50, 50, 150, 30);
		btnTemp[0].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("仓库领料");
				OutStockPanel outStockPanel = new OutStockPanel(PanelFlagNames.OutStock_OCL,conn);
				outStockPanel.initPanel();
			}
		});
		
		btnTemp[1].setText("仓库退料");
		btnTemp[1].setBounds(50, 250, 150, 30);
		btnTemp[1].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("仓库退料");
				OutStockPanel outStockPanel = new OutStockPanel(PanelFlagNames.OutStock_OCT,conn);
				outStockPanel.initPanel();
			}
		});	
		

		btnTemp[2].setText("台账记录");
		btnTemp[2].setBounds(300, 150, 150, 30);
		btnTemp[2].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
			
				System.out.println("台账记录");
				
				LedgerPanel ledgerPanel = new LedgerPanel(conn);
				ledgerPanel.initPanel();
			}
		});
	
		
		btnTemp[3].setText("出库单据");
		btnTemp[3].setBounds(300, 50, 150, 30);
		btnTemp[3].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println("出库单据");
				IN_OUT_Info inOutInfo = new IN_OUT_Info(PanelFlagNames.OutStock_ODJ,conn);
				inOutInfo.initPanel();					
			}
			
		});
		
		
		btnTemp[4].setText("当前库存查询");
		btnTemp[4].setBounds(300, 250, 150, 30);
		btnTemp[4].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				System.out.println("库存查询");
				
				MainStockPanel mainstockPanel = new MainStockPanel(conn);
				mainstockPanel.initPanel();
			}
		});
		
		
		for(int i = 0;i<btnTemp.length;i++) {
			panel.add(btnTemp[i]);
		}
	
	}
	

	/*
	 * 仓库管理信息数据的处理 
	 */
	
	private void createStock(JPanel panel) {
		btnTemp = createButton(4);		 
		
			
		btnTemp[0].setText("库存调拨");
		btnTemp[0].setBounds(50, 50, 150, 30);
		btnTemp[0].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("库存调拨");
				
				AllotStockPanel allotStock = new AllotStockPanel(conn);
				allotStock.initPanel();
				
			}
		});
		
		btnTemp[1].setText("库存盘点");
		btnTemp[1].setBounds(50, 250, 150, 30);
		btnTemp[1].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("库存盘点");
				
				InventoryPanel inventoryPanel = new InventoryPanel(conn);
				inventoryPanel.initPanel();
				
			}
		});
		   
	
		btnTemp[2].setText("库存查询");
		btnTemp[2].setBounds(300, 50, 150, 30);
		btnTemp[2].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("库存查询");
				MainStockPanel mainStockPanel = new MainStockPanel(conn);
				mainStockPanel.initPanel();
			}
		});
		
		
		btnTemp[3].setText("盘点清单");
		btnTemp[3].setBounds(300, 250, 150, 30);
		btnTemp[3].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前盘点清单的面板
				 */
				System.out.println("盘点清单");
				SearchIVTPanel searchIvt = new SearchIVTPanel(conn);
				searchIvt.initPanel();
			}
		});		
		
		for(int i = 0;i<btnTemp.length;i++) {
			panel.add(btnTemp[i]);
		}	
			
	}
	
	//设定关于库存/日常管理的按钮的文字和样式以及触发事件
	private void createManage(JPanel panel) {
		
		btnTemp = createButton(4);
	
		btnTemp[0].setText("供应商管理");
		btnTemp[0].setBounds(50, 50, 150, 30);
		btnTemp[0].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("供应商管理");
				InfoCopePanel infoCope = new InfoCopePanel(conn,WMStables.supply);
				
				infoCope.setFields(FieldsArray.getFieldSupply());
				infoCope.setFields_EN(FieldsArray.getFieldSupply_EN());
				infoCope.setPanelMessage("供应商信息");
				
				infoCope.initPanel();
				
			}
		});
		
		btnTemp[1].setText("员工管理");
		btnTemp[1].setBounds(50, 250, 150, 30);
		btnTemp[1].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("员工管理");
				
				InfoCopePanel infoCope = new InfoCopePanel(conn,WMStables.employee);
				
				infoCope.setFields(FieldsArray.getFieldEmp());
				infoCope.setFields_EN(FieldsArray.getFieldEmp_EN());
				infoCope.setPanelMessage("员工信息");
				
				infoCope.initPanel();
			}
		});
			
		btnTemp[2].setText("部门管理");
		btnTemp[2].setBounds(300, 50, 150, 30);
		btnTemp[2].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("部门管理");
				
				InfoCopePanel infoCope = new InfoCopePanel(conn,WMStables.dept);
				
				infoCope.setFields(FieldsArray.getFieldDept());
				infoCope.setFields_EN(FieldsArray.getFieldDept_EN());
				infoCope.setPanelMessage("部门信息");
				
				infoCope.initPanel();
			}
		});
		
		
		btnTemp[3].setText("管理员管理");
		btnTemp[3].setBounds(300, 250, 150, 30);
		btnTemp[3].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("管理员管理");
				InfoCopePanel infoCope = new InfoCopePanel(conn,WMStables.administrator);
				
				infoCope.setFields(FieldsArray.getFieldAdmin());
				infoCope.setFields_EN(FieldsArray.getFieldAdmin_EN());
				infoCope.setPanelMessage("管理员信息");
				
				infoCope.initPanel();
			}
		});			
		
		
		for(int i = 0;i<btnTemp.length;i++) {
			panel.add(btnTemp[i]);
		}	
		
	}
	
	/*
	 * 系统设置管理
	 */
	

	//设定关于系统设置的按钮的文字和样式以及触发事件
	private void createSetting(JPanel panel) {
	
		btnTemp = createButton(5);		
			
		btnTemp[0].setText("货品信息");
		btnTemp[0].setBounds(50, 50, 150, 30);
		btnTemp[0].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("货品信息");
				
				ProductInfoPanel productInfoPanel = new ProductInfoPanel(conn);
				productInfoPanel.initPanel();
				
			}
		});
		
		btnTemp[1].setText("仓库设置");
		btnTemp[1].setBounds(300, 50, 150, 30);
		btnTemp[1].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("仓库设置");
				
				InfoCopePanel infoCope = new InfoCopePanel(conn,WMStables.wareHouse);
				
				infoCope.setFields(FieldsArray.getFieldWarehouse());
				infoCope.setFields_EN(FieldsArray.getFieldWarehouse_EN());
				infoCope.setPanelMessage("仓库信息");
				
				infoCope.initPanel();
			}
		});
		   
	
		btnTemp[2].setText("员工设置");
		btnTemp[2].setBounds(300, 150, 150, 30);
		btnTemp[2].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前库存的面板
				 */
				System.out.println("员工设置");
				InfoCopePanel infoCope = new InfoCopePanel(conn,WMStables.employee);
				
				infoCope.setFields(FieldsArray.getFieldEmp());
				infoCope.setFields_EN(FieldsArray.getFieldEmp_EN());
				infoCope.setPanelMessage("员工信息");
				
				infoCope.initPanel();
			}
		});
		
		
		btnTemp[3].setText("系统维护");
		btnTemp[3].setBounds(50, 250, 150, 30);
		btnTemp[3].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前盘点清单的面板
				 */
				System.out.println("系统维护");
				JOptionPane.showMessageDialog(MeanPanel,"此服务正在维护中","提示信息",JOptionPane.INFORMATION_MESSAGE);
				
			}
		});			
			
		btnTemp[4].setText("系统设置");
		btnTemp[4].setBounds(300, 250, 150, 30);
		btnTemp[4].addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				/*
				 * 弹出当前盘点清单的面板
				 */
				System.out.println("系统设置");
				JOptionPane.showMessageDialog(MeanPanel,"仅限修改用户密码","提示信息",JOptionPane.INFORMATION_MESSAGE);
				
				updatePassWord upPwd = new updatePassWord(conn);
				upPwd.initPanel();
			}
		});	
	
		for(int i = 0;i<btnTemp.length;i++) {
			panel.add(btnTemp[i]);
		}		
		
	}
	
	
	//创建按钮方法
	private JButton[] createButton(int num) {
		
		JButton[] btn = new JButton[num];
		
		for(int i = 0;i<num;i++) {
			btn[i] = new JButton();
			btn[i].setBackground(Color.decode("#FAF0E6"));
			btn[i].setFont(new Font("楷体",Font.PLAIN,18));
		}
		
		return btn;
	}
	

}
