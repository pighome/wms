package com.zjnu.UI;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;

import javax.swing.*;
import javax.swing.border.Border;

import com.zjnu.Panels.ProductInfoPanel;
import com.zjnu.Util.WMStables;
import com.zjnu.empPanel.FieldsArray;
import com.zjnu.empPanel.InfoCopePanel;

public class BottomMean {
	
	private JLabel goods = null;
	private JLabel supplier = null;
	private JLabel stock = null;
	private JLabel employee = null;
	private Connection conn = null;
	
	private JPanel bottomMean = null;
	
	{
		bottomMean = new JPanel(null);
		Border border = BorderFactory.createLineBorder(Color.decode("#666633"), 5);
		bottomMean.setBorder(border);
		bottomMean.setOpaque(false);
		bottomMean.setBounds(10,10, 800, 100);
		
		bottomMean.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				
				//System.out.println("bottomMean"+bottomMean.getWidth());
				
				int width = bottomMean.getWidth();
				
				if(width>500) {
					goods.setBounds(width/16,10, 120, 30);
					supplier.setBounds(width*5/16,10, 120, 30);
					stock.setBounds(width*9/16,10, 120, 30);
					employee.setBounds(width*13/16,10, 120, 30);
				}else {
					int distance = width/16;
					goods.setBounds(distance,10, 120, 30);
					supplier.setBounds(distance+125,10, 120, 30);
					stock.setBounds(distance+250,10, 120, 30);
					employee.setBounds(distance+375,10, 120, 30);
				}
				
			}
		});
	}
	
	public BottomMean() {
		this(null);
	}
	
	public BottomMean(Connection conn) {
		this.conn = conn;
		createLabelMean();
	}
	
	//取得父组件的对象
	
	public void createLabelMean() {
		//货品信息的设置
		goods = new JLabel("货品信息");
		setStyle(goods);
		
		goods.addMouseListener(new MouseAdapter() {			
			public void mouseClicked(MouseEvent e){
				System.out.println("货品信息");
				ProductInfoPanel product = new ProductInfoPanel(conn);
				product.initPanel();
			}
			
		});
		
		//客户信息的设置
		supplier = new JLabel("客户设置");
		setStyle(supplier);
		
		supplier.addMouseListener(new MouseAdapter() {			
			public void mouseClicked(MouseEvent e){
				System.out.println("客户设置");
				
				System.out.println("系统维护");
				JOptionPane option = new JOptionPane();
				option.setBounds(0, 0, 200, 100);
				
				JOptionPane.showMessageDialog(bottomMean,"此服务正在维护中","提示信息",JOptionPane.INFORMATION_MESSAGE);
			
			}
			
		});
		
		//仓库设置
		stock = new JLabel("仓库设置");
		setStyle(stock);
		
		stock.addMouseListener(new MouseAdapter() {			
			public void mouseClicked(MouseEvent e){
				System.out.println("仓库设置");
				InfoCopePanel infoCope = new InfoCopePanel(conn,WMStables.wareHouse);
				
				infoCope.setFields(FieldsArray.getFieldWarehouse());
				infoCope.setFields_EN(FieldsArray.getFieldWarehouse_EN());
				infoCope.setPanelMessage("仓库信息");
				
				infoCope.initPanel();
			}
			
		});
		
		//员工设置
		employee = new JLabel("员工信息");
		setStyle(employee);
		
		employee.addMouseListener(new MouseAdapter() {			
			public void mouseClicked(MouseEvent e){
				System.out.println("员工信息");
				
				InfoCopePanel infoCope = new InfoCopePanel(conn,WMStables.employee);
				
				infoCope.setFields(FieldsArray.getFieldEmp());
				infoCope.setFields_EN(FieldsArray.getFieldEmp_EN());
				infoCope.setPanelMessage("员工信息");
				
				infoCope.initPanel();
			}
			
		});
		
		bottomMean.add(goods);
		bottomMean.add(supplier);
		bottomMean.add(stock);
		bottomMean.add(employee);
	}
	

	private void setStyle(JLabel label) {
		label.setFont(new Font(null,Font.BOLD,20));
		label.setIcon(new ImageIcon(LeftMean.class.getResource("/com/zjnu/sources/point.png")));
		label.setHorizontalTextPosition(SwingConstants.RIGHT);
		label.setForeground(Color.CYAN);
	}



	public JPanel getBottomMean() {
		return bottomMean;
	}


	public void setBottomMean(JPanel bottomMean) {
		this.bottomMean = bottomMean;
	}

}
