package com.zjnu.UI;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;

import javax.swing.*;

public class LeftMean {

	private JPanel leftMean = null;

	private JLabel lab[] = {
			new JLabel("进货管理"),new JLabel("出货管理"),new JLabel("库存管理"),new JLabel("日常管理"),new JLabel("系统设置")
	};
	
	private static CenterFrame centerMean = null;
	private static JPanel[] mean = new JPanel[5];
	
	//菜单栏系统运行只需执行一次
	{
		leftMean = new JPanel(null);
		leftMean.setBounds(30, 40, 200, 800);
		leftMean.setOpaque(false);	
		
		createLabel();
		
		
	}
	
	
	
	public LeftMean(){
		
	}
	
	public LeftMean(CenterFrame centerMean) {
		LeftMean.centerMean = centerMean;
		mean[0] = centerMean.getInStocks();
		mean[1] = centerMean.getOutStocks();
		mean[2] = centerMean.getStocks();
		mean[3] = centerMean.getManages();
		mean[4] = centerMean.getSetting();
			
	}
	
	
	public CenterFrame getCenterMean() {
		return centerMean;
	}
	
	

	public void setCenterMean(CenterFrame center) {
		LeftMean.centerMean = center;
	}


	public JPanel getLeftMean() {
		return leftMean;
	}

	public void setLeftMean(JPanel leftMean) {
		this.leftMean = leftMean;
	}
	
	//设置右侧主菜单面板的可见性
	
	private static void setCenterMean(int index) {
		
		for(int i=0;i<mean.length;i++) {
			if(i == index) {
				centerMean.showPanel(mean[i]);
			}else {
				centerMean.hidePanel(mean[i]);
			}
		}
	}

	//创建侧边栏的标签属性及触发事件
	private void createLabel() {				
		
		
		lab[0].setBounds(20, 100, 150, 50);
		lab[0].setIcon(new ImageIcon(LeftMean.class.getResource("/com/zjnu/sources/add.png")));
		lab[0].setHorizontalTextPosition(SwingConstants.RIGHT);
		setBeforStyle(lab[0]);

		
		lab[0].addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("labelIn");
				setAfterStyle(lab[0]);
				recoverOther(lab[0]);
				
				LeftMean.setCenterMean(0);//要显示的操作面板
				
			}

		});

		
		lab[1].setBounds(20, 170, 150, 50);
		lab[1].setIcon(new ImageIcon(LeftMean.class.getResource("/com/zjnu/sources/sub.png")));
		lab[1].setHorizontalTextPosition(SwingConstants.RIGHT);
		setBeforStyle(lab[1]);
		lab[1].addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("laelOut");
				setAfterStyle(lab[1]);
				recoverOther(lab[1]);
				
				LeftMean.setCenterMean(1);//要显示的操作面板
				
			}
		});

		
 
		lab[2].setBounds(20, 240, 150, 50);
		lab[2].setIcon(new ImageIcon(LeftMean.class.getResource("/com/zjnu/sources/stock.png")));
		lab[2].setHorizontalTextPosition(SwingConstants.RIGHT);
		setBeforStyle(lab[2]);
		lab[2].addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.out.println("labelStock");
				setAfterStyle(lab[2]);
				recoverOther(lab[2]);
				

				LeftMean.setCenterMean(2);//要显示的操作面板
				
			}
		});

		

		lab[3].setBounds(20, 310, 150, 50);
		lab[3].setIcon(new ImageIcon(LeftMean.class.getResource("/com/zjnu/sources/manageTool.png")));
		lab[3].setHorizontalTextPosition(SwingConstants.RIGHT);
		setBeforStyle(lab[3]);
		lab[3].addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.out.println("labelManage");
				setAfterStyle(lab[3]);
				recoverOther(lab[3]);
				

				LeftMean.setCenterMean(3);//要显示的操作面板
			
			}
		});
		

		lab[4].setBounds(20, 380, 150, 50);
		lab[4].setIcon(new ImageIcon(LeftMean.class.getResource("/com/zjnu/sources/setting.png")));
		lab[4].setHorizontalTextPosition(SwingConstants.RIGHT);
		setBeforStyle(lab[4]);
		lab[4].addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				System.out.println("labelSetting");
				setAfterStyle(lab[4]);
				recoverOther(lab[4]);
				

				LeftMean.setCenterMean(4);//要显示的操作面板
			
			}
		});
		
	}

	
	//除当前点击按钮外恢复其他的按钮
	private void recoverOther(JLabel label) {
		for(int i=0;i<lab.length;i++) {
			if(lab[i]!=label) {
				setBeforStyle(lab[i]);
			}
		}
	}
	
	//设置按钮点击之前的样式
	private void setBeforStyle(JLabel label) {
		label.setBorder(BorderFactory.createRaisedBevelBorder());
		label.setFont(new Font("楷体", Font.BOLD, 20));
		label.setForeground(Color.black);
		leftMean.add(label);
	}

	//设置按钮点击之后的样式
	private void setAfterStyle(JLabel label) {
		label.setBorder(BorderFactory.createLoweredBevelBorder());
		label.setForeground(Color.decode("#FF4500"));
	}


}
