package com.zjnu.UI;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.sql.Connection;

import javax.swing.*;
import javax.swing.border.Border;

import com.zjnu.ToolBar.MeanBar;
import com.zjnu.Util.ImageUtils;

public class MainUI extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JFrame jf = null;
	private Dimension dem =null;
	private Connection conn = null;
	
	{
		jf = new JFrame("主界面");
		dem = Toolkit.getDefaultToolkit().getScreenSize();
		
		jf.setSize(dem.width,dem.height-20);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	

	public MainUI() {
		this(null);		
	}
	
	public MainUI(Connection conn) {
		this.setLayout(null);
		this.conn = conn;
	}
	
	//设置背景图片
	public void paintComponent(Graphics g) {
		Image img = ImageUtils.getImage("/com/zjnu/sources/MainImage.jpg");
		Border border = BorderFactory.createLineBorder(Color.LIGHT_GRAY, 10);
		this.setBorder(border);	
		g.drawImage(img, 0, 0, dem.width, dem.height, null);	
	}
	
	
	public JFrame getJf() {
		return jf;
	}

	public void setJf(JFrame jf) {
		MainUI.jf = jf;
	}

	//添加其他的菜单面板进行组合
	public void openWindow() {
		jf.add(this);
		
		CenterFrame center = new CenterFrame(this.conn);		//中心菜单面板		
		
		LeftMean left = new LeftMean(center);	//侧边栏
		
		BottomMean bottom = new BottomMean(conn);	//底部栏
		
		MeanBar top = new MeanBar(jf,conn);
		
		this.add(left.getLeftMean());
		
		this.add(center.getMeanPanel());
		
		this.add(bottom.getBottomMean());
		this.add(top.getPanelBar());
		
		//自适应窗口组件
		this.addComponentListener(new ComponentAdapter() {	//拖到窗口监视器
			public void componentResized(ComponentEvent e) {
				MainUI mainUI = MainUI.this;
				
				int Width = mainUI.getWidth();
				int height = mainUI.getHeight();
				if(Width>630) {
					center.getMeanPanel().setBounds(Width/3, height/7, 600, 400);
				}else {
					int distance = 210;
					center.getMeanPanel().setBounds(distance, height/7, 600, 400);
				}
				
				bottom.getBottomMean().setBounds(Width/6, height*7/9, Width*3/4,50);
			}
		});
		
		
		jf.setVisible(true);
	}
	
	
	
}
