package com.zjnu.UI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import com.zjnu.Util.ImageUtils;
import com.zjnu.Util.JDBCUtil;
import com.zjnu.procedure.Login;

public class LoginUI extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField  userName = null;
	private JPasswordField passWord = null; 
	//private Login login = null;
	private JLabel alert = null;
	private JFrame JFLogin = null;
	private JFrame beforeJF = null;
	private boolean changeJob = false;
	
	private Login login = null;
	
	//此方法用于绘画各种图像，且与组件不关联------必须继承JPanel
	public void paintComponent(Graphics g) {
		Image img = ImageUtils.getImage("/com/zjnu/sources/login.png");
		g.drawImage(img, 0, 0, 400, 300, null);		
	}

	public LoginUI() {
		this.setLayout(null);
		
		createLogin();
	}

	
	public JFrame getBeforeJF() {
		return beforeJF;
	}

	public void setBeforeJF(JFrame beforeJF) {
		this.beforeJF = beforeJF;
	}

	public boolean isChangeJob() {
		return changeJob;
	}

	public void setChangeJob(boolean changeJob) {
		this.changeJob = changeJob;
	}

	private void createLogin() {	
		
		JLabel label = new JLabel("欢  迎  使  用");
		label.setFont(new Font("草书",Font.BOLD,40));
		label.setBounds(85, 50, 300, 50);
		this.add(label);
		
		JPanel user = new JPanel();
		
		user.setOpaque(false);
		user.setLayout(new FlowLayout());	
		user.setBounds(100, 120, 200,25);		
		user.add(new JLabel("用户名"));	
		
		userName = new JTextField(8);
		userName.setFont(new Font(null,Font.PLAIN,15));
		userName.setBackground(Color.decode("#FFFAF0"));
		user.add(userName);		
		
		JPanel pwd = new JPanel();
				
		pwd.setBounds(100, 150, 200, 25);
		pwd.setOpaque(false);
		pwd.add(new JLabel("密   码"));	
		
		passWord = new JPasswordField(8);
		passWord.setFont(new Font(null,Font.PLAIN,15));
		passWord.setBackground(Color.decode("#FFFAF0"));
		pwd.add(passWord);		
		
		
		//设置提示框
		alert = new JLabel("");
		alert.setSize(100, 25);
		alert.setFont(new Font(null,Font.BOLD,18));
		alert.setForeground(Color.decode("#EE0000"));
		alert.setBounds(125, 180, 200, 25);
		alert.setBackground(Color.black);
		
		JPanel btn = new JPanel(null);
		btn.setBounds(100, 210, 200, 30);
		btn.setOpaque(false);
		
		JButton btnLogin = new JButton("登陆");
		btnLogin.setBackground(Color.decode("#EEE8CD"));
		btnLogin.setBorderPainted(true);		
		btnLogin.setBounds(30, 0, 60, 25);
		btnLogin.addActionListener(new BtnLand());

		JButton btnExit = new JButton("退出");
		btnExit.setBackground(Color.decode("#EEE8CD"));
		btnExit.setBorderPainted(true);
		btnExit.setBounds(120, 0, 60, 25);
		btnExit.addActionListener(new BtnExit());
		
		btn.add(btnLogin);
		btn.add(btnExit);

		this.add(user);
		this.add(pwd);
		this.add(btn);
		this.add(alert);
	}
	
	private class BtnLand implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String username = userName.getText();
			String password = new String(passWord.getPassword());
			//String password = passWord.getText();
			System.out.println(e.getID()+"\n"+username+"\n"+password);
			
			//如果成功则new一个主界面，并关闭当前界面，否则弹出提示
			
			login.setUser(username);
			login.setPwd(password);
			
			boolean isSuccessfully = login.isLogin();
			
			if(isSuccessfully) {
				//新建主界面窗口，关闭当前界面
				
				MainUI main = new MainUI(login.getConn());
				
				main.openWindow();
				
				JFLogin.dispose();
				
				if(beforeJF != null) {
					beforeJF.dispose();				
				}
				
			}else {
				alert.setText("用户名或密码错误");
			}
		
		}
		
	}
	
	private class BtnExit implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int option = JOptionPane.NO_OPTION;
			if(!changeJob) {
				option = JOptionPane.showConfirmDialog(JFLogin, "确认退出系统","系统提示", JOptionPane.YES_NO_OPTION);		
				if(option == JOptionPane.YES_OPTION) {
					JFLogin.dispose();
					JDBCUtil.close(null, null, login.getConn());
					System.exit(0);	
				}
			}else {				
				JFLogin.dispose();
			}
		
		}
		
	}	
	
	//设置登陆窗口
	public void initLoginUI() {
		JFLogin = new JFrame("登陆窗口");
		JFLogin.setSize(400, 300);
		JFLogin.setResizable(false);
		JFLogin.setLocationRelativeTo(null);
		if(!changeJob) {
			login = new Login();
			JFLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			if(login.getMessage() != null && login.getMessage().length()>0) {
				alert.setText(login.getMessage());
			}
			
		}else {
			JFLogin.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		}
		
		JFLogin.add(this);	
		JFLogin.setVisible(true);
	}
	
	/*
	 * 测试类的数据模型
	 */
	
	public static void main(String[] args) {		
		
		LoginUI panel = new LoginUI();	
		
		panel.initLoginUI();
		
		
	}

}
