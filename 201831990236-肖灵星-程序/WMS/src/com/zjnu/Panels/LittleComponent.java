package com.zjnu.Panels;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableModel;

public class LittleComponent {
	private static Object selectedItem = null;
	private static JTable table = null;
	
	private LittleComponent() {
		
	}
	
	//创建下拉列表展示信息
	public static JComboBox<Object> selectMean(Object... items) {
		
		JComboBox<Object> combo = new JComboBox<Object>(items);
		combo.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				 if (e.getStateChange() == ItemEvent.SELECTED) {
					 selectedItem =  combo.getSelectedItem();
	                 System.out.println("选中: " + combo.getSelectedIndex() + " = " + combo.getSelectedItem());
	             }	
			}
			
		});
		
		//combo.setSelectedIndex(1);
		return combo;
	}

	public static Object getSelectedItem() {
		return selectedItem;
	}

	public static void setSelectedItem(Object selectedItem) {
		LittleComponent.selectedItem = selectedItem;
	}
	
	public static JTable getTable() {
		return table;
	}

	public static void setTable(JTable table) {
		LittleComponent.table = table;
	}

	//创建商品信息表格内容	
	/*
	 * 货品信息通过文本框手动填写
	 */
	public static JPanel GoodsInfo(String[] columnNames,Object[][] contents) {
		

		  JPanel panel = new JPanel(new BorderLayout());


	        // 自定义表格模型，创建一个表格
	        JTable table = new JTable(new AbstractTableModel() {
	            /**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
	            public int getRowCount() {
	                return contents.length;
	            }

	            @Override
	            public int getColumnCount() {
	                return contents[0].length;
	            }

	            @Override
	            public String getColumnName(int column) {
	                return columnNames[column].toString();
	            }

	            @Override
	            public boolean isCellEditable(int rowIndex, int columnIndex) {
	                // 总分列的索引为 4，总分列不允许编辑，其他列允许编辑，
	                // 总分列的数值由 语文、数学、英语 这三列的值相加得出，并同步更新
	                return columnIndex != (columnNames.length-1);
	            }

	            @Override
	            public Object getValueAt(int rowIndex, int columnIndex) {
	                return contents[rowIndex][columnIndex];
	            }

	            @Override
	            public void setValueAt(Object newValue, int rowIndex, int columnIndex) {
	                // 设置新的单元格数据时，必须把新值设置到原数据数值中，
	                // 待更新UI重新调用 getValueAt(...) 获取单元格值时才能获取到最新值
	            	contents[rowIndex][columnIndex] = newValue;
	                // 设置完数据后，必须通知表格去更新UI（重绘单元格），否则显示的数据不会改变
	                fireTableCellUpdated(rowIndex, columnIndex);
	            }
	        });

	        //设置单元个宽高
	        DefaultTableCellRenderer render = new DefaultTableCellRenderer();
		       render.setHorizontalAlignment(SwingConstants.CENTER);
		       
		       DefaultTableColumnModel dcm = (DefaultTableColumnModel) table.getColumnModel();
		       
		       table.setRowHeight(35);
		       
		       for(int i=0;i<columnNames.length;i++) {
		    	   table.getColumn(columnNames[i]).setCellRenderer(render);
		    	   dcm.getColumn(i).setPreferredWidth(110);
		       }
	       
	        // 获取 表格模型
	        TableModel tableModel = table.getModel();
	        // 在 表格模型上 添加 数据改变监听器
	        tableModel.addTableModelListener(new TableModelListener() {
	            @Override
	            public void tableChanged(TableModelEvent e) {
	               
	                // 获取被改变的列
	                int column = e.getColumn();

	                // 事件的类型，可能的值有:
	                //     TableModelEvent.INSERT   新行或新列的添加
	                //     TableModelEvent.UPDATE   现有数据的更改
	                //     TableModelEvent.DELETE   有行或列被移除
	                int type = e.getType();

	                // 针对 现有数据的更改 更新其他单元格数据
	                if (type == TableModelEvent.UPDATE) {
	                    // 只处理 语文、数学、英语 这三列（索引分别为1、2、3）的分数的更改
	                    if ("数量".equalsIgnoreCase(columnNames[column])|| "单价".equalsIgnoreCase(columnNames[column])) {
	                    	int index1 = 0,index2 = 0,index3 = 0;
	                    	//找到变化的数量列和单价列的下标
	                    	for(int index=0;index<columnNames.length;index++) {
	                    		if("数量".equalsIgnoreCase(columnNames[index])) {
	                    			index1 = index;
	                    		}else if("单价".equalsIgnoreCase(columnNames[index])) {
	                    			index2 = index;
	                    		}else if("总金额".equalsIgnoreCase(columnNames[index])){
	                    			index3 = index;
	                    		}
	                    
	                    	}
	                    	Object objNum = tableModel.getValueAt(0,index1);
	                    	Object objPrice = tableModel.getValueAt(0,index2);
	                    	Integer num = 0;
	                        Double price = 0.0;
	                    	if(objNum != null) {
	                    		num = Integer.valueOf(""+objNum);
	                    	}
	                    	
	                    	if(objPrice != null) {
	                    		price = Double.valueOf(""+objPrice);
	                    	}
	                        
	                        Double sum = 0.0;
	                        if(num != null && price != null) {
	                        	sum = price * num;
	                        }
	                        
	                        tableModel.setValueAt(sum, 0, index3);
	                        
	                    }else {
	                    	return ;
	                    }
	                }
	            }
	        });
	        
//	        RowSorter<TableModel> rowSorter = new TableRowSorter<TableModel>(tableModel);
//	        
//	        table.setRowSorter(rowSorter);
	        

	        // 把 表头 添加到容器顶部（使用普通的中间容器添加表格时，表头 和 内容 需要分开添加）
	        panel.add(table.getTableHeader(), BorderLayout.NORTH);
	        // 把 表格内容 添加到容器中心
	        panel.add(table, BorderLayout.CENTER);
	        
	        LittleComponent.table = table;
	        
		return panel;
	}
	
	
	
}
