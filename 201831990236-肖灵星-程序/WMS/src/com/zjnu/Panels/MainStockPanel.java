package com.zjnu.Panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.zjnu.ToolBar.Toolbars;
import com.zjnu.Util.JDBCUtil;
import com.zjnu.Util.WMSfields;
import com.zjnu.Util.WMStables;

public class MainStockPanel {
	private JFrame JFenter = null;
	private Connection conn = null;
	
	//属性字段的名称	
	private JComboBox<?> stockInfo = null;
	private JTable table = null;
	private JTextField options = null;
	private String[] fields = {
			"货号","货品名称","规格","数量","单位","单价","总金额","备注"
		};
	
	private String[] fields_EN = {
			WMSfields.ItemNo,WMSfields.ItemName,WMSfields.norms,WMSfields.nowAmout,WMSfields.company,WMSfields.avgPrice,WMSfields.nowAmout,WMSfields.remarks
		};

	
	public MainStockPanel() {
		this(null);
	}
	
	public MainStockPanel(Connection conn) {
		this.conn = conn;
	}
	
	
	public void initPanel() {

		this.JFenter = new JFrame();
		this.JFenter.setSize(800,600);
		this.JFenter.setBackground(Color.decode("#FAF0E6"));
		this.JFenter.setResizable(false);		
		this.JFenter.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JPanel content = new JPanel(null);
		
		this.JFenter.add(content);
		
		
		content.add(createTitle());
		content.add(createHead());
		content.add(createTable());
		content.add(createFoot());
		
		this.JFenter.setVisible(true);
	}
	
	private JPanel createTitle() {
		JPanel title = new JPanel(null);
		title.setBounds(0, 10, 800, 70);
		title.setOpaque(false);
		
		JLabel labTitle = new JLabel();
		labTitle.setFont(new Font(null,Font.BOLD,25));
		labTitle.setHorizontalAlignment(SwingConstants.CENTER);;
		labTitle.setBounds(300, 10, 200, 40);
		
		
		labTitle.setText("库存查询");
		this.JFenter.setTitle("库存查询");		
		
		title.add(labTitle);
		
		return title;
	}
	
	private JPanel createHead() {
		JPanel head = new JPanel();
		head.setBounds(0, 80, 800, 60);
		FlowLayout layout = new FlowLayout();
		layout.setHgap(100);
		head.setLayout(layout);
		head.setOpaque(false);
		//创建供货商标签
	
		
		//创建收货仓库
		JPanel BStock = new JPanel();
		JLabel labBstock = new JLabel("仓库：");
		BStock.add(labBstock);
		
		Object[] itemsBstock = null;	//从数据库查询数据传送到这里。

		
		ResultSet rs = LitterUtils.selected(conn, WMStables.wareHouse);
		itemsBstock = LitterUtils.ResultSetToSingleArray(rs, WMSfields.wareHouseNo);
		
		stockInfo = LittleComponent.selectMean(itemsBstock);	//************//
		BStock.add(stockInfo);
		
		
		//创键查询具体条件
		JPanel search = new JPanel();
		JLabel labSearch = new JLabel("货品名称或编号");
		options = new JTextField(12);
		JButton mbtn = new JButton("查询");
		mbtn.setBackground(Color.cyan);
		
		mbtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				searchBtn();
			}
			
		});
		
		search.add(labSearch);
		search.add(options);
		search.add(mbtn);
		
		head.add(BStock);
		head.add(search);
		
		return head;
	}
	
	//添加数据导表格中
	private void addDateTables(ResultSet rs,String[] fields,JTable table) throws SQLException {

		LitterUtils.clearTable(table);
		
		int row = 0;
		
		while(rs.next()) {
			 for(int col = 0;col<fields.length;col++) {
				
			 	table.setValueAt(rs.getObject(fields[col]), row, col);
					
			 }
				
			 row++;			
		 }
		
	}
	
	
	
	//点击按钮进行详细查询
	private void searchBtn(){
		
		PreparedStatement pst = null;
		ResultSet rs = null;
		//需要加一个判断语句，来确认是否执行此语句

		try {
			if(!"".equals(options.getText())) {
				//拼接查询语句
				StringBuffer selectSql = new StringBuffer("select * from ");
				selectSql.append(WMStables.mainStock);
				selectSql.append(" where ");
				selectSql.append(WMSfields.ItemNo);
				//保证所差数据在仓库中
				selectSql.append(" in (Select ");
				selectSql.append(WMSfields.ItemNo);
				selectSql.append(" from ");
				if("*".equals(stockInfo.getSelectedItem())) {
					selectSql.append(WMStables.mainStock);
					selectSql.append(" where ");
				}else {
					selectSql.append(WMStables.bstock);
					selectSql.append(" where ");
					selectSql.append(WMSfields.wareHouseNo);
					selectSql.append(" = ? and ");
				}				
				
				selectSql.append(WMSfields.ItemNo);
				selectSql.append(" = ?)");
				
				pst = conn.prepareStatement(selectSql.toString());	
				
				if("*".equals(stockInfo.getSelectedItem())){
					pst.setObject(1,options.getText());
				}else {
					pst.setObject(1, stockInfo.getSelectedItem());
					pst.setObject(2,options.getText());
				}
				
				
				
				rs = pst.executeQuery();
			}else {
				rs = LitterUtils.limitSelected(conn,stockInfo.getSelectedItem(), 0);
			}

			addDateTables(rs, fields_EN, table);
			Toolbars.getPage().setText(String.valueOf(0));			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, pst, null);
		}
	}
	
	
	private JPanel createTable() {
		
		JPanel panel = new JPanel();
		panel.setBounds(5, 150, 770, 400);
		panel.setOpaque(false);
		ResultSet rs = null;
	
	
		Object[][] contents = new Object[20][fields.length];
		//从数据库获取数据
		
		JPanel tablePanel = new JPanel(new BorderLayout());  
		
		//设置表格的样式
		DefaultTableModel tableModel = new DefaultTableModel(contents,fields);
		JTable table = new JTable(tableModel);
	    //设置字体居中
	    DefaultTableCellRenderer render = new DefaultTableCellRenderer();
	    render.setHorizontalAlignment(SwingConstants.CENTER);
	       
	    DefaultTableColumnModel dcm = (DefaultTableColumnModel) table.getColumnModel();
	   	       
	    for(int i=0;i<fields.length;i++) {
	       table.getColumn(fields[i]).setCellRenderer(render);
	       dcm.getColumn(i).setPreferredWidth(95);
	    }
	       
	    RowSorter<TableModel> rowSorter = new TableRowSorter<TableModel>(table.getModel());
	    table.setRowSorter(rowSorter);
	      
	    //为单元格添加数据

		try {
			rs = LitterUtils.limitSelected(conn, stockInfo.getSelectedItem(), 0);
			addDateTables(rs, fields_EN, table);
			
			
			Object[] insertRow = new Object[fields.length];
			((DefaultTableModel) table.getModel()).addRow(insertRow);
	    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}
	   
	    this.table = table;
	 	       
	   // 把 表头 添加到容器顶部（使用普通的中间容器添加表格时，表头 和 内容 需要分开添加）
	  
	    tablePanel.add(table.getTableHeader(),BorderLayout.NORTH);
	        
	    tablePanel.add(table,BorderLayout.CENTER);
		panel.add(tablePanel);
		
		return panel;
	}
	
	//创建底部菜单栏	---通过反射
	private JPanel createFoot() {
		//分页显示，可以不进行，只将数据每次进行刷新就可以了
		JPanel panel = new Toolbars(table,conn,String.valueOf(stockInfo.getSelectedItem())).getPanelBar();
		panel.setBounds(5, 510, 770, 50);
		
		return panel;
	}
	

}
