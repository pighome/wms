package com.zjnu.Panels;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.zjnu.Util.WMSDatas;
import com.zjnu.Util.WMSfields;
import com.zjnu.Util.WMStables;
import com.zjnu.procedure.InStockInfo;

public class InStockPanel {
	private JFrame JFenter = null;
	private String operName = null;
	private Connection conn = null;
	
	//属性字段的名称
	private JLabel Inodd = null;
	private JComboBox<?> supplyInfo = null;	
	private JComboBox<?> stockInfo = null;
	private JTable table = null;
	private JTextField textSend = null;
	private JTextField textCheck = null;
	private JComboBox<?> checkStatus = null;
	private JTextField textCreator = null;
	private JTextField textRemark = null;
	private JTextField textBuy = null;
	
	public InStockPanel() {
		this(null,null);
	}
	
	public InStockPanel(String operName,Connection conn) {
		this.operName = operName;
		this.conn = conn;
	}
	

	public void initPanel() {

		this.JFenter = new JFrame();
		this.JFenter.setSize(800,600);
		this.JFenter.setBackground(Color.decode("#FAF0E6"));
		this.JFenter.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		this.JFenter.setResizable(false);		
		
		JPanel content = new JPanel(null);
		
		this.JFenter.add(content);
		
		
		content.add(createTitle());
		content.add(createHead());
		content.add(createTable());
		content.add(createFoot());
		
		this.JFenter.setVisible(true);
	}
	
	private JPanel createTitle() {
		JPanel title = new JPanel(null);
		title.setBounds(0, 10, 800, 70);
		title.setOpaque(false);
		
		JLabel labTitle = new JLabel();
		labTitle.setFont(new Font(null,Font.BOLD,25));
		labTitle.setHorizontalAlignment(SwingConstants.CENTER);;
		labTitle.setBounds(300, 10, 200, 40);
		
		//设置单号
		Inodd = new JLabel();
		
		if("ICJ".equalsIgnoreCase(operName)) {
			labTitle.setText("进货入库");
			this.JFenter.setTitle("进货入库");
			Inodd.setText(CreateOddUtils.createINOdd(true));
		}else if("ICT".equalsIgnoreCase(operName)){
			labTitle.setText("退货出库");
			this.JFenter.setTitle("退货出库");
			Inodd.setText(CreateOddUtils.createINOdd(false));
		}
		
		Inodd.setFont(new Font(null,Font.BOLD,16));
		Inodd.setForeground(Color.red);
		Inodd.setBounds(550, 20, 200, 30);
		
		title.add(labTitle);
		title.add(Inodd);
		
		return title;
	}
	
	private JPanel createHead() {
		JPanel head = new JPanel();
		head.setBounds(0, 100, 800, 80);
		FlowLayout layout = new FlowLayout();
		layout.setHgap(100);
		head.setLayout(layout);
		head.setOpaque(false);
		//创建供货商标签
		JPanel supply = new JPanel();
		JLabel labsupply = new JLabel("供货商：");
		supply.add(labsupply);
		
		Object[] itemsSupply = null;	//从数据库查询数据传送到这里。
		
		ResultSet rs = LitterUtils.selected(conn, WMStables.supply);
		
		itemsSupply = LitterUtils.ResultSetToSingleArray(rs, WMSfields.supplierId);
		
		supplyInfo = LittleComponent.selectMean(itemsSupply);//************//
		supply.add(supplyInfo);
		
	
		
		//创建收货仓库
		JPanel BStock = new JPanel();
		JLabel labBstock = new JLabel("收货仓库：");
		BStock.add(labBstock);
		
		Object[] itemsBstock = null;	//从数据库查询数据传送到这里。

		rs = LitterUtils.selected(conn, WMStables.wareHouse);
		
		itemsBstock = LitterUtils.ResultSetToSingleArray(rs, WMSfields.wareHouseNo);
		
		stockInfo = LittleComponent.selectMean(itemsBstock);	//************//
		BStock.add(stockInfo);
		
		
		//创建收货标签
		//JPanel nowTime = new JPanel();
		String dateTime = LitterUtils.formatDate(new Date());
		JLabel labTime = new JLabel("收货日期:"+dateTime);
		
		head.add(supply);
		head.add(BStock);
		head.add(labTime);
		
		return head;
	}
	
	
	private JPanel createTable() {
		
		JPanel panel = new JPanel();
		panel.setBounds(15, 180, 770, 250);
		panel.setOpaque(false);
		
		String[] fields = {
			"货号","货品名称","规格","数量","单位","单价","总金额"
		};

		Object[][] contents = new Object[5][7];
		
		//添加表格面板到此内容中
		panel.add(LittleComponent.GoodsInfo(fields, contents));
		table = LittleComponent.getTable();
		
		return panel;
	}
	
	private JPanel createFoot() {
		JPanel panel = new JPanel();
		panel.setBounds(0, 430, 800, 100);
		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.LEFT);
		panel.setLayout(layout);
		panel.setOpaque(false);
		
		//设置送货人，检查员，是否审核通过，制单人，备注
			//送货人
		JPanel send = new JPanel();
		textSend = new JTextField(8);
		JLabel labSender = new JLabel("送货人:");
		send.add(labSender);
		send.add(textSend);
		
		//进货添加采购员，退货则不添加
		if("ICJ".equalsIgnoreCase(operName)) {
			JPanel buyer = new JPanel();
			textBuy = new JTextField(8);
			JLabel labBuyer = new JLabel("采购员:");
			buyer.add(labBuyer);
			buyer.add(textBuy);
			panel.add(buyer);
		}
		
		//检查员
		JPanel check = new JPanel();
		textCheck = new JTextField(8);
		JLabel labChecker = new JLabel("检查员:");
		check.add(labChecker);
		check.add(textCheck);
		
		//审核状态
		JPanel cstatus = new JPanel();
		JLabel labCheckStatus = new JLabel();
		cstatus.add(labCheckStatus);
		
		checkStatus = LittleComponent.selectMean("审核通过","审核不通过");
		
		cstatus.add(checkStatus);
		
		
		//制单人
		JPanel create = new JPanel();
		textCreator = new JTextField(8);
		JLabel labCreator = new JLabel("制单人：");
		create.add(labCreator);
		create.add(textCreator);
		
		//备注信息
		JPanel remark = new JPanel();
		textRemark = new JTextField(50);
		JLabel labRemark = new JLabel("备注：");
		remark.add(labRemark);
		remark.add(textRemark);
		
		
		panel.add(send);
		panel.add(check);
		panel.add(cstatus);
		panel.add(create);
		panel.add(remark);
		
		//加入按钮
		panel.add(buttons());
		
		return panel;
	}
	
	//设置保存和退出按钮
	private JPanel buttons() {
		JPanel panel = new JPanel();
		
		panel.setSize(150,100);
		
		JButton saveBtn = new JButton("保存");	//保存按钮应包含将信息读入数组，并调用入库事务
		setStyle(saveBtn);
		
		saveBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				saveInfo();
			}
			
		});
		
		
		JButton exitBtn = new JButton("退出");	//退出按钮应包含关闭当前窗口资源
		setStyle(exitBtn);
		
		exitBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				exitInfo();
			}
			
		});
		
		panel.add(saveBtn);
		panel.add(exitBtn);
		
		return panel;
		
	}
	
	//设置按钮背景颜色
	private void setStyle(JButton btn) {
		btn.setBackground(Color.CYAN);
		btn.setSize(60,40);
		btn.setFont(new Font(null,Font.BOLD,18));
	}
	
	//点击保存数据的按钮时做的程序处理
	private void saveInfo() {
		boolean isSuccessful = false;
		InStockInfo inStockInfo = new InStockInfo(this.conn);
		
		if("ICJ".equalsIgnoreCase(operName)) {
			WMSDatas.setInTables(textBuy.getText(), Inodd.getText().substring(3), textSend.getText(), stockInfo.getSelectedItem(), new Date(), textCheck.getText(), checkStatus.getSelectedItem(), supplyInfo.getSelectedItem(), textCreator.getText(), textRemark.getText());
			WMSDatas.setInMaterials(table.getValueAt(0,0), table.getValueAt(0,1),Inodd.getText().substring(3), table.getValueAt(0,2), table.getValueAt(0,3), table.getValueAt(0,4), table.getValueAt(0,5), table.getValueAt(0,6), textRemark.getText());
		
		    isSuccessful = inStockInfo.InCopeWith();
		    
		
		}else if("ICT".equalsIgnoreCase(operName)) {
			WMSDatas.setInTables("", Inodd.getText().substring(3), textSend.getText(), stockInfo.getSelectedItem(), new Date(), textCheck.getText(), checkStatus.getSelectedItem(), supplyInfo.getSelectedItem(), textCreator.getText(), textRemark.getText());
			WMSDatas.setInMaterials(table.getValueAt(0,0), table.getValueAt(0,1),Inodd.getText().substring(3), table.getValueAt(0,2), "-"+table.getValueAt(0,3), table.getValueAt(0,4), table.getValueAt(0,5), "-"+table.getValueAt(0,6), textRemark.getText());
		
		    isSuccessful = inStockInfo.InCopeWith();
		    
		}
		
		 
		String message = "";
		if(isSuccessful) {
			message = "数据已保存更新至数据库";

			for(int i = 0;i<table.getColumnCount();i++) {
	        	table.setValueAt(null, 0, i);
	        }
			
		}else {
			message="数据更新失败，请重新尝试";
		}
		
		JOptionPane.showMessageDialog(this.JFenter,message,"提示信息",JOptionPane.INFORMATION_MESSAGE);
      
		//要在调用一下的事务处理流程
		
	}
	
	private void exitInfo() {
		this.JFenter.dispose();
	}
	

}







