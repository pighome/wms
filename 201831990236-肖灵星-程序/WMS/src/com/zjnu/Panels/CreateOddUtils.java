package com.zjnu.Panels;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Set;

public class CreateOddUtils {
	
	private CreateOddUtils() {
		
	}
	
	static {
		resetInfo();
	}
	
	private static String read(String key) {
		String strnum = null;
		Properties pro = new Properties();
		FileReader reader = null;
		try {
			reader = new FileReader("currentInfo.properties");
			pro.load(reader);
			strnum = pro.getProperty(key);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		return strnum;
	}
	
	private static boolean write(String key,Integer value) {
		boolean successful = false;
		Properties pro = new Properties();
		FileReader reader = null;
		FileWriter writer = null;
		try {
			//先加载properties文件中的信息防止被覆盖
			reader = new FileReader("currentInfo.properties");
			pro.load(reader);
			
			writer = new FileWriter("currentInfo.properties");

			pro.setProperty(key, String.valueOf(value));
			
			pro.store(writer, pro.toString());
			
			successful = true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			successful = false;
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			successful = false;
			e.printStackTrace();
		}finally {
			if(reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		return successful;
	}
	
	public static String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyMM");
		String formatTime = null;
		formatTime = sdf.format(date);
		return formatTime;
	}
	
	public static void resetInfo() {
		String dateTime = read("dateTime");
		String nowTime = formatDate(new Date());
		FileReader reader = null;
		FileWriter writer = null;
		
		if(!nowTime.equalsIgnoreCase(dateTime)) {
			Properties pro = new Properties();
			try {
				//先加载properties文件中的信息防止被覆盖
				reader = new FileReader("currentInfo.properties");
				pro.load(reader);
				
				Set<Object> keys = pro.keySet();
				
				for(Object key:keys) {
					System.out.println(key + "="+pro.get(key));
					pro.put(key, "0");
				}
				
				writer = new FileWriter("currentInfo.properties");

				pro.setProperty("dateTime", nowTime);
				
				pro.store(writer, pro.toString());
				
				
			} catch (FileNotFoundException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				
				e.printStackTrace();
			}finally {
				if(reader != null) {
					try {
						reader.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(writer != null) {
					try {
						writer.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}	
		}
		
	}
	
	public static String createINOdd(boolean isInStock) {
		StringBuffer INodd = null;
		String numStr = null;
		Integer num = null;
		if(isInStock) {
			INodd = new StringBuffer("单号：ICJ");
			numStr = read("ICJSTOCK");
			
			//将文件写入源文件
			num = Integer.valueOf(numStr)+1;
			write("ICJSTOCK",num);
		}else {
			INodd = new StringBuffer("单号：ICT");
			numStr = read("ICTSTOCK");
			
			//将文件写入源文件
			num = Integer.valueOf(numStr)+1;
			write("ICTSTOCK",num);
		}
		
		
		INodd.append(formatDate(new Date()));
		String strnum = String.format("%06d", num);
		
		INodd.append(strnum);
		return INodd.toString();
	}
	
	public static String createOutOdd(boolean isOutStock) {
		StringBuffer OUTodd = null;
		String numStr = null;
		Integer num = null;
		if(isOutStock) {
			OUTodd = new StringBuffer("单号：OCL");
			numStr = read("OCLSTOCK");
			
			//将数据协会源文件
			num = Integer.valueOf(numStr)+1;			
			write("OCLSTOCK",num);
			
		}else {
			OUTodd = new StringBuffer("单号：OCT");
			numStr = read("OCTSTOCK");
			
			//将数据协会源文件
			num = Integer.valueOf(numStr)+1;			
			write("OCTSTOCK",num);
		}
		
		
		OUTodd.append(formatDate(new Date()));
		String strnum = String.format("%06d", num);
		
		OUTodd.append(strnum);
		return OUTodd.toString();
	}
	
	public static String createIVTOdd() {
		StringBuffer IVTodd = new StringBuffer("单号：IVT");
		IVTodd.append(formatDate(new Date()));
		
		String numStr = read("IVTSTOCK");
		
		//将文件写入源文件
		Integer num = Integer.valueOf(numStr);
		num += 1;
		
		write("IVTSTOCK",num);		
		
		String strnum = String.format("%06d", num);
		
		IVTodd.append(strnum);
		return IVTodd.toString();		
	}

	public static String createAllotOdd() {
		StringBuffer IVTodd = new StringBuffer("单号：AOT");
		IVTodd.append(formatDate(new Date()));
		
		String numStr = read("AOTSTOCK");
		
		//将文件写入源文件
		Integer num = Integer.valueOf(numStr);
		num += 1;
		
		write("AOTSTOCK",num);		
		
		String strnum = String.format("%06d", num);
		
		IVTodd.append(strnum);
		return IVTodd.toString();		
	}

	
}
