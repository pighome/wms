package com.zjnu.Panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;

import com.zjnu.Util.JDBCUtil;
import com.zjnu.Util.WMSDatas;
import com.zjnu.Util.WMSfields;
import com.zjnu.Util.WMStables;
import com.zjnu.procedure.OutStockInfo;

public class AllotStockPanel {
	private JFrame JFenter = null;
	private Connection conn = null;
	
	//属性字段的名称
	private JLabel AllotOdd = null;
	private JComboBox<?> InstockInfo = null;
	private JComboBox<?> OutstockInfo = null;
	private JTable table = null;
	private JTextField textSend = null;
	private JTextField textCheck = null;
	private JComboBox<?> checkStatus = null;
	private JTextField textCreator = null;
	private JTextField textRemark = null;
	
	private String[] fields = {
			"货号","货品名称","规格","数量","单位","单价","总金额"
		};

	public AllotStockPanel() {
		this(null);
	}
	
	public AllotStockPanel(Connection conn) {
		
		this.conn = conn;
	}
	
	
	public void initPanel() {

		this.JFenter = new JFrame();
		this.JFenter.setSize(800,600);
		this.JFenter.setBackground(Color.decode("#FAF0E6"));
		this.JFenter.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		this.JFenter.setResizable(false);		
		
		JPanel content = new JPanel(null);
		
		this.JFenter.add(content);
		
		
		content.add(createTitle());
		content.add(createHead());
		content.add(createTable());
		content.add(createFoot());
		
		this.JFenter.setVisible(true);
	}
	
	private JPanel createTitle() {
		JPanel title = new JPanel(null);
		title.setBounds(0, 10, 800, 70);
		title.setOpaque(false);
		
		JLabel labTitle = new JLabel();
		labTitle.setFont(new Font(null,Font.BOLD,25));
		labTitle.setHorizontalAlignment(SwingConstants.CENTER);;
		labTitle.setBounds(300, 10, 200, 40);
		
		//设置单号
		AllotOdd = new JLabel();
		
		
		labTitle.setText("仓库调拨");
		this.JFenter.setTitle("仓库调拨");
		AllotOdd.setText(CreateOddUtils.createAllotOdd());
		
		AllotOdd.setFont(new Font(null,Font.BOLD,16));
		AllotOdd.setForeground(Color.red);
		AllotOdd.setBounds(550, 20, 200, 30);
		
		title.add(labTitle);
		title.add(AllotOdd);
		
		return title;
	}
	
	private JPanel createHead() {
		JPanel head = new JPanel();
		head.setBounds(0, 100, 800, 80);
		FlowLayout layout = new FlowLayout();
		layout.setHgap(100);
		head.setLayout(layout);
		head.setOpaque(false);
		

		Object[] itemsBstock = null;	//从数据库查询数据传送到这里。
	
		//创建收货仓库
		JPanel OutBStock = new JPanel();
		JLabel labOutBstock = new JLabel("调出仓库：");
		OutBStock.add(labOutBstock);
		
		
		ResultSet rs = LitterUtils.selected(conn, WMStables.wareHouse);
		
		itemsBstock = LitterUtils.ResultSetToSingleArray(rs,WMSfields.wareHouseNo);
		
		OutstockInfo = LittleComponent.selectMean(itemsBstock);	//************//
		OutBStock.add(OutstockInfo);
		
	
		
		//创建收货仓库
		JPanel InBStock = new JPanel();
		JLabel labInBstock = new JLabel("调入仓库：");
		InBStock.add(labInBstock);
		

		
		rs = LitterUtils.selected(conn, WMStables.wareHouse);
		itemsBstock = LitterUtils.ResultSetToSingleArray(rs,WMSfields.wareHouseNo);
		
		InstockInfo = LittleComponent.selectMean(itemsBstock);	//************//
		InBStock.add(InstockInfo);
		
		
		//创建收货标签
		//JPanel nowTime = new JPanel();
		String dateTime = LitterUtils.formatDate(new Date());
		JLabel labTime = new JLabel("调拨日期:"+dateTime);
		
		head.add(OutBStock);
		head.add(InBStock);
		head.add(labTime);
		
		return head;
	}
	
	
	private JPanel createTable() {
		
		JPanel panel = new JPanel();
		panel.setBounds(15, 180, 770, 250);
		panel.setOpaque(false);

		Object[][] contents = new Object[5][fields.length];
		
		//添加表格面板到此内容中
		JPanel tablePanel = new JPanel(new BorderLayout());
		
		DefaultTableCellRenderer render = new DefaultTableCellRenderer();
	       render.setHorizontalAlignment(SwingConstants.CENTER);
	       
	       
		DefaultTableModel tableModel = new DefaultTableModel(contents,fields) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			//设置表格内容不可变
			public boolean isCellEditable(int row,int column){

				if( column ==6  ||column == 4||column == 5){
	
				return false;
	
				}else{
	
				return true;
	
				}
			}
		};
		JTable table = new JTable(tableModel);
		//设置单元个宽高
		DefaultTableColumnModel dcm = (DefaultTableColumnModel) table.getColumnModel();
	       
	       table.setRowHeight(35);
	       
	       for(int i=0;i<fields.length;i++) {
	    	   table.getColumn(fields[i]).setCellRenderer(render);
	    	   dcm.getColumn(i).setPreferredWidth(110);
	       }
		
	       //设置表格监听事件
	    setAddListener(tableModel); 
			  
		this.table = table;
		
		tablePanel.add(table.getTableHeader(),BorderLayout.NORTH);
		tablePanel.add(table,BorderLayout.CENTER);
		
		panel.add(tablePanel);
		
		return panel;
	}
	/*
	 * 设置表格的监听事件
	 */
	private void setAddListener(DefaultTableModel tableModel) {
		
		 
		 tableModel.addTableModelListener(new TableModelListener() {
		
				@SuppressWarnings("resource")
				@Override
				public void tableChanged(TableModelEvent e) {
					// TODO Auto-generated method stub
					int column = e.getColumn();
					int type = e.getType();
					
					if(type == TableModelEvent.UPDATE &&( column == 0 || column == 1)) {
						//查询补充条件，自动填充货号或货名以及规格
						PreparedStatement pst = null;
						ResultSet rs = null;
						StringBuffer sql = new StringBuffer("select * from ");
						sql.append(WMStables.mainStock);
						sql.append(" where ");
						sql.append(WMSfields.ItemNo);
						sql.append("=? or ");
						sql.append(WMSfields.ItemName);
						sql.append("=?");
						
						try {
							//查询本仓，填充规格单位名称等数据
							pst = conn.prepareStatement(sql.toString());
							
							pst.setObject(1, table.getValueAt(0, 0));//加一个空指针判断一下
							pst.setObject(2, table.getValueAt(0, 1));
							
							rs = pst.executeQuery();
							tableModel.removeTableModelListener(this);
							while(rs.next()) {
								if(column == 1) {
									table.setValueAt(rs.getObject(WMSfields.ItemNo), 0, 0);									
								}else {
									table.setValueAt(rs.getObject(WMSfields.ItemName), 0, 1);
								}	
								table.setValueAt(rs.getObject(WMSfields.norms), 0, 2);
								table.setValueAt(rs.getObject(WMSfields.company), 0, 4);
							}
							
							//查询本仓填充货品单价
							
							StringBuffer sql2 = new StringBuffer("select * from ");
							sql2.append(WMStables.bstock);
							sql2.append(" where ");
							sql2.append(WMSfields.ItemNo);
							sql2.append("=? and ");
							sql2.append(WMSfields.wareHouseNo);
							sql2.append("=?");
							
							pst = conn.prepareStatement(sql2.toString());
							
							pst.setObject(1, table.getValueAt(0, 0));
							pst.setObject(2, OutstockInfo.getSelectedItem());
							
							if(rs != null) {
								rs.close();
								rs = null;
							}
							rs = pst.executeQuery();
							
							while(rs.next()) {
								table.setValueAt(rs.getObject(WMSfields.avgPrice), 0, 5);
							}
							
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}finally {
							JDBCUtil.close(rs, pst, null);
						}					
						
						tableModel.addTableModelListener(this);
						
					}else if(column == 3 && type == TableModelEvent.UPDATE) {
						Integer allotNum = null;
						Double price = null;
						if(table.getValueAt(0, column)!=null) {
							allotNum = Integer.valueOf(""+table.getValueAt(0, column));
						}
						
						if(table.getValueAt(0, 5) != null) {
							price = Double.valueOf(""+table.getValueAt(0, 5));
						}					
						
						Double sum = 0.0;
						if(allotNum != null && price != null) {
							sum = allotNum * price;
						}						
						tableModel.removeTableModelListener(this);
						table.setValueAt(sum, 0, 6);
						tableModel.addTableModelListener(this);
					}
				}
		    	
		});
	}

	//创建底部数据栏
	private JPanel createFoot() {
		JPanel panel = new JPanel();
		panel.setBounds(0, 430, 800, 100);
		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.LEFT);
		panel.setLayout(layout);
		panel.setOpaque(false);
		
		//设置送货人，检查员，是否审核通过，制单人，备注
			//送货人
		JPanel operator = new JPanel();
		textSend = new JTextField(8);
		JLabel labSender = new JLabel("操作员:");
		operator.add(labSender);
		operator.add(textSend);
		
		//进货添加采购员，退货则不添加
		
		
		//检查员
		JPanel check = new JPanel();
		textCheck = new JTextField(8);
		JLabel labChecker = new JLabel("检查员:");
		check.add(labChecker);
		check.add(textCheck);
		
		//审核状态
		JPanel cstatus = new JPanel();
		JLabel labCheckStatus = new JLabel();
		cstatus.add(labCheckStatus);
		
		checkStatus = LittleComponent.selectMean("审核通过","审核不通过");
		
		cstatus.add(checkStatus);
		
		
		//制单人
		JPanel create = new JPanel();
		textCreator = new JTextField(8);
		JLabel labCreator = new JLabel("制单人：");
		create.add(labCreator);
		create.add(textCreator);
		
		//备注信息
		JPanel remark = new JPanel();
		textRemark = new JTextField(50);
		JLabel labRemark = new JLabel("备注：");
		remark.add(labRemark);
		remark.add(textRemark);
		
		
		panel.add(operator);
		panel.add(check);
		panel.add(cstatus);
		panel.add(create);
		panel.add(remark);
		
		//加入按钮
		panel.add(buttons());
		
		return panel;
	}
	
	//设置保存和退出按钮
	private JPanel buttons() {
		JPanel panel = new JPanel();
		
		panel.setSize(150,100);
		
		JButton saveBtn = new JButton("保存");	//保存按钮应包含将信息读入数组，并调用入库事务
		setStyle(saveBtn);
		
		saveBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				saveInfo();
			}
			
		});
		
		
		JButton exitBtn = new JButton("退出");	//退出按钮应包含关闭当前窗口资源
		setStyle(exitBtn);
		
		exitBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				exitInfo();
			}
			
		});
		
		panel.add(saveBtn);
		panel.add(exitBtn);
		
		return panel;
		
	}
	
	//设置按钮背景颜色
	private void setStyle(JButton btn) {
		btn.setBackground(Color.CYAN);
		btn.setSize(60,40);
		btn.setFont(new Font(null,Font.BOLD,18));
	}
	
	//点击保存数据的按钮时做的程序处理
	private void saveInfo() {
		boolean isSuccessful = false;
		OutStockInfo outStockPanel = new OutStockInfo(this.conn);
		
		//调出仓库的数据
		WMSDatas.setOutTables("", "O_"+AllotOdd.getText().substring(3), -1, OutstockInfo.getSelectedItem(), new Date(), textCheck.getText(), checkStatus.getSelectedItem(), textRemark.getText(), textCreator.getText());
		WMSDatas.setOutMaterials(table.getValueAt(0, 0), table.getValueAt(0, 1), "O_"+AllotOdd.getText().substring(3), table.getValueAt(0,3), table.getValueAt(0, 5), table.getValueAt(0, 6),textRemark.getText());
		//调用出库表事务处理过程
		outStockPanel.setCommit(false);
		
		isSuccessful = outStockPanel.OutCopeWith();
		
		//调入仓库的数据
		WMSDatas.setOutTables("", "I_"+AllotOdd.getText().substring(3), -1, InstockInfo.getSelectedItem(), new Date(), textCheck.getText(), checkStatus.getSelectedItem(), textRemark.getText(), textCreator.getText());
		WMSDatas.setOutMaterials(table.getValueAt(0, 0), table.getValueAt(0, 1), "I_"+AllotOdd.getText().substring(3), "-"+table.getValueAt(0,3), table.getValueAt(0, 5), "-"+table.getValueAt(0, 6),textRemark.getText());
		
		//调用入库表事务处理过程
		outStockPanel.setCommit(true);
		isSuccessful = outStockPanel.OutCopeWith();
		
		String message = "";
		if(isSuccessful) {
			message = "数据已保存更新至数据库";

			for(int i = 0;i<table.getColumnCount();i++) {
	        	table.setValueAt(null, 0, i);
	        }
			
		}else {
			message="数据更新失败，请重新尝试";
		}
		 
		JOptionPane.showMessageDialog(this.JFenter,message,"提示信息",JOptionPane.INFORMATION_MESSAGE);
      
		
	}
	
	private void exitInfo() {
		this.JFenter.dispose();
	}
	
	
	
	
}
