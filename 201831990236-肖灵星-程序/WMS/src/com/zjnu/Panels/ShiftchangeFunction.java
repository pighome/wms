package com.zjnu.Panels;

import javax.swing.JFrame;

import com.zjnu.UI.LoginUI;

public class ShiftchangeFunction {
	
	private JFrame jf = null;
	
	public ShiftchangeFunction() {
		
	}
	
	public ShiftchangeFunction(JFrame jf) {
		this.jf = jf;
		
	}
	
	public void initSetting() {
		LoginUI loginUi = new LoginUI();
		loginUi.setChangeJob(true);
		loginUi.initLoginUI();	
		loginUi.setBeforeJF(jf);
		
	}
	
}
