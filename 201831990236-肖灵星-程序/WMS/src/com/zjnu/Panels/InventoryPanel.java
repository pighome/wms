package com.zjnu.Panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;

import com.zjnu.Util.JDBCUtil;
import com.zjnu.Util.WMSDatas;
import com.zjnu.Util.WMSfields;
import com.zjnu.Util.WMStables;
import com.zjnu.procedure.InventoryInfo;

public class InventoryPanel {
	private JFrame JFenter = null;
	private Connection conn = null;
	
	//属性字段的名称
	private JLabel IVTodd = null;
	private JComboBox<?> stockInfo = null;
	private JTable table = null;
	private JTextField textOperator = null;
	private JTextField textRemark = null;
	
	public InventoryPanel() {
		this(null);
	}
	
	public InventoryPanel(Connection conn) {
		
		this.conn = conn;
	}
	
	
	public void initPanel() {

		this.JFenter = new JFrame();
		this.JFenter.setSize(800,600);
		this.JFenter.setBackground(Color.decode("#FAF0E6"));
		this.JFenter.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		this.JFenter.setResizable(false);		
		
		JPanel content = new JPanel(null);
		
		this.JFenter.add(content);
		
		
		content.add(createTitle());
		content.add(createHead());
		content.add(createTable());
		content.add(createFoot());
		
		this.JFenter.setVisible(true);
	}
	
	private JPanel createTitle() {
		JPanel title = new JPanel(null);
		title.setBounds(0, 10, 800, 70);
		title.setOpaque(false);
		
		JLabel labTitle = new JLabel();
		labTitle.setFont(new Font(null,Font.BOLD,25));
		labTitle.setHorizontalAlignment(SwingConstants.CENTER);;
		labTitle.setBounds(300, 10, 200, 40);
		
		//设置单号
		IVTodd = new JLabel();		
		
		labTitle.setText("盘点仓库数据");
		this.JFenter.setTitle("盘点仓库数据");
		IVTodd.setText(CreateOddUtils.createIVTOdd());		
		
		IVTodd.setFont(new Font(null,Font.BOLD,16));
		IVTodd.setForeground(Color.red);
		IVTodd.setBounds(550, 20, 200, 30);
		
		title.add(labTitle);
		title.add(IVTodd);
		
		return title;
	}
	
	
	
	private JPanel createHead() {
		JPanel head = new JPanel();
		head.setBounds(0, 100, 800, 80);
		FlowLayout layout = new FlowLayout();
		layout.setHgap(100);
		head.setLayout(layout);
		head.setOpaque(false);
		
		
		//创建盘点仓库
		JPanel BStock = new JPanel();
		JLabel labBstock = new JLabel("盘点仓库：");
		BStock.add(labBstock);
		
		Object[] itemsBstock = null;	//从数据库查询数据传送到这里。

		ResultSet rs = LitterUtils.selected(conn, WMStables.wareHouse);
			
		itemsBstock = LitterUtils.ResultSetToSingleArray(rs, WMSfields.wareHouseNo);
		
		itemsBstock[0] = null;
		
		stockInfo = LittleComponent.selectMean(itemsBstock);	//************//
		BStock.add(stockInfo);
		
		
		//创建收货标签
		
		String dateTime = LitterUtils.formatDate(new Date());
		JLabel labTime = new JLabel("盘点日期:"+dateTime);
		
		head.add(BStock);
		head.add(labTime);
		
		return head;
	}
	
	
	private JPanel createTable() {
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 180, 800, 250);
		panel.setOpaque(false);
		
		String[] fields = {
			"货号","货品名称","规格","盘点数量","库存数量","盘盈亏"
		};
		
		JPanel tablePanel = new JPanel(new BorderLayout());
		tablePanel.setPreferredSize(new Dimension(750,250));

		Object[][] contents = new Object[6][7];
		
        DefaultTableCellRenderer render = new DefaultTableCellRenderer();
	       render.setHorizontalAlignment(SwingConstants.CENTER);
	       
	       
		DefaultTableModel tableModel = new DefaultTableModel(contents,fields) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			//设置表格内容不可变
			public boolean isCellEditable(int row,int column){

				if( column == 2 ||column == 4||column == 5){
	
				return false;
	
				}else{
	
				return true;
	
				}
			}
		};
		JTable table = new JTable(tableModel);
	
		DefaultTableColumnModel dcm = (DefaultTableColumnModel) table.getColumnModel();
	       
	       table.setRowHeight(35);
	       
	       for(int i=0;i<fields.length;i++) {
	    	   table.getColumn(fields[i]).setCellRenderer(render);
	    	   dcm.getColumn(i).setPreferredWidth(110);
	       }
	  
	    setAddListener(tableModel);		
//		
//	    RowSorter<TableModel> rowSorter = new TableRowSorter<TableModel>(table.getModel());
//	    table.setRowSorter(rowSorter);
	    
	    JScrollPane scollPanel = new JScrollPane(); 	
	    scollPanel.setViewportView(table);
		
	    tablePanel.add(scollPanel);
		//添加表格面板到此内容中
		panel.add(tablePanel);
	
		this.table = table;
		
		return panel;
	}
	
	
	//设置表格的监听事件
	private void setAddListener(DefaultTableModel tableModel) {
		  //设置表格监听事件
	    tableModel.addTableModelListener(new TableModelListener() {

			@SuppressWarnings("resource")
			@Override
			public void tableChanged(TableModelEvent e) {
				// TODO Auto-generated method stub
				int column = e.getColumn();
				int type = e.getType();
				
				if(type == TableModelEvent.UPDATE &&( column == 0 || column == 1)) {
					//查询补充条件
					PreparedStatement pst = null;
					ResultSet rs = null;
					StringBuffer sql = new StringBuffer("select * from ");
					sql.append(WMStables.mainStock);
					sql.append(" where ");
					sql.append(WMSfields.ItemNo);
					sql.append("=? or ");
					sql.append(WMSfields.ItemName);
					sql.append("=?");
					
					try {
						pst = conn.prepareStatement(sql.toString());
						
						pst.setObject(1, table.getValueAt(0, 0));
						pst.setObject(2, table.getValueAt(0, 1));
						
						rs = pst.executeQuery();
						tableModel.removeTableModelListener(this);
						while(rs.next()) {
							if(column == 1) {
								table.setValueAt(rs.getObject(WMSfields.ItemNo), 0, 0);
							}else {
								table.setValueAt(rs.getObject(WMSfields.ItemName), 0, 1);
							}			
							
							table.setValueAt(rs.getObject(WMSfields.norms), 0, 2);
						}
						
						
						StringBuffer sql2 = new StringBuffer("select * from ");
						sql2.append(WMStables.bstock);
						sql2.append(" where ");
						sql2.append(WMSfields.ItemNo);
						sql2.append("=? and ");
						sql2.append(WMSfields.wareHouseNo);
						sql2.append("=?");
						
						pst = conn.prepareStatement(sql2.toString());
						
						pst.setObject(1, table.getValueAt(0, 0));
						pst.setObject(2, stockInfo.getSelectedItem());
						
						rs = pst.executeQuery();
						while(rs.next()) {
							table.setValueAt(rs.getObject(WMSfields.nowAmout), 0, 4);
						}
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}finally {
						JDBCUtil.close(rs, pst, null);
					}					
					
					tableModel.addTableModelListener(this);					
					
				}else if(column == 3 && type == TableModelEvent.UPDATE) {
					Integer stockNum = null;
					Integer realNum = null;
					if(table.getValueAt(0, column)!=null) {
						realNum = Integer.valueOf(""+table.getValueAt(0, column));
					}
					
					if(table.getValueAt(0, column+1) != null) {
						stockNum = Integer.valueOf(""+table.getValueAt(0, column+1));
					}					
					
					Integer isProfit = 0;
					if(realNum != null && stockNum != null) {
						isProfit = realNum - stockNum;
					}
					
					tableModel.removeTableModelListener(this);
					table.setValueAt(isProfit, 0, 5);
					tableModel.addTableModelListener(this);
					
				}
			}
	    	
	    });
	}
	
	//创建底部数据
	
	private JPanel createFoot() {
		JPanel panel = new JPanel();
		panel.setBounds(0, 430, 800, 100);
		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.CENTER);
		panel.setLayout(layout);
		panel.setOpaque(false);
		
		
		//制单人
		JPanel create = new JPanel();
		textOperator = new JTextField(8);
		JLabel labCreator = new JLabel("操作员：");
		create.add(labCreator);
		create.add(textOperator);
		
		//备注信息
		JPanel remark = new JPanel();
		textRemark = new JTextField(50);
		JLabel labRemark = new JLabel("备注：");
		remark.add(labRemark);
		remark.add(textRemark);
		
		
		panel.add(create);
		panel.add(remark);
		
		//加入按钮
		panel.add(buttons());
		
		return panel;
	}
	
	//设置保存和退出按钮
	private JPanel buttons() {
		JPanel panel = new JPanel();
		
		panel.setSize(150,100);
		
		JButton saveBtn = new JButton("保存");	//保存按钮应包含将信息读入数组，并调用入库事务
		setStyle(saveBtn);
		
		saveBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				saveInfo();
			}
			
		});
		
		
		JButton exitBtn = new JButton("退出");	//退出按钮应包含关闭当前窗口资源
		setStyle(exitBtn);
		
		exitBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				exitInfo();
			}
			
		});
		
		panel.add(saveBtn);
		panel.add(exitBtn);
		
		return panel;
		
	}

	
	//设置按钮背景颜色
	private void setStyle(JButton btn) {
		btn.setBackground(Color.CYAN);
		btn.setSize(60,40);
		btn.setFont(new Font(null,Font.BOLD,18));
	}
	
	//点击保存数据的按钮时做的程序处理
	private void saveInfo() {
		
		boolean isSuccessful = false;
		
		WMSDatas.setInventory(IVTodd.getText().substring(3), table.getValueAt(0, 0), stockInfo.getSelectedItem(), new Date(), table.getValueAt(0, 3), table.getValueAt(0, 4), table.getValueAt(0, 5), textRemark.getText(), textOperator.getText());
		
		InventoryInfo inventoryInfo = new InventoryInfo(this.conn);
		
		isSuccessful = inventoryInfo.checkCopeWith();	
		
		 
		String message = "";
		if(isSuccessful) {
			message = "数据已保存更新至数据库";

			for(int i = 0;i<table.getColumnCount();i++) {
	        	table.setValueAt(null, 0, i);
	        }
			
		}else {
			message="数据更新失败，请重新尝试";
		}
		
		JOptionPane.showMessageDialog(this.JFenter,message,"提示信息",JOptionPane.INFORMATION_MESSAGE);
      
		//要在调用一下的事务处理流程
		
	}
	
	private void exitInfo() {
		this.JFenter.dispose();
	}
	
	
	
}
