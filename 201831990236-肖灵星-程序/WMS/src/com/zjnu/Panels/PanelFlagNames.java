package com.zjnu.Panels;

public class PanelFlagNames {
	private PanelFlagNames() {
		
	};
	
	public static final String INStock_ICJ = "ICJ";
	public static final String INStock_ICT = "ICT";
	public static final String OutStock_OCL = "OCL";
	public static final String OutStock_OCT = "OCT";
	public static final String OutStock_ODJ = "ODJ";
	public static final String OutStock_IDJ = "IDJ";
	
	public static final String InStock_Info = "IN";
	public static final String OutStock_Info = "OUT";
	
}
