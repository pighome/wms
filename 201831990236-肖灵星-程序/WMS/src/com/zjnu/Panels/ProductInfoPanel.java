package com.zjnu.Panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.zjnu.JTree.MainJTree;
import com.zjnu.Util.JDBCUtil;
import com.zjnu.Util.WMSfields;
import com.zjnu.Util.WMStables;

public class ProductInfoPanel {
	private JTable table = null;
	private Connection conn = null;
	private JTextField textSearchInfo = null;
	private JFrame jf = null;
	
	private String[] fields = {
			"货号","货品名称","规格","数量","单位","单价","总金额","备注"
	};
	
	private String[] fields_EN = {
			WMSfields.ItemNo,WMSfields.ItemName,WMSfields.norms,WMSfields.nowAmout,WMSfields.company,
			WMSfields.avgPrice,WMSfields.amountSum,WMSfields.remarks
		};
	
	public ProductInfoPanel() {
		this(null);
	}
	
	public ProductInfoPanel(Connection conn) {
		this.conn = conn;
	}
	/*
	 * 需要两个部分，javatree  和一个查询信息显示面板
	 */
	
	//初始化面板
	
	public void initPanel() {

		this.jf = new JFrame();
		this.jf.setSize(900, 600);
		this.jf.setBackground(Color.decode("#FAF0E6"));
		this.jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		this.jf.setResizable(false);
		
		this.jf.add(splitPanel());


		this.jf.setVisible(true);
	}
	
	
	//创建分割面板
	private JSplitPane splitPanel() {
		JSplitPane splitPane = new JSplitPane();		
		
		splitPane.setRightComponent(createRightPanel());
		splitPane.setLeftComponent(createLeftPanel());
		
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation(300);
		
		return splitPane;
	}
	
	//添加左边面板
	private JPanel createLeftPanel() {
		JPanel panel = new JPanel();
		
		MainJTree tree = new MainJTree(conn);
		//panel = tree.getJtreeUI().getJpContent();
		
		panel = tree.getJtreeUI();
		
		tree.getJtreeUI().setTable(this.table);
		
		//tree.getJtreeUI().setConn(conn);
		
		return panel;
	}
	
	private JPanel createHead() {
		JPanel panel = new JPanel();
		JLabel lab = new JLabel("货品信息");
		lab.setFont(new Font(null,Font.BOLD,25));
		panel.add(lab);
		return panel;
		
	}
	
	//创建右边的面板
	private JPanel createRightPanel() {
		JPanel rightPanel = new JPanel(new BorderLayout());
		
		rightPanel.add(createHead(),BorderLayout.NORTH);
		
		rightPanel.add(createSearch(),BorderLayout.CENTER);
		
		rightPanel.add(createJTable(),BorderLayout.SOUTH);
		
		return rightPanel;
	}
	
	private JPanel createJTable() {
		JPanel panel = new JPanel();
		
		//尺寸应该随着主面板变换而变化的
		//panel.setPreferredSize(preferredSize);
		
		JScrollPane scrollpanel = new JScrollPane();
		
		Object[][] contents = new Object[10][fields.length];
		
		//创建表格
		DefaultTableModel tableModel = new DefaultTableModel(contents,fields) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			@Override
			//设置表格内容不可变
			public boolean isCellEditable(int row,int column){

				return false;
	
			}
			
		};
		
		
		JTable table = new JTable(tableModel);
	     //设置单元格排序
		RowSorter<TableModel> rowSorter = new TableRowSorter<TableModel>(table.getModel());
		table.setRowSorter(rowSorter);
		this.table = table;
		

		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setAutoscrolls(false);
		
		//设置单元格宽度
		for(int i = 0;i< fields.length;i++) {
			table.getColumnModel().getColumn(i).setPreferredWidth(80);
		}
				
		
		scrollpanel.setViewportView(table);
		
		panel.add(scrollpanel);
		
		return panel;
	}
	
	//创建查询数据的面板
	
	private JPanel createSearch() {
		JPanel panel = new JPanel();
		JLabel labInfo = new JLabel("货号/名称：");
		textSearchInfo = new JTextField(12);
		
		JButton searchBtn = new JButton("查询");
		searchBtn.setBackground(Color.CYAN);
		searchBtn.setFont(new Font(null,Font.CENTER_BASELINE,16));
		
		searchBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				addDatas();
			}
			
		});
		
		panel.add(labInfo);
		panel.add(textSearchInfo);
		panel.add(searchBtn);
		
		return panel;
	}
	
	private void addDatas() {
		StringBuffer sql = new StringBuffer("select * from ");
		sql.append(WMStables.mainStock);
		sql.append(" where (");
		sql.append(WMSfields.ItemNo);
		sql.append(" like ?) or (");
		sql.append(WMSfields.ItemName);
		sql.append(" like ?)");
		
		LitterUtils.clearTable(table);
		
		String options = textSearchInfo.getText().trim() + "%";
		
		ResultSet rs = null;
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql.toString());
			pst.setObject(1, options);
			pst.setObject(2, options);
			
			rs = pst.executeQuery();
			
			int row = 0;
			while(rs.next()) {
				for(int col=0;col<fields.length;col++) {					
					table.setValueAt(rs.getObject(fields_EN[col]), row, col);
				
				}
				row ++;
				if((table.getRowCount()-row)<5) {
					Object[] insertRow = new Object[fields.length];
					((DefaultTableModel) table.getModel()).addRow(insertRow);
				}
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, pst, null);
		}
		
	}
	

}
