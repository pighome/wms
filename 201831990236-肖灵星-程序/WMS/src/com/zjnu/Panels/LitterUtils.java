package com.zjnu.Panels;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.zjnu.ToolBar.Toolbars;
import com.zjnu.Util.JDBCUtil;
import com.zjnu.Util.WMSfields;
import com.zjnu.Util.WMStables;

public class LitterUtils {
	private static long index = 0;
	private static Integer pageSize = 20;
	
	//标记字段
	private static int yearflag = 0;
	private static int mouthflag = 0;
	private static int dayflag = 0;
	
	private LitterUtils() {
		
	}
	
	public static String formatDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd");
		String formatTime = null;
		formatTime = sdf.format(date);
		return formatTime;
	}
	
	//可能有问题
	public static ResultSet selected(Connection conn,String tableName) {
		StringBuffer selectSql = new StringBuffer("select * from ");
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			
			selectSql.append(tableName);
			
			pst = conn.prepareStatement(selectSql.toString());
			
			rs = pst.executeQuery();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		if(rs != null) {
			return rs;
		}
			
		JDBCUtil.close(rs, pst, null);
		return null;
	}
	
	/*
	 * 将查询结果集转换为二数组
	 */
	public static Object[][] ResultSetToTwoArray(ResultSet rs,String...queryOption){
		Object[][] obj = null;
		try {
			rs.last();
			
			int row = rs.getRow();
			rs.beforeFirst();
					
			obj = new Object[row][];
			
			row = 0;
			while(rs.next()) {
				for(int col = 0;col < queryOption.length;col++) {
					obj[row][col] = rs.getObject(queryOption[col]);
				}
				row ++;;				
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}
		
		
		return obj;
	}
	
	//转换为一维数组存储
	public static Object[] ResultSetToSingleArray(ResultSet rs,String queryOption){
		Object[] obj = null;
		try {
			rs.last();
			
			int row = rs.getRow();
			rs.beforeFirst();
					
			obj = new Object[row+1];
			obj[0] = "*";
			row = 1;
			
			while(rs.next()) {				
				obj[row++] = rs.getObject(queryOption);			
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			JDBCUtil.close(rs, null, null);
		}
		
		
		return obj;
	}
	
	//注意这里的firstIndex应该有正负的，每下一页，就为1
	@SuppressWarnings("resource")
	public static ResultSet limitSelected(Connection conn,Object tableItem,int pageTemp) throws SQLException {
		
		String firstSql = null;
		String nextSql = null;	

		PreparedStatement pst = null;
		ResultSet rs = null;	
		
		if(pageTemp == 0) {
			index = 0;
		}else if(pageTemp == 1 || pageTemp == -1){
			index += (pageTemp*20);
		}else if(pageTemp == 2) {
			//此时求取最后一页的数据，确定开始位置和最后位置即可
			//查询最后的数据			
			long count = 0;
			String lastSql = "select count(*) from "+WMStables.mainStock;
			pst = conn.prepareStatement(lastSql);
			//pst.setObject(1, WMStables.mainStock);
			
			rs = pst.executeQuery();
			
			if(rs.next()) {
				count = rs.getLong(1);
			}
			
			
			long position = count % 20;				
			index = count - position;	
			
			Toolbars.getPage().setText(String.valueOf(count / 20));			
			
			if(rs != null) {
				rs.close();
				rs = null;
			}
		}
		
		
		if(rs == null) {	
		
			if("*".equals(tableItem)) {
				if(index <= 0) {
					firstSql = new String("select * from mainStock limit ?");
					pst = conn.prepareStatement(firstSql);
					pst.setObject(1, pageSize);
				}else {
					nextSql = new String("select * from mainStock limit ?,?");
					pst = conn.prepareStatement(nextSql);
					pst.setObject(1, index);
					pst.setObject(2, pageSize);
				}
			}else {
				if(index <= 0) {					
					firstSql = new String("select * from mainStock where Item_No in (Select Item_No from bstock where Warehouse_No = ?) limit ?");
					pst = conn.prepareStatement(firstSql);
					pst.setObject(1, tableItem);
					pst.setObject(2, pageSize);
					
				}else {
					nextSql = new String("select * from mainStock where Item_No in (Select Item_No from bstock where Warehouse_No = ?) limit ?,?");
					pst = conn.prepareStatement(nextSql);
					pst.setObject(1, tableItem);
					pst.setObject(2, index);
					pst.setObject(3, pageSize);
				}
			}				
			
			rs = pst.executeQuery();	
			
		}
		return rs;
	}
	
	public static void selectLikeData(Connection conn,Object itemNo,JTable table) {
		StringBuffer sql = new StringBuffer("select * from ");
		sql.append(WMStables.mainStock);
		sql.append(" where ");
		sql.append(WMSfields.ItemNo);
		sql.append(" like ?");
		
		clearTable(table);
		
		ResultSet rs = null;
		PreparedStatement pst = null;
		try {
			pst = conn.prepareStatement(sql.toString());
			pst.setObject(1,itemNo+"%");
			
			rs = pst.executeQuery();
			
			int row = 0;
			while(rs.next()) {
				int col = 0;
				table.setValueAt(rs.getObject(WMSfields.ItemNo), row, col++);
				table.setValueAt(rs.getObject(WMSfields.ItemName), row, col++);
				table.setValueAt(rs.getObject(WMSfields.norms), row, col++);
				table.setValueAt(rs.getObject(WMSfields.nowAmout), row, col++);
				table.setValueAt(rs.getObject(WMSfields.company), row, col++);
				table.setValueAt(rs.getObject(WMSfields.avgPrice), row, col++);
				table.setValueAt(rs.getObject(WMSfields.amountSum), row, col++);
				table.setValueAt(rs.getObject(WMSfields.remarks), row, col++);
				
				row++;
				if((table.getRowCount()-row)<5) {
					Object[] insertRow = new Object[table.getColumnCount()];
					((DefaultTableModel) table.getModel()).addRow(insertRow);
				}
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/*
	 * 设置日期中的标签事件
	 */
	//判断是否为闰年
	private static boolean judgeLeapyear(Integer year) {
		if((year%4==0&&year%100!=0)||year%400==0)		
			return true;
		return false;
	}
	
	//每月最大值
	private static int maxDays(Integer year,int mouth) {
		
		switch(mouth){
			case 1:case 3:case 5:case 7:case 8:case 10:case 12:return 31;
			case 4:case 6:case 9:case 11:return 30;
			case 2:if(judgeLeapyear(year)) {
						return 29;
					}else return 28;
			default:return 0;
		}
	}

	
	public static void setActionDate(JTextField year,JTextField mouth,JTextField day,boolean isEnd) {
			
		year.addMouseWheelListener(new MouseWheelListener() {
		  
		  @Override
		  public void mouseWheelMoved(MouseWheelEvent e) { 
			  int flag = 0;  
				if(isEnd) {
					flag = -1;
				}else {
					flag = 1;
				}
				  		
			if(yearflag == flag) {
				
				if(e.getWheelRotation()< 0) {
					String strs = year.getText();
					Integer num = Integer.valueOf(strs);
					num += 1;
					strs = String.format("%04d", num);
					
					year.setText(strs);
					
				}else {
					String strs = year.getText();
					Integer num = Integer.valueOf(strs);
					num -= 1;
					if(num <1) {
						num = 1;
					}
					
					strs = String.format("%04d", num);
					
					year.setText(strs);
				}
			
			 }
		  }		  
		  
		});
		 
		year.addFocusListener(new FocusListener() {
	
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				yearflag = 0;
				year.setBackground(Color.white);
			}
	
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				if(isEnd) {
					yearflag = -1;
				}else {
					yearflag = 1;
				}
				
				year.setBackground(Color.decode("#AEEEEE"));
			}
		});
	
		//*****************设置月份事件*******************/
		
		mouth.addMouseWheelListener(new MouseWheelListener() {
		  
		  @Override
		  public void mouseWheelMoved(MouseWheelEvent e) { 
			  int flag = 0;  
				if(isEnd) {
					flag = -1;
				}else {
					flag = 1;
				}
				  		
			if(mouthflag == flag) {
				
				if(e.getWheelRotation()< 0) {
					String strs = mouth.getText();
					Integer num = Integer.valueOf(strs);
					num += 1;
					if(num >12) {
						num = 12;
					}
					strs = String.format("%02d", num);
					
					mouth.setText(strs);
					
					Integer tempYear = Integer.valueOf(year.getText());					
					num = maxDays(tempYear, num);
					strs = String.format("%02d", num);
					day.setText(strs);
				}else {
					String strs = mouth.getText();
					Integer num = Integer.valueOf(strs);
					num -= 1;
					if(num <1) {
						num = 1;
					}
					strs = String.format("%02d", num);
				
					mouth.setText(strs);
					
					Integer tempYear = Integer.valueOf(year.getText());					
					num = maxDays(tempYear, num);
					strs = String.format("%02d", num);
					day.setText(strs);
				} 
			  }
			}		  
		  
		});
		 
		mouth.addFocusListener(new FocusListener() {
	
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				mouthflag = 0;
				mouth.setBackground(Color.white);
			}
	
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				if(isEnd) {
					mouthflag = -1;
				}else {
					mouthflag = 1;
				}
				
				mouth.setBackground(Color.decode("#AEEEEE"));
			}
		});
	
		//********************设置日的规格***************//
		
		day.addMouseWheelListener(new MouseWheelListener() {
		  
		  @Override
		  public void mouseWheelMoved(MouseWheelEvent e) { 
			  int flag = 0;  
				if(isEnd) {
					flag = -1;
				}else {
					flag = 1;
				}
			if(dayflag == flag) {
				
				if(e.getWheelRotation()<0) {
					//判断是否为闰月
					Integer tempYear = Integer.valueOf(year.getText());
					Integer tempMouth = Integer.valueOf(mouth.getText());
					
					Integer maxDay = maxDays(tempYear,tempMouth);
					String strs = day.getText().trim();
					Integer num = Integer.valueOf(strs);
					num += 1;
					if(num >maxDay) {
						num = maxDay;
					}
					strs = String.format("%02d", num);
					
					day.setText(strs);
					
				}else {
					String strs = day.getText().trim();
					Integer num = Integer.valueOf(strs);
					num -= 1;
					if(num < 0) {
						num = 0;
					}
					strs = String.format("%02d", num);
					
					day.setText(strs);
				}
				
			 }
			}		  
		  
		});
		 
		day.addFocusListener(new FocusListener() {
	
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
				dayflag = 0;
				day.setBackground(Color.white);
			}
	
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				if(isEnd) {
					dayflag = -1;
				}else {
					dayflag = 1;
				}
				
				day.setBackground(Color.decode("#AEEEEE"));
			}
		});

	}


	//清空表格数据
	public static void clearTable(JTable table) {
		for(int i = 0;i < table.getRowCount();i ++) {
			for(int j = 0;j<table.getColumnCount();j++) {
				table.setValueAt(null, i, j);
			}
		}
	}
	
}
