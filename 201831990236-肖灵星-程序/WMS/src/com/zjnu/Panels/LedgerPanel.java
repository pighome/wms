package com.zjnu.Panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.zjnu.Util.JDBCUtil;
import com.zjnu.Util.WMSUtils;
import com.zjnu.Util.WMSfields;
import com.zjnu.Util.WMStables;

public class LedgerPanel {
	private JFrame JFenter = null;
	private Connection conn = null;

	// 属性字段的名称
	private JTable table = null;
	private JTextField options = null;
	
	
	//日期
	private JTextField endday = null;
	private JTextField endmouth = null;
	private JTextField endyear = null;
	private JTextField startday = null;
	private JTextField startmouth = null;
	private JTextField startyear = null;
	
	private String nowDate[] = new String[3];
	
	
	private String[] Infields = {
			"单号","货号","仓库号","购入数量","购入单价","购入总金额","购出数量","购出单价","购出总金额","结存数量","结存单价","结存总金额","购出总金额","日期","备注"	
			};

	private String[] Infields_EN = {
			WMSfields.Odds,WMSfields.ItemNo,WMSfields.wareHouseNo,WMSfields.InNumber,WMSfields.InPrice,WMSfields.InSum,
			WMSfields.OutNumber,WMSfields.OutPrice,WMSfields.OutSum,WMSfields.balanceNum,WMSfields.balancePrice,WMSfields.balanceSum,
			WMSfields.dateTime,WMSfields.remarks
			
		};

	{
		Integer num = 0;
		
		num = Integer.valueOf(Calendar.getInstance().get(Calendar.YEAR));
		nowDate[0] = String.valueOf(String.format("%04d", num));
		
		num = Integer.valueOf(Calendar.getInstance().get(Calendar.MONTH));
		nowDate[1] = String.valueOf(String.format("%02d", num+1));
		
		num = Integer.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		nowDate[2] = String.valueOf(String.format("%02d", num));
		
	}
	public LedgerPanel() {
		this(null);
	}

	public LedgerPanel( Connection conn) {
		this.conn = conn;
	}

	public void initPanel() {

		this.JFenter = new JFrame();
		this.JFenter.setSize(900, 600);
		this.JFenter.setBackground(Color.decode("#FAF0E6"));
		this.JFenter.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		this.JFenter.setResizable(false);

		JPanel content = new JPanel(null);
		
		this.JFenter.add(content);

		content.add(createTitle());
		content.add(createHead());
	
		content.add(createTable(Infields));
		

		this.JFenter.setVisible(true);
	}

	private JPanel createTitle() {
		JPanel title = new JPanel(null);
		title.setBounds(0, 10, 900, 70);
		title.setOpaque(false);

		JLabel labTitle = new JLabel();
		labTitle.setFont(new Font(null, Font.BOLD, 25));
		labTitle.setHorizontalAlignment(SwingConstants.CENTER);
		
		labTitle.setBounds(300, 10, 200, 40);

		labTitle.setText("台账信息查询");
		this.JFenter.setTitle("台账信息查询");
		

		title.add(labTitle);

		return title;
	}
	

	private JPanel createHead() {
		JPanel head = new JPanel();
		head.setBounds(0, 80, 900, 60);
		FlowLayout layout = new FlowLayout();
		layout.setHgap(100);
		head.setLayout(layout);
		head.setOpaque(false);
		// 创建供货商标签

		//*****************开始创建日期卡片**************//
		JPanel startTime = new JPanel();
		JLabel labStart = new JLabel("开始时间:");
		JLabel splitMark1 = new JLabel("-");
		JLabel splitMark2 = new JLabel("-");
		
		startyear = new JTextField(4);		
		startyear.setEditable(false);
		startyear.setHorizontalAlignment(SwingConstants.CENTER);
		startyear.setText(nowDate[0]);
		startyear.setBackground(Color.white);
			

		//*****************设置月份事件*******************/
		startmouth = new JTextField(4);		
		startmouth.setEditable(false);
		startmouth.setHorizontalAlignment(SwingConstants.CENTER);
		startmouth.setText(nowDate[1]);
		startmouth.setBackground(Color.white);
		

		//********************设置日的规格***************//
		startday = new JTextField(4);		
		startday.setEditable(false);
		startday.setHorizontalAlignment(SwingConstants.CENTER);
		startday.setText(nowDate[2]);
		startday.setBackground(Color.white);
		

		LitterUtils.setActionDate(startyear,startmouth,startday,false);
		/*
		 * 添加数据到面板
		 */
		startTime.add(labStart);
		startTime.add(startyear);
		startTime.add(splitMark1);
		startTime.add(startmouth);
		startTime.add(splitMark2);
		startTime.add(startday);

		//****************截至日期***************//
		endyear = new JTextField(4);		
		endyear.setEditable(false);
		endyear.setHorizontalAlignment(SwingConstants.CENTER);
		endyear.setText(nowDate[0]);
		endyear.setBackground(Color.white);
			

		//*****************设置月份事件*******************//
		endmouth = new JTextField(4);		
		endmouth.setEditable(false);
		endmouth.setHorizontalAlignment(SwingConstants.CENTER);
		endmouth.setText(nowDate[1]);
		endmouth.setBackground(Color.white);
		

		//********************设置日的规格***************//
		endday = new JTextField(4);		
		endday.setEditable(false);
		endday.setHorizontalAlignment(SwingConstants.CENTER);
		endday.setText(nowDate[2]);
		endday.setBackground(Color.white);
		
		LitterUtils.setActionDate(endyear,endmouth,endday,true);
		
		/*
		 * 添加数据到面板
		 */
		JLabel to = new JLabel("至");
		startTime.add(to);
		startTime.add(endyear);
		startTime.add(splitMark1);
		startTime.add(endmouth);
		startTime.add(splitMark2);
		startTime.add(endday);
		
		// 创键查询具体条件
		JPanel search = new JPanel();
		JLabel labSearch = new JLabel();
		
		labSearch.setText("出/入库单号:");
		
		options = new JTextField(12);
		JButton mbtn = new JButton("查询");
		mbtn.setBackground(Color.cyan);

		mbtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				searchBtn();
			}

		});

		search.add(labSearch);
		search.add(options);
		search.add(mbtn);

		head.add(startTime);

		head.add(search);

		return head;
	}

	// 清空表格数据
	public void clearTable(JTable table) {
		for (int i = 0; i < table.getRowCount(); i++) {
			for (int j = 0; j < table.getColumnCount(); j++) {
				table.setValueAt(null, i, j);
			}
		}
	}
	
	
	// 点击按钮进行详细查询
	private void searchBtn() {
	
		PreparedStatement pst = null;
		ResultSet rs=null;
		
			try {
				
				if(options.getText().length()>0) {
					rs = WMSUtils.selectSQL(conn, WMStables.ledger, options.getText());
				}else {
					StringBuffer simpleSql = new StringBuffer("select * from ");
					simpleSql.append(WMStables.ledger);
					simpleSql.append(" where ");
					simpleSql.append(WMSfields.dateTime);
					simpleSql.append(" between ? and ?");
					
					String startTimes = startyear.getText()+"-"+startmouth.getText()+"-"+startday.getText();
					String endTimes = endyear.getText()+"-"+endmouth.getText()+"-"+endday.getText();
					pst = conn.prepareStatement(simpleSql.toString());
					
					pst.setObject(1,startTimes);
					pst.setObject(2, endTimes);
					
					rs = pst.executeQuery();
				}
				
				clearTable(table);
				addDatas(Infields_EN, rs, table);
			
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				JDBCUtil.close(rs, pst, null);
			}
		
		
		
	}
	
	//为表增加数据
	private void addDatas(String[] fields,ResultSet rs,JTable table) {
		
		try {
			int row = 0;
			while(rs.next()) {
				for(int i=0;i<fields.length;i++) {
				
					table.setValueAt(rs.getObject(fields[i]), row, i);
				
				}
				row ++;
				Object[] insertRow = new Object[fields.length];
				((DefaultTableModel) table.getModel()).addRow(insertRow);
		  
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	//创建表格信息
	private JPanel createTable(String[] fields) {
		JPanel panel = new JPanel();
		panel.setBounds(0, 140, 900, 400);	
		
		JPanel tablePanel = new JPanel(new BorderLayout());
		tablePanel.setPreferredSize(new Dimension(850,380));
		
		Object[][] contents = new Object[20][fields.length];
	    
		/*
		 * JTable中想要动态添加表格行或列，需要先定义表结构，然后将表结构填充到table对象中
		 */
		DefaultTableModel tableModel = new DefaultTableModel(contents,fields);
		JTable table = new JTable(tableModel);
		
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setAutoscrolls(true);
		
		//设置单元格宽度
		for(int i = 0;i< fields.length;i++) {
			table.getColumnModel().getColumn(i).setPreferredWidth(80);
		}
		
		
	    RowSorter<TableModel> rowSorter = new TableRowSorter<TableModel>(table.getModel());
	    table.setRowSorter(rowSorter);
	       
	    this.table = table; 
	   
	    //将表格添加到滚动面板
	    JScrollPane topPanel = new JScrollPane(); 	
	    topPanel.setViewportView(table);
	    
	    //将滚动面板添加到新面板
		tablePanel.add(topPanel);
	    panel.add(tablePanel);
		
		return panel;
	}


}
