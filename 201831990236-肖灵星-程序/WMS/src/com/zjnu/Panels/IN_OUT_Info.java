package com.zjnu.Panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.zjnu.Util.JDBCUtil;
import com.zjnu.Util.WMSUtils;
import com.zjnu.Util.WMSfields;
import com.zjnu.Util.WMStables;

public class IN_OUT_Info {
	private JFrame JFenter = null;
	private Connection conn = null;

	// 属性字段的名称
	private JTable table = null;
	private JTable table2 = null;
	private JTextField options = null;
	private String operName = null;
	
	
	//日期
	private JTextField endday = null;
	private JTextField endmouth = null;
	private JTextField endyear = null;
	private JTextField startday = null;
	private JTextField startmouth = null;
	private JTextField startyear = null;
	
	private String nowDate[] = new String[3];
	
	private String[] Infields1 = {
			"采购员","入库号","送货员","仓库编号","日期","检查员",
			"审核情况","供应商","制表人","备注"
			};
	
	private String[] infields1_tables = {
			WMSfields.Buyer,WMSfields.InOdd,WMSfields.sender,WMSfields.wareHouseNo,WMSfields.dateTime,WMSfields.checker,
			WMSfields.checkStatus,WMSfields.supplierId,WMSfields.creator,WMSfields.remarks
			};
	
	private String[] Infields2 = {
			"货号","货名","入库号","规格","数量","单位","单价","总金额","备注"	
			};
	
	private String[] infields2_materials = {
			WMSfields.ItemNo,WMSfields.ItemName,WMSfields.InOdd,WMSfields.norms,WMSfields.InNumber,WMSfields.company,WMSfields.unitPrice,WMSfields.total,WMSfields.remarks
	        };
	

	private String[] Outfields1 = {
			"提货人","出库号","用货部门","仓库编号","日期","检查员",
			"审核情况","制表人","备注"
			};
 
	private String[] outfields1_tables = {
			WMSfields.receiver,WMSfields.OutOdd,WMSfields.deptno,WMSfields.wareHouseNo,WMSfields.dateTime,
			WMSfields.checker,WMSfields.checkStatus,WMSfields.creator,WMSfields.remarks			
			};
	
	
	private  String[] Outfields2 = {
			"货号","货名","出库号","数量","单价","总金额","备注"	
			};
	
	private String[] outfields2_materials = {
			WMSfields.ItemNo,WMSfields.ItemName,WMSfields.OutOdd,WMSfields.OutNumber,WMSfields.unitPrice,WMSfields.total,WMSfields.remarks
			};
		       
	

	{
		Integer num = 0;
		
		num = Integer.valueOf(Calendar.getInstance().get(Calendar.YEAR));
		nowDate[0] = String.valueOf(String.format("%04d", num));
		
		num = Integer.valueOf(Calendar.getInstance().get(Calendar.MONTH));
		nowDate[1] = String.valueOf(String.format("%02d", num+1));
		
		num = Integer.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		nowDate[2] = String.valueOf(String.format("%02d", num));
		
	}
	public IN_OUT_Info() {
		this(null, null);
	}

	public IN_OUT_Info(String operName, Connection conn) {
		this.conn = conn;
		this.operName = operName;
	}


	public void initPanel() {

		this.JFenter = new JFrame();
		this.JFenter.setSize(900, 600);
		this.JFenter.setBackground(Color.decode("#FAF0E6"));
		//this.JFenter.setDefaultCloseOperation(JFenter.EXIT_ON_CLOSE);
		this.JFenter.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		this.JFenter.setResizable(false);

		JPanel content = new JPanel(null);
		//临时存储信息面板
		JPanel tempPanel = null;

		this.JFenter.add(content);

		content.add(createTitle());
		content.add(createHead());
		if(PanelFlagNames.OutStock_IDJ.equalsIgnoreCase(operName)) {
			tempPanel = createTable(Infields1);
			tempPanel.setBounds(0, 140, 900, 200);			
			
			content.add(tempPanel);
		    this.table2 = this.table;
		    
		    tempPanel = createTable(Infields2);
		    tempPanel.setBounds(0, 350, 900, 200);
		    
		    content.add(tempPanel);
		}else if(PanelFlagNames.OutStock_ODJ.equalsIgnoreCase(operName)) {
			
			tempPanel = createTable(Outfields1);
			tempPanel.setBounds(0, 140, 900, 200);			
			
			content.add(tempPanel);
		    this.table2 = this.table;
		    
		    tempPanel = createTable(Outfields2);
		    tempPanel.setBounds(0, 350, 900, 200);
		    
		    content.add(tempPanel);
		  
		}
	   
		// content.add(createFoot());

		this.JFenter.setVisible(true);
	}

	private JPanel createTitle() {
		JPanel title = new JPanel(null);
		title.setBounds(0, 10, 900, 70);
		title.setOpaque(false);

		JLabel labTitle = new JLabel();
		labTitle.setFont(new Font(null, Font.BOLD, 25));
		labTitle.setHorizontalAlignment(SwingConstants.CENTER);
		
		labTitle.setBounds(300, 10, 200, 40);

		if(PanelFlagNames.OutStock_IDJ.equalsIgnoreCase(operName)) {
			labTitle.setText("入库信息查询");
			this.JFenter.setTitle("入信息查询");
		}else if(PanelFlagNames.OutStock_ODJ.equalsIgnoreCase(operName)) {
			labTitle.setText("出库信息查询");
			this.JFenter.setTitle("出信息查询");
		}
		

		title.add(labTitle);

		return title;
	}
	
	
	private JPanel createHead() {
		JPanel head = new JPanel();
		head.setBounds(0, 80, 900, 60);
		FlowLayout layout = new FlowLayout();
		layout.setHgap(100);
		head.setLayout(layout);
		head.setOpaque(false);
		// 创建供货商标签

		//*****************开始创建日期卡片**************//
		JPanel startTime = new JPanel();
		JLabel labStart = new JLabel("开始时间:");
		JLabel splitMark1 = new JLabel("-");
		JLabel splitMark2 = new JLabel("-");
		
		startyear = new JTextField(4);		
		startyear.setEditable(false);
		startyear.setHorizontalAlignment(SwingConstants.CENTER);
		startyear.setText(nowDate[0]);
		startyear.setBackground(Color.white);

		//*****************设置月份事件*******************/
		startmouth = new JTextField(4);		
		startmouth.setEditable(false);
		startmouth.setHorizontalAlignment(SwingConstants.CENTER);
		startmouth.setText(nowDate[1]);
		startmouth.setBackground(Color.white);
		

		//********************设置日的规格***************//
		startday = new JTextField(4);		
		startday.setEditable(false);
		startday.setHorizontalAlignment(SwingConstants.CENTER);
		startday.setText(nowDate[2]);
		startday.setBackground(Color.white);

		
		LitterUtils.setActionDate(startyear,startmouth,startday,false);

		/*
		 * 添加数据到面板
		 */
		startTime.add(labStart);
		startTime.add(startyear);
		startTime.add(splitMark1);
		startTime.add(startmouth);
		startTime.add(splitMark2);
		startTime.add(startday);

		//****************截至日期***************//
		endyear = new JTextField(4);		
		endyear.setEditable(false);
		endyear.setHorizontalAlignment(SwingConstants.CENTER);
		endyear.setText(nowDate[0]);
		endyear.setBackground(Color.white);
			


		//*****************设置月份事件*******************//
		endmouth = new JTextField(4);		
		endmouth.setEditable(false);
		endmouth.setHorizontalAlignment(SwingConstants.CENTER);
		endmouth.setText(nowDate[1]);
		endmouth.setBackground(Color.white);


		//********************设置日的规格***************//
		endday = new JTextField(4);		
		endday.setEditable(false);
		endday.setHorizontalAlignment(SwingConstants.CENTER);
		endday.setText(nowDate[2]);
		endday.setBackground(Color.white);
		
		//设置触发事件
		LitterUtils.setActionDate(endyear,endmouth,endday,true);

		/*
		 * 添加数据到面板
		 */
		JLabel to = new JLabel("至");
		startTime.add(to);
		startTime.add(endyear);
		startTime.add(splitMark1);
		startTime.add(endmouth);
		startTime.add(splitMark2);
		startTime.add(endday);
		
		// 创键查询具体条件
		JPanel search = new JPanel();
		JLabel labSearch = new JLabel();
		if(PanelFlagNames.OutStock_IDJ.equalsIgnoreCase(operName)) {
			labSearch.setText("入库单号:");
		}else if(PanelFlagNames.OutStock_ODJ.equalsIgnoreCase(operName)) {
			labSearch.setText("出库单号:");
		}
		
		options = new JTextField(12);
		JButton mbtn = new JButton("查询");
		mbtn.setBackground(Color.cyan);

		mbtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				searchBtn();
			}

		});

		search.add(labSearch);
		search.add(options);
		search.add(mbtn);

		head.add(startTime);

		head.add(search);

		return head;
	}

	
	// 点击按钮进行详细查询
	@SuppressWarnings("resource")
	private void searchBtn() {
		StringBuffer simpleSql = new StringBuffer("select * from ");
		StringBuffer complexSql = new StringBuffer("select * from "); 
		
		String SearchInfo = options.getText();
		
		System.out.println(table == table2);
		
		PreparedStatement pst = null;
		ResultSet rs1=null,rs2=null;
		if(PanelFlagNames.OutStock_IDJ.equalsIgnoreCase(operName)) {
			try {
				//第一入库信息报警
				if(SearchInfo.length()>0) {
					rs1 = WMSUtils.selectSQL(conn, WMStables.InTable, options.getText());
				}else {
					simpleSql.append(WMStables.InTable);
					simpleSql.append(" where ");
					simpleSql.append(WMSfields.dateTime);
					simpleSql.append(" between ? and ?");
					
					String startTimes = startyear.getText()+"-"+startmouth.getText()+"-"+startday.getText();
					String endTimes = endyear.getText()+"-"+endmouth.getText()+"-"+endday.getText();
					pst = conn.prepareStatement(simpleSql.toString());
					
					pst.setObject(1,startTimes);
					pst.setObject(2, endTimes);
					
					rs1 = pst.executeQuery();
				}
				
				addDatas(infields1_tables, rs1, table2);
				

				//第二个材料信息报表
				if(SearchInfo.length()>0) {
					rs2 = WMSUtils.selectSQL(conn, WMStables.InMaterials, options.getText());
				}else {
					
					complexSql.append(WMStables.InMaterials);
					complexSql.append(" where ? in (select ? from ");
					complexSql.append(WMStables.InTable);
					complexSql.append(" where ");
					complexSql.append(WMSfields.dateTime);
					complexSql.append(" between ? and ?)");
					
					String startTimes = startyear.getText()+"-"+startmouth.getText()+"-"+startday.getText();
					String endTimes = endyear.getText()+"-"+endmouth.getText()+"-"+endday.getText();
					
					pst = conn.prepareStatement(complexSql.toString());
					
					pst.setObject(1,WMSfields.InOdd);
					pst.setObject(2,WMSfields.InOdd);
					
					pst.setObject(3,startTimes);
					pst.setObject(4, endTimes);
					
					rs2 = pst.executeQuery();
				}

				addDatas(infields2_materials,rs2,table);
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				JDBCUtil.close(rs1, pst, null);
				JDBCUtil.close(rs2, pst, null);
			}
			
		}else if(PanelFlagNames.OutStock_ODJ.equalsIgnoreCase(operName)) {	//*****出库信息******//
			try {
				
				if(SearchInfo.length()>0) {
					rs1 = WMSUtils.selectSQL(conn, WMStables.OutTable, options.getText());
				}else {
					simpleSql.append(WMStables.OutTable);
					simpleSql.append(" where ");
					simpleSql.append(WMSfields.dateTime);
					simpleSql.append(" between ? and ?");
					
					String startTimes = startyear.getText()+"-"+startmouth.getText()+"-"+startday.getText();
					String endTimes = endyear.getText()+"-"+endmouth.getText()+"-"+endday.getText();
					pst = conn.prepareStatement(simpleSql.toString());
					
					pst.setObject(1,startTimes);
					pst.setObject(2, endTimes);
					
					rs1 = pst.executeQuery();
				}
				
				addDatas(outfields1_tables, rs1, table2);
				
			
				if(SearchInfo.length()>0) {
					rs2 = WMSUtils.selectSQL(conn, WMStables.OutMaterials, options.getText());
				}else {
					
					complexSql.append(WMStables.OutMaterials);
					complexSql.append(" where ? in (select ? from ");
					complexSql.append(WMStables.OutTable);
					complexSql.append(" where ");
					complexSql.append(WMSfields.dateTime);
					complexSql.append(" between ? and ?)");
					
					
					String startTimes = startyear.getText()+"-"+startmouth.getText()+"-"+startday.getText();
					String endTimes = endyear.getText()+"-"+endmouth.getText()+"-"+endday.getText();
					
					pst = conn.prepareStatement(complexSql.toString());
					
					pst.setObject(1,WMSfields.OutOdd);
					pst.setObject(2,WMSfields.OutOdd);
					
					pst.setObject(3,startTimes);
					pst.setObject(4, endTimes);
					
					rs2 = pst.executeQuery();
				}
				
				addDatas(outfields2_materials, rs2, table);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				JDBCUtil.close(rs1, pst, null);
				JDBCUtil.close(rs2, pst, null);
			}
		}
		
		
	}
	
	//为表增加数据
	private void addDatas(String[] fields,ResultSet rs,JTable table) throws SQLException {
		
		LitterUtils.clearTable(table);

		int row  = 0;
		while(rs.next()) {
			for(int i=0;i<fields.length;i++) {				
				table.setValueAt(rs.getObject(fields[i]), row, i);
			}
			
			row ++;
			Object[] insertRow = new Object[fields.length];
			((DefaultTableModel) table.getModel()).addRow(insertRow);
	  
		}
	
		
	}
	
	
	//创建表格信息
	private JPanel createTable(String[] fields) {
		JPanel panel = new JPanel();
		
		JPanel tablePanel = new JPanel(new BorderLayout());
		tablePanel.setPreferredSize(new Dimension(850,200));
		
		Object[][] contents = new Object[10][fields.length];
	    
		/*
		 * JTable中想要动态添加表格行或列，需要先定义表结构，然后将表结构填充到table对象中
		 */
		DefaultTableModel tableModel = new DefaultTableModel(contents,fields);
		JTable tables = new JTable(tableModel);
	
		
	    RowSorter<TableModel> rowSorter = new TableRowSorter<TableModel>(tables.getModel());
	    tables.setRowSorter(rowSorter);
	       
	    this.table = tables; 
	   
	    //将表格添加到滚动面板
	    JScrollPane topPanel = new JScrollPane(); 	
	    topPanel.setViewportView(tables);
	    
	    //将滚动面板添加到新面板
		tablePanel.add(topPanel);
	    panel.add(tablePanel);

		return panel;
	}
	

	// 调用测试


}
