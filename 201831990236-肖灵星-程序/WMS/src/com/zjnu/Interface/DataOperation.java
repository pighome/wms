package com.zjnu.Interface;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Map;

public interface DataOperation {
	
	public void setConn(Connection conn);
	public String getTableName();
	public void setTableName(String tableName);
	
	public int insertData(Map<String,Object>data);
	public int deleteData(Object... ItemIDs);
	public int updateData(Map<String,Object> data,Object...ItemIDs) ;
	public ResultSet selectData(Object condition);
	public boolean truncateData() ;
	
}
